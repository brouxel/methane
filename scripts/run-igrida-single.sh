#!/bin/sh
#OAR -l /nodes=1/core=CORES,walltime=TIMEOUT:10:00
#OAR -t besteffort 
#OAR -t idempotent 
#OAR -O /temp_dd/igrida-fs1/brouxel/logs/tgff_job.%jobid%.log
#OAR -E /temp_dd/igrida-fs1/brouxel/logs/tgff_job.%jobid%.err
set -xv

echo "pwd :"
pwd
echo "=============== RUN ==============="

$OAR_WORKDIR/ILPGen/dist/ilpgen TASKGRAPHFILE

echo "Done"
echo "==================================="
