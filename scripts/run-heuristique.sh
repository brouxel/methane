#!/bin/bash

INDIR=/home/brouxel/Projects/Thesis/Tests/streamit/dist-ecrts2017/
EXEC=/home/brouxel/Projects/C-C++/ILPGen/dist/ilpgen
NBPROCS=$(nproc --all)

EXECNAME=$(basename $EXEC)

for f in $(find $INDIR -iname "*.xml" -not -iname "*res.xml" -print) ; do
    if [[ ! -e "$(echo $f | sed 's/.xml/-res.xml/')" ]] ; then
        nbp=$(pgrep $EXECNAME 2>/dev/null | wc -l)
        while [ $nbp -gt $NBPROCS ] ; do
            sleep 1
            nbp=$(pgrep $EXECNAME 2>/dev/null | wc -l)
        done
        echo ">>>> Run $f"
        $EXEC "$f" >/dev/null &
    fi
done

echo "done"
