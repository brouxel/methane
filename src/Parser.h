/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PARSER_H
#define PARSER_H

#include <string>

#include "config.h"
#include "registry.h"

/*
 * Hack required to create a Registry of Parser. Registry does not allow
 * templatised class without the implementation.
 */
class ParserHack {
public:
    virtual void parse_file(const std::string &) = 0;
};

template<class OutIRR>
class Parser : public ParserHack {
protected:
    OutIRR *out;
    config_t *conf;
    
    explicit Parser(OutIRR *o, config_t *c) : out(o), conf(c) {};
};

using ParserRegistry = registry::Registry<ParserHack, std::string, void *, config_t *>;

#define REGISTER_PARSER(ClassName, Identifier) \
  REGISTER_SUBCLASS(ParserHack, ClassName, std::string, Identifier, void *, config_t *)

#endif /* PARSER_H */

