/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ConditionnalTimeInterval.h"

const string CTISolver::modelFileName = "/tmp/methane_cti.mod";

const map<string, string> CTISolver::models = {
    {"worst_concurrency", 
#include "ConditionalTimeInterval/cpcti_worst_concurrency.mod"
    }
};

CTISolver::CTISolver(const SystemModel& tg, const config_t& conf) : Solver(tg, conf) {
#ifndef CPLEXOPL
    throw MyException("solver", "cplex undefined, you can't use a ConditionalTimeInterval solver");
#endif
}

void CTISolver::run(Schedule_t* result) {
#ifdef CPLEXOPL
    Utils::WARN("EXPERIMENTAL feature");
    IloEnv env;
    IloOplRunConfiguration rc(env, CTISolver::modelFileName.c_str(), conf.files.lp.c_str());
    IloOplModel opl = rc.getOplModel();
    IloOplSettings settings = opl.getSettings();
    settings.setWithLocations(IloTrue);
    settings.setWithNames(IloTrue);
    settings.setForceElementUsage(IloFalse);
    
    opl.generate();
    try {
        result->status = opl.getCP().solve() != 0;
        result->makespan = opl.getCP().getObjValue();
        opl.postProcess();
        
        IloTupleSet solution =  opl.getElement("solution").asTupleSet();
        IloTupleIterator* it = solution.iterator();
        do {
            IloTuple el = **it;
            string task = el.getStringValue("task");
            tmp_results["start_"+task] = el.getIntValue("start");
            tmp_results["delay_"+task] = el.getIntValue("delay");
            tmp_results["p_"+task+"_"+string(el.getStringValue("p"))] = 1;
        }
        while(it->next());
    } 
    catch( IloException &e ) {
        throw MyException("solver", e.getMessage());
    }
    env.end();
#endif
}

void CTISolver::presolve(Schedule_t* result) {
    ofstream mos(modelFileName);
    mos << CTISolver::models.at(conf.solving.type) ;
    mos.close();
    
    ofstream dos(conf.files.lp);
    dos << "T_slot = " << tg.interconnect()->getActiveWindowTime() << ";" << endl;
    string tmp = ""; 
    for(meth_processor_t *p : tg.processors())
        tmp += "\""+*p+"\", ";
    dos << "Processors = {" << tmp.substr(0, tmp.length()-2) << " };" << endl;
    
    tmp = "";
    for(Task *t : tg.tasks())
        tmp += "\""+*t+"\", ";
    dos << "ExecTasks = {" << tmp.substr(0, tmp.length()-2) << " };" << endl;
    
    tmp = "";
    for(Task *t : tg.tasks())
        tmp += to_string(t->C)+", ";
    dos << "WCET = [" << tmp.substr(0, tmp.length()-2) << " ];" << endl;
    
    tmp = "";
    for(Task *t : tg.tasks()) {
        for(Task::prev_t el : t->previous)
            tmp += "<\""+*(el.first)+"\", \""+t+"\", "+to_string(el.second* conf.datatypes.size_bits.at(t->previous_datatypes[el.first]))+">, \n";
    }
    dos << "Precedences = {" << endl << tmp.substr(0, tmp.length()-3) << endl << " };";
    dos << endl;
    dos.close();
}
void CTISolver::postsolve(Schedule_t* result) {
    unlink(modelFileName.c_str());
    
    for(const pair<string, int> &el : tmp_results) {
        Utils::DEBUG(el.first+ " : "+to_string(el.second)); 
    }
    
    for(Task *t : tg.tasks()) {
        meth_processor_t *proc;
        for(meth_processor_t *p : tg.processors()) {
            if(tmp_results.count("p_"+*t+"_"+*p)) {
                if(!tmp_results.count("p_R"+*t+"_"+p) || !tmp_results.count("p_W"+*t+"_"+p))
                    throw MyException("solver", "Error when solving, communication tasks of "+*t+" aren't mapped on the same core "+p+" as the exec part");
                
                proc = p;
                break;
            }
        }
        string datatypeR = (t->previous_datatypes.size() > 0) ? (*(t->previous_datatypes.begin())).second : "void";
        string datatypeW = (t->successors.size() > 0) ? t->successors.at(0)->previous_datatypes[t] : "void";
        map_task_proc(result, proc, t, t->C,
            tmp_results.at("start_R"+*t), tmp_results.at("data_R"+*t), datatypeR, tmp_results.at("delay_R"+*t), 0,
            tmp_results.at("start_"+*t),
            tmp_results.at("start_W"+*t), tmp_results.at("data_W"+*t), datatypeW, tmp_results.at("delay_W"+*t),0
        );
    }
}
