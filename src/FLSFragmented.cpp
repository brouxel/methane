/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "FLSFragmented.h"
#include "FLSFragmentedQreadyBuilder.h"

void FLSFragmented::run(Schedule_t *sched) {
    assert(conf.interconnect.behavior == "non-blocking");
    Utils::WARN(string("Experimental", "code is still a work in progress, and stuff needs to be fixed.\n")+
    "In addition, there is a lot a redundant code with ForwardListScheduling.cpp, and a smart merge should be done.\n"+
    "(Give to FLSFragmented the capalities to handle FLS cases, not the opposite.)");
    
    // BFS is better on test_complex and audiobeam
    // burst: edge is better for audiobeam
    // burst: packet is better for test_complex
    // BFS has a better gain on StreamIT
    // DFSdelay, with STG (heur vs ILP), has a wider standard deviation with a smaller min degradation and a bigger max degradation. But the avg degradation is smaller than BFS
    Schedule_t schedDFSdelay;
    try {
        vector<SchedElt*> schedorder;
        FLSFragQreadyBuilder::buildListDFS(tg,conf, strategy, &schedorder);
        do_run(&schedDFSdelay, schedorder);
    }
    catch(Unschedulable &){}
    
    Schedule_t schedDFS;
    try {
        vector<SchedElt*> schedorder;
        FLSFragQreadyBuilder::buildListDFSWithRDelay(tg,conf, strategy, &schedorder);
        do_run(&schedDFS, schedorder);
    }
    catch(Unschedulable &){}
    
    Schedule_t schedBFS;
    try {
        vector<SchedElt*> schedorder;
        FLSFragQreadyBuilder::buildListBFS(tg,conf, strategy, &schedorder);
        do_run(&schedBFS, schedorder);
    }
    catch(Unschedulable &){}
    
    uint64_t schedLengthDFSDelay = retrieve_schedule_length(schedDFSdelay);
    uint64_t schedLengthDFS = retrieve_schedule_length(schedDFS);
    uint64_t schedLengthBFS = retrieve_schedule_length(schedBFS);
    Utils::DEBUG("\t====> Schedule length DFSDelay ("+to_string(schedDFSdelay.status)+") = "+to_string(schedLengthDFSDelay));
    Utils::DEBUG("\t====> Schedule length DFS ("+to_string(schedDFS.status)+") = "+to_string(schedLengthDFS));
    Utils::DEBUG("\t====> Schedule length BFS ("+to_string(schedBFS.status)+") = "+to_string(schedLengthBFS));
    
    sched->status = 0;
    sched->makespan = (unsigned)-1;
    if(schedBFS.status == 1) 
        *sched = schedBFS;
    if(schedDFS.status == 1 && schedLengthDFS <= sched->makespan) 
        *sched = schedDFS;
    if(schedDFSdelay.status == 1 && schedLengthDFSDelay <= sched->makespan)
        *sched = schedDFSdelay;
    
    sched->heur_DFSDelay = schedDFSdelay.status == 1 ? schedLengthDFSDelay : -1;
    sched->heur_DFS = schedDFS.status == 1 ? schedLengthDFS : -1;
    sched->heur_BFS = schedBFS.status == 1 ? schedLengthBFS : -1;
    
    Utils::DEBUG(""+*sched);
    Utils::DEBUG("\t====> Final Schedule length = "+to_string(retrieve_schedule_length(*sched)));
}

void FLSFragmented::do_run(Schedule_t *sched, vector<SchedElt*> &schedorder) {
    chrono::duration<double> running_time = chrono::high_resolution_clock::now() - start_time;
    if(conf.solving.timeout <=  running_time.count()) {
        sched->status = 0;
        // Time-out already reached
        return;
    }
    for(meth_processor_t *p: tg.processors()) {
        sched->procs[p->id].available_spm_size = p->spm_size;
        sched->procs[p->id].total_spm_size = p->spm_size;
    }
    
    vector<SchedElt*> Qdone;
    
    map<SchedElt*, struct memento_sched_state_s> memento;
    
    Utils::DEBUG("/***************** Start Mapping ********************/");
    while(!schedorder.empty()) {
        // Check memory usage
        if((Utils::get_ram_usage()/Utils::get_total_ram())*100 > 90 || Utils::get_swap_usage() > 0) {
            sched->status = 0;
            sched->makespan = 0;
            throw Unschedulable("Host out-of-memory -- abording");
        }
        // Check running time
        chrono::duration<double> running_time = chrono::high_resolution_clock::now() - start_time;
        if(conf.solving.timeout <=  running_time.count()) {
            sched->status = 0;
            cerr << "\e[91mWarning:\e[96m Time Out!\e[0m" << endl;
            break;
        }
        
        // get next element
        SchedElt *t = *(schedorder.begin());
        
        // store recovering point
        memento[t].schedule = *sched;
        memento[t].schedorder = schedorder;
        memento[t].scheddone = Qdone;
        memento[t].forbid_proc = strategy->forbidden_proc_mapping;
        
        // consume the element to schedule
        schedorder.erase(schedorder.begin());
        Qdone.push_back(t);
        
        Utils::memory_usage("-> Sched "+t->toString());
        Schedule_t copy = *sched;
        try {
            // do the mapping/scheduling
            strategy->chose_proc(&copy, Qdone, t);
            
            // memory allocation
            if(conf.archi.spm.assign_region) {
                SchedEltTask *concerned_task = t->getTask();

                if(find(Qdone.begin(), Qdone.end(), concerned_task) != Qdone.end() && all_of(concerned_task->packet_list.begin(), concerned_task->packet_list.end(), [Qdone](SchedEltPacket *a) {
                    return find(Qdone.begin(), Qdone.end(), a) != Qdone.end();
                })) {
                    strategy->map_phases_to_region(&copy, Qdone, concerned_task->task);
                }
            }
            
            *sched = copy;
            if(sched->status == 0)
                break;
        }
        catch(UnschedulableSPMSpace &e) {
            Utils::DEBUG(e.what());
            // as for now, this exception is raised only when there is no more space on SPM
            // mapping to SPM happens only when the whole task is scheduled (packets+exec)

            // Grab the task we were scheduling (well, the exec phase as it is the one that is mapped on a core)
            SchedEltTask *concerned_task = t->getTask();
            
            // Return to a stable known state
            *sched = memento[concerned_task].schedule;
            schedorder = memento[concerned_task].schedorder;
            Qdone = memento[concerned_task].scheddone;
            strategy->forbidden_proc_mapping = memento[concerned_task].forbid_proc;
            
            // Add the last tested core to the forbidden list to not test it again
            strategy->forbidden_proc_mapping[concerned_task].push_back(copy.tasks[concerned_task->task->id].proc);
            Utils::DEBUG("=====================> add "+*(copy.tasks[concerned_task->task->id].proc)+" to forbid list for "+concerned_task->toString());
            
            // Check if we tried to map the task on every core, if so raise exception
            if(strategy->forbidden_proc_mapping[concerned_task].size() == tg.processors().size()) {
                sched->status = 0;
                sched->makespan = 0;
                // we tried every processor, and on all of them we end up in an unschedulable state, so be it
                
                for(SchedElt *elt : Qdone) delete elt;
                throw Unschedulable("No SPM space left on any processor for elt: "+t->toString());
            }
        }
    }
    
    Utils::memory_usage("***************** Done *******************");
    for(SchedElt *elt : Qdone) delete elt;
    
    if(sched->status == 0) {
        sched->makespan = 0;
        return;
    }
    
    uint64_t scheduleLength = retrieve_schedule_length(*sched);
    sched->makespan = scheduleLength;
}

void FLSFragStrategy::chose_proc(Schedule_t* sched, const vector<SchedElt*>& Qdone, SchedElt* elt) {
    if(Utils::isa<SchedEltTask*>(elt)) {
        chose_proc(sched, Qdone, (SchedEltTask*)elt);
    }
    else {
        chose_proc(sched, Qdone, (SchedEltPacket*)elt);
    }
}

void FLSFragStrategy::map_phases_to_region(Schedule_t* sched, const vector<SchedElt*>& Qdone, Task* current) {
    if(!conf.archi.spm.assign_region) return;
    
    //Maybe TODO: if "Allocation XY overlap" arise, then maybe the phase Y should go in an other region, 
    
    uint32_t data_exec = current->local_memory_storage;
    if(!conf.archi.prefetch_code && conf.archi.spm.store_code)
        data_exec += current->memory_code_size;
    
    uint64_t startresa = 0; // resident exec region !conf.archi.prefetch_code && conf.archi.spm.store_code
    uint64_t endresa = -1;
    if(conf.archi.prefetch_code && conf.archi.spm.store_code) {
        startresa = sched->tasks[current->id].rtR();
        endresa = sched->tasks[current->id].rtE+current->C;
    }
    else if(!(!conf.archi.prefetch_code && conf.archi.spm.store_code)) { // only local data
        startresa = sched->tasks[current->id].rtE;
        endresa = sched->tasks[current->id].rtE+current->C;
    }
    spm_region_info sre = allocate_region(sched, Qdone, current, data_exec, 
        startresa, endresa,
        "E");
    sched->tasks[current->id].memory_region.exec = sre;
    
    if(conf.interconnect.mechanism == "sharedonly") {
        for(Task::prev_t el : current->previous) {
            uint32_t data_read = el.second * conf.datatypes.size_bits.at(current->previous_datatypes[el.first]);
            spm_region_info srr = allocate_region(sched, Qdone, current, data_read,
                sched->tasks[current->id].rtR(), sched->tasks[current->id].rtE+current->C, 
                "R");
            sched->tasks[current->id].memory_region.read[el.first->id] = srr;
        }
        for(Task *succ : current->successors) {
            uint32_t data_written = current->data_written(*succ)* conf.datatypes.size_bits.at(succ->previous_datatypes[current]);
            spm_region_info srw = allocate_region(sched, Qdone, current, data_written, 
                sched->tasks[current->id].rtE, sched->tasks[current->id].end(),
                "W");
            sched->tasks[current->id].memory_region.write[succ->id] = srw;
        }
    }
}

spm_region_info FLSFragStrategy::allocate_region(Schedule_t* sched, const vector<SchedElt*>& Qdone, Task* current, uint32_t data, uint64_t startresa, uint64_t endresa, const string &suffix) {
    spm_region_info truc;
    if(data == 0) return truc;
    
    string procid = sched->tasks[current->id].proc->id;
      
    // no region has been assigned
    
    // build set of already created region where there is enough space, and the endresa of the region finishes before the current task needs
    // take the smallest region that can fit the demand
    // if not found, then throw unschedulable on that core

    vector<spm_region_info> candidate(sched->procs[procid].memory_regions.size());
    auto &tmpconf = conf;
    if(suffix != "E" || (!tmpconf.archi.prefetch_code && tmpconf.archi.spm.store_code)) {
        vector<spm_region_info>::iterator it = copy_if(sched->procs[procid].memory_regions.begin(), sched->procs[procid].memory_regions.end(), candidate.begin(), 
            [sched, &tmpconf, current, data, startresa, suffix](const spm_region_info &a) {
            if(a.size <= data || a.endresa >= startresa) // here just checking for endresa might create unschedulable, maybe storing a vector of resa period should be better to find a free one
                return false;

            if(!tmpconf.archi.prefetch_code && tmpconf.archi.spm.store_code) {
                // check if the region is assigned to an exec phase, for the case where the code is loaded a the platform startup
                for(pair<string, task_schedule_info> el : sched->tasks) {
                    if(el.second.memory_region.exec == a)
                        return false;
                }
                return true;
            }

            return true;
        });
        candidate.resize(distance(candidate.begin(), it));
    
        if(candidate.size() > 0) {
            sort(candidate.begin(), candidate.end(), [](const spm_region_info &a, const spm_region_info &b) { return a.size < b.size; });
            spm_region_info goodone = *(candidate.begin());
            goodone.endresa = endresa;

            //Utils::DEBUG("\t" + current->id + " will reuse region "+goodone->label+" on "+procid+" for "+suffix);
            return goodone;
        }
    }
    
    if(sched->procs[procid].available_spm_size >= data) {
        //there is enough free space, we  create region for read phase
        string label = current->ilp_label+suffix;
        
        spm_region_info sr(label, data);
        sr.startresa = startresa;
        sr.endresa = endresa;
        sched->procs[procid].memory_regions.push_back(sr);
        
        sched->procs[procid].available_spm_size -= data;
        
        Utils::DEBUG("\t" + current->id + " create a region on core "+procid+" with size "+to_string(data)+" with name "+label);
        
        return sr;
    }
    
    /*if(suffix != "E") {
        candidate.clear();
        candidate.resize(sched->procs[procid].memory_regions.size());
        vector<spm_region_info>::iterator it = copy_if(sched->procs[procid].memory_regions.begin(), sched->procs[procid].memory_regions.end(), candidate.begin(), 
            [sched, &tmpconf, current, data, startresa, suffix](const spm_region_info &a) {
            if(a.size <= data)
                return false;

            if(!tmpconf.archi.prefetch_code && tmpconf.archi.spm.store_code) {
                // check if the region is assigned to an exec phase, for the case where the code is loaded a the platform startup
                for(pair<string, task_schedule_info> el : sched->tasks) {
                    if(el.second.memory_region.exec == a)
                        return false;
                }
                return true;
            }

            return true;
        });
        candidate.resize(distance(candidate.begin(), it));
        if(candidate.size() > 0) {
            sort(candidate.begin(), candidate.end(), [](const spm_region_info &a, const spm_region_info &b) { return a.endresa < b.endresa; });
            throw UnschedulableDelayPacket()
        }
    }*/
        
    throw UnschedulableSPMSpace(current, "No spm space on proc "+procid+"("+to_string(sched->procs[procid].available_spm_size)+")"+" for "+suffix+" phase of "+current->id+" or maybe look for a region that have enough space and is free for use on the time slot required by current, or resize one -- size needed: "+to_string(data));
}