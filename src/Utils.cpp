/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Utils.h"

#include "SystemModel.h"
using namespace std;

namespace Utils {
    bool file_exists(const char* name) {
        struct stat buffer;   
        return (stat (name, &buffer) == 0); 
    }
    bool file_exists(const string &name) {
        return file_exists(name.c_str()); 
    }
    
    string demangle(const string & str) {
        int status;
        string demangled = str;
        char *realname = abi::__cxa_demangle(str.c_str(), 0, 0, &status);
        if(status == 0) {
          demangled = realname;
          demangled = demangled.substr(0, demangled.find_first_of('('));
        }
        if(realname != NULL)
          free(realname);

        return demangled;
    }
    
    bool sort_Q_byComm(Task *a, Task *b) {
        uint32_t a_max = 0;
        for(Task::prev_t el : a->previous) {
            if(el.second > a_max)
                a_max = el.second;
        }
        uint32_t b_max = 0;
        for(Task::prev_t el : b->previous) {
            if(el.second > b_max)
                b_max = el.second;
        }
        return a_max < b_max;
    }
    bool sort_Q_byName(Task *a, Task *b) {
        return a->id > b->id;
    }
    bool sort_Q_byMemUsageDec(Task *a, Task *b) {
        return a->memory_footprint < b->memory_footprint;
    }
    bool sort_Q_byWCET(Task *a, Task *b) {
        return a->C > b->C;
    }
    
    void DEBUG(const string &str) {
#ifdef _DEBUG
        cerr << str << endl;
#endif
    }
    
    void WARN(const string &lbl, const string &str) {
        cerr << "\e[91m" << lbl << " ERROR:\e[96m " << str << "\e[0m" << endl;
    }
    
    void WARN(const string &str) {
        WARN("warning", str);
    }
    
    uint64_t size_to_uint(const string &str) {
        uint64_t val = stoull(str);
        char unit, prefix;
        try { 
            prefix = tolower(str.at(str.size()-2)); 
            if(prefix == 'k') 
                val *= 1024;
            else if(prefix == 'm')
                val *= 1024 * 1024;
            else if(prefix == 'g')
                val *= 1024 * 1024 * 1024;
            else if(prefix != ' ')
                throw out_of_range("");
        } 
        catch(out_of_range &) { 
#ifdef _DEBUG
            cerr << "\e[91mWARNING:\e[0m bad prefix '" << prefix <<"' unit for size in \"" << str << "\" using default: \" \"" << endl; 
#endif
            prefix = ' '; 
        }
        try { 
            unit = str.at(str.size()-1); 
            if(unit == 'o' || unit == 'B') 
                val *= 8;
            else if(unit != 'b')
                throw out_of_range(""); 
        } 
        catch(out_of_range &) { 
            unit = 'b'; 
#ifdef _DEBUG
            cerr << "\e[91mWARNING:\e[0m bad unit for size in \"" << str << "\" using default: \"" << unit << "\"" << endl; 
#endif
        }
        
        return val;
    }
    
    string uint_to_size(uint64_t val) {
        
        int gb=0;
        while((val-gb*(1024*1024*1024)) / (1024*1024*1024) > 0) gb++;
        if(gb) {
            int flt = (val-gb*(1024*1024*1024)) % (1024*1024*1024);
            return to_string(gb)+(flt ? ","+to_string(flt) : "")+"Gb";
        }
        
        int mb=0;
        while((val-mb*(1024*1024)) / (1024*1024) > 0) mb++;
        if(mb) {
            int flt = (val-mb*(1024*1024)) % (1024*1024);
            return to_string(mb)+(flt ? ","+to_string(flt) : "")+"Mb";
        }
        
        int kb=0;
        while((val-kb*1024) / 1024 > 0) kb++;
        if(kb) {
            int flt = (val-kb*1024) % 1024;
            return to_string(kb)+(flt ? ","+to_string(flt) : "")+"kb";
        }
        
        return to_string(val)+" b";
    }
    
#if _LIBCPP_STD_VER <= 14
#endif
    
    uint64_t lcm(const vector<uint64_t>& values)  {
        return accumulate(values.begin(), values.end(), (uint64_t)1, [](const uint64_t & a, const uint64_t & b) {
            #if _LIBCPP_STD_VER > 14
            return a * b / __gcd(a, b);
            #else
            return a * b / boost::integer::gcd(a,b);
            #endif
        });
    }
    
    void memory_usage(const string &msg) {
        if(!msg.empty())
            Utils::DEBUG(msg);
        
        static int64_t prev_used = 0;
        static int64_t prev_used_swap = 0;
        
        uint64_t used_ram = Utils::get_ram_usage();
        uint64_t used_swap = Utils::get_swap_usage();
        uint64_t tot_ram = Utils::get_total_ram();
        uint64_t tot_swap = Utils::get_total_swap();
        
        Utils::DEBUG("total ram: "+to_string(tot_ram)+" -- used memory by proc: "+to_string(used_ram)+" -- freed/prod since last check : "+to_string(used_ram - prev_used));
        Utils::DEBUG("total swap: "+to_string(tot_swap)+" -- used swap by proc: "+to_string(used_swap)+" -- freed/prod since last check : "+to_string(used_swap - prev_used_swap));
        
        prev_used = used_ram ;
        prev_used_swap = used_swap ;
    }
    
    uint64_t get_ram_usage() {
#ifdef __linux__
        bool found = false;
        ifstream file("/proc/self/status");
        string key, value;
        while(!file.eof()) {
            file >> key >> value ;
            if(key.find("VmRSS") != string::npos) {
                file.close();
                return stoull(value);
            }
        }
#endif   
#ifdef __APPLE__
        struct task_basic_info t_info;
        mach_msg_type_number_t t_info_count = TASK_BASIC_INFO_COUNT;
        // resident size is in t_info.resident_size;
        // virtual size is in t_info.virtual_size;
        if (KERN_SUCCESS == task_info(mach_task_self(),
                                      TASK_BASIC_INFO, (task_info_t)&t_info, 
                                      &t_info_count))
        {
            return t_info.resident_size;
        }
#endif
        return 0;
    }
    
    uint64_t get_swap_usage() {
#ifdef __linux__
        ifstream file("/proc/self/status");
        string key, value;
        while(!file.eof()) {
            file >> key >> value ;
            if(key.find("VmSwap") != string::npos) {
                file.close();
                return stoull(value);
            }
        }
#endif   
#ifdef __APPLE__
       // no preallocated swap, when prog start consuming too much memory, process is killed, not swapped
#endif
        return 0;
    }
    
    uint64_t get_total_ram() {
#ifdef __linux__
        struct sysinfo info;
        sysinfo(&info);
        return info.totalram;
#else
#ifdef __APPLE__
        int mib[2] = {CTL_HW, HW_MEMSIZE};
        uint64_t physical_memory;
        size_t length = sizeof(uint64_t);
        sysctl(mib, 2, &physical_memory, &length, NULL, 0);
        return physical_memory;
#else
        return 0;
#endif
#endif
        
    }
    uint64_t get_total_swap() {
#ifdef __linux__
        struct sysinfo info;
        sysinfo(&info);
        return info.totalswap;
#else
#ifdef __APPLE__
       // no preallocated swap, when prog start consuming too much memory, process is killed, not swapped
#endif
        return 0;
#endif
    }
}

