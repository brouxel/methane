/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HEURISTIQUE_H
#define HEURISTIQUE_H

#include <cstdlib>
#include <vector>
#include <string>
#include <map>
#include <algorithm> 
#include <array>
#include <cmath>
#include <cassert>
#include <chrono>
#include <tuple>
//#include <stdint.h> 

#include <boost/algorithm/string.hpp> 

#include "config.h"
#include "Utils.h"
#include "SystemModel.h"
#include "Schedule.h"
#include "Solver.h"
#include "registry.h"

class FLSStrategy {
protected:
  void allocate_region(Schedule_t* sched, const vector<Task*>& Qdone, Task* current, uint32_t data, spm_region_info *current_region, uint64_t startresa, uint64_t endresa, const string &suffix);
  
protected:
    const SystemModel &tg;
    const config_t &conf;
    
    virtual uint32_t compute_read_concurrency(Schedule_t *sched, const vector<Task*> &Qdone, Task *t) = 0;
    virtual uint32_t compute_write_concurrency(Schedule_t *sched, const vector<Task*> &Qdone, Task *t) = 0;
    
    virtual uint32_t compute_data_read(Schedule_t *sched, Task *t, const vector<Task*> &Qdone);
    virtual uint32_t compute_data_written(Schedule_t *sched, Task *t, const vector<Task*> &Qdone);
    
    virtual uint64_t find_best_start_read(const Schedule_t &sched, const vector<Task*> &Qdone, Task *t);
    virtual uint64_t find_best_start_write(const Schedule_t &sched, const vector<Task*> &Qdone, Task *t);
    virtual uint64_t find_best_start_exec(const Schedule_t &sched, const vector<Task*> &Qdone, Task *t);
    
    virtual uint64_t ensure_task_mutex(const Schedule_t &sched, const vector<Task*> &enemies, Task *t, uint64_t bestStart);
    virtual uint64_t ensure_read_mutex(const Schedule_t &sched, const vector<Task*> &enemies, Task *t, uint64_t bestStart);
    virtual uint64_t ensure_write_mutex(const Schedule_t &sched, const vector<Task*> &enemies, Task *t, uint64_t bestStart);
    virtual uint64_t ensure_exec_mutex(const Schedule_t &sched, const vector<Task*> &enemies, Task *t, uint64_t bestStart);
    
    virtual void build_related(Schedule_t *sched, const vector<Task*> &Qdone, Task *current, vector<Task*> *related);
    
public:
    explicit FLSStrategy(const SystemModel &m, const config_t &c) : tg(m), conf(c) {};
    
    virtual void chose_proc(Schedule_t *sched, const vector<Task*> &Qdone, Task *t);
    virtual void map_phases_to_region(Schedule_t *sched, const vector<Task*> &Qdone, Task *current);
    virtual void adjust_region_endresa(Schedule_t *sched, const vector<Task*> &Qdone, Task *current);
    
    virtual void adjust_schedule(Schedule_t *sched, const vector<Task*> &Qdone, Task *t) = 0;
    
    virtual uint32_t compute_data_read(Schedule_t *sched, Task *t, meth_processor_t *candidate, const vector<Task*> &Qdone);
    virtual uint32_t compute_data_written(Schedule_t *sched, Task *t, meth_processor_t *candidate, const vector<Task*> &Qdone);
    
};

using FLSStrategyRegistry = registry::Registry<FLSStrategy, string, const SystemModel &, const config_t &>;

#define REGISTER_FLSSTRATEGY(ClassName, Identifier) \
  REGISTER_SUBCLASS(FLSStrategy, ClassName, string, Identifier, const SystemModel &, const config_t &)

class ForwardListScheduling : public Solver {
private:
    FLSStrategy *strategy;
public:
    explicit ForwardListScheduling(const SystemModel &tg, const config_t &conf) : Solver(tg, conf) {
        strategy = FLSStrategyRegistry::Create(conf.solving.type, tg, conf);
    };
        
protected:
    virtual void run(Schedule_t *sched);
    
    template<typename __Compare__>
    void get_next_ready(vector<Task*> &Qdone, Task*current, vector<Task*> *Qready, __Compare__ __comp) {
        vector<Task*> Qtmp;

        for(Task *s : current->successors) {
            if(find(Qdone.begin(), Qdone.end(), s) != Qdone.end())
                continue;

            bool ok = true;
            for(Task::prev_t el : s->previous) {
                if(find(Qdone.begin(), Qdone.end(), el.first) == Qdone.end()) {
                    ok = false;
                    break;
                }
            }
            if(ok)
                Qtmp.push_back(s);
        }
        sort(Qtmp.begin(), Qtmp.end(), __comp);
        reverse(Qtmp.begin(), Qtmp.end());
        Qready->insert(Qready->end(), Qtmp.begin(), Qtmp.end());
    }
    
    template<typename __Compare__>
    void build_Qready(vector<Task*> &Qready, vector<Task*> &Qdone, __Compare__ __comp) {
        vector<Task*> nextQready;
        for(Task *t : Qready) {
            Qdone.push_back(t);
            get_next_ready(Qdone, t, &nextQready, __comp);
        }
        if(nextQready.empty()) {
            return;
        }

        build_Qready(nextQready, Qdone, __comp);
        Qready.insert(Qready.end(), nextQready.begin(), nextQready.end());
    }
    
    template<typename __Compare__>
    void get_next_readyDFS(vector<Task*> &Qdone, Task*current, vector<Task*> *Qready, __Compare__ __comp) {
        
        bool ok = true;
        for(Task::prev_t el : current->previous) {
            if(find(Qdone.begin(), Qdone.end(), el.first) == Qdone.end()) {
                ok = false;
                break;
            }
        }
        if(ok) {
            Qready->push_back(current);
            Qdone.push_back(current);
            
            vector<Task*> succs(current->successors);
            sort(succs.begin(), succs.end(), __comp);
            for(Task *s : succs) {
                get_next_readyDFS(Qdone, s, Qready, __comp);
            }
        }
    }
    
    template<typename __Compare__>
    void build_QreadyDFS(vector<Task*> &Qready, __Compare__ __comp) {
        vector<Task*> nextQready;
        vector<Task*> Qdone;
        
        for(Task *root : Qready) {
            Qdone.push_back(root);
            
            vector<Task*> succs(root->successors);
            sort(succs.begin(), succs.end(), __comp);
            for(Task *s : succs) {
                get_next_readyDFS(Qdone, s, &nextQready, __comp);
            }
        }
        
        Qready.insert(Qready.end(), nextQready.begin(), nextQready.end());
    }

    virtual void do_schedule(Schedule_t *sched, vector<Task*> Qready, vector<Task*> &Qdone);
};

REGISTER_SOLVER(ForwardListScheduling, "fls")

#endif /* HEURISTIQUE_H */
