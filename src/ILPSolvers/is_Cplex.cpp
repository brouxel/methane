/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ILP.h"

using namespace std;

#ifdef CPLEX

#include <ilcplex/ilocplex.h>
#include <ilcplex/cplex.h>
#include <ilconcert/ilotupleset.h>
#include <ilconcert/ilosys.h>

class Cplex : public ILPSolver {
public:
    explicit Cplex(const SystemModel &tg, const config_t &conf) : ILPSolver(tg, conf) {};
    
protected:
    void run(Schedule_t *result) {
        string filename = conf.files.lp;
        string paramfile = filename+".param";
        
        IloEnv env;
        IloModel model(env);
        IloCplex cplex(env);
        IloNumVarArray var(env);
        try {
            IloObjective   obj;
            IloRangeArray  rng(env);

            cplex.importModel(model, filename.c_str(), obj, var, rng);
            cplex.extract(model);
            
            if(conf.clean)
               unlink(filename.c_str());
            unlink((filename+".sol").c_str());
            
            if(Utils::file_exists(paramfile.c_str())) 
                cplex.readParam(paramfile.c_str());
            
            IloCplex::ParameterSet paramset(env);
            paramset = cplex.getParameterSet();
            
            if(conf.solving.timeout > 0) {
                cplex.setParam(IloCplex::NumParam::TiLim, conf.solving.timeout);
                cplex.setParam(IloCplex::NumParam::TuningTiLim, conf.solving.timeout/2);
            }
            
            if(conf.solving.cores > 0) {
                cplex.setParam(IloCplex::IntParam::Threads, conf.solving.cores);
            }
            
            // Tunning time do not account for number of threads
            /*if(tg.tasks().size() > 15) {
                cplex.setParam(IloCplex::IntParam::TuningDisplay, 1);
                cplex.setParam(IloCplex::IntParam::TuningRepeat, 1);

    //            IloCplex::TuneParamHandle handler = cplex.tuneParam(paramset, true);
    //            handler.joinTuneParam();
                cplex.tuneParam(paramset);
            }*/
            cplex.solve();

            tmp_results.clear();
            uint32_t nbvars = var.getSize();
            for(int i=0 ; i < nbvars ; ++i) {
                tmp_results.insert({var[i].getName(), round(cplex.getValue(var[i]))});
            }

            //cplex.exportModel("/tmp/plop.lp");
            
            result->makespan = round(cplex.getObjValue());
            result->status = (cplex.getStatus() == IloAlgorithm::Status::Optimal);
            if(!result->status && cplex.getStatus() == IloAlgorithm::Status::Feasible)
                result->status = 2;
            cout << "solution status is " << (cplex.getStatus() == IloAlgorithm::Status::Optimal) << " - " << cplex.getStatus() << endl;
            cout << "solution value  is " << cplex.getObjValue() << endl;
            cout << "Gap is " << cplex.getMIPRelativeGap()*100 << endl;

            if(!conf.clean) {
                cplex.writeParam(paramfile.c_str());
                cplex.writeSolution((filename+".sol").c_str(), 0);
            }
            env.end();
        }
        catch(IloException& e) {
            if(cplex.getStatus() == IloAlgorithm::Status::Infeasible) {
                result->makespan = -1;
                result->status = 0;
                env.end();
            }
            else {
                //cplex.exportModel(filename.c_str());
                //cplex.writeParam(paramfile.c_str());
                env.end();
                throw MyException("solver", e.getMessage());
            }
        }
    }
};
REGISTER_SOLVER(Cplex, "cplex")
        
#endif
