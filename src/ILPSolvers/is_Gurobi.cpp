/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ILP.h"

using namespace std;

#ifdef GUROBI

#include <gurobi_c++.h>

class Gurobi : public ILPSolver {
public:
    explicit Gurobi(const SystemModel &tg, const config_t &conf) : ILPSolver(tg, conf) {};
    
protected:
    void run(Schedule_t *result) {
        string filename = conf.files.lp;

        try {
            GRBEnv env = GRBEnv();
            GRBModel model = GRBModel(env, filename);
            GRBEnv menv = model.getEnv();

            int method = 3 ; // Anh m'a dit de prendre celle-ci qui est mieux, depuis l'exemple on a -1 
            menv.set(GRB_IntParam_Method, method);
            if(conf.solving.timeout > 0)
                menv.set(GRB_DoubleParam_TimeLimit, conf.solving.timeout);

            model.optimize();
            
            if(model.get(GRB_IntAttr_Status) == GRB_INFEASIBLE)
                throw Unschedulable("Gurobi ERROR : No solution exists");

            tmp_results.clear();
            GRBVar* vars = model.getVars();
            uint32_t nbvars = model.get(GRB_IntAttr_NumVars);
            for(uint32_t i=0 ; i < nbvars ; ++i) {
                tmp_results.insert({vars[i].get(GRB_StringAttr_VarName), round(vars[i].get(GRB_DoubleAttr_X))});
            }

            cout << "solution status is " << (model.get(GRB_IntAttr_Status) == GRB_OPTIMAL) << " - " << model.get(GRB_IntAttr_Status) << endl;
            cout << "solution value  is " << model.get(GRB_DoubleAttr_ObjVal) << endl;
            cout << "Gap is " << model.get(GRB_DoubleAttr_MIPGap)*100 << endl;

            result->makespan = model.get(GRB_DoubleAttr_ObjVal);
            result->status = (model.get(GRB_IntAttr_Status) == GRB_OPTIMAL);
        }
        catch(GRBException& e) {
            throw MyException("solver", e.getMessage());
        }

    }
};
REGISTER_SOLVER(Gurobi, "gurobi")

#endif