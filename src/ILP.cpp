/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ILP.h"

using namespace std;

void ILPSolver::presolve(Schedule_t *) {
    ILPFormGen * generator = ILPFormRegistry::Create(conf.solving.type, tg, conf);
#ifndef _DEBUG
    if(!Utils::file_exists(conf.files.lp.c_str()))
#endif
        generator->gen();
}

void ILPSolver::postsolve(Schedule_t *result) {
    if(result->status == 0) return;
    
    for(const pair<string, int> &el : tmp_results) {
        Utils::DEBUG(el.first+ " : "+to_string(el.second)); 
    }
    
    for(meth_processor_t *p : tg.processors()) {
        result->procs[p->id].available_spm_size = 0;
        result->procs[p->id].total_spm_size = p->spm_size;
        for(string r : tg.memory_regions()) {
            if(tmp_results.count("srspm_"+*p+"_"+r) && tmp_results.count("srspm_"+*p+"_"+r) > 0) {
                spm_region_info sr(r, tmp_results.at("srspm_"+*p+"_"+r));
                result->procs[p->id].memory_regions.push_back(sr);
            }
        }
    }

    for(Task *t : tg.tasks()) {
        meth_processor_t *proc;
        for(meth_processor_t *p : tg.processors()) {
            if(tmp_results.at("p_"+*t+"_"+*p)) {
                proc = p;
                break;
            }
        }

        string datatypeR = (t->previous_datatypes.size() > 0) ? (*(t->previous_datatypes.begin())).second : "void";
        string datatypeW = (t->successors.size() > 0) ? t->successors.at(0)->previous_datatypes[t] : "void";
        map_task_proc(result, proc, t, t->C,
            tmp_results.at("rho_r_"+*t), tmp_results.at("DR_"+*t)/conf.datatypes.size_bits.at(datatypeR), datatypeR, -1, tmp_results.at("delay_r_"+*t),
            tmp_results.at("rho_"+*t),
            tmp_results.at("rho_w_"+*t), tmp_results.at("DW_"+*t)/conf.datatypes.size_bits.at(datatypeW), datatypeW, -1, tmp_results.at("delay_w_"+*t)
        );

        if(conf.archi.spm.assign_region) {
            for(string r : tg.memory_regions()) {
                if(tmp_results.count("Eresa_r_"+r+"_"+t) && tmp_results.at("Eresa_r_"+r+"_"+t)) {
                    result->tasks[t->id].memory_region.read[t->id] = *(find(result->procs[proc->id].memory_regions.begin(), result->procs[proc->id].memory_regions.end(), r));
                }
                if(tmp_results.count("Eresa_e_"+r+"_"+t) && tmp_results.at("Eresa_e_"+r+"_"+t)) {
                    result->tasks[t->id].memory_region.exec = *(find(result->procs[proc->id].memory_regions.begin(), result->procs[proc->id].memory_regions.end(), r));
                }
                if(tmp_results.count("Eresa_w_"+r+"_"+t) && tmp_results.at("Eresa_w_"+r+"_"+t)) {
                    result->tasks[t->id].memory_region.write[t->id] = *(find(result->procs[proc->id].memory_regions.begin(), result->procs[proc->id].memory_regions.end(), r));
                }
            }

            //---
            vector<string> regions;
            map<Task *, vector<string>> readpackets, writepackets, execpackets; 
            for(Task *j : tg.tasks()) {
                for(Task::prev_t el : j->previous) {
                    Task *i = el.first;
                    uint32_t D_ij = el.second;
                    vector<uint32_t> slots = CoreMemInterConnect::burst(conf, D_ij, conf.datatypes.size_bits.at(j->previous_datatypes[i]));
                    if(slots.size() == 0) continue;

                    for(uint32_t s=0 ; s < slots.size() ; ++s) {
                        regions.push_back("MR"+*i+*j+to_string(s));
                        readpackets[i].push_back("_r_"+*j+"_"+*i+"_"+to_string(s));
                    }
                }
            }
            for(Task *i : tg.tasks()) {
                for(Task *j : i->successors) {
                    uint32_t D_ij = i->data_written(*j);
                    vector<uint32_t> slots = CoreMemInterConnect::burst(conf, D_ij, conf.datatypes.size_bits.at(j->previous_datatypes[i]));
                    if(slots.size() == 0) continue;

                    for(uint32_t s=0 ; s < slots.size() ; ++s) {
                        regions.push_back("MR"+*i+*j+to_string(s));
                        writepackets[i].push_back("_w_"+*i+"_"+*j+"_"+to_string(s));
                    }
                }
            }
            for(Task *i : tg.tasks()) {
                regions.push_back("MR"+*i);
                execpackets[i].push_back("_e_"+*i);
            }
            for(meth_processor_t *p : tg.processors()) {
                result->procs[p->id].available_spm_size = 0;
                result->procs[p->id].total_spm_size = p->spm_size;
                for(string r : regions) {
                    if(tmp_results.count("spmregsize_"+*p+"_"+r) && tmp_results.count("spmregsize_"+*p+"_"+r) > 0) {
                        spm_region_info sr(r, tmp_results.at("spmregsize_"+*p+"_"+r));
                        result->procs[p->id].memory_regions.push_back(sr);
                    }
                }
            }
            for(meth_processor_t *proc : tg.processors()) {
                for(string r : regions) {
                    for(pair<Task *, vector<string>> el : readpackets) {
                        for(string pac : el.second) {
                            if(tmp_results.count("spmp"+pac+"_"+*proc+"_"+r) && tmp_results.at("spmp"+pac+"_"+*proc+"_"+r)) {
                                result->tasks[el.first->id].memory_region.read[el.first->id] = *(find(result->procs[proc->id].memory_regions.begin(), result->procs[proc->id].memory_regions.end(), r));
                            }
                        }
                    }
                    for(pair<Task *, vector<string>> el : writepackets) {
                        for(string pac : el.second) {
                            if(tmp_results.count("spmp"+pac+"_"+*proc+"_"+r) && tmp_results.at("spmp"+pac+"_"+*proc+"_"+r)) {
                                result->tasks[el.first->id].memory_region.write[el.first->id] = *(find(result->procs[proc->id].memory_regions.begin(), result->procs[proc->id].memory_regions.end(), r));
                            }
                        }
                    }
                    for(pair<Task *, vector<string>> el : execpackets) {
                        for(string pac : el.second) {
                            if(tmp_results.count("spmp"+pac+"_"+*proc+"_"+r) && tmp_results.at("spmp"+pac+"_"+*proc+"_"+r)) {
                                result->tasks[el.first->id].memory_region.exec = *(find(result->procs[proc->id].memory_regions.begin(), result->procs[proc->id].memory_regions.end(), r));
                            }
                        }
                    }
                }
            }
        }
        //---
    }
    
    if(!conf.solving.optimal) {
        // because the ILP var makespan is unlinked to find a feasible schedule, then we must determine the makespan in an other way
        result->makespan = 0;
        for(Task *t : tg.tasks()) {
            if(result->tasks[t->id].end() > result->makespan)
                result->makespan = result->tasks[t->id].end() ;
        }
    }
}

void ILPFormGen::objectiv_func() {
    os << "\\Problem name: Scheduling task graph" << endl;
    os << "\\Correspondances: " << endl;
    for(Task *t : tg.tasks()) 
        os << "\\\t" << t << " -> " << t->id << endl;
    os << endl;
    os << "minimize" << endl;
    os << "obj: makespan" << endl;
    os << endl << "Subject To" << endl;
}

void ILPFormGen::linkObj() {
    if(!conf.solving.optimal) return; //no optimal result required, we do not link vars to the makespan
    
    os << endl;
    for (Task *i : tg.tasks()) {
        os << "OC_" << i << ": end_" << i << " - makespan <= 0" << endl;
    }
}

void ILPFormGen::unicity() {
    // Unicity   
    for (Task* i : tg.tasks()) {
        string tmp;
        for (meth_processor_t* j : tg.processors()) {
            string v = "p_" + *i + "_" + *j;
            tmp += v + " + ";
            binvar += v+"\n";
        }
        tmp = tmp.substr(0, tmp.length() - 2);
        os << "Uni_" << i << ": " << tmp << " = 1" << endl;
    }
}

void ILPFormGen::detect_samecore() {
    os << endl;
    // same core
    for (vector<Task*>::const_iterator it = tg.tasks().begin(), et=tg.tasks().end() ; it != et ; ++it) {
        for (vector<Task*>::const_iterator jt = it+1 ; jt != et ; ++jt) {
            Task *i = *it, *j = *jt;
            string tmp = "";
            for (meth_processor_t *k : tg.processors()) {
                string lbl_tmp_m = "tmp_m_" + *i + "_" + *j + "_" + *k;
                binvar += lbl_tmp_m+"\n";

                os << "OSC_1_" << i << "_" << j << "_" << k << ": p_" << i << "_" << k << " + p_" << j << "_" << k << " - " << lbl_tmp_m << " <= 1" << endl;
                os << "OSC_2_" << i << "_" << j << "_" << k << ": p_" << i << "_" << k << " - " << lbl_tmp_m << " >= 0" << endl;
                os << "OSC_3_" << i << "_" << j << "_" << k << ": p_" << j << "_" << k << " - " << lbl_tmp_m << " >= 0" << endl;

                tmp += lbl_tmp_m + " + ";
            }
            tmp = tmp.substr(0, tmp.length() - 2);
            os << "OSC_4_" << i << "_" << j << ": " << tmp << " - m_" << i << "_" << j << " = 0" << endl;
            os << "OSC_5_" << i << "_" << j << ": m_" << i << "_" << j << " - m_" << j << "_" << i << " = 0" << endl;
            os << endl;
            binvar += "m_"+*i+"_"+j+"\n";
            binvar += "m_"+*j+"_"+i+"\n";
        }
    }
}

void ILPFormGen::precedence() {
    os << endl;
    // precedence
    for (vector<Task*>::const_iterator i = tg.tasks().begin(), e = tg.tasks().end(); i != e; ++i) {
        for (vector<Task*>::const_iterator j = i + 1; j != e; ++j) {
            os << "PR_" << *i << "_" << *j << ": a_" << *i << "_" << *j << " + a_" << *j << "_" << *i << " = 1" << endl;
        }
    }
}

void ILPFormGen::precedence_communications() {
    for (vector<Task*>::const_iterator i = tg.tasks().begin(), e = tg.tasks().end(); i != e; ++i) {
        for (vector<Task*>::const_iterator j = i + 1; j != e; ++j) {
            os << "PR_nb1_" << *i << "_" << *j << ":a_rr_" << *i << "_" << *j << " + a_rr_" << *j << "_" << *i << " = 1" << endl;
            os << "PR_nb2_" << *i << "_" << *j << ":a_ww_" << *i << "_" << *j << " + a_ww_" << *j << "_" << *i << " = 1" << endl;
            os << "PR_nb3_" << *i << "_" << *j << ":a_rw_" << *i << "_" << *j << " + a_wr_" << *j << "_" << *i << " = 1" << endl;
            os << "PR_nb4_" << *i << "_" << *j << ":a_wr_" << *i << "_" << *j << " + a_rw_" << *j << "_" << *i << " = 1" << endl;

            binvar += "a_rr_"+**i+"_"+*j+"\n";
            binvar += "a_rr_"+**j+"_"+*i+"\n";
            binvar += "a_ww_"+**i+"_"+*j+"\n";
            binvar += "a_ww_"+**j+"_"+*i+"\n";
            binvar += "a_rw_"+**i+"_"+*j+"\n";
            binvar += "a_rw_"+**j+"_"+*i+"\n";
            binvar += "a_wr_"+**i+"_"+*j+"\n";
            binvar += "a_wr_"+**j+"_"+*i+"\n";
        }
    }
}

void ILPFormGen::precedence_samecore() {
    os << endl;
    // precedence same core
    for (Task *i : tg.tasks()) {
        for (Task *j : tg.tasks()) {
            if (i == j) continue;
            
            os << "PROSM_1_" << i << "_" << j << ": a_" << i << "_" << j << " + m_" << i << "_" << j << " - " <<
                    "am_" << i << "_" << j << " <= 1" << endl;
            os << "PROSM_2_" << i << "_" << j << ": a_" << i << "_" << j << " - am_" << i << "_" << j << " >= 0" << endl;
            os << "PROSM_3_" << i << "_" << j << ": m_" << i << "_" << j << " - am_" << i << "_" << j << " >= 0" << endl;
            
            binvar += "am_"+*i+"_"+j+"\n";
        }
    }
}
void ILPFormGen::precedence_samecore_communications() {
    for (Task *i : tg.tasks()) {
        for (Task *j : tg.tasks()) {
            if (i == j) continue;

            os << "PROSM_13_" << i << "_" << j << ": a_rr_" << i << "_" << j << " + m_" << i << "_" << j << " - " <<
                    "a_rr_m_" << i << "_" << j << " <= 1" << endl;
            os << "PROSM_14_" << i << "_" << j << ": a_rr_" << i << "_" << j << " - a_rr_m_" << i << "_" << j << " >= 0" << endl;
            os << "PROSM_15_" << i << "_" << j << ": m_" << i << "_" << j << " - a_rr_m_" << i << "_" << j << " >= 0" << endl;

            os << "PROSM_4_" << i << "_" << j << ": a_ww_" << i << "_" << j << " + m_" << i << "_" << j << " - " <<
                    "a_ww_m_" << i << "_" << j << " <= 1" << endl;
            os << "PROSM_5_" << i << "_" << j << ": a_ww_" << i << "_" << j << " - a_ww_m_" << i << "_" << j << " >= 0" << endl;
            os << "PROSM_6_" << i << "_" << j << ": m_" << i << "_" << j << " - a_ww_m_" << i << "_" << j << " >= 0" << endl;

            os << "PROSM_7_" << i << "_" << j << ": a_wr_" << i << "_" << j << " + m_" << i << "_" << j << " - " <<
                    "a_wr_m_" << i << "_" << j << " <= 1" << endl;
            os << "PROSM_8_" << i << "_" << j << ": a_wr_" << i << "_" << j << " - a_wr_m_" << i << "_" << j << " >= 0" << endl;
            os << "PROSM_9_" << i << "_" << j << ": m_" << i << "_" << j << " - a_wr_m_" << i << "_" << j << " >= 0" << endl;

            os << "PROSM_10_" << i << "_" << j << ": a_rw_" << i << "_" << j << " + m_" << i << "_" << j << " - " <<
                    "a_rw_m_" << i << "_" << j << " <= 1" << endl;
            os << "PROSM_11_" << i << "_" << j << ": a_rw_" << i << "_" << j << " - a_rw_m_" << i << "_" << j << " >= 0" << endl;
            os << "PROSM_12_" << i << "_" << j << ": m_" << i << "_" << j << " - a_rw_m_" << i << "_" << j << " >= 0" << endl;

            binvar += "a_rr_m_"+*i+"_"+*j+"\n";
            binvar += "a_ww_m_"+*i+"_"+*j+"\n";
            binvar += "a_rw_m_"+*i+"_"+*j+"\n";
            binvar += "a_wr_m_"+*i+"_"+*j+"\n";
        }
    }
}

void ILPFormGen::conflicts() {
    os << endl;
    
    if(conf.interconnect.behavior == "blocking") {
        for(Task *i : tg.tasks()) {
            for(Task *j : tg.tasks()) {
                if(i == j) continue;
                // rho_r_i + delay_r_i + WCETi + delay_w_i <= rho_r_j + M (1 - amij) : no overlapping of tasks on the same core
                os << "Conf_" << i << "_" << j << ": end_" << i << " - rho_r_" << j << "" << " + " << SeqTauMax << " am_" << i << "_" << j << " <= " << SeqTauMax << endl;
            }
        }
        return;
    }
    
    if(conf.interconnect.behavior == "non-blocking") {
        for(Task *i : tg.tasks()) {
            for(Task *j : tg.tasks()) {
                if(i == j) continue;
                // rho_i + WCETi <= rho_j + M (1 - amij) : no overlapping of exec phases on the same core
                os << "Conf1_" << i << "_" << j << ": rho_" << i << " - rho_" << j << " + " << SeqTauMax << " am_" << i << "_" << j << " <= " << SeqTauMax-i->C << endl;
            }
        }
        return;
    }
}

void ILPFormGen::conflicts_communications() {
    for(Task *i : tg.tasks()) {
        for(Task *j : tg.tasks()) {
            if(i == j) continue;
            // rho^r_i + delay^r_i <= rho^r_j + M (1 - am_rr_ij) : no overlapping of read phase on the same core
            os << "Conf2_" << i << "_" << j << ": rho_r_" << i << " + delay_r_" << i << " - rho_r_" << j << " + " << SeqTauMax << " a_rr_m_" << i << "_" << j << " <= " << SeqTauMax << endl;
            // rho_w_i + delay_w_i <= rho_w_j + M (1 - am_ww_ij) : no overlapping of write phase on the same core
            os << "Conf3_" << i << "_" << j << ": end_" << i << " - rho_w_" << j << " + " << SeqTauMax << " a_ww_m_" << i << "_" << j << " <= " << SeqTauMax << endl;
            // rho_r_i + delay_r_i <= rho_w_j + M (1 - am_rw_ij) : no overlapping of read phase and write on the same core, with the read before the write
            os << "Conf4_" << i << "_" << j<< ": rho_r_" << i << " + delay_r_" << i << " - rho_w_" << j << " + " << SeqTauMax << " a_rw_m_" << i << "_" << j << " <= " << SeqTauMax << endl;
            // rho_w_i + delay_w_i <= rho_r_j + M (1 - am_wr_ij) : no overlapping of read phase and write on the same core, with the write before the read
            os << "Conf5_" << i << "_" << j<< ": end_" << i << " - rho_r_" << j << " + " << SeqTauMax << " a_wr_m_" << i << "_" << j << " <= " << SeqTauMax << endl;
        }
    }
}

void ILPFormGen::read_exec_write() {
    os << endl;
    if(conf.interconnect.behavior == "blocking") {
        for(Task *t : tg.tasks()) {
            os << "Ps_" << t << ": rho_w_" << t << " - rho_" << t << " = " << t->C << endl;
            os << "Pe_" << t << ": end_" << t << " - rho_w_" << t << " - delay_w_" << t << " = 0" << endl;
            os << "Pr_" << t << ": rho_" << t << " - rho_r_" << t << " - delay_r_" << t << " = 0" << endl;
        }
    }
    else {
        for(Task *t : tg.tasks()) {
            os << "Ps_" << t << ": rho_w_" << t << " - rho_" << t << " >= " << t->C << endl;
            os << "Pe_" << t << ": end_" << t << " - rho_w_" << t << " - delay_w_" << t << " = 0" << endl;
            os << "Pr_" << t << ": rho_" << t << " - rho_r_" << t << " - delay_r_" << t << " >= 0" << endl;
        }
    }
}

void ILPFormGen::causality() {
    os << endl;
    if(conf.interconnect.behavior == "blocking") {
        for(Task *t : tg.tasks()) {
            for(Task::prev_t el : t->previous) {
                os << "Cau_1_" << t << "_" << el.first << ": rho_r_" << t << " - end_" << el.first << " >= 0" << endl;
                os << "Cau_2_" << el.first << "_" << t << ": a_" << el.first << "_" << t << " = 1" << endl;
            }
        }
    }
    else {
        for(Task *t : tg.tasks()) {
            for(Task::prev_t el : t->previous) {
                // \rho_j + WCET_j <= \rho_i
                os << "Cau_1_" << t << "_" << el.first << ": rho_" << t << " - rho_" << el.first << " >= " << el.first->C << endl;
                os << "Cau_2_" << el.first << "_" << t << ": a_" << el.first << "_" << t << " = 1" << endl;
            }
        }
    }
}

void ILPFormGen::causality_communications() {
    os << endl;
    for(Task *t : tg.tasks()) {
        for(Task::prev_t el : t->previous) {
            // \rho_j + WCET_j <= \rho_i
            os << "Cau_3_" << el.first << "_" << t << ": a_rr_" << el.first << "_" << t << " = 1" << endl;
            os << "Cau_4_" << el.first << "_" << t << ": a_ww_" << el.first << "_" << t << " = 1" << endl;
            os << "Cau_5_" << el.first << "_" << t << ": a_rw_" << el.first << "_" << t << " = 1" << endl;
            os << "Cau_6_" << el.first << "_" << t << ": a_wr_" << el.first << "_" << t << " = 1" << endl;

            // end_j <= \rho^r_i + M m_ij
            os << "Cau_7_" << el.first << "_" << t << ": end_" << el.first << " - rho_r_" << t << " - " << SeqTauMax << " m_" << t << "_" << el.first << " <= 0" << endl;
        }
    }
}

void ILPFormGen::add_bounds(const string & bounds) {
    add_bounds();
    os << bounds;
}
void ILPFormGen::add_bounds() {
    uint64_t deadline = SeqTauMax;
    
    os << endl << "Bounds" << endl;
    os << "0 <= makespan <= " << deadline  << endl;
    for(Task *t : tg.tasks()) {
        os << "0 <= rho_" << t << " <= " << deadline << endl;
        os << "0 <= rho_r_" << t << " <= " << deadline << endl;
        os << "0 <= rho_w_" << t << " <= " << deadline << endl;
        os << "0 <= end_" << t << " <= " << deadline << endl;
    }
}

void ILPFormGen::declare_binaries() {
    os << endl << "Binary" << endl;
    os << binvar;
}

void ILPFormGen::declare_generals() {
    os << endl << "Generals" << endl;
    os << "makespan" << endl;
    for(Task *t : tg.tasks()) {
        os << "rho_" << t << endl;
        os << "rho_r_" << t << endl;
        os << "rho_w_" << t << endl;
        os << "end_" << t << endl;
        os << "delay_r_" << t << endl;
        os << "delay_w_" << t << endl;
    }
    os << genvar;
}

void ILPFormGen::concludeILPFile() {
    os << "End" << endl;
}
void ILPFormGen::concludeILPFile(string &remaining) {
    os << remaining;
    concludeILPFile();
}

void ILPFormGen::force_mapping() {
    for(Task *t : tg.tasks()) {
        if(!t->force_mapping_proc.empty()) {
            os << "ForceMap_" << t << " : p_" << t << "_" << t->force_mapping_proc << " = 1" << endl;
            
            for(Task *t2 : tg.tasks()) {
                if(t == t2 || t2->force_mapping_proc.empty()) continue;
                os << "ForceMap_m_" << t << "_" << t2 << " : m_" << t << "_" << t2 << " = " << ((t->force_mapping_proc == t2->force_mapping_proc)) << endl;
            }
        }
    }
    os << endl;
}

void ILPFormGen::force_schedule() {
    map<string, vector<Task*>> assignment;
    for(Task *t : tg.tasks()) {
        if(t->force_sched_order_proc >= 0) {
            assignment[t->force_mapping_proc].push_back(t);
        }
    }
    for(pair<string, vector<Task*>> el : assignment) {
        sort(el.second.begin(), el.second.end(), [](Task *a, Task *b) {
            return a->force_sched_order_proc < b->force_sched_order_proc;
        });
        for(vector<Task*>::iterator it=el.second.begin(), jt=it+1, et=el.second.end() ; jt != et ; ++it, ++jt) {
            os << "ForceSched_" << *it << "_" << *jt << " : end_" << *it << " - " << "rho_" << *jt << " <= 0" << endl;
        }
    }
    os << endl;
}

void ILPFormGen::map_spm_region_to_task() {
    if(!conf.archi.spm.assign_region)
        return;
    
    // Unicity, a task is mapped on only one region
    for(Task *t : tg.tasks()) {
        string procunicity_r = "", procunicity_e = "", procunicity_w = "";
        for(string r : tg.memory_regions()) {
            binvar += "Eresa_r_"+r+"_"+*t+"\n";
            procunicity_r += "Eresa_r_"+r+"_"+*t+" + ";
            binvar += "Eresa_e_"+r+"_"+*t+"\n";
            procunicity_e += "Eresa_e_"+r+"_"+*t+" + ";
            binvar += "Eresa_w_"+r+"_"+*t+"\n";
            procunicity_w += "Eresa_w_"+r+"_"+*t+" + ";

        }
        os << "RSPM_UP_R_" << "_" << t << ": " << procunicity_r.substr(0, procunicity_r.size()-2) << " = 1" << endl;
        os << "RSPM_UP_E_" << "_" << t << ": " << procunicity_e.substr(0, procunicity_e.size()-2) << " = 1" << endl;
        os << "RSPM_UP_W_" << "_" << t << ": " << procunicity_w.substr(0, procunicity_w.size()-2) << " = 1" << endl;
    }

    // Determine the size of the region that is the biggest amount of stored data in it
    for(meth_processor_t *p : tg.processors()) {
        os << endl;
        for(string r : tg.memory_regions()) {
            for(Task *t : tg.tasks()) {
                uint32_t data_read = t->data_read();
                // code prefetched in read phase, and stored in the same region as the read phase
                if(conf.archi.prefetch_code && conf.archi.spm.store_code)
                    data_read += t->memory_code_size;
                
                uint32_t data_exec = t->local_memory_storage;
                // no prefetching of code, thus it is resident in the exec region
                if(!conf.archi.prefetch_code && conf.archi.spm.store_code)
                    data_exec += t->memory_code_size;
                
                // srspm_{p,r}  >= data ( Eresa_{r,t} /\ (p_{t,p} - 1))
                os << "RSPM_S_R_" << p << "_" << r << "_" << t << 
                        " : srspm_" << p << "_" << r << " - " << data_read << " Eresa_r_" << r << "_" << t << " - " << SeqTauMax << " p_" << t << "_" << p << " >= -" << SeqTauMax << endl;
                os << "RSPM_S_E_" << p << "_" << r << "_" << t << 
                        " : srspm_" << p << "_" << r << " - " <<  data_exec << " Eresa_e_" << r << "_" << t << " - " << SeqTauMax << " p_" << t << "_" << p << " >= -" << SeqTauMax << endl;
                os << "RSPM_S_W_" << p << "_" << r << "_" << t << 
                        " : srspm_" << p << "_" << r << " - " << t->data_written() << " Eresa_w_" << r << "_" << t << " - " << SeqTauMax << " p_" << t << "_" << p << " >= -" << SeqTauMax << endl;
            }
        }
    }

    // The total size of regions must not exceed the size of the spm
    os << endl;
    for(meth_processor_t *p : tg.processors()) {
        string totalsize = "";
        for(string r : tg.memory_regions()) {
            genvar += "srspm_"+*p+"_"+r+"\n";
            totalsize += "srspm_"+*p+"_"+r+" + ";
        }
        os << "RSPM_TS_" << p << " : " << totalsize.substr(0, totalsize.size()-2) << " <= " << p->spm_size << endl;
    }
    
    //if we don't force communication through external memory, edge where src and dst are mapped on the same core must have the same region
    if(conf.interconnect.mechanism != "sharedonly") {
        for(Task *a : tg.tasks()) {
            for(Task *b : a->successors) {
                for(string r : tg.memory_regions()) {
                    os << "RSPM_SR1_" << a << "_" << b << "_" << r << ": Eresa_w_" << r << "_" << a << " - Eresa_r_" << r << "_" << b << " - " << SeqTauMax << " m_" << a << "_" << b << " >= -" << SeqTauMax << endl;
                    os << "RSPM_SR2_" << a << "_" << b << "_" << r << ": Eresa_w_" << r << "_" << a << " - Eresa_r_" << r << "_" << b << " + " << SeqTauMax << " m_" << a << "_" << b << " <= " << SeqTauMax << endl;
                }
            }
        }
    }
    
    // no prefetching of code, thus it is resident in the exec region and this region can't be reused by any other phase
    // the exec region is loaded when the board boot, and the code remain in place, this is why no body can use it.
    if(!conf.archi.prefetch_code && conf.archi.spm.store_code) {
        for(Task* i : tg.tasks()) {
            for(Task* j : tg.tasks()) {
                for (string r : tg.memory_regions()) {
                    os << "Eresa_e_" << r << "_" << i << " + Eresa_r_" << r << "_" << j << " + " << SeqTauMax << " m_" << i << "_" << j << " <= " << (1+SeqTauMax) << endl;
                    os << "Eresa_e_" << r << "_" << i << " + Eresa_w_" << r << "_" << j << " + " << SeqTauMax << " m_" << i << "_" << j << " <= " << (1+SeqTauMax) << endl;
                }
                if(i == j) continue;
                for (string r : tg.memory_regions()) {
                    os << "Eresa_e_" << r << "_" << i << " + Eresa_e_" << r << "_" << j << " + " << SeqTauMax << " m_" << i << "_" << j << " <= " << (1+SeqTauMax) << endl;
                }
            }
        }
    }
    
    // Determine si 2 phases are assigned to the same region, similar to the "m_{i,j}" for processor assignment
    for (vector<Task*>::const_iterator it = tg.tasks().begin(), et=tg.tasks().end() ; it != et ; ++it) {
        Task *i = *it;
        for (vector<Task*>::const_iterator jt = it+1 ; jt != et ; ++jt) {
            Task *j = *jt;
            string tmp_rr, tmp_ee, tmp_ww, tmp_re, tmp_rw, tmp_ew;
            for (string r : tg.memory_regions()) {
                string lbl_tmp_m = "spmrrtmp_m_" + *i + "_" + *j + "_" + r;
                binvar += lbl_tmp_m+"\n";
                os << "spmOSC_1rr_" << i << "_" << j << "_" << r << ": Eresa_r_" << r << "_" << i << " + Eresa_r_" << r << "_" << j << "  - " << lbl_tmp_m << " <= 1" << endl;
                os << "spmOSC_2rr_" << i << "_" << j << "_" << r << ": Eresa_r_" << r << "_" << i << " - " << lbl_tmp_m << " >= 0" << endl;
                os << "spmOSC_3rr_" << i << "_" << j << "_" << r << ": Eresa_r_" << r << "_" << j << " - " << lbl_tmp_m << " >= 0" << endl;
                tmp_rr += lbl_tmp_m + " + ";
                
                lbl_tmp_m = "spmwwtmp_m_" + *i + "_" + *j + "_" + r;
                binvar += lbl_tmp_m+"\n";
                os << "spmOSC_1ww_" << i << "_" << j << "_" << r << ": Eresa_w_" << r << "_" << i << " + Eresa_w_" << r << "_" << j << "  - " << lbl_tmp_m << " <= 1" << endl;
                os << "spmOSC_2ww_" << i << "_" << j << "_" << r << ": Eresa_w_" << r << "_" << i << " - " << lbl_tmp_m << " >= 0" << endl;
                os << "spmOSC_3ww_" << i << "_" << j << "_" << r << ": Eresa_w_" << r << "_" << j << " - " << lbl_tmp_m << " >= 0" << endl;
                tmp_ww += lbl_tmp_m + " + ";
                
                lbl_tmp_m = "spmrwtmp_m_" + *i + "_" + *j + "_" + r;
                binvar += lbl_tmp_m+"\n";
                os << "spmOSC_1rw_" << i << "_" << j << "_" << r << ": Eresa_r_" << r << "_" << i << " + Eresa_w_" << r << "_" << j << "  - " << lbl_tmp_m << " <= 1" << endl;
                os << "spmOSC_2rw_" << i << "_" << j << "_" << r << ": Eresa_r_" << r << "_" << i << " - " << lbl_tmp_m << " >= 0" << endl;
                os << "spmOSC_3rw_" << i << "_" << j << "_" << r << ": Eresa_w_" << r << "_" << j << " - " << lbl_tmp_m << " >= 0" << endl;
                tmp_rw += lbl_tmp_m + " + ";
                
                if(!(!conf.archi.prefetch_code && conf.archi.spm.store_code)) { // the exec region handles local data only
                    lbl_tmp_m = "spmeetmp_m_" + *i + "_" + *j + "_" + r;
                    binvar += lbl_tmp_m+"\n";
                    os << "spmOSC_1ee_" << i << "_" << j << "_" << r << ": Eresa_e_" << r << "_" << i << " + Eresa_e_" << r << "_" << j << "  - " << lbl_tmp_m << " <= 1" << endl;
                    os << "spmOSC_2ee_" << i << "_" << j << "_" << r << ": Eresa_e_" << r << "_" << i << " - " << lbl_tmp_m << " >= 0" << endl;
                    os << "spmOSC_3ee_" << i << "_" << j << "_" << r << ": Eresa_e_" << r << "_" << j << " - " << lbl_tmp_m << " >= 0" << endl;
                    tmp_ee += lbl_tmp_m + " + ";
                    
                    lbl_tmp_m = "spmretmp_m_" + *i + "_" + *j + "_" + r;
                    binvar += lbl_tmp_m+"\n";
                    os << "spmOSC_1re_" << i << "_" << j << "_" << r << ": Eresa_r_" << r << "_" << i << " + Eresa_e_" << r << "_" << j << "  - " << lbl_tmp_m << " <= 1" << endl;
                    os << "spmOSC_2re_" << i << "_" << j << "_" << r << ": Eresa_r_" << r << "_" << i << " - " << lbl_tmp_m << " >= 0" << endl;
                    os << "spmOSC_3re_" << i << "_" << j << "_" << r << ": Eresa_e_" << r << "_" << j << " - " << lbl_tmp_m << " >= 0" << endl;
                    tmp_re += lbl_tmp_m + " + ";
                    
                    lbl_tmp_m = "spmewtmp_m_" + *i + "_" + *j + "_" + r;
                    binvar += lbl_tmp_m+"\n";
                    os << "spmOSC_1ew_" << i << "_" << j << "_" << r << ": Eresa_e_" << r << "_" << i << " + Eresa_w_" << r << "_" << j << "  - " << lbl_tmp_m << " <= 1" << endl;
                    os << "spmOSC_2ew_" << i << "_" << j << "_" << r << ": Eresa_e_" << r << "_" << i << " - " << lbl_tmp_m << " >= 0" << endl;
                    os << "spmOSC_3ew_" << i << "_" << j << "_" << r << ": Eresa_w_" << r << "_" << j << " - " << lbl_tmp_m << " >= 0" << endl;
                    tmp_rw += lbl_tmp_m + " + ";
                }
            }
            binvar += "spmmtmp2_rr_"+*i+"_"+j+"\n";
            os << "spmOSC_4rr_" << i << "_" << j << ": " << tmp_rr.substr(0, tmp_rr.length() - 2) << " - spmmtmp2_rr_" << i << "_" << j << " = 0" << endl;
            os << "spmOSC_5rr_" << i << "_" << j << ": m_" << i << "_" << j << " + spmmtmp2_rr_" << i << "_" << j << " - spmm_rr_" << i << "_" << j << " <= 1" << endl;
            os << "spmOSC_6rr_" << i << "_" << j << ": m_" << i << "_" << j << " - spmm_rr_" << i << "_" << j << " >= 0" << endl;
            os << "spmOSC_7rr_" << i << "_" << j << ": spmmtmp2_rr_" << i << "_" << j << " - spmm_rr_" << i << "_" << j << " >= 0" << endl;
            
            binvar += "spmmtmp2_ww_"+*i+"_"+j+"\n";
            os << "spmOSC_4ww_" << i << "_" << j << ": " << tmp_ww.substr(0, tmp_ww.length() - 2) << " - spmmtmp2_ww_" << i << "_" << j << " = 0" << endl;
            os << "spmOSC_5ww_" << i << "_" << j << ": m_" << i << "_" << j << " + spmmtmp2_ww_" << i << "_" << j << " - spmm_ww_" << i << "_" << j << " <= 1" << endl;
            os << "spmOSC_6ww_" << i << "_" << j << ": m_" << i << "_" << j << " - spmm_ww_" << i << "_" << j << " >= 0" << endl;
            os << "spmOSC_7ww_" << i << "_" << j << ": spmmtmp2_ww_" << i << "_" << j << " - spmm_ww_" << i << "_" << j << " >= 0" << endl;
            
            binvar += "spmmtmp2_rw_"+*i+"_"+j+"\n";
            os << "spmOSC_4rw_" << i << "_" << j << ": " << tmp_rw.substr(0, tmp_rw.length() - 2) << " - spmmtmp2_rw_" << i << "_" << j << " = 0" << endl;
            os << "spmOSC_5rw_" << i << "_" << j << ": m_" << i << "_" << j << " + spmmtmp2_rw_" << i << "_" << j << " - spmm_rw_" << i << "_" << j << " <= 1" << endl;
            os << "spmOSC_6rw_" << i << "_" << j << ": m_" << i << "_" << j << " - spmm_rw_" << i << "_" << j << " >= 0" << endl;
            os << "spmOSC_7rw_" << i << "_" << j << ": spmmtmp2_rw_" << i << "_" << j << " - spmm_rw_" << i << "_" << j << " >= 0" << endl;
            
            if(!(!conf.archi.prefetch_code && conf.archi.spm.store_code)) { // the exec region handles local data only
                binvar += "spmmtmp2_ee_"+*i+"_"+j+"\n";
                os << "spmOSC_4ee_" << i << "_" << j << ": " << tmp_ee.substr(0, tmp_ee.length() - 2) << " - spmmtmp2_ee_" << i << "_" << j << " = 0" << endl;
                os << "spmOSC_5ee_" << i << "_" << j << ": m_" << i << "_" << j << " + spmmtmp2_ee_" << i << "_" << j << " - spmm_ee_" << i << "_" << j << " <= 1" << endl;
                os << "spmOSC_6ee_" << i << "_" << j << ": m_" << i << "_" << j << " - spmm_ee_" << i << "_" << j << " >= 0" << endl;
                os << "spmOSC_7ee_" << i << "_" << j << ": spmmtmp2_ee_" << i << "_" << j << " - spmm_ee_" << i << "_" << j << " >= 0" << endl;
            
                binvar += "spmmtmp2_re_"+*i+"_"+j+"\n";
                os << "spmOSC_4re_" << i << "_" << j << ": " << tmp_re.substr(0, tmp_re.length() - 2) << " - spmmtmp2_re_" << i << "_" << j << " = 0" << endl;
                os << "spmOSC_5re_" << i << "_" << j << ": m_" << i << "_" << j << " + spmmtmp2_re_" << i << "_" << j << " - spmm_re_" << i << "_" << j << " <= 1" << endl;
                os << "spmOSC_6re_" << i << "_" << j << ": m_" << i << "_" << j << " - spmm_re_" << i << "_" << j << " >= 0" << endl;
                os << "spmOSC_7re_" << i << "_" << j << ": spmmtmp2_re_" << i << "_" << j << " - spmm_re_" << i << "_" << j << " >= 0" << endl;
                
                binvar += "spmmtmp2_ew_"+*i+"_"+j+"\n";
                os << "spmOSC_4ew_" << i << "_" << j << ": " << tmp_ew.substr(0, tmp_ew.length() - 2) << " - spmmtmp2_ew_" << i << "_" << j << " = 0" << endl;
                os << "spmOSC_5ew_" << i << "_" << j << ": m_" << i << "_" << j << " + spmmtmp2_ew_" << i << "_" << j << " - spmm_ew_" << i << "_" << j << " <= 1" << endl;
                os << "spmOSC_6ew_" << i << "_" << j << ": m_" << i << "_" << j << " - spmm_ew_" << i << "_" << j << " >= 0" << endl;
                os << "spmOSC_7ew_" << i << "_" << j << ": spmmtmp2_ew_" << i << "_" << j << " - spmm_ew_" << i << "_" << j << " >= 0" << endl;
            }
            
            os << "spmOSC_8rr_" << i << "_" << j << ": spmm_rr_" << i << "_" << j << " - spmm_rr_" << j << "_" << i << " = 0" << endl;
            os << "spmOSC_8ww_" << i << "_" << j << ": spmm_ww_" << i << "_" << j << " - spmm_ww_" << j << "_" << i << " = 0" << endl;
            os << "spmOSC_8rw_" << i << "_" << j << ": spmm_rw_" << i << "_" << j << " - spmm_wr_" << j << "_" << i << " = 0" << endl;
            if(!(!conf.archi.prefetch_code && conf.archi.spm.store_code)) { // the exec region handles local data only
                os << "spmOSC_8ee_" << i << "_" << j << ": spmm_ee_" << i << "_" << j << " - spmm_ee_" << j << "_" << i << " = 0" << endl;
                os << "spmOSC_8re_" << i << "_" << j << ": spmm_re_" << i << "_" << j << " - spmm_er_" << j << "_" << i << " = 0" << endl;
                os << "spmOSC_8ew_" << i << "_" << j << ": spmm_ew_" << i << "_" << j << " - spmm_we_" << j << "_" << i << " = 0" << endl;
            }
            os << endl;
            
            binvar += "spmm_rr_"+*i+"_"+j+"\n";
            binvar += "spmm_ww_"+*i+"_"+j+"\n";
            binvar += "spmm_rw_"+*i+"_"+j+"\n";
            binvar += "spmm_wr_"+*i+"_"+j+"\n";
            binvar += "spmm_rr_"+*j+"_"+i+"\n";
            binvar += "spmm_ww_"+*j+"_"+i+"\n";
            binvar += "spmm_rw_"+*j+"_"+i+"\n";
            binvar += "spmm_wr_"+*j+"_"+i+"\n";
            if(!(!conf.archi.prefetch_code && conf.archi.spm.store_code)) { // the exec region handles local data only
                binvar += "spmm_ee_"+*i+"_"+j+"\n";
                binvar += "spmm_re_"+*i+"_"+j+"\n";
                binvar += "spmm_we_"+*i+"_"+j+"\n";
                binvar += "spmm_er_"+*i+"_"+j+"\n";
                binvar += "spmm_ew_"+*i+"_"+j+"\n";
                binvar += "spmm_ee_"+*j+"_"+i+"\n";
                binvar += "spmm_re_"+*j+"_"+i+"\n";
                binvar += "spmm_we_"+*j+"_"+i+"\n";
                binvar += "spmm_er_"+*j+"_"+i+"\n";
                binvar += "spmm_ew_"+*j+"_"+i+"\n";
            }
        }
    }
    
    if(conf.interconnect.behavior == "non-blocking") {
        if(conf.archi.prefetch_code || !(!conf.archi.prefetch_code && conf.archi.spm.store_code)) {
            // later we reuse the "a_rr_" and "a_ww_" etc.. variable from the task scheduling part. Because there is no such variables between phases execute and read/write and we need some
            // for the region allocation, equivalent constraints are added to fulfill this, see precedence and causality
            for (vector<Task*>::const_iterator i = tg.tasks().begin(), e = tg.tasks().end(); i != e; ++i) {
                for (vector<Task*>::const_iterator j = i + 1; j != e; ++j) {
                    os << "spmPR_nb5_" << *i << "_" << *j << ":a_re_" << *i << "_" << *j << " + a_er_" << *j << "_" << *i << " = 1" << endl;
                    os << "spmPR_nb7_" << *i << "_" << *j << ":a_we_" << *i << "_" << *j << " + a_ew_" << *j << "_" << *i << " = 1" << endl;

                    binvar += "a_er_"+**i+"_"+*j+"\n";
                    binvar += "a_er_"+**j+"_"+*i+"\n";
                    binvar += "a_ew_"+**i+"_"+*j+"\n";
                    binvar += "a_ew_"+**j+"_"+*i+"\n";
                    binvar += "a_re_"+**i+"_"+*j+"\n";
                    binvar += "a_re_"+**j+"_"+*i+"\n";
                    binvar += "a_we_"+**i+"_"+*j+"\n";
                    binvar += "a_we_"+**j+"_"+*i+"\n";
                }
            }
            for(Task *t : tg.tasks()) {
                for(Task::prev_t el : t->previous) {
                    os << "spmCau_nb1_" << el.first << "_" << t << ": a_re_" << el.first << "_" << t << " = 1" << endl;
                    os << "spmCau_nb2_" << el.first << "_" << t << ": a_er_" << el.first << "_" << t << " = 1" << endl;
                    os << "spmCau_nb3_" << el.first << "_" << t << ": a_we_" << el.first << "_" << t << " = 1" << endl;
                    os << "spmCau_nb4_" << el.first << "_" << t << ": a_ew_" << el.first << "_" << t << " = 1" << endl;
                }
            }
        }

        // Determine spmam_XY_i,j similar to am_XY_i,j
        //    spmam_XY_i,j = a_XY_i,j && spmm_XY_i,j 
        for(Task* i : tg.tasks()) {
            for(Task* j : tg.tasks()) {
                if(i == j) continue;

                os << "a_rr_" << i << "_" << j << " + spmm_rr_" << i << "_" << j << " - spmam_rr_" << i << "_" << j << " <= 1" << endl;
                os << "a_rr_" << i << "_" << j << " - " << "spmam_rr_" << i << "_" << j << " >= 0" << endl;
                os << "spmm_rr_" << i << "_" << j << " - " << "spmam_rr_" << i << "_" << j << " >= 0" << endl;
                binvar += "spmam_rr_"+*i+"_"+j+"\n";

                os << "a_ww_" << i << "_" << j << " + spmm_ww_" << i << "_" << j << " - spmam_ww_" << i << "_" << j << " <= 1" << endl;
                os << "a_ww_" << i << "_" << j << " - " << "spmam_ww_" << i << "_" << j << " >= 0" << endl;
                os << "spmm_ww_" << i << "_" << j << " - " << "spmam_ww_" << i << "_" << j << " >= 0" << endl;
                binvar += "spmam_ww_"+*i+"_"+j+"\n";

                os << "a_rw_" << i << "_" << j << " + spmm_rw_" << i << "_" << j << " - spmam_rw_" << i << "_" << j << " <= 1" << endl;
                os << "a_rw_" << i << "_" << j << " - " << "spmam_rw_" << i << "_" << j << " >= 0" << endl;
                os << "spmm_rw_" << i << "_" << j << " - " << "spmam_rw_" << i << "_" << j << " >= 0" << endl;
                binvar += "spmam_rw_"+*i+"_"+j+"\n";

                os << "a_wr_" << i << "_" << j << " + spmm_wr_" << i << "_" << j << " - spmam_wr_" << i << "_" << j << " <= 1" << endl;
                os << "a_wr_" << i << "_" << j << " - " << "spmam_wr_" << i << "_" << j << " >= 0" << endl;
                os << "spmm_wr_" << i << "_" << j << " - " << "spmam_wr_" << i << "_" << j << " >= 0" << endl;
                binvar += "spmam_wr_"+*i+"_"+j+"\n";

                if(!(!conf.archi.prefetch_code && conf.archi.spm.store_code)) { // the exec region handles local data only
                    os << "a_" << i << "_" << j << " + spmm_ee_" << i << "_" << j << " - spmam_ee_" << i << "_" << j << " <= 1" << endl;
                    os << "a_" << i << "_" << j << " - " << "spmam_ee_" << i << "_" << j << " >= 0" << endl;
                    os << "spmm_ee_" << i << "_" << j << " - " << "spmam_ee_" << i << "_" << j << " >= 0" << endl;
                    binvar += "spmam_ee_"+*i+"_"+j+"\n";

                    os << "a_re_" << i << "_" << j << " + spmm_re_" << i << "_" << j << " - spmam_re_" << i << "_" << j << " <= 1" << endl;
                    os << "a_re_" << i << "_" << j << " - " << "spmam_re_" << i << "_" << j << " >= 0" << endl;
                    os << "spmm_re_" << i << "_" << j << " - " << "spmam_re_" << i << "_" << j << " >= 0" << endl;
                    binvar += "spmam_re_"+*i+"_"+j+"\n";

                    os << "a_er_" << i << "_" << j << " + spmm_er_" << i << "_" << j << " - spmam_er_" << i << "_" << j << " <= 1" << endl;
                    os << "a_er_" << i << "_" << j << " - " << "spmam_er_" << i << "_" << j << " >= 0" << endl;
                    os << "spmm_er_" << i << "_" << j << " - " << "spmam_er_" << i << "_" << j << " >= 0" << endl;
                    binvar += "spmam_er_"+*i+"_"+j+"\n";

                    os << "a_ew_" << i << "_" << j << " + spmm_ew_" << i << "_" << j << " - spmam_ew_" << i << "_" << j << " <= 1" << endl;
                    os << "a_ew_" << i << "_" << j << " - " << "spmam_ew_" << i << "_" << j << " >= 0" << endl;
                    os << "spmm_ew_" << i << "_" << j << " - " << "spmam_ew_" << i << "_" << j << " >= 0" << endl;
                    binvar += "spmam_ew_"+*i+"_"+j+"\n";

                    os << "a_we_" << i << "_" << j << " + spmm_we_" << i << "_" << j << " - spmam_we_" << i << "_" << j << " <= 1" << endl;
                    os << "a_we_" << i << "_" << j << " - " << "spmam_we_" << i << "_" << j << " >= 0" << endl;
                    os << "spmm_we_" << i << "_" << j << " - " << "spmam_we_" << i << "_" << j << " >= 0" << endl;
                    binvar += "spmam_we_"+*i+"_"+j+"\n";
                }
            }
        }
    }
    else { // comm are blocking
        // Determine spmam_XY_i,j similar to am_XY_i,j
        //    spmam_XY_i,j = a_XY_i,j && spmm_XY_i,j 
        for(Task* i : tg.tasks()) {
            for(Task* j : tg.tasks()) {
                if(i == j) continue;

                os << "a_" << i << "_" << j << " + spmm_rr_" << i << "_" << j << " - spmam_rr_" << i << "_" << j << " <= 1" << endl;
                os << "a_" << i << "_" << j << " - " << "spmam_rr_" << i << "_" << j << " >= 0" << endl;
                os << "spmm_rr_" << i << "_" << j << " - " << "spmam_rr_" << i << "_" << j << " >= 0" << endl;
                binvar += "spmam_rr_"+*i+"_"+j+"\n";

                os << "a_" << i << "_" << j << " + spmm_ww_" << i << "_" << j << " - spmam_ww_" << i << "_" << j << " <= 1" << endl;
                os << "a_" << i << "_" << j << " - " << "spmam_ww_" << i << "_" << j << " >= 0" << endl;
                os << "spmm_ww_" << i << "_" << j << " - " << "spmam_ww_" << i << "_" << j << " >= 0" << endl;
                binvar += "spmam_ww_"+*i+"_"+j+"\n";

                os << "a_" << i << "_" << j << " + spmm_rw_" << i << "_" << j << " - spmam_rw_" << i << "_" << j << " <= 1" << endl;
                os << "a_" << i << "_" << j << " - " << "spmam_rw_" << i << "_" << j << " >= 0" << endl;
                os << "spmm_rw_" << i << "_" << j << " - " << "spmam_rw_" << i << "_" << j << " >= 0" << endl;
                binvar += "spmam_rw_"+*i+"_"+j+"\n";

                os << "a_" << i << "_" << j << " + spmm_wr_" << i << "_" << j << " - spmam_wr_" << i << "_" << j << " <= 1" << endl;
                os << "a_" << i << "_" << j << " - " << "spmam_wr_" << i << "_" << j << " >= 0" << endl;
                os << "spmm_wr_" << i << "_" << j << " - " << "spmam_wr_" << i << "_" << j << " >= 0" << endl;
                binvar += "spmam_wr_"+*i+"_"+j+"\n";

                if(!(!conf.archi.prefetch_code && conf.archi.spm.store_code)) { // the exec region handles local data only
                    os << "a_" << i << "_" << j << " + spmm_ee_" << i << "_" << j << " - spmam_ee_" << i << "_" << j << " <= 1" << endl;
                    os << "a_" << i << "_" << j << " - " << "spmam_ee_" << i << "_" << j << " >= 0" << endl;
                    os << "spmm_ee_" << i << "_" << j << " - " << "spmam_ee_" << i << "_" << j << " >= 0" << endl;
                    binvar += "spmam_ee_"+*i+"_"+j+"\n";

                    os << "a_" << i << "_" << j << " + spmm_re_" << i << "_" << j << " - spmam_re_" << i << "_" << j << " <= 1" << endl;
                    os << "a_" << i << "_" << j << " - " << "spmam_re_" << i << "_" << j << " >= 0" << endl;
                    os << "spmm_re_" << i << "_" << j << " - " << "spmam_re_" << i << "_" << j << " >= 0" << endl;
                    binvar += "spmam_re_"+*i+"_"+j+"\n";

                    os << "a_" << i << "_" << j << " + spmm_er_" << i << "_" << j << " - spmam_er_" << i << "_" << j << " <= 1" << endl;
                    os << "a_" << i << "_" << j << " - " << "spmam_er_" << i << "_" << j << " >= 0" << endl;
                    os << "spmm_er_" << i << "_" << j << " - " << "spmam_er_" << i << "_" << j << " >= 0" << endl;
                    binvar += "spmam_er_"+*i+"_"+j+"\n";

                    os << "a_" << i << "_" << j << " + spmm_ew_" << i << "_" << j << " - spmam_ew_" << i << "_" << j << " <= 1" << endl;
                    os << "a_" << i << "_" << j << " - " << "spmam_ew_" << i << "_" << j << " >= 0" << endl;
                    os << "spmm_ew_" << i << "_" << j << " - " << "spmam_ew_" << i << "_" << j << " >= 0" << endl;
                    binvar += "spmam_ew_"+*i+"_"+j+"\n";

                    os << "a_" << i << "_" << j << " + spmm_we_" << i << "_" << j << " - spmam_we_" << i << "_" << j << " <= 1" << endl;
                    os << "a_" << i << "_" << j << " - " << "spmam_we_" << i << "_" << j << " >= 0" << endl;
                    os << "spmm_we_" << i << "_" << j << " - " << "spmam_we_" << i << "_" << j << " >= 0" << endl;
                    binvar += "spmam_we_"+*i+"_"+j+"\n";
                }
            }
        }
    }
    
    for(Task *a : tg.tasks()) {
        genvar += "startresa_r_"+*a+"\n";
        genvar += "endresa_r_"+*a+"\n";
        genvar += "startresa_w_"+*a+"\n";
        genvar += "endresa_w_"+*a+"\n";
        
        os << "startresa_r_" << a << " - rho_r_" << a << " = 0" << endl;
        os << "endresa_r_" << a << " - rho_" << a << " = " << a->C << endl;
        os << "startresa_w_" << a << " - rho_" << a << " = 0" << endl;
        os << "endresa_w_" << a << " - end_" << a << " >= 0" << endl;
        
        if(!(!conf.archi.prefetch_code && conf.archi.spm.store_code)) { // the exec region handles local data only
            genvar += "startresa_e_"+*a+"\n";
            genvar += "endresa_e_"+*a+"\n";
            os << "startresa_e_" << a << " - rho_" << a << " = 0" << endl;
            os << "endresa_e_" << a << " - rho_" << a << " = " << a->C << endl;
        }
    }
    
    //if we don't force communication through external memory, endresa of write phase is the max(startresa of successors mapped on same core)
    if(conf.interconnect.mechanism != "sharedonly") {
        for(Task *a : tg.tasks()) {
            // if some successors are on the same core, then the endresa is the start resa of the successor that starts the later
            for(Task* el : a->successors) {
                os << "endresa_w_" << a << " - startresa_r_" << el << " - " << SeqTauMax << "m_" << a << "_" << el << " >= -" << SeqTauMax << endl;
            }
        }
    }
    
    for(Task* i : tg.tasks()) {
        for(Task* j : tg.tasks()) {
            if(i == j) continue;
            os << "endresa_r_" << i << " - startresa_r_" << j << " + " << SeqTauMax << " spmam_rr_" << i << "_" << j << " <= " << SeqTauMax << endl;
            os << "endresa_r_" << i << " - startresa_w_" << j << " + " << SeqTauMax << " spmam_rw_" << i << "_" << j << " <= " << SeqTauMax << endl;
            os << "endresa_w_" << i << " - startresa_r_" << j << " + " << SeqTauMax << " spmam_wr_" << i << "_" << j << " <= " << SeqTauMax << endl;
            os << "endresa_w_" << i << " - startresa_w_" << j << " + " << SeqTauMax << " spmam_ww_" << i << "_" << j << " <= " << SeqTauMax << endl;
            
            if(!(!conf.archi.prefetch_code && conf.archi.spm.store_code)) { // the exec region handles local data only
                os << "endresa_e_" << i << " - startresa_r_" << j << " + " << SeqTauMax << " spmam_er_" << i << "_" << j << " <= " << SeqTauMax << endl;
                os << "endresa_e_" << i << " - startresa_e_" << j << " + " << SeqTauMax << " spmam_ee_" << i << "_" << j << " <= " << SeqTauMax << endl;
                os << "endresa_e_" << i << " - startresa_w_" << j << " + " << SeqTauMax << " spmam_ew_" << i << "_" << j << " <= " << SeqTauMax << endl;
                os << "endresa_r_" << i << " - startresa_e_" << j << " + " << SeqTauMax << " spmam_re_" << i << "_" << j << " <= " << SeqTauMax << endl;
                os << "endresa_w_" << i << " - startresa_e_" << j << " + " << SeqTauMax << " spmam_we_" << i << "_" << j << " <= " << SeqTauMax << endl;
            }
        }
    }
}
