/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include <string>
#include <libgen.h>
#include <boost/filesystem/path.hpp>
#include <boost/program_options.hpp>

#include "config.h"
#include "Solver.h"
#include "Schedule.h"
#include "Parser.h"
#include "InputParser/par_TgXmlSax.h"
#include "InputParser/par_ResXmlSax.h"
#include "InputParser/par_CplexXmlSax.h"
#include "Generator.h"

using namespace std;
namespace po = boost::program_options;

void isxmlfile(const string &str) {
    if(str.empty())
        throw MyException("CLI opts", "missing XML file");
    if(!Utils::file_exists(str))
        throw MyException("CLI opts", "file "+str+" doesn't exist");
    if(str.substr(str.size()-4, str.size()) != ".xml")
        throw MyException("CLI opts", "file "+str+" is not an XML file");
}

/*
 * 
 */
int main(int argc, char** argv) {
    config_t conf;
    init_config(&conf);
    
    string tpcoord, tpcsv, resfile, cplexsolfile;
    
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help,h", "produce help message")
        ("config, c", po::value<string>(&conf.files.in)->required()->notifier(&isxmlfile), "Configuration file, if no option given it must be the first argument")
        ("rfile", po::value<string>(&resfile)->default_value(""), "From result file")
        ("cplexsolfile", po::value<string>(&cplexsolfile)->default_value(""), "From Cplex solution file")
    ;
    po::positional_options_description pod;
    pod.add("config", 1);
    
    po::variables_map vm;
    try {
        po::store(po::command_line_parser(argc, argv).options(desc).positional(pod).run(), vm);
        
        if (vm.count("help")) {
            cout << desc << "\n";
            return EXIT_SUCCESS;
        }
        
        po::notify(vm);
    }
    catch(po::error &e) {
        Utils::WARN("Boost error", e.what());
        cout << desc << endl;
        return EXIT_FAILURE;
    }
    catch(MyException &e) {
        Utils::WARN(e.type(), e.what());
        cout << desc << endl;
        return EXIT_FAILURE;
    }
    catch(exception &e) {
        Utils::WARN("Unknow error", e.what());
        cout << desc << endl;
        return EXIT_FAILURE;
    }
    
    string tmp = conf.files.in.substr(0, conf.files.in.size()-4); // remove .xml in normal behaviour
    conf.files.lp = tmp + ".lp";
    conf.files.dot = tmp + ".dot";
    conf.files.trdot = tmp + "-trclos.dot";
    conf.files.svg = tmp + ".svg";
    conf.files.out = tmp + "-res.xml";
    
    boost::filesystem::path p(conf.files.in);
    conf.files.path = p.parent_path().c_str();
    conf.files.filename = p.leaf().c_str();
    
    try {
        SystemModel tg;
        Schedule_t result;
        
        // get task-graph and config from related Xml task-graph
        ParserRegistry::Create(".xml", &tg, &conf)->parse_file(conf.files.in);
        
        // we just want to rebuild Output from a previously created results file, not running the solver
        if((!resfile.empty() && Utils::file_exists(resfile))) {
            ParserRegistry::Create("-res.xml", &result, &conf)->parse_file(resfile);
        }
        else if((!cplexsolfile.empty() && Utils::file_exists(cplexsolfile))) {
            ParserRegistry::Create(".lp.sol", &result, &conf)->parse_file(cplexsolfile);
        }
        else {
            check_todo(conf);
            
            if(conf.solving.solve) {
                Solver *solver = SolverRegistry::Create(conf.solving.solver, tg, conf);
                solver->solve(&result);
                free(solver);
            }
        }
        
        for(string gen : conf.outputs) {
            Generator *g = GeneratorRegistry::Create(gen, tg, conf, result);
            g->generate();
            free(g);
        }
    }
    catch(MyException &e) {
        Utils::WARN(e.type(), e.what());
        return EXIT_FAILURE;
    }
    catch(Unschedulable &e) {
        Utils::WARN("Un-Schedulable", e.what());
        return EXIT_FAILURE;
    }
    catch(Todo &e) {
        Utils::WARN("Schedule configuration is not ready yead (@TODO)", e.what());
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

