/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPLITFLSQREADYBUILDER_H
#define SPLITFLSQREADYBUILDER_H

#include "FLSFragmented.h"


class FLSFragQreadyBuilder {
protected:
    const SystemModel &tg;
    const config_t &conf;
    
    FLSFragStrategy *strategy;
    
    void build_packet_list_noburst(SchedEltTask *schedtask);
    void build_packet_list_burst(SchedEltTask *schedtask, map<Task*, SchedElt*> *pool);
    void build_successors_noburst(SchedEltTask *, map<Task*, SchedElt*> *pool);
public:
    typedef std::function<void (vector<SchedElt*>*, vector<SchedElt*>&, map<Task*, SchedElt*>*)> func_graph_walk_through_t;
    
    static void buildListDFS(const SystemModel &m, const config_t &c, FLSFragStrategy *s, vector<SchedElt*> *schedorder);
    static void buildListDFSWithRDelay(const SystemModel &m, const config_t &c, FLSFragStrategy *s, vector<SchedElt*> *schedorder);
    static void buildListBFS(const SystemModel &m, const config_t &c, FLSFragStrategy *s, vector<SchedElt*> *schedorder);
    
    static bool sortFun(SchedElt *a, SchedElt *b);
    
    virtual void buildList(vector<SchedElt*> *schedorder, FLSFragQreadyBuilder::func_graph_walk_through_t fun_walk_through);
    
    virtual void get_next_readyDFS(vector<SchedElt*> &Qdone, SchedElt *current, vector<SchedElt*> *Qready, map<Task*, SchedElt*> *pool);
    virtual void build_QreadyDFS(vector<SchedElt*> *Qready, vector<SchedElt*> &unused_compat_fun_walk_through, map<Task*, SchedElt*> *pool);
    
    virtual void get_next_readyBFS(vector<SchedElt*> &Qdone, SchedElt *current, vector<SchedElt*> *Qready, map<Task*, SchedElt*> *pool);
    virtual void build_QreadyBFS(vector<SchedElt*> *Qready, vector<SchedElt*> &Qdone, map<Task*, SchedElt*> *pool);
    
    virtual void get_next_readyDFSwithreaddelay(vector<SchedElt*> &Qdone, vector<SchedElt*> &current, vector<SchedElt*> *Qready, map<Task*, SchedElt*> *pool);
    virtual void build_QreadyDFSwithreaddelay(vector<SchedElt*> *Qready, vector<SchedElt*> &unused_compat_fun_walk_through, map<Task*, SchedElt*> *pool);
    
private:
    explicit FLSFragQreadyBuilder(const SystemModel &m, const config_t &c, FLSFragStrategy *s) : tg(m), conf(c), strategy(s) {
    };
};

#endif /* SPLITFLSQREADYBUILDER_H */

