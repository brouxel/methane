/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "SystemModel.h"
#include "Utils.h"

using namespace std;

size_t Task::task_counter = 0;

/******************************************************************************/

ostream& operator<< (ostream& os, const Task &a) { return os << a.ilp_label; }
ostream& operator<< (ostream& os, const Task *a) { return os << a->ilp_label; }
string operator+ (const string &a, const Task &b) { return a+ b.ilp_label; }
string operator+ (const Task &b, const string &a) { return b.ilp_label + a; }
string operator+ (const string &a, const Task *b) { return a+ b->ilp_label; }
string operator+ (const Task *b, const string &a) { return b->ilp_label + a; }
string operator+ (const char* a, const Task &b) { return a+ b.ilp_label; }
string operator+ (const Task &b, const char* a) { return b.ilp_label + a; }

bool operator== (const Task *a, const Task &b) { return a->id == b.id; }
bool operator== (const Task &a, const Task *b) { return a.id == b->id; }
bool operator== (const Task &a, const Task &b) { return a.id == b.id; }

bool operator!= (const Task *a, const Task &b) { return a->id != b.id; }
bool operator!= (const Task &a, const Task *b) { return a.id != b->id; }
bool operator!= (const Task &a, const Task &b) { return a.id != b.id; }

//------------------------------------------------------------------------------

ostream& operator<< (ostream& os, const meth_processor_t& a) { return os << a.id; }
ostream& operator<< (ostream& os, const meth_processor_t* a) { return os << a->id; }
string operator+ (const string &a, const meth_processor_t &b) { return a+ b.id; }
string operator+ (const meth_processor_t &b, const string &a) { return b.id + a; }
string operator+ (const string &a, const meth_processor_t *b) { return a+ b->id; }
string operator+ (const meth_processor_t *b, const string &a) { return b->id + a; }
string operator+ (const char* a, const meth_processor_t &b) { return a+ b.id; }
string operator+ (const meth_processor_t &b, const char* a) { return b.id + a;}

bool operator== (const meth_processor_t *a, const meth_processor_t &b) { return a->id == b.id; }
bool operator== (const meth_processor_t &a, const meth_processor_t *b) { return a.id == b->id; }
bool operator== (const meth_processor_t &a, const meth_processor_t &b) { return a.id == b.id; }

bool operator!= (const meth_processor_t *a, const meth_processor_t &b) { return a->id != b.id; }
bool operator!= (const meth_processor_t &a, const meth_processor_t*b) { return a.id != b->id; }
bool operator!= (const meth_processor_t &a, const meth_processor_t &b) { return a.id != b.id; }

/******************************************************************************/

#include <cstdio>
#include <cstdlib>
#include <iostream>

// do not clone the transitiv closure, it must be recomputed
void Task::clone(const Task &rhs) {
    id = rhs.id;
    C = rhs.C;
    D = rhs.D;
    T = rhs.T;
    ilp_label = rhs.ilp_label;
    _data_read = rhs._data_read;
    _data_written = rhs._data_written;
    memory_footprint = rhs.memory_footprint;
    local_memory_storage = rhs.local_memory_storage;
    memory_code_size = rhs.memory_code_size;
    
    force_mapping_proc = rhs.force_mapping_proc;
    force_sched_order_proc = rhs.force_sched_order_proc;
    force_read_delay = rhs.force_read_delay;
    force_write_delay = rhs.force_write_delay;

    for(Task::prev_t el : rhs.previous)
        previous.insert({el.first, el.second});
    
    for(Task *el : rhs.previous_transitiv)
        previous_transitiv.push_back(const_cast<Task*>(el));
    
    for(Task *el : rhs.successors)
        successors.push_back(el);
        
    for(pair<Task*, string> el : rhs.previous_datatypes)
        previous_datatypes[el.first] = el.second;
        
    is_job = rhs.is_job;
    meta_task = rhs.meta_task;
}

bool Task::is_parallel(const Task *b) const {
    if(this == b)
        return false;

    for(Task *ta : previous_transitiv) {
        if(ta == b)
            return false;
    }

    for(Task *tb : b->previous_transitiv) {
        if(tb == this)
            return false;
    }

    return true;
}

Task::Task()  {
    ilp_label = "lbl"+to_string(++task_counter);
    local_memory_storage = 0;
    memory_footprint = 0;
    _data_read = 0;
    _data_written = 0;
    memory_code_size = 0;
}

void Task::add_previous(Task* prev, uint32_t nbtoks, std::string datatype) {
    if(previous.count(prev)) return; // sometimes with TGFF we have several times the same "prev" tag

    previous[prev] = nbtoks;
    prev->successors.push_back(this);
    previous_datatypes[prev] = datatype;
}

uint32_t Task::data_read(const Task& from) const {
    for(Task::prev_t el : previous) {
        if(el.first == from) {
            return el.second;
        }
    }
    return 0;
}

uint32_t Task::data_written(const Task& to) const {
    return to.data_read(*this);
}

SystemModel::SystemModel()  {
}

SystemModel::~SystemModel() {
    for(Task *t : _tasks)
        delete t;
    _tasks.clear();

    for(meth_processor_t *p : _processors)
        delete p;
    _processors.clear();
    
    delete _interconnect;
}
Task* SystemModel::task(const string &id) const {
    for(Task * i : _tasks) {
        if(i->id == id)
            return i;
    }
    return nullptr;
}

void SystemModel::clean_previous() {
    for(Task *it : _tasks) {
        vector<Task*> to_delete;
        for(auto ip = it->previous.begin(), ep = it->previous.end() ; ip != ep ; ++ip) {
            if(ip->first == nullptr)
                to_delete.push_back(ip->first);
        }

        for(auto id : to_delete) {
            it->previous.erase(id);
        }
    }
}

void SystemModel::compute_transitive_closure() {
    for(Task *t : _tasks) {
        t->previous_transitiv.clear();
    }
    vector<Task*> explored;
    for(Task *t : _tasks) {
        if(t->successors.size() > 0) continue;
        
        explored.push_back(t);
        compute_transitive_closure_aux(t, &explored);
    }
    
    for(Task *t : _tasks) {
        sort(t->previous_transitiv.begin(), t->previous_transitiv.end(), [](Task *a, Task *b) {
            return a->id < b->id;
        });
        vector<Task*>::iterator it = unique(t->previous_transitiv.begin(), t->previous_transitiv.end());
        t->previous_transitiv.resize(distance(t->previous_transitiv.begin(), it));
        
        sort(t->successors_transitiv.begin(), t->successors_transitiv.end(), [](Task *a, Task *b) {
            return a->id < b->id;
        });
        it = unique(t->successors_transitiv.begin(), t->successors_transitiv.end());
        t->successors_transitiv.resize(distance(t->successors_transitiv.begin(), it));
    }
}

void SystemModel::compute_transitive_closure_aux(Task *tc, vector<Task*> *explored) {
    for(Task::prev_t tmp : tc->previous) {
        tc->previous_transitiv.push_back(tmp.first);
        tmp.first->successors_transitiv.push_back(tc);
        
        if(find(explored->begin(),explored->end(), tmp.first) == explored->end()) {
            explored->push_back(tmp.first);
            compute_transitive_closure_aux(tmp.first, explored);
        }
        
        tc->previous_transitiv.insert(tc->previous_transitiv.end(), tmp.first->previous_transitiv.begin(), tmp.first->previous_transitiv.end());
        for_each(tmp.first->previous_transitiv.begin(), tmp.first->previous_transitiv.end(), [tc](Task *a) {
            a->successors_transitiv.push_back(tc);
        });
        
        sort(tc->previous_transitiv.begin(), tc->previous_transitiv.end(), [](Task *a, Task *b) {
            return a->id < b->id;
        });
        vector<Task*>::iterator it = unique(tc->previous_transitiv.begin(), tc->previous_transitiv.end());
        tc->previous_transitiv.resize(distance(tc->previous_transitiv.begin(), it));
    }
}

void SystemModel::compute_sequential_schedule_length() {
    _global_deadline = 0;
    for (Task* i : _tasks) {
        _global_deadline += i->C;
        if(i->previous.size() > 0)
            _global_deadline += _interconnect->comm_delay(processors().size()-1, i->data_read(), "double", 0);
        if(i->successors.size() > 0)
            _global_deadline += _interconnect->comm_delay(processors().size()-1, i->data_written(), "double", 0);
    }
}

void SystemModel::populate_memory_usage() {
    for(Task *t : _tasks) {
        t->_data_read = 0;
        for(Task::prev_t el : t->previous) {
            t->_data_read += el.second;
        }
        t->_data_written = 0;
        for(Task *succ : t->successors) {
            t->_data_written += t->data_written(*succ);
        }
        
        t->memory_footprint = t->_data_read + t->local_memory_storage + t->memory_code_size + t->_data_written;
    }
}

void SystemModel::build_memory_model() {
    uint32_t cnt = 0;
    for(Task *t : _tasks) {
        // one for each phase
        _memory.push_back("MR"+to_string(cnt++));
        _memory.push_back("MR"+to_string(cnt++));
        _memory.push_back("MR"+to_string(cnt++));
        
        if(t->memory_code_size == 0)
            t->memory_code_size = t->C;
    }
}

void SystemModel::expand_periodic_model() {
    _is_periodic_model = true;
    vector<uint64_t> periods;
    for(Task *t : _tasks) {
        _is_periodic_model = _is_periodic_model && t->D > 0 && t->T > 0;
        periods.push_back(t->T);
    }
    if(!_is_periodic_model)
        return;
    
    //compute hyperperiod
    _hyperperiod = Utils::lcm(periods);
    //compute number of job firing
    //expand task to job
        //assign absolute activation/deadline
        //add meta task to job
    map<Task*, vector<Task*>> jobs;
    for(Task* t : _tasks) {
        for(uint32_t i=_hyperperiod/t->T; i > 0 ; --i) {
            Task *j = new Task(*t);
            j->id += "_j"+to_string(i);
            j->ilp_label += "_j"+to_string(i);
            j->previous.clear();
            //j->previous_transitiv.clear(); // should not be filled at the moment, check SAXParser::on_end_document
            //j->successors.clear();
            //j->successors_transitiv.clear();
            j->is_job = true;
            j->T = (i-1)*t->T;
            j->D = j->T+t->D;
            j->meta_task = t;
            jobs[t].push_back(j);
        }
    }
    //replace task by jobs in the graph
    //handle dependencies
    _tasks.clear();
    for(pair<Task*, vector<Task*>> el : jobs) {
        for(Task *j : el.second) {
            _tasks.push_back(j);
            for(Task::prev_t predel : el.first->previous) {
                for(Task *predj : jobs[predel.first]) {
                    j->add_previous(predj, predel.second, el.first->previous_datatypes[predj]);
                }
            }
        }
    }
}