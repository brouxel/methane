/*
    Copyright (C) 2018  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CoreMemInterConnect.h"

class NoComm : public CoreMemInterConnect {
public:
    NoComm(const SystemModel& t, const config_t& c) : CoreMemInterConnect(t, c) {}

    virtual uint32_t comm_delay(uint32_t concurrency, uint32_t data, string datatype, uint64_t forced_delay) const override {return 0; }
    virtual uint32_t wait_time(uint32_t concurrency, uint32_t data) const override {return 0; }
    
    virtual uint32_t getActiveWindowTime() override { return 0; }
};

REGISTER_INTERCONNECT(NoComm, "NONE");