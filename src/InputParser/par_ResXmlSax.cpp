/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "InputParser/par_ResXmlSax.h"

using namespace std;

ResXmlSaxParser::~ResXmlSaxParser() {

}

void ResXmlSaxParser::on_start_element(const Glib::ustring& name, const AttributeList& properties) {
    if(name == "schedule") {
        handle_schedule(properties);
    }
    else if(name == "task") {
        handle_task(properties);
    }
    else if(name == "transmission") {
        handle_transmission(properties);
    }
    else if(name == "processor") {
        handle_processor(properties);
    }
    else if(name == "region") {
        handle_region(properties);
    }
    else
        throw MyException("xml", "Unknow node: "+name);
}

void ResXmlSaxParser::on_end_element(const Glib::ustring& name) {
    if(name == "processor") {
        procs[current_proc->id] = current_proc;
    }
}

void ResXmlSaxParser::on_end_document() {
    int cnt = 0;
    for(map<string, task_schedule_info>::iterator it = out->tasks.begin(), et = out->tasks.end() ; it != et ; ++it) {
        task_schedule_info &info = it->second;
        string taskid = it->first;
        meth_processor_t *proc = procs[map_task_proc[taskid]];
        info.proc = proc;
        
        vector<string> allocs(3);
        boost::split(allocs, mem_alloc[taskid], boost::is_any_of(" -- "));
        for(string alloc : allocs) {
            vector<string> regions;
            boost::trim(alloc);
            if(alloc.size() <= 2)
                continue; // no region allocated, no data to store then
            
            string tmp = alloc.substr(3, alloc.size()-5); // end with a 3+",{"
            boost::split(regions, tmp, boost::is_any_of(",")); // remove r:{ e:{ or w:{
            for(string reg : regions) {
                boost::trim(reg);
                if(!out->procs.count(proc->id)) continue; // Due to a timeout, res file might be incomplete
                
                for(spm_region_info spminfo : out->procs[proc->id].memory_regions) {
                    if(spminfo.label == reg) {
                        uint64_t begin=-1, end=0;
                        
                        if(begin < spminfo.startresa)
                            spminfo.startresa = begin;
                        if(end > spminfo.endresa)
                            spminfo.endresa = end;
                        
                        if(alloc[0] == 'r') {
                            begin = info.rtR();
                            end = info.rtE+info.WCET;
                            // should be the task id of the task that created the region, but we lost this information at that point
                            info.memory_region.read["uniq"+to_string(cnt++)] = spminfo;
                        }
                        else if(alloc[0] == 'e') { 
                            // depends on the communication model chosen, but lost here
                            begin = info.rtE;
                            end = info.rtE+info.WCET;
                            info.memory_region.exec = spminfo;
                        }
                        else if(alloc[0] == 'w') {
                            begin = info.rtE;
                            end = info.end();
                            // should be the task id of the task that created the region, but we lost this information at that point
                            info.memory_region.write["uniq"+to_string(cnt++)] = spminfo;
                        }
                        
                        break;
                    }
                }
            }
        }
        
    }
}

void ResXmlSaxParser::on_error(const Glib::ustring& text) {
    throw MyException("xml", text);
}

void ResXmlSaxParser::on_fatal_error(const Glib::ustring& text) {
    throw MyException("xml", text);
}

void ResXmlSaxParser::handle_schedule(const AttributeList& properties) {
    for(Attribute attr : properties) {
        if(attr.name == "time") 
            out->solvetime = stof(attr.value);
        else if(attr.name == "status")
            out->status = stoi(attr.value);
        else if(attr.name == "length") {
            out->makespan = stod(attr.value);
        }
        else if(attr.name == "heur-DFSDelay")
            out->heur_DFSDelay = stod(attr.value);
        else if(attr.name == "heur-DFS")
            out->heur_DFS = stod(attr.value);
        else if(attr.name == "heur-BFS")
            out->heur_BFS = stod(attr.value);
    }
}

void ResXmlSaxParser::handle_task(const AttributeList& properties) {
    task_schedule_info info;
    string core = "", memory = "";
    
    current_taskid="";
        
    for(Attribute attr : properties) {
        if(attr.name == "id")
            current_taskid = attr.value;
        else if(attr.name == "WCET")
            info.WCET = stoul(attr.value);
        else if(attr.name == "release")
            info.rtE = stoul(attr.value);
        else if(attr.name == "core")
            core = attr.value;
        else if(attr.name == "memory_region")
            memory = attr.value;
    }
    
    if(current_taskid.empty())
        throw MyException("xml parser", "Missing task id");
    
    out->tasks[current_taskid] = info;
    map_task_proc[current_taskid] = core;
    mem_alloc[current_taskid] = memory;
}

void ResXmlSaxParser::handle_processor(const AttributeList& properties) {
    current_proc = new meth_processor_t();
    if(current_proc == NULL)
        throw MyException("xml", "no more mem proc_t");
    current_proc->id = properties.at(0).value;
    current_proc->spm_size = 0;
    
    proc_schedule_info info;
    info.available_spm_size = 0;
    info.total_spm_size = 0;
    out->procs[current_proc->id] = info;
}

void ResXmlSaxParser::handle_region(const AttributeList& properties) {
    spm_region_info info;
    
    for(Attribute attr : properties) {
        if(attr.name == "id")
            info.label = attr.value;
        else if(attr.name ==  "size") {
            // temporary patch as previous version of view.cpp didn't use the Utils::uint_to_size
            if(attr.value.find("bits") != string::npos)
                attr.value = attr.value.substr(0, attr.value.length()-3);
            //--
            string tmp = attr.value;
            boost::trim(tmp);
            info.size = tmp.empty() ? 0 : Utils::size_to_uint(tmp);
        }
    }
    out->procs[current_proc->id].memory_regions.push_back(info);
    out->procs[current_proc->id].total_spm_size += info.size;
}

void ResXmlSaxParser::handle_transmission(const AttributeList& properties) {
    transmission_phase_info tr;
    string related_taskid = "";
    for(Attribute attr : properties) {
        if(attr.name == "type")
            tr.type = attr.value;
        else if(attr.name == "release")
            tr.rt = stoul(attr.value);
        else if(attr.name == "delay")
            tr.delay = stoul(attr.value);
        else if(attr.name == "task")
            related_taskid = attr.value;
        else if(attr.name == "data")
            tr.data = stoul(attr.value);
        else if(attr.name == "datatype")
            tr.datatype = attr.value;
        else if(attr.name == "conc")
            tr.conc = stoul(attr.value);
    }
    if(tr.type == "read")
        out->tasks[current_taskid].reads[related_taskid].push_back(tr);
    else if(tr.type == "write")
        out->tasks[current_taskid].writes[related_taskid].push_back(tr);
    else 
        throw MyException("xml parser", "wrong tranmission type");
}