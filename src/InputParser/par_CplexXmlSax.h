/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef XMLCPLEXSAXPARSER_H
#define XMLCPLEXSAXPARSER_H

#include <cstdlib>
#include <cstdio>
#include <string>
#include <iostream>
#include <map>
#include <utility>

#include <libxml++/libxml++.h>
#include <boost/algorithm/string.hpp>

#include "Parser.h"
#include "SystemModel.h"
#include "Schedule.h"
#include "Utils.h"
#include "ILP.h"

class CplexXmlSaxParser;
class FakeCplexSolver : public ILPSolver {
    friend class CplexXmlSaxParser;
protected:
    virtual void run(Schedule_t *result) override {};
public:
    FakeCplexSolver(const SystemModel& tg, const config_t& conf) : ILPSolver(tg, conf) {}
    void pastevalues(const map<string, long long> &tmp) {
        tmp_results = tmp;
    }
}; 

class CplexXmlSaxParser : public Parser<Schedule_t>, public xmlpp::SaxParser {
protected:
    map<string, long long> tmp_results;
public:
    CplexXmlSaxParser() = delete;
    CplexXmlSaxParser(const CplexXmlSaxParser&) = delete;
    
    explicit CplexXmlSaxParser(void *s, config_t *c) : Parser<Schedule_t>((Schedule_t*)s, c), SaxParser()  {}
    
    ~CplexXmlSaxParser() override;
    
    void parse_file(const string &file) {
        xmlpp::SaxParser::parse_file(file);
    }
    
protected:
    //overrides:
    //void on_start_document() override;
    void on_end_document() override;
    void on_start_element(const Glib::ustring& name, const AttributeList& properties) override;
    //void on_end_element(const Glib::ustring& name) override;
    //  void on_characters(const Glib::ustring& characters) override;
    //  void on_comment(const Glib::ustring& text) override;
    //  void on_warning(const Glib::ustring& text) override;
    void on_error(const Glib::ustring& text) override;
    void on_fatal_error(const Glib::ustring& text) override;
    
private:
    void handle_schedule(const AttributeList& properties); 
    void handle_variable(const AttributeList& properties);
};

REGISTER_PARSER(CplexXmlSaxParser, ".lp.sol")

#endif /* XMLCPLEXSAXPARSER_H */

