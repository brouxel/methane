/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <regex>

#include "InputParser/par_CplexXmlSax.h"

using namespace std;

CplexXmlSaxParser::~CplexXmlSaxParser() {

}

void CplexXmlSaxParser::on_start_element(const Glib::ustring& name, const AttributeList& properties) {
    if(name == "header") {
        handle_schedule(properties);
    }
    else if(name == "variable") {
        handle_variable(properties);
    }
}

void CplexXmlSaxParser::on_end_document() {
    SystemModel tg;
    regex r("^p_(.*)_(P[0-9]+)$");
    set<string> tasks, processors;
    for(pair<string, long long> el : tmp_results) {
        smatch m;
        if(regex_search(el.first, m, r)) {
            tasks.insert(*(m.begin()+1));
            processors.insert(*(m.begin()+2));
        }
    }
    for(string s : tasks) {
        Task *t = new Task();
        t->id = s;
#ifdef _DEBUG
    t->ilp_label = s;
#endif
        tg.tasks().push_back(t);
    }
    for(string s : processors) {
        meth_processor_t *p = new meth_processor_t();
        p->id = s;
        tg.processors().push_back(p);
    }
    
    FakeCplexSolver solver(tg, const_cast<config_t &>(*conf));
    solver.pastevalues(tmp_results);
    solver.postsolve(out);
}

void CplexXmlSaxParser::on_error(const Glib::ustring& text) {
    throw MyException("xml", text);
}

void CplexXmlSaxParser::on_fatal_error(const Glib::ustring& text) {
    throw MyException("xml", text);
}

void CplexXmlSaxParser::handle_schedule(const AttributeList& properties) {
    for(Attribute attr : properties) {
        if(attr.name == "time") 
            out->solvetime = stof(attr.value);
        else if(attr.name == "solutionStatusValue")
            out->status = attr.value == "127" ? 2 : 1; // it is always 127 ???
        else if(attr.name == "objectiveValue") {
            out->makespan = stod(attr.value);
        }
    }
}

void CplexXmlSaxParser::handle_variable(const AttributeList& properties) {
    string var, val;
    for(Attribute attr : properties) {
        if(attr.name == "name")
            var = attr.value;
        else if(attr.name == "value")
            val = attr.value;
    }
    tmp_results[var] = stoul(val);
}

