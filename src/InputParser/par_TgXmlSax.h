/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SAXPARSER_H
#define SAXPARSER_H

#include <cstdlib>
#include <cstdio>
#include <string>
#include <iostream>

#include <libxml++/libxml++.h>

#include "Parser.h"
#include "SystemModel.h"
#include "CoreMemInterConnect.h"
#include "Utils.h"

using namespace std;

class TgXmlSaxParser : public Parser<SystemModel>, public xmlpp::SaxParser {
    Task *current_task = NULL;
    meth_processor_t *current_proc = NULL;
    
    // task is preceded by the list of string/task id, exchanging uint32_t/data, of string/data-type 
    map<Task*, vector<tuple<string, uint32_t, string>>> tmp_precedence;
    
    //deadline global temporaire, to handle percentage of sequential execution
    string tmp_global_deadline = "";
public:
    TgXmlSaxParser() = delete;
    TgXmlSaxParser(const TgXmlSaxParser&) = delete;
    
    explicit TgXmlSaxParser(void *t, config_t *c) : Parser<SystemModel>((SystemModel*)t, c), SaxParser() {}
    
    ~TgXmlSaxParser() override;
    
    void parse_file(const string &file) {
        xmlpp::SaxParser::parse_file(file);
    }

protected:
    //overrides:
    void on_start_document() override;
    void on_end_document() override;
    void on_start_element(const Glib::ustring& name, const AttributeList& properties) override;
    void on_end_element(const Glib::ustring& name) override;
    //  void on_characters(const Glib::ustring& characters) override;
    //  void on_comment(const Glib::ustring& text) override;
    //  void on_warning(const Glib::ustring& text) override;
    void on_error(const Glib::ustring& text) override;
    void on_fatal_error(const Glib::ustring& text) override;
    
private:
    void handle_config(const AttributeList& properties);
    void handle_config_view(const AttributeList& properties);
    void handle_config_solve(const AttributeList& properties);
    void handle_config_codgen(const AttributeList& properties);
    void handle_config_mem_datatype(const AttributeList& properties);
    void handle_config_architecture(const AttributeList& properties);
    void handle_tasks_global_properties(const AttributeList& properties);
    
    void handle_resource(const AttributeList& properties);
    void handle_task_resource(const AttributeList& properties);
    void handle_memfootprint_task(const AttributeList& properties);
    void handle_task(const AttributeList& properties);
    void handle_communication(const AttributeList& properties);
    void handle_processor(const AttributeList& properties);
    void handle_precedence(const AttributeList& properties);
    
    void propagate_precedence();
    void propagate_proc_distance();
    void propagate_global_deadline();
    
    void checktypes();
};

REGISTER_PARSER(TgXmlSaxParser, ".xml")

#endif /* SAXPARSER_H */

