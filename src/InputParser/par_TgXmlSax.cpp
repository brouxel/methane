/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "InputParser/par_TgXmlSax.h"

TgXmlSaxParser::~TgXmlSaxParser() {
}

void TgXmlSaxParser::on_start_document() {
    this->set_throw_messages(false);
}

void TgXmlSaxParser::on_end_document() {
    out->_interconnect = InterConnectRegistry::Create(conf->interconnect.type, const_cast<SystemModel&>(*out), const_cast<config_t&>(*conf));
    
    checktypes();
    
    propagate_precedence();

    out->clean_previous();
    out->populate_memory_usage();
    out->build_memory_model();
    
    out->expand_periodic_model();
    
    out->compute_transitive_closure();
    
    propagate_global_deadline();
}

void TgXmlSaxParser::on_start_element(const Glib::ustring& name, const AttributeList& properties) {
    if(name == "config")
        handle_config(properties);
    else if(name == "view")
        handle_config_view(properties);
    else if(name == "solving") {
        handle_config_solve(properties);
    }
    else if(name == "codegen") {
        handle_config_codgen(properties);
    }
    else if(name == "data-type") {
        handle_config_mem_datatype(properties);
    }
    else if(name == "processor") {
        handle_processor(properties);
    }
    else if(name == "communication") {
        handle_communication(properties);
    }
    else if(name == "task") {
        handle_task(properties);
    }
    else if(name == "prev") {
        handle_precedence(properties);
    }
    else if(name == "memfootprint") {
        handle_memfootprint_task(properties);
    }
    else if(name == "architecture" || name == "spm")
        handle_config_architecture(properties);
    else if(name == "tasks") 
        handle_tasks_global_properties(properties);
    else if(name != "appl" && name != "processors" && name != "data-types" && name != "memory")
        throw MyException("xml", "Unknow node: "+name);
    
    if(name == "memory")
        Utils::WARN("Deprecated tag \"memory\", use \"data-types\" instead");
}

void TgXmlSaxParser::on_end_element(const Glib::ustring& name) {
    if(name == "task")
        current_task = NULL;
    if(name == "processor")
        current_proc = NULL;
}

void TgXmlSaxParser::on_error(const Glib::ustring& text) {
    throw MyException("xml", text);
}

void TgXmlSaxParser::on_fatal_error(const Glib::ustring& text) {
    throw MyException("xml", text);
}

void TgXmlSaxParser::handle_config(const AttributeList& properties) {
    for(Attribute attr : properties) {
        if(attr.name == "clean")
            conf->clean = (attr.value == "true");
    }
}

void TgXmlSaxParser::handle_config_solve(const AttributeList& properties) {
    for(Attribute attr : properties) {
        if(attr.name == "solve")
            conf->solving.solve = (attr.value == "true");
        else if(attr.name == "timeout")
            conf->solving.timeout = stoi(attr.value);
        else if(attr.name == "type")
            conf->solving.type = attr.value;
        else if(attr.name == "solver")
            conf->solving.solver = attr.value;
        else if(attr.name == "optimal")
            conf->solving.optimal = (attr.value == "true" || attr.value == "1");
        else if(attr.name == "cores")
            conf->solving.cores = stoi(attr.value);
    }
}

void TgXmlSaxParser::handle_config_mem_datatype(const AttributeList& properties) {
    string name, sval;
    for(Attribute attr : properties) {
        if(attr.name == "name")
            name = attr.value;
        else if(attr.name == "size")
            sval = attr.value;
    }
    if(name.empty() || sval.empty()) {
        Utils::WARN("missing config data-type attribute name or size"); 
        return;
    }
    conf->datatypes.size_bits[name] = Utils::size_to_uint(sval);
}

void TgXmlSaxParser::handle_config_view(const AttributeList& properties) {
    for(Attribute attr : properties) {
        if(attr.name == "svg" && attr.value == "true")
            conf->outputs.insert("svg");
        else if(attr.name == "dot" && attr.value == "true")
            conf->outputs.insert("dot");
        else if(attr.name == "trdot" && attr.value == "true")
            conf->outputs.insert("trdot");
        else if(attr.name == "resxml" && attr.value == "false") {
            conf->outputs.erase(conf->outputs.find("resxml"));
        }
        else if(attr.name == "zoomx")
            conf->view.zoomX = stoi(attr.value);
        else if(attr.name == "zoomy")
            conf->view.zoomY = stoi(attr.value);
        else if(attr.name == "step")
            conf->view.step = stoi(attr.value);
    }
}

void TgXmlSaxParser::handle_config_architecture(const AttributeList& properties) {
    for(Attribute attr : properties) {
        if(attr.name == "prefetch_code")
            conf->archi.prefetch_code = (attr.value == "true");
        else if(attr.name == "assign_region")
            conf->archi.spm.assign_region = (attr.value == "true");
        else if(attr.name == "store_code" || attr.name == "store-code")
            conf->archi.spm.store_code = (attr.value == "true");
    }
}

void TgXmlSaxParser::handle_config_codgen(const AttributeList& properties) {
    bool generate = false;
    for(Attribute attr : properties) {
        if(attr.name == "generate")
            generate = (attr.value == "true");
        else if(attr.name == "machine")
            conf->codegen.machine = attr.value;
        else if(attr.name == "maxiter")
            conf->codegen.maxiter = stoi(attr.value);
        else if(attr.name == "with-delay-printf")
            conf->codegen.with_delay_printf = (attr.value == "true");
        else if(attr.name == "safety")
            conf->codegen.safety = stof(attr.value);
    }
    if(generate)
        conf->outputs.insert(conf->codegen.machine);
}

void TgXmlSaxParser::handle_tasks_global_properties(const AttributeList& properties) {
    for(Attribute attr : properties) {
        if(attr.name == "deadline") {
            tmp_global_deadline = attr.value;
        }
    }
}

void TgXmlSaxParser::handle_task(const AttributeList& properties) {
    string id, duree, deadline, period, proc = "", read, write;
    int order = -1;
    for(Attribute attr : properties) {
        if(attr.name == "id")
            id = attr.value;
        else if(attr.name == "WCET")
            duree = attr.value;
        else if(attr.name == "period")
            period = attr.value;
        else if(attr.name == "deadline")
            deadline = attr.value;
        else if(attr.name == "processor")
            proc = attr.value;
        else if(attr.name == "order")
            order = stoul(attr.value);
        else if(attr.name == "readDelay" && !attr.value.empty())
            read = attr.value;
        else if(attr.name == "writeDelay" && !attr.value.empty())
            write = attr.value;
    }
    
    if(id.empty() || duree.empty())
        throw MyException("xml", "Malformed XML : missing id or duree or ressid in task");
        
    Task* t = new Task();
    if(t == NULL)
        throw MyException("xml", "no more mem proc_t");
    t->id = id;
#ifdef _DEBUG
    t->ilp_label = id;
#endif
    t->C = stoull(duree);
    t->T = (period.empty()) ? DEFAULT_TASK_PERIOD : stoi(period);
    t->D = (deadline.empty()) ? t->T : stoi(deadline);
    t->force_mapping_proc = proc;
    t->force_sched_order_proc = order;
    if(!read.empty())
        t->force_read_delay = stoull(read);
    if(!write.empty())
        t->force_write_delay = stoull(write);
    current_task = t;
    
    out->tasks().push_back(t);
}

void TgXmlSaxParser::handle_memfootprint_task(const AttributeList& properties) {
    if(current_task == NULL)
        throw MyException("xml", "Mal formed XML : memfootprint found ouside a task");
    
    for(Attribute attr : properties) {
        if(attr.name == "local-storage")
            current_task->local_memory_storage = Utils::size_to_uint(attr.value);
        else if(attr.name == "code-size")
            current_task->memory_code_size = Utils::size_to_uint(attr.value);
    }
}

void TgXmlSaxParser::handle_communication(const AttributeList& properties) {
    for(Attribute attr : properties) {
        if(attr.name == "type")
            conf->interconnect.type = attr.value;
        else if(attr.name == "arbiter")
            conf->interconnect.arbiter = attr.value;
        else if(attr.name == "active-window-time" && !attr.value.empty())
            conf->interconnect.active_ress_time = stoul(attr.value);
        else if(attr.name == "bit-per-timeunit" && !attr.value.empty())
            conf->interconnect.bit_per_timeunit = stoul(attr.value);
        else if(attr.name == "mechanism")
            conf->interconnect.mechanism = attr.value;
        else if(attr.name == "behavior")
            conf->interconnect.behavior = attr.value;
        else if(attr.name == "burst")
            conf->interconnect.burst = attr.value;
    }
}

void TgXmlSaxParser::handle_processor(const AttributeList& properties) {
    string id;
    uint64_t mem_size = -1;
    for(Attribute attr : properties) {
        if(attr.name == "id")
            id = attr.value;
        else if(attr.name == "spm-size")
            mem_size = Utils::size_to_uint(attr.value);
    }
    if(id.empty())
        throw MyException("xml", "Bad parameter number for processor, missing id");
    
    meth_processor_t *p = new meth_processor_t();
    if(p == NULL)
        throw MyException("xml", "no more mem proc_t");
    
    p->id = id;
    p->spm_size = mem_size;
    out->processors().push_back(p);
    current_proc = p;
}

void TgXmlSaxParser::handle_precedence(const AttributeList& properties) {
    string to, data, type;
    for(Attribute attr : properties) {
        if(attr.name == "id")
            to = attr.value;
        else if(attr.name == "data-sent") 
            data = attr.value;
        else if(attr.name == "data-type")
            type = attr.value;
    }
    
    if(current_task == NULL)
        throw MyException("xml", "Mal formed XML : prev found ouside a task <prev id=\""+to+"\" ...");
   
    if(to.empty())
        throw MyException("xml", "Malformed XML : missing id for prev in task "+current_task->id);
    if(data.empty())
        data = "0";
    if(type.empty())
        type = conf->datatypes.default_type;
    tmp_precedence[current_task].push_back(make_tuple(to, stoul(data), type));
}

void TgXmlSaxParser::propagate_precedence() {
    for(pair<Task*, vector<tuple<string, uint32_t, string>>> cur : tmp_precedence) {
        for(tuple<string, uint32_t, string> parent : cur.second) {
            Task *tp = out->task(get<0>(parent));
            if(tp != NULL) {
                try {
                    cur.first->add_previous(tp, get<1>(parent), get<2>(parent));
                }
                catch(out_of_range) {
                    cur.first->add_previous(tp, get<1>(parent), "bit");
                    Utils::WARN("data-type size propagation, using 1 bit per \""+get<2>(parent)+"\""); 
                }
            }
        }
    }
}

void TgXmlSaxParser::propagate_global_deadline() {
    if(!tmp_global_deadline.empty())
        out->_global_deadline = stoul(tmp_global_deadline);
    else
        out->compute_sequential_schedule_length();
}

void TgXmlSaxParser::checktypes() {
    for(Task *t : out->_tasks) {
        for(pair<Task*, string> el : t->previous_datatypes) {
            if(!conf->datatypes.size_bits.count(el.second)) 
                throw MyException("Xml parser", "Unknow datatype : "+el.second);
        }
    }
}
