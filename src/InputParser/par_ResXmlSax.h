/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef XMLRESSAXPARSER_H
#define XMLRESSAXPARSER_H

#include <cstdlib>
#include <cstdio>
#include <string>
#include <iostream>
#include <map>
#include <utility>

#include <libxml++/libxml++.h>
#include <boost/algorithm/string.hpp>

#include "Parser.h"
#include "SystemModel.h"
#include "Schedule.h"
#include "Utils.h"

class ResXmlSaxParser : public Parser<Schedule_t>, public xmlpp::SaxParser {
protected:
    std::map<std::string, meth_processor_t*> procs;
    std::map<std::string, std::string> map_task_proc;
    std::map<std::string, std::string> mem_alloc;
    
    meth_processor_t *current_proc;
    std::string current_taskid;
public:
    ResXmlSaxParser() = delete;
    ResXmlSaxParser(const ResXmlSaxParser&) = delete;
    
    explicit ResXmlSaxParser(void *s, config_t *c) : Parser<Schedule_t>((Schedule_t*)s, c), SaxParser() {}
    
    ~ResXmlSaxParser() override;
    
    void parse_file(const string &file) {
        xmlpp::SaxParser::parse_file(file);
    }
    
protected:
    //overrides:
    //void on_start_document() override;
    virtual void on_end_document() override;
    virtual void on_start_element(const Glib::ustring& name, const AttributeList& properties) override;
    virtual void on_end_element(const Glib::ustring& name) override;
    //  void on_characters(const Glib::ustring& characters) override;
    //  void on_comment(const Glib::ustring& text) override;
    //  void on_warning(const Glib::ustring& text) override;
    virtual void on_error(const Glib::ustring& text) override;
    virtual void on_fatal_error(const Glib::ustring& text) override;
    
private:
    virtual void handle_schedule(const AttributeList& properties); 
    virtual void handle_task(const AttributeList& properties);
    virtual void handle_transmission(const AttributeList& properties);
    virtual void handle_region(const AttributeList& properties);
    virtual void handle_processor(const AttributeList& properties);
};

REGISTER_PARSER(ResXmlSaxParser, "-res.xml")

#endif /* XMLRESSAXPARSER_H */

