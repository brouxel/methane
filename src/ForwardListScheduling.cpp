/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ForwardListScheduling.h"
#ifdef _DEBUG
#include "view.h"
#endif

using namespace std;

void ForwardListScheduling::run(Schedule_t *sched) {
    assert(conf.interconnect.behavior == "blocking");
    
    vector<Task*> Qready, Qdone;
    for(Task *t : tg.tasks()) {
        if(t->previous.size() == 0) {
            Qready.push_back(t);
        }
    }
    sort(Qready.begin(), Qready.end(), [](Task *a, Task *b) {return a->successors > b->successors;});
    
    for(meth_processor_t *p: tg.processors()) {
        sched->procs[p->id].available_spm_size = p->spm_size;
        sched->procs[p->id].total_spm_size = p->spm_size;
    }
    
    Schedule_t schedBFS(*sched);
    try {
        vector<Task*> tmpQready(Qready);
        build_Qready(tmpQready, Qdone, Utils::sort_Q_byMemUsageDec);
        Qdone.clear();
        do_schedule(&schedBFS, tmpQready, Qdone);
    }
    catch(Unschedulable &e) {}
    
    Schedule_t schedDFS(*sched);
    try {
        vector<Task*> tmpQready(Qready);
        build_QreadyDFS(tmpQready, Utils::sort_Q_byMemUsageDec);
        Qdone.clear();
        do_schedule(&schedDFS, tmpQready, Qdone);
    }
    catch(Unschedulable &e) {}
    
    uint64_t schedLengthDFS = retrieve_schedule_length(schedDFS);
    uint64_t schedLengthBFS = retrieve_schedule_length(schedBFS);
    Utils::DEBUG("\t====> Schedule length DFS = "+to_string(schedLengthDFS));
    Utils::DEBUG("\t====> Schedule length BFS = "+to_string(schedLengthBFS));
    
    sched->status = 0;
    sched->makespan = (unsigned)-1;
    if(schedBFS.status == 1) 
        *sched = schedBFS;
    if(schedDFS.status == 1 && schedLengthDFS <= sched->makespan) 
        *sched = schedDFS;

    if(schedDFS.status == 2 || schedBFS.status == 2)
	*sched = schedBFS;
    
    sched->heur_DFS = schedDFS.status == 1 ? schedLengthDFS : -1;
    sched->heur_BFS = schedBFS.status == 1 ? schedLengthBFS : -1;
    
    Utils::DEBUG(""+*sched);
    Utils::DEBUG("\t====> Final Schedule length = "+to_string(retrieve_schedule_length(*sched))+" -- status: "+to_string(sched->status));
}

void ForwardListScheduling::do_schedule(Schedule_t *sched, vector<Task*> Qready, vector<Task*> &Qdone) {
    while(!Qready.empty()) {
        chrono::high_resolution_clock::time_point curtime = chrono::high_resolution_clock::now();
        chrono::duration<double> diff = curtime-start_time;
        if(diff.count() > (double)conf.solving.timeout) {
            sched->status = 0;
            Utils::DEBUG(">>>>>> Timeout <<<<<<<<");
            break;
        }
            
        Task *t = *(Qready.begin());
        Qready.erase(Qready.begin());
        
        Qdone.push_back(t);
        
        Utils::DEBUG("-> Task "+t->id);
        strategy->chose_proc(sched, Qdone, t);
        if(sched->status == 0)
            break;
    }
    
    if(sched->status == 0) {
        sched->makespan = 0;
        return;
    }
    
    uint64_t scheduleLength = retrieve_schedule_length(*sched);
    sched->makespan = scheduleLength;
}

void FLSStrategy::chose_proc(Schedule_t *sched, const vector<Task*> &Qdone, Task *t) {
    uint64_t bestScheduleTime = -1;
    Schedule_t bestSchedule;
    bestSchedule.status = 0;
    
    for(meth_processor_t *candidate : tg.processors()) {
        try {
            Utils::DEBUG("\tcandidate "+*candidate);

            Schedule_t schedule = *sched;
            uint32_t dataR = this->compute_data_read(&schedule, t, candidate, Qdone);
            uint32_t concR = compute_read_concurrency(sched, Qdone, t);
            uint32_t dataW = this->compute_data_written(&schedule, t, candidate, Qdone);
            uint32_t concW = compute_write_concurrency(sched, Qdone, t);
            string datatypeR = (t->previous_datatypes.size() > 0) ? (*(t->previous_datatypes.begin())).second : "void";
            string datatypeW = (t->successors.size() > 0) ? t->successors.at(0)->previous_datatypes[t] : "void";
            map_task_proc(&schedule, candidate, t,
                dataR, datatypeR, concR, tg.interconnect()->comm_delay(concR, dataR, datatypeR, t->force_read_delay),
                dataW, datatypeW, concW, tg.interconnect()->comm_delay(concW, dataW, datatypeW, t->force_write_delay)
            );
            adjust_schedule(&schedule, Qdone, t);
            adjust_region_endresa(&schedule, Qdone, t);
            map_phases_to_region(&schedule, Qdone, t);
            Utils::DEBUG("\t\t overlap: "+*t+" -> "+schedule.tasks[t->id]);

            schedule.makespan = retrieve_schedule_length(schedule);
            
            if(bestScheduleTime > schedule.makespan) {
                bestScheduleTime = schedule.makespan;
                bestSchedule = schedule;
                bestSchedule.status = 1;
                Utils::DEBUG("\t\tbest candidate makespan -- "+to_string(schedule.makespan));
            }
            else if(bestScheduleTime == schedule.makespan ) {
                if(bestSchedule.tasks[t->id].rtR() > schedule.tasks[t->id].rtR()) {
                    bestScheduleTime = schedule.makespan;
                    bestSchedule = schedule;
                    bestSchedule.status = 1;
                    Utils::DEBUG("\t\tbest candidate start -- "+to_string(schedule.makespan));
                }
                else if(bestSchedule.tasks[t->id].rtR() == schedule.tasks[t->id].rtR() &&
                        bestSchedule.tasks[t->id].rtE > schedule.tasks[t->id].rtE) {
                    bestScheduleTime = schedule.makespan;
                    bestSchedule = schedule;
                    bestSchedule.status = 1;
                    Utils::DEBUG("\t\tbest candidate exec -- "+to_string(schedule.makespan));
                }
                else if(bestSchedule.tasks[t->id].rtR() == schedule.tasks[t->id].rtR() &&
                        bestSchedule.tasks[t->id].rtE == schedule.tasks[t->id].rtE &&
                        bestSchedule.tasks[t->id].end() > schedule.tasks[t->id].end()) {
                    bestScheduleTime = schedule.makespan;
                    bestSchedule = schedule;
                    bestSchedule.status = 1;
                    Utils::DEBUG("\t\tbest candidate end -- "+to_string(schedule.makespan));
                }
            }
            else {
                Utils::DEBUG("\t\t\t\tbad candidate makespan -- "+to_string(schedule.makespan));
            }
    #ifdef _DEBUG
            static unsigned int __cnt__ = 0;
            //buildSVG(conf, schedule, "/tmp/iter-"+to_string(++__cnt__)+"-"+candidate->id+"-"+t->id+".svg");
    #endif
        }
        catch(Unschedulable &e) {
            Utils::DEBUG("--> "+string(e.what())+"\n");
        }
    }
    
    *sched = bestSchedule;
    Utils::DEBUG(""+*sched);
}

uint32_t FLSStrategy::compute_data_read(Schedule_t *sched, Task *t, const vector<Task*> &Qdone) {
    return compute_data_read(sched, t, sched->tasks[t->id].proc, Qdone);
}
uint32_t FLSStrategy::compute_data_written(Schedule_t *sched, Task *t, const vector<Task*> &Qdone) {
    return compute_data_written(sched, t, sched->tasks[t->id].proc, Qdone);
}

uint32_t FLSStrategy::compute_data_read(Schedule_t *sched, Task *t, meth_processor_t *candidate, const vector<Task*> &Qdone) {
    if(conf.interconnect.mechanism == "sharedonly") 
        return t->data_read();
    
    // mechanism == "shared" or "direct"
    uint32_t data = 0;
    for(Task::prev_t el : t->previous) {
        if(sched->tasks[el.first->id].proc != candidate) //only exchange when mapped on different core
            data += el.second;
    }
    return data;
}

uint32_t FLSStrategy::compute_data_written(Schedule_t *sched, Task *t, meth_processor_t *candidate, const vector<Task*> &Qdone) {
    if(conf.interconnect.mechanism == "sharedonly") 
        return t->data_written();
    
    uint32_t data = 0;
    for(Task *succ : t->successors) {
        if(sched->tasks[succ->id].proc == nullptr) continue; // task succ is still not mapped
        if(sched->tasks[succ->id].proc != candidate) //only exchange when mapped on different core
            data += t->data_written(*succ);
    }
    return data;
}

void FLSStrategy::build_related(Schedule_t *sched, const vector<Task*> &Qdone, Task *current, vector<Task*> *related) {
    Task *first_prev = (current->previous.size() > 0) ? current->previous.begin()->first : current;

    vector<Task*> tmpQdone(Qdone);
    sort(tmpQdone.begin(), tmpQdone.end(), [&sched](Task *a, Task *b) {
        return sched->tasks.at(a->id).rtR() < sched->tasks.at(b->id).rtR();
    });

    vector<Task*>::iterator it = tmpQdone.begin();
    for(; it != tmpQdone.end() ; ++it) {
        if(sched->tasks[first_prev->id].end() > sched->tasks[(*it)->id].rtR() && sched->tasks[(*it)->id].end() > sched->tasks[first_prev->id].rtR()) {
            break;
        }
    }

    for( ; it != tmpQdone.end() ; ++it)
        related->push_back(*it);
}

uint64_t FLSStrategy::find_best_start_read(const Schedule_t &sched, const vector<Task*> &Qdone, Task *t) {
    uint64_t bestStart = 0;
    for(Task::prev_t el : t->previous) {
        if(sched.tasks.at(el.first->id).end() > bestStart) {
            bestStart = sched.tasks.at(el.first->id).end();
        }
    }
    
    vector<Task*> onSameCore = build_samecore_taskset(sched, Qdone, t); 
    
    bestStart = ensure_task_mutex(sched, onSameCore, t, bestStart);

    return bestStart;
}
uint64_t FLSStrategy::ensure_task_mutex(const Schedule_t &sched, const vector<Task*> &enemies, Task *t, uint64_t bestStart) {
    uint32_t delayR = sched.tasks.at(t->id).delayR();
    uint64_t endtime = bestStart + delayR + t->C + sched.tasks.at(t->id).delayW();

    for(Task *e : enemies) {
        uint64_t enemyRtDR = sched.tasks.at(e->id).rtR();
        uint64_t enemyEnd = sched.tasks.at(e->id).end();
        
        if(enemyRtDR < endtime && bestStart < enemyEnd) {
            bestStart = enemyEnd;
            endtime = bestStart + delayR + t->C + sched.tasks.at(t->id).delayW();
            return ensure_task_mutex(sched, enemies, t, bestStart);
        }
    }
    return bestStart;
}

uint64_t FLSStrategy::ensure_read_mutex(const Schedule_t &sched, const vector<Task*> &enemies, Task *t, uint64_t bestStart) {
    if(sched.tasks.at(t->id).delayR() == 0)
        return bestStart;

    uint32_t delayR = sched.tasks.at(t->id).delayR();
    uint64_t endTimeR = bestStart + delayR;

    for(Task *e : enemies) {
        uint64_t eRtR = sched.tasks.at(e->id).rtR();
        uint32_t eDelayR = sched.tasks.at(e->id).delayR();
        uint64_t eRtE = eRtR + eDelayR;
        uint64_t eRtW = sched.tasks.at(e->id).rtW();
        uint32_t eDelayW = sched.tasks.at(e->id).delayW();
        uint64_t eEndTime = sched.tasks.at(e->id).end();
        
        //RR
        if(eDelayR > 0 && eRtR < endTimeR && bestStart < eRtR+eDelayR) {
            bestStart = eRtR+eDelayR;
            endTimeR = bestStart+delayR;
            return ensure_read_mutex(sched, enemies, t, bestStart);
        }
        //RW
        if(eDelayW > 0 && eRtW < endTimeR && bestStart < eEndTime) {
            bestStart = eEndTime;
            endTimeR = bestStart+delayR;
            return ensure_read_mutex(sched, enemies, t, bestStart);
        }
    }

    return bestStart;
}

uint64_t FLSStrategy::find_best_start_write(const Schedule_t &sched, const vector<Task*> &Qdone, Task *t) {
    uint64_t bestStart = sched.tasks.at(t->id).rtE + t->C;
    return bestStart;
}

uint64_t FLSStrategy::ensure_write_mutex(const Schedule_t &sched, const vector<Task*> &enemies, Task *t, uint64_t bestStart) {
    if(sched.tasks.at(t->id).delayW() == 0)
        return bestStart;

    uint64_t endtime = bestStart + sched.tasks.at(t->id).delayW();

    for(Task *e : enemies) {
        uint64_t eRtR = sched.tasks.at(e->id).rtR();
        uint32_t eDelayR = sched.tasks.at(e->id).delayR();
        uint64_t eRtE = eRtR + eDelayR;
        uint64_t eRtW = sched.tasks.at(e->id).rtW();
        uint32_t eDelayW = sched.tasks.at(e->id).delayW();
        uint64_t eEndTime = sched.tasks.at(e->id).end();

        //WW
        if(eDelayW > 0 && eRtW < endtime && bestStart < eEndTime) {
            bestStart = eEndTime;
            endtime = bestStart + sched.tasks.at(t->id).delayW();
            return ensure_write_mutex(sched, enemies, t, bestStart);
        }
        //WR
        if(eDelayR > 0 && eRtR < endtime && bestStart < eRtE) {
            bestStart = eRtE;
            endtime = bestStart + sched.tasks.at(t->id).delayW();
            return ensure_write_mutex(sched, enemies, t, bestStart);
        }
    }

    return bestStart;
}

uint64_t FLSStrategy::find_best_start_exec(const Schedule_t &sched, const vector<Task*> &Qdone, Task *t) {
    uint64_t bestStart = sched.tasks.at(t->id).rtR()+sched.tasks.at(t->id).delayR();
    
    return bestStart;
}

uint64_t FLSStrategy::ensure_exec_mutex(const Schedule_t &sched, const vector<Task*> &enemies, Task *t, uint64_t bestStart) {
    uint64_t endtime = bestStart + t->C;

    for(Task *e : enemies) {
        uint64_t eRtE = sched.tasks.at(e->id).rtE;
        uint64_t eEndTime = eRtE + e->C;
        
        if(eRtE < endtime && bestStart < eEndTime) {
            bestStart = eEndTime;
            endtime = bestStart + t->C;
            return ensure_exec_mutex(sched, enemies, t, bestStart);
        }
    }

    return bestStart;
}

void FLSStrategy::map_phases_to_region(Schedule_t* sched, const vector<Task*>& Qdone, Task* current) {
    if(!conf.archi.spm.assign_region) return;
    
    //Maybe TODO: if "Allocation XY overlap" arise, then maybe the phase Y should go in an other region, 
    
    uint32_t data_exec = current->local_memory_storage;
    if(!conf.archi.prefetch_code && conf.archi.spm.store_code)
        data_exec += current->memory_code_size;
    
    uint64_t startresa = 0; // resident exec region !conf.archi.prefetch_code && conf.archi.spm.store_code
    uint64_t endresa = -1;
    if(conf.archi.prefetch_code && conf.archi.spm.store_code) {
        startresa = sched->tasks[current->id].rtR();
        endresa = sched->tasks[current->id].rtE+current->C;
    }
    else if(!(!conf.archi.prefetch_code && conf.archi.spm.store_code)) { // only local data
        startresa = sched->tasks[current->id].rtE;
        endresa = sched->tasks[current->id].rtE+current->C;
    }
    allocate_region(sched, Qdone, current, data_exec, &(sched->tasks[current->id].memory_region.exec), 
        startresa, endresa,
        "E");
    
    if(conf.interconnect.mechanism == "sharedonly") {
        uint32_t data_read = 0; 
        string datatypeR = (current->previous_datatypes.size() > 0) ? (*(current->previous_datatypes.begin())).second : "void";
        data_read = current->data_read() * conf.datatypes.size_bits.at(datatypeR);
        if(conf.archi.prefetch_code && conf.archi.spm.store_code)
            data_read += current->memory_code_size;
        
        allocate_region(sched, Qdone, current, data_read, &(sched->tasks[current->id].memory_region.read[current->id]),
            sched->tasks[current->id].rtR(), sched->tasks[current->id].rtE+current->C, 
            "R");
        
        string datatypeW = (current->successors.size() > 0) ? current->successors.at(0)->previous_datatypes[current] : "void";
        uint32_t data_written = current->data_written() * conf.datatypes.size_bits.at(datatypeW);
        allocate_region(sched, Qdone, current, data_written, &(sched->tasks[current->id].memory_region.write[current->id]), 
            sched->tasks[current->id].rtE, sched->tasks[current->id].end(),
            "W");
    }
    else if(conf.interconnect.mechanism == "shared") {
        if(conf.archi.prefetch_code && conf.archi.spm.store_code) {
            allocate_region(sched, Qdone, current, current->memory_code_size, &(sched->tasks[current->id].memory_region.read[current->id]),
                sched->tasks[current->id].rtR(), sched->tasks[current->id].rtE+current->C, 
                "Rcode");
        }
        
        for(Task::prev_t el : current->previous) {
            if(el.second == 0) continue;
            
            if(sched->tasks[current->id].proc == sched->tasks[el.first->id].proc) {
                sched->tasks[current->id].memory_region.read[el.first->id] = sched->tasks[el.first->id].memory_region.write[current->id];
                if(sched->tasks[current->id].memory_region.read[el.first->id].endresa < sched->tasks[current->id].rtE+current->C)
                    sched->tasks[current->id].memory_region.read[el.first->id].endresa = sched->tasks[current->id].rtE+current->C;
            }
            else {
                allocate_region(sched, Qdone, current, el.second*conf.datatypes.size_bits.at(current->previous_datatypes[el.first]), 
                    &(sched->tasks[current->id].memory_region.read[el.first->id]),
                    sched->tasks[current->id].rtR(), sched->tasks[current->id].rtE+current->C, 
                    "Rprev"+el.first->ilp_label);
            }
        }
        
        for(Task *succ : current->successors) {
            allocate_region(sched, Qdone, current, current->data_written(*succ)*conf.datatypes.size_bits.at(succ->previous_datatypes[current]), 
                &(sched->tasks[current->id].memory_region.write[succ->id]), 
                sched->tasks[current->id].rtE, sched->tasks[current->id].end(),
                "Wsucc"+succ->ilp_label);
        }
    }
}


void FLSStrategy::allocate_region(Schedule_t* sched, const vector<Task*>& Qdone, Task* current, uint32_t data, spm_region_info *current_region, uint64_t startresa, uint64_t endresa, const string &suffix) {
    if(data == 0) return;
    
    string procid = sched->tasks[current->id].proc->id;
      
    // no region has been assigned
    
    // build set of already created region where there is enough space, and the endresa of the region finishes before the current task needs
    // take the smallest region that can fit the demand
    // if not found, then throw unschedulable on that core

    vector<spm_region_info> candidate(sched->procs[procid].memory_regions.size());
    auto &tmpconf = conf;
    if(suffix != "E" || (!tmpconf.archi.prefetch_code && tmpconf.archi.spm.store_code)) {
        vector<spm_region_info>::iterator it = copy_if(sched->procs[procid].memory_regions.begin(), sched->procs[procid].memory_regions.end(), candidate.begin(), 
            [sched, &tmpconf, current, data, startresa, suffix](const spm_region_info &a) {
            if(a.size < data || a.endresa >= startresa) // here just checking for endresa might create unschedulable, maybe storing a vector of resa period should be better to find a free one
                return false;

            if(!tmpconf.archi.prefetch_code && tmpconf.archi.spm.store_code) {
                // check if the region is assigned to an exec phase, for the case where the code is loaded a the platform startup
                for(pair<string, task_schedule_info> el : sched->tasks) {
                    if(el.second.memory_region.exec == a)
                        return false;
                }
                return true;
            }

            return true;
        });
        candidate.resize(distance(candidate.begin(), it));
    
        if(candidate.size() > 0) {
            sort(candidate.begin(), candidate.end(), [](const spm_region_info &a, const spm_region_info &b) { return a.size < b.size; });
            spm_region_info goodone = *(candidate.begin());
            goodone.endresa = endresa;
            *current_region = goodone;

      //      Utils::DEBUG("\t" + current->id + " will reuse region "+goodone->label+" on "+procid+" for "+suffix);
            return;
        }
    }
    
    if(sched->procs[procid].available_spm_size >= data) {
        //there is enough free space, we  create region for read phase
        string label = current->ilp_label+suffix;
        
        spm_region_info sr(label, data);
        sr.startresa = startresa;
        sr.endresa = endresa;
        sched->procs[procid].memory_regions.push_back(sr);
        
        sched->procs[procid].available_spm_size -= data;
        *current_region = sr;
        
        //Utils::DEBUG("\t" + current->id + " create a region on core "+procid+" with size "+to_string(data)+" with name "+label);
        
        return;
    }
        
    throw Unschedulable(current, "No spm space on proc "+procid+"("+to_string(sched->procs[procid].available_spm_size)+")"+" for "+suffix+" phase of "+current->id+" or maybe look for a region that have enough space and is free for use on the time slot required by current, or resize one -- size needed: "+to_string(data));
        
}

void FLSStrategy::adjust_region_endresa(Schedule_t* sched, const vector<Task*>& Qdone, Task *current) {
    if(!conf.archi.spm.assign_region) return;
    
    for(Task *t : Qdone) {
        if (t == current) continue; // nothing has been reserved for current at the moment
        
        string procid = sched->tasks[t->id].proc->id;
        
        for(pair<string, spm_region_info> el : sched->tasks[t->id].memory_region.read) {
            if(!el.second.label.empty()&& el.second.endresa < sched->tasks[t->id].rtE+t->C)
                sched->tasks[t->id].memory_region.read[el.first].endresa = sched->tasks[t->id].rtE+t->C;
        }
        
        if(!sched->tasks[t->id].memory_region.exec.label.empty() && sched->tasks[t->id].memory_region.exec.endresa < sched->tasks[t->id].rtE+t->C)
            sched->tasks[t->id].memory_region.exec.endresa = sched->tasks[t->id].rtE+t->C;
        
        for(pair<string, spm_region_info> el : sched->tasks[t->id].memory_region.write) {
            if(!el.second.label.empty() && el.second.endresa < sched->tasks[t->id].end())
                sched->tasks[t->id].memory_region.write[el.first].endresa = sched->tasks[t->id].end();
        }
    }
}
