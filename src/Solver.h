/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SOLVER_H
#define SOLVER_H

#include <string>
#include <map>
#include <chrono>

#include "config.h"
#include "registry.h"
#include "Schedule.h"

class Solver {
protected:
    const SystemModel &tg;
    const config_t &conf;
    chrono::high_resolution_clock::time_point start_time;
    
    virtual void run(Schedule_t *result) = 0;
    virtual void presolve(Schedule_t *) {}
    virtual void postsolve(Schedule_t *) {}
    
public:
    explicit Solver(const SystemModel &m, const config_t &c) : tg(m), conf(c) {};
    
    virtual void solve(Schedule_t *result) {
        start_time = chrono::high_resolution_clock::now();
        try {
            this->presolve(result);
            this->run(result);
            this->postsolve(result);
            chrono::high_resolution_clock::time_point end = chrono::high_resolution_clock::now();
            chrono::duration<double> diff = end-start_time;
            cout << "==> Schedule time: " << diff.count() << " seconds" << endl;
            result->solvetime = diff.count();
        }
        catch(Unschedulable &e) {
            chrono::high_resolution_clock::time_point end = chrono::high_resolution_clock::now();
            chrono::duration<double> diff = end-start_time;
            cout << "==> Schedule time: " << diff.count() << " seconds" << endl;
            result->solvetime = diff.count();
            throw e;
        }
    }
};

using SolverRegistry = registry::Registry<Solver, std::string, const SystemModel &, const config_t &>;

#define REGISTER_SOLVER(ClassName, Identifier) \
  REGISTER_SUBCLASS(Solver, ClassName, std::string, Identifier, const SystemModel &, const config_t &)

#endif /* SOLVER_H */

