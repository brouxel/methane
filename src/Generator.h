/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GENERATOR_H
#define GENERATOR_H

#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <cassert>

#include "config.h"
#include "SystemModel.h"
#include "Schedule.h"
#include "registry.h"

class Generator {
protected:
    const SystemModel &tg;
    const config_t &conf;
    const Schedule_t &sched;
    
public:
    explicit Generator(const SystemModel &t, const config_t &c, const Schedule_t &s) : tg(t), conf(c), sched(s) {};
    
    virtual void generate() = 0;
};

using GeneratorRegistry = registry::Registry<Generator, std::string, const SystemModel &, const config_t &, const Schedule_t &>;

#define REGISTER_GENERATOR(ClassName, Identifier) \
  REGISTER_SUBCLASS(Generator, ClassName, std::string, Identifier, const SystemModel &, const config_t &, const Schedule_t &)

#endif /* GENERATOR_H */

