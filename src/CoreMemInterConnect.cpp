/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "CoreMemInterConnect.h"

#include "SystemModel.h"


uint32_t CoreMemInterConnect::token_per_burst_chunk(const config_t &conf, uint32_t datatypesize) {
    if(conf.interconnect.burst == "flit") {
        return conf.interconnect.bit_per_timeunit / datatypesize;
    }
    else if(conf.interconnect.burst == "packet") {
        return (conf.interconnect.bit_per_timeunit * conf.interconnect.active_ress_time ) / datatypesize;
    }
    return (uint32_t)-1; //infinity
}

//return a number of tokens, can not send half a token
vector<uint32_t> CoreMemInterConnect::burst(const config_t &conf, uint32_t data, uint32_t datatypesize) {
    vector<uint32_t> res;
    uint32_t burst_chunk_size = token_per_burst_chunk(conf, datatypesize);
    
    while(data >= burst_chunk_size) {
        res.push_back(burst_chunk_size);
        data -= burst_chunk_size;
    }
    
    // burst == "edge" will just add the data here
    if(data > 0)
        res.push_back(data);
 
    return res;
}





