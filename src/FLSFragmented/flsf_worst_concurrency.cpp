/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "FLSFragmented.h"

using namespace std;

class SFLSWorstConcurrency : public FLSFragStrategy {
public:
    explicit SFLSWorstConcurrency(const SystemModel &m, const config_t &c) : FLSFragStrategy(m, c) {}
    
    virtual uint16_t compute_read_concurrency(SchedEltPacket *t) {
        return tg.processors().size()-1;
    }
    virtual uint16_t compute_write_concurrency(SchedEltPacket *t) {
        return tg.processors().size()-1;
    }
    
    uint64_t find_best_start_read(const Schedule_t &sched, const vector<SchedElt*> &Qdone, SchedEltPacket *t) {
        return find_best_start_write(sched, Qdone, t);
    }
    
    uint64_t find_best_start_write(const Schedule_t &sched, const vector<SchedElt*> &Qdone, SchedEltPacket *t) {
        uint64_t bestStart = 0;
        for(SchedElt *elt : t->previous) {
            if(elt->rt + elt->wct > bestStart)
                bestStart = elt->rt + elt->wct;
        }
        
        vector<SchedElt*> copy(Qdone);
        sort(copy.begin(), copy.end(), [](SchedElt *a, SchedElt *b) {
            return a->rt < b->rt;
        });
        
        uint64_t endtime = bestStart + t->wct;
        for(SchedElt *elt : copy) {
            if(elt == t) continue;
            SchedEltTask *task = elt->getTask();
            if(t->getTask() == task) continue;
            if(!sched.tasks.count(task->task->id)) continue;
            if(sched.tasks.at(t->getTask()->task->id).proc != sched.tasks.at(task->task->id).proc) continue;
            
            uint64_t eRtE = elt->rt;
            uint64_t eEndTime = eRtE + elt->wct;
            
            if(eRtE < endtime && bestStart < eEndTime) {
                bestStart = eEndTime;
                endtime = bestStart + t->wct;
            }
        }
        
        return bestStart;
    }
    
    uint64_t find_best_start_exec(const Schedule_t &sched, const vector<SchedElt*> &Qdone, SchedEltTask *t) {
        uint64_t bestStart = 0;
        for(SchedElt *elt : t->previous) {
            if(elt->rt + elt->wct > bestStart)
                bestStart = elt->rt + elt->wct;
        }
        
        uint64_t endtime = bestStart + t->wct;
        for(SchedElt *elt : Qdone) {
            if(elt == t) continue;
            if(!Utils::isa<SchedEltTask*>(elt)) continue;
            
            if(sched.tasks.at(((SchedEltTask*)elt)->task->id).proc != sched.tasks.at(t->task->id).proc) continue;
            
            uint64_t eRtE = elt->rt;
            uint64_t eEndTime = eRtE + elt->wct;
            if(eRtE < endtime && bestStart < eEndTime) {
                bestStart = eEndTime;
                endtime = bestStart + t->wct;
            }
        }

        return bestStart;
    }
    
    // because Packet must not overlap when originating from the same core
    //   but can overlap with from different core, then they must be scheduled when the task is mapped/scheduled
    void chose_proc(Schedule_t *sched, const vector<SchedElt*> &Qdone, SchedEltPacket *t) {
    }
    void chose_proc_internal(Schedule_t *sched, const vector<SchedElt*> &Qdone, SchedEltPacket *t) {
        t->rt = find_best_start_write(*sched, Qdone, t);
        transmission_phase_info tr;
        tr.delay = t->wct;
        tr.data = t->data;
        tr.rt = t->rt;
        tr.datatype = t->datatype;
        tr.packetnum = t->packetnum;
        tr.packetnumber = t->transmission_nb_packet;
        
        
        Utils::WARN("Further push_back generate a random segfault depending on how the compilation performed I assume."+
                "In case of segfault here, just make clean, and make should do the trick, dunno why.");
        if(t->dirlbl == "w") {
            tr.type = "write";
            tr.conc = compute_write_concurrency(t);
            sched->tasks[t->schedtask->task->id].writes[t->tofromschedtask->task->id].push_back(tr);
        }
        //else should be a read
        else {
            tr.type = "read";
            tr.conc = compute_read_concurrency(t);
            sched->tasks[t->schedtask->task->id].reads[t->tofromschedtask->task->id].push_back(tr);
        }
    }
    
    void chose_proc(Schedule_t *sched, const vector<SchedElt*> &Qdone, SchedEltTask *t) {
        uint64_t bestScheduleTime = -1;
        Schedule_t bestSchedule;
        bestSchedule.status = 0;

        for(meth_processor_t *candidate : tg.processors()) {
            if(find(forbidden_proc_mapping[t].begin(), forbidden_proc_mapping[t].end(), candidate) != forbidden_proc_mapping[t].end()) {
                Utils::DEBUG("\tcandidate "+*candidate+" unusable due to previous unschedulability issue");
                continue;
            }
            
            Utils::DEBUG("\tcandidate "+*candidate);

            Schedule_t schedule = *sched;
            schedule.tasks[t->task->id].proc = candidate;
            schedule.tasks[t->task->id].WCET = t->wct;
            
            if(t->packet_list[0] != nullptr)
                chose_proc_internal(&schedule, Qdone, t->packet_list[0]); // sched read packet
            schedule.tasks[t->task->id].rtE = t->rt = find_best_start_exec(schedule, Qdone, t);
            Utils::WARN("Write packet should not be placed here as it is not placed in Qdone (and removed from Qready), "+
            "then it creates a wrong schedule where 2 communications phases are active from the same core.");
            if(t->packet_list[1] != nullptr) 
                chose_proc_internal(&schedule, Qdone, t->packet_list[1]); // sched write packet

            Utils::DEBUG("\t\t overlap: "+*t->task+" -> "+schedule.tasks[t->task->id]);

            schedule.makespan = retrieve_schedule_length(schedule);

            if(bestScheduleTime > schedule.makespan) {
                bestScheduleTime = schedule.makespan;
                bestSchedule = schedule;
                bestSchedule.status = 1;
                Utils::DEBUG("\t\tbest candidate makespan -- "+to_string(schedule.makespan));
            }
            else if(bestScheduleTime == schedule.makespan ) {
                if(bestSchedule.tasks[t->task->id].rtR() > schedule.tasks[t->task->id].rtR()) {
                    bestScheduleTime = schedule.makespan;
                    bestSchedule = schedule;
                    bestSchedule.status = 1;
                    Utils::DEBUG("\t\tbest candidate start -- "+to_string(schedule.makespan));
                }
                else if(bestSchedule.tasks[t->task->id].rtR() == schedule.tasks[t->task->id].rtR() &&
                        bestSchedule.tasks[t->task->id].rtE > schedule.tasks[t->task->id].rtE) {
                    bestScheduleTime = schedule.makespan;
                    bestSchedule = schedule;
                    bestSchedule.status = 1;
                    Utils::DEBUG("\t\tbest candidate exec -- "+to_string(schedule.makespan));
                }
                else if(bestSchedule.tasks[t->task->id].rtR() == schedule.tasks[t->task->id].rtR() &&
                        bestSchedule.tasks[t->task->id].rtE == schedule.tasks[t->task->id].rtE &&
                        bestSchedule.tasks[t->task->id].end() > schedule.tasks[t->task->id].end()) {
                    bestScheduleTime = schedule.makespan;
                    bestSchedule = schedule;
                    bestSchedule.status = 1;
                    Utils::DEBUG("\t\tbest candidate end -- "+to_string(schedule.makespan));
                }
            }
            else {
                Utils::DEBUG("\t\t\t\tbad candidate makespan -- "+to_string(schedule.makespan));
            }
        }

        t->rt = bestSchedule.tasks[t->task->id].rtE;
        *sched = bestSchedule;
        Utils::DEBUG(""+*sched);
    }
    
};
REGISTER_SPLITFLSSTRATEGY(SFLSWorstConcurrency, "worst_concurrency")