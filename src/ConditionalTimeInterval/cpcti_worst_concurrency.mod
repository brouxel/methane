/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
//*
R"CPPRAWMARKER(
//*/

using CP;
 
int T_slot = ...;

{string} Processors = ...;
int nbProcessors = 0;
execute {
	nbProcessors = Processors.size;
};
{string} ExecTasks = ...;
int WCET[ExecTasks] = ...;
int SeqSchedLength = sum(t in ExecTasks) WCET[t];

string ReadTaskNames[ExecTasks];
string WriteTaskNames[ExecTasks];
execute {
    for(var t in ExecTasks) {
        ReadTaskNames[t] = "R"+t;
        WriteTaskNames[t] = "W"+t;
    }
};
{string} ReadTasks = {ReadTaskNames[t] | t in ExecTasks};
{string} WriteTasks = {WriteTaskNames[t] | t in ExecTasks};
{string} Tasks = ReadTasks union ExecTasks union WriteTasks;

tuple Precedence {
   string pre;
   string post;
   int data;
};
{Precedence} Precedences = ...;


dvar interval schedule[t in Tasks] in 0 .. SeqSchedLength;
dvar interval mapping[t in Tasks][p in Processors] optional;

dvar int data_r[t in ReadTasks] in 0 .. SeqSchedLength;
dvar int data_w[t in WriteTasks] in 0 .. SeqSchedLength;

dvar boolean samemapping[a in Tasks][b in Tasks];

minimize max(t in Tasks) endOf(schedule[t]);
subject to {
  forall(t in ExecTasks) {
  	forall(p in Processors) {
  	  presenceOf(mapping[t][p]) => presenceOf(mapping[ReadTaskNames[t]][p]);
  	  presenceOf(mapping[t][p]) => presenceOf(mapping[WriteTaskNames[t]][p]);
    }
    startAtEnd(schedule[t], schedule[ReadTaskNames[t]]);
    startAtEnd(schedule[WriteTaskNames[t]], schedule[t]);
  }
  
  forall(<a,b, data> in Precedences) {
	samemapping[a][b] == or(p in Processors) (presenceOf(mapping[a][p]) && presenceOf(mapping[b][p]));    
  }
  
  forall(t in ExecTasks) {
  	sizeOf(schedule[t]) == WCET[t];  
  
  	data_r[ReadTaskNames[t]] == 
  		sum(<pre,post,data> in Precedences : post == t) 
  			(data * (1-samemapping[pre][post]));  
  	sizeOf(schedule[ReadTaskNames[t]]) == 
  		T_slot * ceil(data_r[ReadTaskNames[t]] / T_slot) * (nbProcessors-1)
  		+ T_slot * floor(data_r[ReadTaskNames[t]] / T_slot)
  		+ data_r[ReadTaskNames[t]] % T_slot;
  
  	data_w[WriteTaskNames[t]] == 
  		sum(<pre,post,data> in Precedences : pre == t) 
  			(data * (1-samemapping[pre][post]));  
  	sizeOf(schedule[WriteTaskNames[t]]) == 
  		T_slot * ceil(data_w[WriteTaskNames[t]] / T_slot) * (nbProcessors-1)
  		+ T_slot * floor(data_w[WriteTaskNames[t]] / T_slot)
  		+ data_w[WriteTaskNames[t]] % T_slot;
  }

  forall(p in Precedences)
	endBeforeStart(schedule[WriteTaskNames[p.pre]], schedule[ReadTaskNames[p.post]]);    
	
  forall(t in Tasks)
    alternative(schedule[t], all(p in Processors) mapping[t][p]);
    
  forall(p in Processors)
  	noOverlap(all(t in Tasks) mapping[t][p]);
  	
};

tuple solutionT {
	string task;
	int start;
	int delay;
	string p;
};
{solutionT} solution = {<t, startOf(schedule[t]), lengthOf(schedule[t]), pr> | t in Tasks, pr in Processors : presenceOf(mapping[t][pr])};

execute {
    for(var s in solution) {
		writeln(s.task+" --> "+s.start+", "+s.delay+" -- "+s.p);
    }		
}

//*
)CPPRAWMARKER"
//*/