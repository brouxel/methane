/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONFIG_H
#define CONFIG_H

#include <cstdio>
#include <cstdlib>
#include <string>
#include <execinfo.h>
#include <map>
#include <iostream>
#include <set>

using namespace std;

#define DEFAULT_TASK_PERIOD 0

#define TRACE_BUFFER_SIZE 100

#ifdef __GNU_LIBRARY__ //Linux
#define NOEX _GLIBCXX_USE_NOEXCEPT
#endif
#ifdef _NOEXCEPT    //Darwin
#define NOEX _NOEXCEPT
#endif
#ifndef NOEX
#define NOEX
#endif

class MyException : public exception {
    string msg;
    string _type;
    bool _backtrace;
    
public:
    explicit MyException(const string &t, const string &m, bool bt=false) : exception(), _type(t), _backtrace(bt) {
        msg = m + "\e[0m\n";
        
        if(_backtrace) {
            void *buffer[TRACE_BUFFER_SIZE];
            int nbtracerecords = backtrace(buffer, TRACE_BUFFER_SIZE);
            char **tracerecords = backtrace_symbols(buffer, nbtracerecords);
            if (tracerecords == NULL) {
                perror("backtrace_symbols");
                exit(EXIT_FAILURE);
            }
        
            for(int i=0 ; i < nbtracerecords ; ++i) {
                msg += "\t";
                msg += tracerecords[i];
                msg += "\n";
            }
            if(nbtracerecords > 0)
                free(tracerecords);
        }
    }
    virtual ~MyException() NOEX {
    }
    
    virtual const char* what() const NOEX override {
        return msg.c_str();
    }
    
    virtual const string & type() const {return _type;}
    virtual const string & type() {return _type;}
};

class Todo : public exception {
    string msg;
    
public:
    explicit Todo(const string &m) : exception(), msg(m) {}
    virtual const char* what() const NOEX override {
        return msg.c_str();
    }
};

#define DEFAULT_TEST_LOCATION "/home/brouxel/Projects/C-C++/Methane/resources/"

typedef struct {
    struct {
        string path; // path to use case
        string filename; // filename use case
        string in; // input parameter
        string lp; // lp filename
        string dot; // dot filename
        string trdot; // transitively closed filename
        string svg; // SVG filename
        string out; // output filename
    } files;
    
    bool clean; // should delete temporary files
    set<string> outputs; // what are the wished outputs, see into Generator folder
    
    struct {
        uint32_t zoomX = 0;
        uint32_t zoomY = 0;
        uint32_t step = 0;
    } view;
    
    struct {
        string type; // BUS / NOC
        string arbiter; // FAIR / TDM
        uint32_t active_ress_time; // active window time
        uint32_t bit_per_timeunit;// number of token per unit of time
        string mechanism; //direct: SPM -> SPM ; shared : SPM -> MEM -> SPM ; sharedonly : idem shared and forced for dependent tasks on the same core
        string behavior; // blocking/non-blocking
        string burst; // none / edge / packet (Dslot) / fine (bit/timeunit)
    } interconnect;
    
    struct {
        bool solve;
        uint32_t timeout; // number of seconds
        string solver; //solver to use, see in Solver folder
        string type;
        bool optimal; // if optimal result is wished, or if a feasible is enough, mostly for ILP solver
        uint32_t cores; // nb cores to use on the host machine to solve the problem
    } solving;

    struct {
        string default_type; // default datatype
        map<string, uint64_t> size_bits; // map datatype names and their corresponding size
    } datatypes;
    
    struct {
        bool prefetch_code; //should the read phase of a task prefetch the code?
        struct {
            bool assign_region; //should a phase of SPM allocation be done?
            bool store_code; //should the SPM store the code?
            bool prefetch_code; 
        } spm;
    } archi;
    
    struct {
        string machine;
        int maxiter;
        bool with_delay_printf;
        float safety; // safety margin for each release time
    } codegen;
} config_t;


void init_config(config_t *conf);
void check_todo(const config_t &conf);
#endif /* CONFIG_H */

