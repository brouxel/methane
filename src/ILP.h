/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROXYILPSOLVER_H
#define PROXYILPSOLVER_H

#include <vector>
#include <string>
#include <map>
#include <algorithm> 
#include <array>
#include <iostream>
#include <fstream>

#include "config.h"
#include "SystemModel.h"
#include "Schedule.h"
#include "Solver.h"
#include "Utils.h"

class ILPSolver : public Solver {
protected:
    map<string, long long> tmp_results;
    
    virtual void presolve(Schedule_t *result);
    virtual void postsolve(Schedule_t *result);
public:
    explicit ILPSolver(const SystemModel &tg, const config_t &conf) : Solver(tg, conf) {};
};

class ILPFormGen {
protected:
    const SystemModel &tg;
    const config_t &conf;
    
    std::ofstream os;
    uint64_t SeqTauMax = -1;
    
    string binvar = "";
    string genvar = "";
    
public:
    explicit ILPFormGen(const SystemModel &m, const config_t &c) : tg(m), conf(c) {}
    virtual void gen() = 0;
    
protected:
    void objectiv_func();
    void linkObj();
    void unicity();
    void detect_samecore();
    
    void precedence();
    void precedence_communications();
    void precedence_samecore();
    void precedence_samecore_communications();
    
    void conflicts();
    void conflicts_communications();
    void read_exec_write();
    void causality();
    void causality_communications();
    
    virtual void communication_delay() = 0;
    
    void map_spm_region_to_task();

    void add_bounds(const string & bounds);
    void add_bounds();

    void force_mapping();
    void force_schedule();
    
    void declare_binaries();
    void declare_generals();
    void concludeILPFile();
    void concludeILPFile(string &remaining);
    
    //@experimental -- not finished yet
    void periodicity_deadlines();
    void job_causality();
    
};

using ILPFormRegistry = registry::Registry<ILPFormGen, string, const SystemModel &, const config_t &>;

#define REGISTER_ILPGENERATOR(ClassName, Identifier) \
  REGISTER_SUBCLASS(ILPFormGen, ClassName, string, Identifier, const SystemModel &, const config_t &)


#endif /* PROXYILPSOLVER_H */

