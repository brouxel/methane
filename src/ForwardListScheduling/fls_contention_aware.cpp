/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ForwardListScheduling.h"

using namespace std;

class FLSContentionAware : public FLSStrategy {
public:
    
    explicit FLSContentionAware(const SystemModel &m, const config_t &c) : FLSStrategy(m, c) {}
    
    virtual uint32_t compute_read_concurrency(Schedule_t *sched, const vector<Task*> &Qdone, Task *t) {
        
        sched->tasks[t->id].__RR_conc_ = 0;
        sched->tasks[t->id].__RW_conc_ = 0;
        
        if(sched->tasks[t->id].is_in_mutex)
            return 0;
        
        uint64_t rtR = sched->tasks[t->id].rtR();
        uint64_t endTimeR = sched->tasks[t->id].rtR() + sched->tasks[t->id].delayR();
        
        for(Task *e : Qdone) {
            if(e == t || sched->tasks[t->id].proc == sched->tasks[e->id].proc || sched->tasks[e->id].is_in_mutex) continue;

            uint64_t eRtR = sched->tasks[e->id].rtR();
            uint32_t eDelayR = sched->tasks[e->id].delayR();
            uint64_t eRtE = eRtR + eDelayR;
            uint64_t eRtW = sched->tasks[e->id].rtW();
            uint32_t eDelayW = sched->tasks[e->id].delayW();
            uint64_t eEndTime = eRtW + eDelayW;

            if(eDelayR > 0 && eRtR < endTimeR && rtR < eRtE)
                sched->tasks[t->id].__RR_conc_++;
                
            if(eDelayW > 0 && eRtW < endTimeR && rtR < eEndTime)
                sched->tasks[t->id].__RW_conc_++;
        }
        return sched->tasks[t->id].__RR_conc_ + sched->tasks[t->id].__RW_conc_;
    }
    
    virtual uint32_t compute_write_concurrency(Schedule_t *sched, const vector<Task*> &Qdone, Task *t) {
        sched->tasks[t->id].__WW_conc_ = 0;
        sched->tasks[t->id].__WR_conc_ = 0;
        
        if(sched->tasks[t->id].is_in_mutex)
            return 0;
        
        uint64_t rtW = sched->tasks[t->id].rtW();
        uint64_t endtime = sched->tasks[t->id].rtW() + sched->tasks[t->id].delayW();

        for(Task *e : Qdone) {
            if(e == t || sched->tasks[t->id].proc == sched->tasks[e->id].proc || sched->tasks[e->id].is_in_mutex) continue;
            
            uint64_t eRtR = sched->tasks[e->id].rtR();
            uint32_t eDelayR = sched->tasks[e->id].delayR();
            uint64_t eRtE = eRtR + eDelayR;
            uint64_t eRtW = sched->tasks[e->id].rtW();
            uint32_t eDelayW = sched->tasks[e->id].delayW();
            uint64_t eEndTime = eRtW + eDelayW;

            //WW
            if(eDelayW > 0 && eRtW < endtime && rtW < eEndTime)
                sched->tasks[t->id].__WW_conc_++;
            
            //WR
            if(eDelayR > 0 && eRtR < endtime && rtW < eRtE) 
                sched->tasks[t->id].__WR_conc_++;
        }
        return sched->tasks[t->id].__RR_conc_ + sched->tasks[t->id].__RW_conc_;
    }
    
    void delay_task(Schedule_t *sched, const vector<Task*> Qdone, Task *t) {
        uint32_t dataR = this->compute_data_read(sched, t, sched->tasks[t->id].proc, Qdone);
        uint32_t concR = compute_read_concurrency(sched, Qdone, t);
        uint32_t dataW = this->compute_data_written(sched, t, sched->tasks[t->id].proc, Qdone);
        uint32_t concW = compute_write_concurrency(sched, Qdone, t);
        string datatypeR = (t->previous_datatypes.size() > 0) ? (*(t->previous_datatypes.begin())).second : "void";
        string datatypeW = (t->successors.size() > 0) ? t->successors.at(0)->previous_datatypes[t] : "void";
        map_task_proc(sched, sched->tasks[t->id].proc, t,
            dataR, datatypeR, concR, tg.interconnect()->comm_delay(concR, dataR, datatypeR, t->force_read_delay),
            dataW, datatypeW, concW, tg.interconnect()->comm_delay(concW, dataW, datatypeW, t->force_write_delay)
        );
                        
        uint64_t bestStartR = sched->tasks[t->id].rtR();
        uint64_t bestStartE = sched->tasks[t->id].rtE;
        uint64_t bestStartW = sched->tasks[t->id].rtW();
        
        vector<Task*> onSameCore = build_samecore_taskset(*sched, Qdone, t); 
        vector<Task*> onDiffCore = build_diffcore_taskset(*sched, Qdone, t); 
        vector<Task*> allToMe = build_allbeforeme_taskset(*sched, Qdone, t);
        
        vector<Task*> onDiffCoreSync(onDiffCore.size());
        vector<Task*>::iterator it = copy_if(onDiffCore.begin(), onDiffCore.end(), onDiffCoreSync.begin(), [sched, t](Task* a) { return sched->tasks[a->id].is_in_mutex; });
        onDiffCoreSync.resize(distance(onDiffCoreSync.begin(), it));
                
        if(!sched->tasks[t->id].is_in_mutex) {
            // ensure a no sync communication does not overlap with a sync one on a different core
            bestStartR = ensure_read_mutex(*sched, onDiffCoreSync, t, bestStartR);

            uint64_t rtW = bestStartR + sched->tasks[t->id].delayR() + t->C;
            uint64_t initW = rtW;
            rtW = ensure_write_mutex(*sched, onDiffCoreSync, t, rtW);
            bestStartR += (rtW - initW);

            // ensure the whole task does not overlap on the same core with an other task
            bestStartR = ensure_task_mutex(*sched, onSameCore, t, bestStartR);
        }
        else {
            bestStartR = ensure_read_mutex(*sched, allToMe, t, bestStartR);

            // ensure the new start guaranty a synchronization between the write phase and other phases
            uint64_t rtW = bestStartR + sched->tasks[t->id].delayR() + t->C;
            uint64_t initW = rtW;
//                    rtW = ensure_write_mutex(*sched, onDiffCore, t, rtW);
            rtW = ensure_write_mutex(*sched, allToMe, t, rtW);
            // no need to check on the same core due to the "blocking" mode, if a read phase overlaps at that time it will be delayed later
            bestStartR += (rtW - initW);

            bestStartR = ensure_task_mutex(*sched, onSameCore, t, bestStartR);
        }
        bestStartE = bestStartR+sched->tasks[t->id].delayR();
        bestStartW = bestStartE+t->C;
        
        map_task_proc(sched, sched->tasks[t->id].proc, t, bestStartR, bestStartE, bestStartW);
    }

    void adjust_schedule(Schedule_t *sched, const vector<Task*> &Qdone, Task *current) {
        /*/
        vector<Task*> copy;
        build_related(sched, sortedQdone, current, &copy);
        if(find(copy.begin(), copy.end(), current) == copy.end())
            copy.push_back(current);
        /*/
        vector<Task*> copy(Qdone);
        sort(copy.begin(), copy.end(), [sched](Task *a, Task *b) {return sched->tasks[a->id].rtR() < sched->tasks[b->id].rtR();});
        //*/
        
        for(Task::prev_t el : current->previous) {
            Task *t = el.first;
            uint32_t dataR = this->compute_data_read(sched, t, sched->tasks[t->id].proc, Qdone);
            uint32_t concR = compute_read_concurrency(sched, Qdone, t);
            uint32_t dataW = this->compute_data_written(sched, t, sched->tasks[t->id].proc, Qdone);
            uint32_t concW = compute_write_concurrency(sched, Qdone, t);
            string datatypeR = (t->previous_datatypes.size() > 0) ? (*(t->previous_datatypes.begin())).second : "void";
            string datatypeW = (t->successors.size() > 0) ? t->successors.at(0)->previous_datatypes[t] : "void";
            map_task_proc(sched, sched->tasks[t->id].proc, t,
                dataR, datatypeR, concR, tg.interconnect()->comm_delay(concR, dataR, datatypeR, t->force_read_delay),
                dataW, datatypeW, concW, tg.interconnect()->comm_delay(concW, dataW, datatypeW, t->force_write_delay)
            );
        }
        
        /*for(Task *t : copy) {
            uint64_t bestStart = find_best_start_read(*sched, Qdone, t);
            map_task_proc(sched, sched->tasks[t->id].proc, t, bestStart);

            bestStart = find_best_start_exec(*sched, Qdone, t);
            map_task_proc(sched, sched->tasks[t->id].proc, t, 
                sched->tasks[t->id].rtR, bestStart, bestStart+t->C
            );

            bestStart = find_best_start_write(*sched, Qdone, t);
            map_task_proc(sched, sched->tasks[t->id].proc, t, 
                sched->tasks[t->id].rtR, sched->tasks[t->id].rtE, bestStart
            );
        }*/
        uint64_t bestStart = find_best_start_read(*sched, Qdone, current);
        map_task_proc(sched, sched->tasks[current->id].proc, current, bestStart, bestStart+sched->tasks[current->id].delayR(), bestStart+sched->tasks[current->id].delayR()+current->C);

        bestStart = find_best_start_exec(*sched, Qdone, current);
        map_task_proc(sched, sched->tasks[current->id].proc, current, 
            sched->tasks[current->id].rtR(), bestStart, bestStart+current->C
        );

        bestStart = find_best_start_write(*sched, Qdone, current);
        map_task_proc(sched, sched->tasks[current->id].proc, current, 
            sched->tasks[current->id].rtR(), sched->tasks[current->id].rtE, bestStart
        );
        
        bool has_changed = true;
        while(has_changed) {
            has_changed = false;
            for(Task *t : copy) {
                uint64_t oldstart = sched->tasks[t->id].rtR();
                uint64_t oldend = sched->tasks[t->id].end();

                delay_task(sched, Qdone, t);
                has_changed = has_changed || oldstart != sched->tasks[t->id].rtR() || oldend != sched->tasks[t->id].end();
            }
        }
    }
    
    void chose_proc(Schedule_t *sched, const vector<Task*> &Qdone, Task *t) {
        Schedule_t schedOverlap = *sched, schedMutex = *sched;
        bool foundOverlapSched = false, foundMutexSched = false;
        
        try {
            FLSStrategy::chose_proc(&schedOverlap, Qdone, t);
            schedOverlap.makespan = retrieve_schedule_length(schedOverlap);
            foundOverlapSched = true;
        }
        catch(Unschedulable) {}
        
        try {
            schedMutex.tasks[t->id].is_in_mutex = true;
            FLSStrategy::chose_proc(&schedMutex, Qdone, t);
            schedMutex.makespan = retrieve_schedule_length(schedMutex);
            foundMutexSched = true;
        }
        catch(Unschedulable) {}
        
        if(!foundOverlapSched && !foundMutexSched)
            throw Unschedulable(t, "Couldn't find a processor with good caracteristics");
        
        if((foundMutexSched && schedMutex.makespan <= schedOverlap.makespan) || !foundOverlapSched) {
            *sched = schedMutex;
            Utils::DEBUG("\t\tbest candidate mutex -- "+to_string(schedMutex.makespan));
        }
        else {
            *sched = schedOverlap;
            Utils::DEBUG("\t\tbest candidate overlap -- "+to_string(schedOverlap.makespan));
        }
        
        Utils::DEBUG(""+*sched);
    }
};

REGISTER_FLSSTRATEGY(FLSContentionAware, "contention_aware")
        