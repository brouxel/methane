/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ForwardListScheduling.h"

using namespace std;
class FLSIdeal : public FLSStrategy {
public:
    explicit FLSIdeal(const SystemModel &m, const config_t &c) : FLSStrategy(m, c) {}
    
    virtual uint32_t compute_read_concurrency(Schedule_t *sched, const vector<Task*> &, Task *t) {
        sched->tasks[t->id].__RR_conc_ = 0;
        sched->tasks[t->id].__RW_conc_ = 0;
        return 0;
    }
    virtual uint32_t compute_write_concurrency(Schedule_t *sched, const vector<Task*> &, Task *t) {
        sched->tasks[t->id].__RR_conc_ = 0;
        sched->tasks[t->id].__RW_conc_ = 0;
        return 0;
    }
    
    virtual uint32_t compute_data_read(Schedule_t *sched, Task *t, meth_processor_t *candidate, const vector<Task*> &Qdone) {
        return 0;
    }

    virtual uint32_t compute_data_written(Schedule_t *sched, Task *t, meth_processor_t *candidate, const vector<Task*> &Qdone) {
        return 0;
    }

    void adjust_schedule(Schedule_t *sched, const vector<Task*> &Qdone, Task *current) {
        /*/
        vector<Task*> copy;
        build_related(sched, sortedQdone, current, &copy);
        if(find(copy.begin(), copy.end(), current) == copy.end())
            copy.push_back(current);
        /*/
        vector<Task*> copy(Qdone);
        //*/
        
        
        for(Task::prev_t el : current->previous) {
            Task *t = el.first;
            uint32_t dataR = this->compute_data_read(sched, t, sched->tasks[t->id].proc, Qdone);
            uint32_t concR = compute_read_concurrency(sched, Qdone, t);
            uint32_t dataW = this->compute_data_written(sched, t, sched->tasks[t->id].proc, Qdone);
            uint32_t concW = compute_write_concurrency(sched, Qdone, t);
            string datatypeR = (t->previous_datatypes.size() > 0) ? (*(t->previous_datatypes.begin())).second : "void";
            string datatypeW = (t->successors.size() > 0) ? t->successors.at(0)->previous_datatypes[t] : "void";
            map_task_proc(sched, sched->tasks[t->id].proc, t,
                dataR, datatypeR, concR, tg.interconnect()->comm_delay(concR, dataR, datatypeR, t->force_read_delay),
                dataW, datatypeW, concW, tg.interconnect()->comm_delay(concW, dataW, datatypeW, t->force_write_delay)
            );
        }
        
        for(Task *t : copy) {
            uint64_t bestStart = find_best_start_read(*sched, Qdone, t);
            map_task_proc(sched, sched->tasks[current->id].proc, current, bestStart, bestStart+sched->tasks[current->id].delayR(), bestStart+sched->tasks[current->id].delayR()+current->C);

            bestStart = find_best_start_exec(*sched, Qdone, t);
            map_task_proc(sched, sched->tasks[t->id].proc, t, 
                sched->tasks[t->id].rtR(), bestStart, bestStart+t->C
            );

            bestStart = find_best_start_write(*sched, Qdone, t);
            map_task_proc(sched, sched->tasks[t->id].proc, t, 
                sched->tasks[t->id].rtR(), sched->tasks[t->id].rtE, bestStart
            );
        }
    }
};
REGISTER_FLSSTRATEGY(FLSIdeal, "ideal")