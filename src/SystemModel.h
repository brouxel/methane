/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TASKMODEL_H
#define TASKMODEL_H

#include <string>
#include <vector>
#include <map>
#include <tuple>
#include <utility>
#include <cstdio>
#include <exception>
#include <algorithm>
#include <set>
#include <cassert>

#include <boost/range/adaptor/map.hpp>
#include <boost/range/algorithm/copy.hpp>
#include <boost/assign.hpp>

#include "config.h"
#include "CoreMemInterConnect.h"

using namespace std;

struct meth_processor_t {
    string id;
    uint64_t spm_size = -1;
};

class Task {
public:
    string id;
    uint64_t C = 0;
    uint32_t D = 0; // represent the relative deadline for a task, represent the absolute deadline for a job
    uint32_t T = 0; // represent the relative period for a task, represent the absolute activation for a job
    string ilp_label;
    static size_t task_counter; //count the number of tasks to add an unique identifier at the end of the ilp_label
    
    uint64_t local_memory_storage = 0; //! value reflecting amount of data stored in local memory 
    uint64_t _data_read = 0; // in bits
    uint64_t _data_written = 0; // in bits
    uint64_t memory_code_size = 0;
    uint64_t memory_footprint = 0;

    //! tasks that need to be previously done 
    // value: data exchanged from prev to this
    typedef pair<Task *, uint32_t> prev_t;
    typedef map<Task *, uint32_t>::iterator prev_iter;
    map<Task*, uint32_t> previous;
    vector<Task*> previous_transitiv;
    map<Task*, std::string> previous_datatypes;
    
    vector<Task*> successors;
    vector<Task*> successors_transitiv;
    
    //force mapping and order
    string force_mapping_proc = "";
    int force_sched_order_proc = -1;
    uint64_t force_read_delay = 0;
    uint64_t force_write_delay = 0;

    //@experimental
    bool is_job = false; //true if this represents a job instance, false if it is a task
    Task *meta_task = nullptr; //save the initial task before the expansion to jobs, will see if it is needed some day
    vector<vector<prev_t>> precedencies_gp; //contains the list of precedencies group
                                                 //if task_i precedes task_j then all jobs of task_i must precedes jobs of task_j
                                                 //we need to have them grouped to allow the solver to chose to respect one of these 
                                                 //dependency in mutex, see forget2010scheduling section IV
            
    Task();
    Task(const Task& rhs) { clone(rhs); };
    Task& operator=(const Task &rhs) { clone(rhs); return *this; };
    void clone(const Task &rhs);
    
    void add_previous(Task* prev, uint32_t nbtoks, std::string datatype);
    
    bool is_parallel(const Task *b) const;
    
    inline uint32_t data_read() const { return _data_read; };
    inline uint32_t data_written() const {return _data_written; };
    uint32_t data_read(const Task& from) const;
    uint32_t data_written(const Task& to) const;
};

class SystemModel {
    friend class TgXmlSaxParser;
    
private:
    bool _is_periodic_model = false; //@experimental
    uint64_t _hyperperiod = 0;
    vector<Task*> _tasks;
    CoreMemInterConnect *_interconnect = NULL;
    vector<meth_processor_t*> _processors;
    vector<string> _memory;
    double _global_deadline = 0;
    
    void topological_sort_aux(uint32_t,vector<Task*>*);
    void compute_transitive_closure_aux(Task *tc, vector<Task*> *);
    
public:
    SystemModel();
    virtual ~SystemModel();
    
    inline const vector<Task*>& tasks() const { return _tasks; }
    inline vector<Task*>& tasks() { return _tasks; }
    inline const vector<meth_processor_t*>& processors() const { return _processors; }
    inline vector<meth_processor_t*>& processors() { return _processors; }
    inline const vector<string>& memory_regions() const { return _memory; }
    inline vector<string>& memory_regions() { return _memory; }
    
    inline const double global_deadline() const { return _global_deadline; }
    inline double global_deadline() { return _global_deadline; }
    
    Task* task(const string &id) const;
    
    inline CoreMemInterConnect* interconnect() const { return _interconnect; }
    
    void clean_previous();
    
    void compute_transitive_closure();
    
    void populate_memory_usage();
    
    void build_memory_model();
    
    //@experimental
    uint64_t get_hyper_period() const {return _hyperperiod; }
    bool is_periodic_model() const {return _is_periodic_model; }
    void expand_periodic_model();
    
    void compute_sequential_schedule_length();
};

/******************************************************************************/

ostream& operator<< (ostream& os, const Task &a);
ostream& operator<< (ostream& os, const Task *a);
string operator+ (const string &a, const Task &b);
string operator+ (const Task &b, const string &a);
string operator+ (const string &a, const Task *b);
string operator+ (const Task *b, const string &a);
string operator+ (const char* a, const Task &b);
string operator+ (const Task &b, const char* a);

bool operator== (const Task *a, const Task &b);
bool operator== (const Task &a, const Task *b);
bool operator== (const Task &a, const Task &b);

bool operator!= (const Task *a, const Task &b);
bool operator!= (const Task &a, const Task *b);
bool operator!= (const Task &a, const Task &b);

//------------------------------------------------------------------------------

ostream& operator<< (ostream& os, const meth_processor_t& a);
ostream& operator<< (ostream& os, const meth_processor_t* a);
string operator+ (const string &a, const meth_processor_t &b);
string operator+ (const meth_processor_t &b, const string &a);
string operator+ (const string &a, const meth_processor_t *b);
string operator+ (const meth_processor_t *b, const string &a);
string operator+ (const char* a, const meth_processor_t &b);
string operator+ (const meth_processor_t &b, const char* a);

bool operator== (const meth_processor_t *a, const meth_processor_t &b);
bool operator== (const meth_processor_t &a, const meth_processor_t *b);
bool operator== (const meth_processor_t &a, const meth_processor_t &b);

bool operator!= (const meth_processor_t *a, const meth_processor_t &b);
bool operator!= (const meth_processor_t &a, const meth_processor_t*b);
bool operator!= (const meth_processor_t &a, const meth_processor_t &b);

/******************************************************************************/

#endif /* TASKMODEL_H */
