/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SCHEDULE_H
#define SCHEDULE_H

#include <string>
#include <sstream>
#include <map>

#include "config.h"
#include "SystemModel.h"
#include "Utils.h"

struct spm_region_info {
    string label = "";
    long size = 0;
    uint64_t startresa = 0;
    uint64_t endresa = 0;
    
    spm_region_info& operator=(const spm_region_info& rhs) {
        label = rhs.label;
        size = rhs.size;
        startresa = rhs.startresa;
        endresa = rhs.endresa;
        
        return *this;
    }
    spm_region_info(const spm_region_info& rhs) {
      *this = rhs;
    }
    spm_region_info() {}
    spm_region_info(const string &lbl) : label(lbl) {}
    spm_region_info(const string &lbl, long s) : label(lbl),size(s) {}
};

bool operator== (const spm_region_info &a, const string &b);
bool operator== (const spm_region_info &a, const spm_region_info &b);

struct transmission_phase_info {
    uint64_t rt = 0;
    uint32_t data = 0;
    string datatype = "";
    uint32_t delay = 0;
    uint32_t conc = 0;
    spm_region_info spm;
    string type = ""; //read / write
    uint32_t packetnum = 1;
    uint32_t packetnumber = 1;
    
    transmission_phase_info& operator=(const transmission_phase_info& rhs);
    transmission_phase_info(const transmission_phase_info& rhs) {
      *this = rhs;
    }
    transmission_phase_info() {}
    transmission_phase_info(std::string t, uint64_t r, uint32_t de, uint32_t da, string dt) : type(t), rt(r), delay(de), data(da), datatype(dt) {}
};

struct proc_schedule_info {
    uint32_t total_spm_size = 0;
    uint32_t available_spm_size = -1;
    std::vector<spm_region_info> memory_regions;
    
    proc_schedule_info& operator=(const proc_schedule_info& rhs) {
        total_spm_size = rhs.total_spm_size;
        available_spm_size = rhs.available_spm_size;
        memory_regions = rhs.memory_regions;
        return *this;
    }
};

struct task_schedule_info {
    bool is_in_mutex = false;
    uint64_t rtE = 0;
    uint64_t WCET = 0;
    meth_processor_t *proc = NULL;
    uint32_t __RR_conc_ = 0;
    uint32_t __WW_conc_ = 0;
    uint32_t __RW_conc_ = 0;
    uint32_t __WR_conc_ = 0;
    
    struct {
      map<std::string, spm_region_info> read;
      spm_region_info exec;
      map<std::string, spm_region_info> write;
    } memory_region;
    
    // related taskid
    map<std::string, vector<transmission_phase_info>> reads;
    map<std::string, vector<transmission_phase_info>> writes;
    
    task_schedule_info& operator=(const task_schedule_info& rhs);
    
    uint64_t rtR() const;
    uint64_t rtW() const;
    uint64_t end() const;
    
    uint32_t delayR() const;
    uint32_t delayW() const;
    uint32_t dataR() const;
    uint32_t dataW() const;
    string datatypeR() const;
    string datatypeW() const;
};

struct Schedule_t {
    map<string, task_schedule_info> tasks;
    map<string, proc_schedule_info> procs;
    
    float solvetime;
    double makespan;
    int status = 0;
    
    double heur_DFSDelay = 0;
    double heur_DFS = 0;
    double heur_BFS = 0;
    
    Schedule_t(): solvetime(0), makespan(0), status(0) {}
    Schedule_t(const Schedule_t &rhs) {
        *this = rhs;
    }
    
    Schedule_t& operator= (const Schedule_t &rhs);
};

class Unschedulable : public std::exception {
    string msg;
    Task *t;
    
public:
    explicit Unschedulable(Task *a, string m) : msg(m), t(a) {
        msg = "Unschedulable at task "+t->id+" : "+m;
    }
    explicit Unschedulable(string m) : msg(m) {
    }
    
    Unschedulable(const Unschedulable& e) {
        msg = e.msg;
        t = e.t;
    }
    
    virtual ~Unschedulable() NOEX {
    }
    
    virtual const char* what() const NOEX override {
        return msg.c_str();
    }
};

class UnschedulableSPMSpace : public Unschedulable {
public:
    explicit UnschedulableSPMSpace(Task *a, string m) : Unschedulable(a,m) {}
    explicit UnschedulableSPMSpace(string m) : Unschedulable(m) {}
    UnschedulableSPMSpace(const Unschedulable& e) : Unschedulable(e) {}
    virtual ~UnschedulableSPMSpace() NOEX {}
}; 

ostream& operator<< (ostream& os, const task_schedule_info &inf);
ostream& operator<< (ostream& os, const Schedule_t &inf);

string operator+ (const string &a, const task_schedule_info &b);
string operator+ (const task_schedule_info &b, const string &a);
string operator+ (const string &a, const task_schedule_info *b);
string operator+ (const task_schedule_info *b, const string &a);
string operator+ (const char* a, const task_schedule_info &b);
string operator+ (const task_schedule_info &b, const char* a);

string operator+ (const string &a, const Schedule_t &b);
string operator+ (const Schedule_t &b, const string &a);
string operator+ (const string &a, const Schedule_t *b);
string operator+ (const Schedule_t *b, const string &a);
string operator+ (const char* a, const Schedule_t &b);
string operator+ (const Schedule_t &b, const char* a);

uint64_t retrieve_schedule_length(const Schedule_t &sched);

void map_task_proc(Schedule_t *sched, meth_processor_t *proc, Task * t, uint64_t release_R, uint64_t release_E, uint64_t release_W);

void map_task_proc(Schedule_t *sched, meth_processor_t *proc, Task * t, 
            uint32_t data_R, string datatypeR, uint32_t conc_R, uint32_t delay_R,
        uint32_t data_W, string datatypeW, uint32_t conc_W, uint32_t delay_W);
void map_task_proc(Schedule_t *sched, meth_processor_t *proc, Task * t, uint64_t wcet, 
        uint64_t release_R, uint32_t data_R, string datatypeR, uint32_t conc_R, uint32_t delay_R,
        uint64_t release_E, 
        uint64_t release_W, uint32_t data_W, string datatypeW, uint32_t conc_W, uint32_t delay_W);

void map_task_proc(Schedule_t *sched, meth_processor_t *proc, Task * t, map<string, vector<transmission_phase_info>> reads, uint64_t release_E, map<string, vector<transmission_phase_info>> writes);

const vector<Task*> build_samecore_taskset(const Schedule_t &sched, const vector<Task*> &Qdone, Task *t);
const vector<Task*> build_diffcore_taskset(const Schedule_t &sched, const vector<Task*> &Qdone, Task *t);
const vector<Task*> build_allbeforeme_taskset(const Schedule_t &sched, const vector<Task*> &Qdone, Task *t);
const vector<Task*> build_allafterme_taskset(const Schedule_t &sched, const vector<Task*> &Qdone, Task *t);

void assign_spm_region(Schedule_t* sched, const string &proc, const string &label, uint32_t size);
#endif /* SCHEDULE_H */

