/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPRAWMARKER(
#include <stdio.h>
#include <stdlib.h>
#include <mppaipc.h>
#include <HAL/hal/hal.h>
#include <time.h>
#include <sys/time.h>
#include <stdint.h>
#include <mppa/osconfig.h>
#include <mppa_power.h>
#include <utask.h>

#include "shared_defs.h"

#define CLUSTER_COUNT 1

%BUFFERS%

static int channel_read_fd ;
static int channel_write_fd;
static const char *exe_name = "%CLUSTERAPPNAME%";

static inline void storemem(void volatile *data, size_t size) {
    mppa_read(channel_read_fd, (void *)data, size);
}
static inline void sendcluster(void volatile *data, size_t size) {
    mppa_write(channel_write_fd, (void *)data, size);
}

void init_schedule_comm() {
    float warmup[100];
    mppa_read(channel_read_fd, warmup, 100*sizeof(float));
    mppa_write(channel_write_fd, warmup, 100*sizeof(float));

    %INITSCHEDULECOMM%
}


void schedule_comm() {
    for( int i=0 ; i < MAX_ITERATION ; i++ ) {
%SCHEDULECOMM%
    }
}

int main() {
    long long dummy = 0;
    long long match = -(1 << CLUSTER_COUNT);
    const char *root_sync = SPAWN_SINK_CHAN;
    int root_sync_fd = mppa_open(root_sync, O_RDONLY);
    int status = root_sync_fd;
    
    status |= mppa_ioctl(root_sync_fd, MPPA_RX_SET_MATCH, match);
    
    // read/write inversed in IO cluster than on PE
    channel_read_fd = mppa_open(CHANNEL_WRITE, O_RDONLY);
    channel_write_fd = mppa_open(CHANNEL_READ, O_WRONLY);
    
    int pid0 = -1;
    // MPPA_POWER_SHUFFLING_DISABLED configure la mémoire en mode banked
    if ((pid0 = mppa_power_base_spawn(0, exe_name, NULL, NULL, MPPA_POWER_SHUFFLING_DISABLED)) < 0) {
        printf("Spawn failed cluster 0.\n");
        return -1;
    }
    printf("Spawn launched. %llu\n",__k1_read_dsu_timestamp());
    
    // wait for CLUSTER_COUNT clusters to end spawning
    status |= mppa_read(root_sync_fd, &dummy, sizeof(dummy));
    
    init_schedule_comm();
    schedule_comm();
    
    if(mppa_waitpid(pid0, &status, 0) < 0) {
        printf("Waitpid failed on cluster 0.\n");
        exit(1);
    }

    return status;
}
)CPPRAWMARKER"