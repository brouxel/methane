/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPRAWMARKER(
#include "cluster.h"
#include <mppa/osconfig.h>

volatile uint64_t world_origin;
int channel_write_fd;
int channel_read_fd;

int main(int argc __attribute__ ((unused)), char *argv[]) {
    utask_timer64_set_time(INT64_MAX);
    mOS_dcache_disable();
    mOS_icache_disable();
    mOS_it_disable();

    printf("Enter cluster %d main on proc %d -- %llu\n", __k1_get_cluster_id(), __k1_get_cpu_id(),__k1_read_dsu_timestamp());
    channel_read_fd = mppa_open(CHANNEL_READ, O_RDONLY);
    channel_write_fd = mppa_open(CHANNEL_WRITE, O_WRONLY);

    utask_barrier_t mybarrier;
    utask_barrier_init(&mybarrier, NULL, %NBTHREADS%+1); // +1 for core0 whose schedules comm
    
    // Notify IO cluster, PE cluster is up and running
    int root_sync_fd = mppa_open(SPAWN_SINK_CHAN, O_WRONLY);
    int status = root_sync_fd;
    long long mask = (long long)1 << __k1_get_cluster_id();
    status |= mppa_write(root_sync_fd, &mask, sizeof(mask));

    init_bench(channel_read_fd, channel_write_fd);

    //*
    %LAUCHSCHEDULEPE%
    
    schedule_comm(&mybarrier);

    %JOINSCHEDULEPE%
    /*/
    sequential_schedule();
    //*/
    
    utask_barrier_destroy(&mybarrier);
    mppa_close(channel_read_fd);
    mppa_close(channel_write_fd);
    
    printf("All done %llu\n",__k1_read_dsu_timestamp());
    
    return 0;
}

)CPPRAWMARKER"