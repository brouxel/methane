/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPRAWMARKER(
typedef struct {
 volatile   uint32_t head;
 volatile   uint32_t tail;
 volatile   %TYPE% buffer[%SIZE%];
} buffer_%TYPE%%SIZE%_t;
static inline void init_buffer_%TYPE%%SIZE%(buffer_%TYPE%%SIZE%_t volatile *buffer) {
    buffer->head = buffer->tail = 0;
}
static inline %TYPE% peek_%TYPE%%SIZE%(buffer_%TYPE%%SIZE%_t volatile *buffer, int offset) {
    return (buffer->buffer)[(buffer->tail+offset)%%SIZE%];
}
static inline %TYPE% pop_%TYPE%%SIZE%(buffer_%TYPE%%SIZE%_t volatile *buffer) {
    register %TYPE% res = (buffer->buffer)[buffer->tail];
    buffer->tail = (buffer->tail+1)%%SIZE%;
    return res;
}
static inline void push_%TYPE%%SIZE%(buffer_%TYPE%%SIZE%_t volatile *buffer, %TYPE% val) {
    (buffer->buffer)[buffer->head] = val;
    buffer->head = (buffer->head+1)%%SIZE%;
}
static inline void insert_%TYPE%%SIZE%(buffer_%TYPE%%SIZE%_t volatile *buffer, %TYPE% val, int offset) {
    (buffer->buffer)[buffer->tail+offset] = val;
}
)CPPRAWMARKER"