/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPRAWMARKER(
#ifndef CLUSTERHEADER_H
#define CLUSTERHEADER_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <vbsp.h>
#include <utask.h>

#include <mppaipc.h>

#include <HAL/hal/hal.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/types.h>
#include <math.h>
#include <signal.h>

#include <limits.h>

#include "shared_defs.h"

extern int channel_read_fd;
static inline void fetch(volatile void* data, size_t size) {
    mppa_read(channel_read_fd, (void*) data, size);
}
extern int channel_write_fd;
static inline void store(volatile void* data, size_t size){
    mppa_write(channel_write_fd, (void*)data, size);
}

void init_bench(int channel_read_fd, int channel_write_fd);
void schedule_comm(utask_barrier_t *mybarrier);
void sequential_schedule();
void close_bench();


%SCHEDULEPES%

#endif /* CLUSTERHEADER_H */

)CPPRAWMARKER"