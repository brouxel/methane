/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPRAWMARKER(
board := developer
system-name := bare
arch := k1b

K1_CLUSTER_APP_NAME := %CLUSTERAPPNAME%

%CLUSTERAPPNAME%-srcs := %CLUSTERFILES%
%CLUSTERAPPNAME%-cflags := -O0 -std=gnu99 -mhypervisor -g -Wno-unused -Wno-unused-parameter -Wno-shadow
%CLUSTERAPPNAME%-lflags := -O0 -Wl,-dTldscript_cluster.ld -mhypervisor -Wl,--defsym=USER_STACK_SIZE=0x2000
%CLUSTERAPPNAME%-lflags += -lm -lvbsp -lutask -lmppapower -lmppanoc -lmpparouting -lmppaipc
#%CLUSTERAPPNAME%-lflags += -Wl,-Map=./link_map_%CLUSTERAPPNAME%.txt
%CLUSTERAPPNAME%-lflags += -Wl,--strip-debug

io-bin := iobin
iobin-srcs := %IOFILES%
iobin-cflags := -O0 -std=gnu99 -g -DMPPA_TRACE_ENABLE -Wall
iobin-lflags := -O0 -lmppaipc -lmppapower -lmppanoc -lmpparouting 

mppa-bin := multibin

multibin-objs = iobin $(K1_CLUSTER_APP_NAME)
cluster-bin :=  $(K1_CLUSTER_APP_NAME)


include $(K1_TOOLCHAIN_DIR)/share/make/Makefile.kalray

.PHONY: run
run: all
	$(K1_TOOLCHAIN_DIR)/bin/k1-jtag-runner --multibinary=./output/bin/multibin.mpk --exec-multibin=IODDR0:iobin

.PHONY: clean2
clean2:
	rm -Rf output
	rm -f ../tasks/*.o

.PHONY: info
info:
	k1-objdump -t output/bin/$(K1_CLUSTER_APP_NAME) | grep _bank

)CPPRAWMARKER"