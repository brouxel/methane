/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPRAWMARKER(
  .mOS K1_BOOT_ADDRESS : AT ( K1_BOOT_ADDRESS )
  {
     /* KEEP(*(.mOS.*)) */
  }

  .boot VIRT_U_MEM_START :  AT (ALIGN ( LOADADDR(.mOS) + MOS_RESERVED, 0x1000))
  {
  }  =0x12345678


  .locked_text ALIGN(0x1000) : AT ( ALIGN( LOADADDR(.boot) + SIZEOF(.boot), 0x1000))
  {
  }  =0x12345678
  _debug_start_lma  = LOADADDR( .locked_text ) + _debug_start - __text_begin;

  .text  ALIGN(0x100) : AT ( ALIGN( LOADADDR(.locked_text) + SIZEOF(.locked_text), 0x100))
  {
  }

  .stack   ALIGN(0x1000) :  AT ( ALIGN( LOADADDR(.text) + SIZEOF(.text) , 0x1000))
  {
  }

  /* This is the value of the GP base */
  .rodata   ALIGN(0x1000) :  AT ( ALIGN( LOADADDR(.stack) + SIZEOF(.stack), 0x1000))  {
  } =0x12345678

  .locked_data ALIGN(0x1000) : AT ( ALIGN( LOADADDR(.rodata) + SIZEOF(.rodata), 0x1000))
  {
  }  =0x12345678

  .bsp_config ALIGN(0x8) : AT ( ALIGN( LOADADDR(.locked_data) + SIZEOF(.locked_data), 0x8))
  {
  }

  MPPA_ARGAREA_SIZE  = 0x1000;
  .scoreboard ALIGN(0x100) :  AT ( ALIGN( LOADADDR(.bsp_config) + SIZEOF(.bsp_config), 0x100))
  {
  }

  MPPA_ARGAREA_FILE_OFFSET  =  LOADADDR( .scoreboard ) + MPPA_ARGAREA_START - _scoreboard_start ;
  
  /* Section used with dynamic executable */  
  .interp ALIGN(0x8):   AT ( ALIGN( LOADADDR(.scoreboard) + SIZEOF(.scoreboard), 0x8))
  {
  }
  .dynstr ALIGN(0x8):   AT ( ALIGN( LOADADDR(.interp) + SIZEOF(.interp), 0x8))
  {
  }

  .hash ALIGN(0x8):   AT ( ALIGN( LOADADDR(.dynstr) + SIZEOF(.dynstr), 0x8))
  {
  }

  .plt ALIGN(0x8):   AT ( ALIGN (LOADADDR(.hash) + SIZEOF(.hash), 0x8))
  {
  }

  .got ALIGN(0x8):   AT ( ALIGN( LOADADDR(.plt) + SIZEOF(.plt), 0x8))
  {
  }

  .dynamic ALIGN(0x8):   AT ( ALIGN( LOADADDR(.got) + SIZEOF(.got), 0x8))
  {
  }

  .dynsym ALIGN(0x8):   AT ( ALIGN( LOADADDR(.dynamic) + SIZEOF(.dynamic), 0x8))
  {
  }

  .rela.dyn ALIGN(0x8):   AT ( ALIGN( LOADADDR(.dynsym) + SIZEOF(.dynsym), 0x8))
  {
  }

  .rela.plt ALIGN(0x8):   AT ( ALIGN( LOADADDR(.rela.dyn) + SIZEOF(.rela.dyn), 0x8))
  {
  }
  
  .data   ALIGN(0x8) :  AT ( ALIGN( LOADADDR(.rela.plt) + SIZEOF(.rela.plt), 0x8))
  {
  }  =0x12345678
  

  .bss   ALIGN(0x20) :  AT ( ALIGN( LOADADDR(.data) + SIZEOF(.data), 0x20))
  {
  } =0x12345678

  /* TLS support */
  .tdata   ALIGN(8) : AT ( ALIGN( LOADADDR(.bss) + SIZEOF(.bss), 8 ))
  {
  } =0x12345678
  . = _tdata_end;
  _tdata_size = _tdata_end - _tdata_start;
  
  .tbss   ALIGN(8) : AT ( ALIGN( LOADADDR(.tdata) + SIZEOF(.tdata), 8))
  {
  } =0x12345678
  . = _tbss_end;
  _tbss_size = _tbss_end - _tbss_start; 

  _tls_size = _tbss_size + _tdata_size;

)CPPRAWMARKER"