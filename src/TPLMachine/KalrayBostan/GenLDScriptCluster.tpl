/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPRAWMARKER(

/* Include board-specific definitions */
INCLUDE  "/usr/local/k1tools/k1-elf/board/developer/k1bdp/le/bare/vboard_u.ld"

__MPPA_BURN_TX = DEFINED(__MPPA_BURN_TX) ? __MPPA_BURN_TX : 7;
__MPPA_BURN_FDIR = DEFINED(__MPPA_BURN_FDIR) ? __MPPA_BURN_FDIR : 4;

DDR_START = 0x80000000;
VIRT_U_MEM_START = ALIGN((INTERNAL_RAM_BASE + MOS_RESERVED), 0x1000);
DDR_SIZE = 0x80000000;
VIRT_U_MEM_END   = INTERNAL_RAM_BASE + ABSOLUTE(INTERNAL_RAM_SIZE);
K1_EXCEPTION_ADDRESS = 0x0;
USER_STACK_SIZE = DEFINED(USER_STACK_SIZE) ? USER_STACK_SIZE : 0x800;
BOOTSTACK_SIZE  = DEFINED(BOOTSTACK_SIZE) ? BOOTSTACK_SIZE   : 0x100;
_VBSP_OVERRIDE_PE_MASK = DEFINED(_VBSP_OVERRIDE_PE_MASK) ? _VBSP_OVERRIDE_PE_MASK : 0xffff;
KSTACK_SIZE    = DEFINED(KSTACK_SIZE) ? KSTACK_SIZE : 0x400;
_MOS_SECURITY_LEVEL = DEFINED(_MOS_SECURITY_LEVEL) ? _MOS_SECURITY_LEVEL : (DEFINED(_LIBNOC_DISABLE_FIFO_FULL_CHECK) ? 0 : 1);
TOTAL_USER_STACK_SPACE = USER_STACK_SIZE * 16;

MPPA_DSM_CLIENT_SPAWNER_OVERRIDE = DEFINED(MPPA_DSM_CLIENT_SPAWNER_OVERRIDE) ? MPPA_DSM_CLIENT_SPAWNER_OVERRIDE : 0xff;

_UTASK_SMEM_HEAP_SIZE = DEFINED(_UTASK_SMEM_HEAP_SIZE) ? _UTASK_SMEM_HEAP_SIZE : 0;

EXTERN( _vstart,__bsp_global_desc, bin_descriptor, locked_assert_func, locked_memset, __bsp_get_router_id, __stack_overflow_detected, bsp_barrier, micro_interface_ctor, libnoc_ctor, __mppa_dsm_client_ctor, main, __proceed, mppa_power_base_exit, _scoreboard, __mppa_remote_client_init_ctor)
/* Definition of sections */
SECTIONS
{
INCLUDE "ldscript_cluster_prologue.ld"
   .tls_pe0 ALIGN(8) :  AT ( ALIGN( LOADADDR(.tbss) + SIZEOF(.tbss), 8))
   {
   } =0x12345678

   .tls_pe1 ALIGN(8) : AT ( ALIGN( LOADADDR(.tls_pe0) + SIZEOF(.tls_pe0), 8))
   {
   } =0x12345678

   .tls_pe2 ALIGN(8) : AT ( ALIGN( LOADADDR(.tls_pe1) + SIZEOF(.tls_pe1), 8))
   {
   } =0x12345678

   .tls_pe3 ALIGN(8) : AT ( ALIGN( LOADADDR(.tls_pe2) + SIZEOF(.tls_pe2), 8))
   {
   } =0x12345678

   .tls_pe4 ALIGN(8) : AT ( ALIGN( LOADADDR(.tls_pe3) + SIZEOF(.tls_pe3), 8))
   {
   } =0x12345678

   .tls_pe5 ALIGN(8) : AT ( ALIGN( LOADADDR(.tls_pe4) + SIZEOF(.tls_pe4), 8))
   {
   } =0x12345678

   .tls_pe6 ALIGN(8) : AT ( ALIGN( LOADADDR(.tls_pe5) + SIZEOF(.tls_pe5), 8))
   {
   } =0x12345678

   .tls_pe7 ALIGN(8) : AT ( ALIGN( LOADADDR(.tls_pe6) + SIZEOF(.tls_pe6), 8))
   {
   } =0x12345678

   .tls_pe8 ALIGN(8) : AT ( ALIGN( LOADADDR(.tls_pe7) + SIZEOF(.tls_pe7), 8))
   {
   } =0x12345678 

   .tls_pe9 ALIGN(8) : AT ( ALIGN( LOADADDR(.tls_pe8) + SIZEOF(.tls_pe8), 8))
   {
   } =0x12345678

   .tls_pe10 ALIGN(8) : AT ( ALIGN( LOADADDR(.tls_pe9) + SIZEOF(.tls_pe9), 8))
   {
   } =0x12345678

   .tls_pe11 ALIGN(8) : AT ( ALIGN( LOADADDR(.tls_pe10) + SIZEOF(.tls_pe10), 8))
   {
   } =0x12345678

   .tls_pe12 ALIGN(8) : AT ( ALIGN( LOADADDR(.tls_pe11) + SIZEOF(.tls_pe11), 8))
   {
   } =0x12345678

   .tls_pe13 ALIGN(8) : AT ( ALIGN( LOADADDR(.tls_pe12) + SIZEOF(.tls_pe12), 8))
   {
   } =0x12345678

   .tls_pe14 ALIGN(8) : AT ( ALIGN( LOADADDR(.tls_pe13) + SIZEOF(.tls_pe13), 8))
   {
   } =0x12345678

   .tls_pe15 ALIGN(8) : AT ( ALIGN( LOADADDR(.tls_pe14) + SIZEOF(.tls_pe14), 8))
   {
   } =0x12345678

   .uncached_data ALIGN(32768) : AT ( ALIGN(LOADADDR(.tls_pe15) + SIZEOF(.tls_pe15), 32768))
   {
   }

   _pag_zone_size = 0;
   _sbss_start = 0;
   _sbss_end = 0;
   _tls_rm_start = 0;

    .text_PE0 ALIGN(32K) : AT ( ALIGN(LOADADDR(.uncached_data) + SIZEOF(.uncached_data), 32K))
    {
        *(.text_PE0)
    }
    .data_PE0 ALIGN(32K) : AT ( ALIGN(LOADADDR(.text_PE0) + SIZEOF(.text_PE0), 32K))
    {
        *(.data_PE0)
        . = ALIGN(0x8);
        _user_stack_end0 = ABSOLUTE(.);
        . += USER_STACK_SIZE;
         _user_stack_start0   = ABSOLUTE(.);
    }

    .text_PE1 ALIGN(32K) : AT ( ALIGN(LOADADDR(.data_PE0) + SIZEOF(.data_PE0), 32K))
    {
        *(.text_PE1)
    }
    .data_PE1 ALIGN(32K) : AT ( ALIGN(LOADADDR(.text_PE1) + SIZEOF(.text_PE1), 32K))
    {
        *(.data_PE1)
        . = ALIGN(0x8);
        _user_stack_end0 = ABSOLUTE(.);
        . += USER_STACK_SIZE;
         _user_stack_start0   = ABSOLUTE(.);
    }

    .text_PE2 ALIGN(32K) : AT ( ALIGN(LOADADDR(.data_PE1) + SIZEOF(.data_PE1), 32K))
    {
        *(.text_PE2)
    }
    .data_PE2 ALIGN(32K) : AT ( ALIGN(LOADADDR(.text_PE2) + SIZEOF(.text_PE2), 32K))
    {
        *(.data_PE2)
        . = ALIGN(0x8);
        _user_stack_end0 = ABSOLUTE(.);
        . += USER_STACK_SIZE;
         _user_stack_start0   = ABSOLUTE(.);
    }

    .text_PE3 ALIGN(32K) : AT ( ALIGN(LOADADDR(.data_PE2) + SIZEOF(.data_PE2), 32K))
    {
        *(.text_PE3)
    }
    .data_PE3 ALIGN(32K) : AT ( ALIGN(LOADADDR(.text_PE3) + SIZEOF(.text_PE3), 32K))
    {
        *(.data_PE3)
        . = ALIGN(0x8);
        _user_stack_end0 = ABSOLUTE(.);
        . += USER_STACK_SIZE;
         _user_stack_start0   = ABSOLUTE(.);
    }

    .text_PE4 ALIGN(32K) : AT ( ALIGN(LOADADDR(.data_PE3) + SIZEOF(.data_PE3), 32K))
    {
        *(.text_PE4)
    }
    .data_PE4 ALIGN(32K) : AT ( ALIGN(LOADADDR(.text_PE4) + SIZEOF(.text_PE4), 32K))
    {
        *(.data_PE4)
        . = ALIGN(0x8);
        _user_stack_end0 = ABSOLUTE(.);
        . += USER_STACK_SIZE;
         _user_stack_start0   = ABSOLUTE(.);
    }

    .text_PE5 ALIGN(32K) : AT ( ALIGN(LOADADDR(.data_PE4) + SIZEOF(.data_PE4), 32K))
    {
        *(.text_PE5)
    }
    .data_PE5 ALIGN(32K) : AT ( ALIGN(LOADADDR(.text_PE5) + SIZEOF(.text_PE5), 32K))
    {
        *(.data_PE5)
        . = ALIGN(0x8);
        _user_stack_end0 = ABSOLUTE(.);
        . += USER_STACK_SIZE;
         _user_stack_start0   = ABSOLUTE(.);
    }

    .text_PE6 ALIGN(32K) : AT ( ALIGN(LOADADDR(.data_PE5) + SIZEOF(.data_PE5), 32K))
    {
        *(.text_PE6)
    }
    .data_PE6 ALIGN(32K) : AT ( ALIGN(LOADADDR(.text_PE6) + SIZEOF(.text_PE6), 32K))
    {
        *(.data_PE6)
        . = ALIGN(0x8);
        _user_stack_end0 = ABSOLUTE(.);
        . += USER_STACK_SIZE;
         _user_stack_start0   = ABSOLUTE(.);
    }

    .text_PE7 ALIGN(32K) : AT ( ALIGN(LOADADDR(.data_PE6) + SIZEOF(.data_PE6), 32K))
    {
        *(.text_PE7)
    }
    .data_PE7 ALIGN(32K) : AT ( ALIGN(LOADADDR(.text_PE7) + SIZEOF(.text_PE7), 32K))
    {
        *(.data_PE7)
        . = ALIGN(0x8);
        _user_stack_end0 = ABSOLUTE(.);
        . += USER_STACK_SIZE;
         _user_stack_start0   = ABSOLUTE(.);
    }

    .text_PE8 ALIGN(32K) : AT ( ALIGN(LOADADDR(.data_PE7) + SIZEOF(.data_PE7), 32K))
    {
        *(.text_PE8)
    }
    .data_PE8 ALIGN(32K) : AT ( ALIGN(LOADADDR(.text_PE8) + SIZEOF(.text_PE8), 32K))
    {
        *(.data_PE8)
        . = ALIGN(0x8);
        _user_stack_end0 = ABSOLUTE(.);
        . += USER_STACK_SIZE;
         _user_stack_start0   = ABSOLUTE(.);
    }

    .text_PE9 ALIGN(32K) : AT ( ALIGN(LOADADDR(.data_PE8) + SIZEOF(.data_PE8), 32K))
    {
        *(.text_PE9)
    }
    .data_PE9 ALIGN(32K) : AT ( ALIGN(LOADADDR(.text_PE9) + SIZEOF(.text_PE9), 32K))
    {
        *(.data_PE9)
        . = ALIGN(0x8);
        _user_stack_end0 = ABSOLUTE(.);
        . += USER_STACK_SIZE;
         _user_stack_start0   = ABSOLUTE(.);
    }

    .text_PE10 ALIGN(32K) : AT ( ALIGN(LOADADDR(.data_PE9) + SIZEOF(.data_PE9), 32K))
    {
        *(.text_PE10)
    }
    .data_PE10 ALIGN(32K) : AT ( ALIGN(LOADADDR(.text_PE10) + SIZEOF(.text_PE10), 32K))
    {
        *(.data_PE10)
        . = ALIGN(0x8);
        _user_stack_end0 = ABSOLUTE(.);
        . += USER_STACK_SIZE;
         _user_stack_start0   = ABSOLUTE(.);
    }

    .text_PE11 ALIGN(32K) : AT ( ALIGN(LOADADDR(.data_PE10) + SIZEOF(.data_PE10), 32K))
    {
        *(.text_PE11)
    }
    .data_PE11 ALIGN(32K) : AT ( ALIGN(LOADADDR(.text_PE11) + SIZEOF(.text_PE11), 32K))
    {
        *(.data_PE11)
        . = ALIGN(0x8);
        _user_stack_end0 = ABSOLUTE(.);
        . += USER_STACK_SIZE;
         _user_stack_start0   = ABSOLUTE(.);
    }

    .text_PE12 ALIGN(32K) : AT ( ALIGN(LOADADDR(.data_PE11) + SIZEOF(.data_PE11), 32K))
    {
        *(.text_PE12)
    }
    .data_PE12 ALIGN(32K) : AT ( ALIGN(LOADADDR(.text_PE12) + SIZEOF(.text_PE12), 32K))
    {
        *(.data_PE12)
        . = ALIGN(0x8);
        _user_stack_end0 = ABSOLUTE(.);
        . += USER_STACK_SIZE;
         _user_stack_start0   = ABSOLUTE(.);
    }


  _smem_heap_start = ABSOLUTE(.);
    . += _UTASK_SMEM_HEAP_SIZE;
  _smem_heap_end = ABSOLUTE(.);

  .heap   ALIGN(0x1000) :  AT ( ALIGN( LOADADDR(.data_PE12) + SIZEOF(.data_PE12), 0x1000))
  {
  }


  _start_async_copy = VIRT_U_MEM_START;
  _end_async_copy = VIRT_U_MEM_END;
  _heap_end      =  VIRT_U_MEM_END;
  _bin_start_frame = LOADADDR(.boot) >> 12;
  _bin_end_frame   = (INTERNAL_RAM_SIZE >> 12);


INCLUDE "epilogue_link.ld"

}
)CPPRAWMARKER"