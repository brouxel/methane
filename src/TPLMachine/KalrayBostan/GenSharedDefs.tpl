/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
R"CPPRAWMARKER(
#ifndef SHAREDDEFS_H
#define SHAREDDEFS_H

%TYPESTRUCT%

#define MAX_ITERATION %MAXITER%

#define SPAWN_SINK_CHAN "/mppa/sync/128:1"
#define CHANNEL_WRITE "/mppa/channel/128:2/0:1"
#define CHANNEL_READ "/mppa/channel/0:2/128:3"


#endif //SHAREDDEFS_H
)CPPRAWMARKER"