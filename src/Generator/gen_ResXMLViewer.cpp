/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Generator.h"

using namespace std;

class RexXMLViewer : public Generator {
public:
    explicit RexXMLViewer(const SystemModel& t, const config_t& c, const Schedule_t& s) : Generator(t, c, s) {}

    virtual void generate() {
        uint32_t nbP = tg.processors().size();

        ofstream out(conf.files.out);
        out << "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" << endl;
        out << "<schedule processors=\"" << tg.processors().size() << 
                "\" time=\"" << sched.solvetime << 
                "\" status=\"" << sched.status << 
                "\" length=\"" << sched.makespan << 
                "\" heur-DFSDelay=\""<< sched.heur_DFSDelay << 
                "\" heur-DFS=\""<< sched.heur_DFS <<
                "\" heur-BFS=\""<< sched.heur_BFS << "\">" << endl;
        if(sched.status > 0) {
            for(Task *t : tg.tasks()) {
                uint32_t wcet = t->C;

                out << "    <task id=\"" << t->id << "\" " ;
                out << " WCET=\"" << wcet << "\" ";
                try {
                    out << " release=\"" << sched.tasks.at(t->id).rtE << "\" " ;
                    out << " core=\"" << sched.tasks.at(t->id).proc->id << "\" ";
                    out << " memory_region=\"r:" ;
                    for(pair<string, spm_region_info> el : sched.tasks.at(t->id).memory_region.read) {
                        if(!el.second.label.empty()) continue;
                        out << "{" << el.first << "," << el.second.label << "},";
                    }
                    out << " -- e:";
                    if(!sched.tasks.at(t->id).memory_region.exec.label.empty())
                        out << sched.tasks.at(t->id).memory_region.exec.label;;
                    out << " -- w:";
                    for(pair<string, spm_region_info> el : sched.tasks.at(t->id).memory_region.write) {
                        if(!el.second.label.empty()) continue;
                        out << "{" << el.first << "," << el.second.label << "},";
                    }
                    out << "\"";
                }
                catch(exception &) {}
                out << ">" << endl;
                for(pair<string, vector<transmission_phase_info>> trs : sched.tasks.at(t->id).reads) {
                    for(transmission_phase_info el : trs.second) {
                        out << "\t<transmission type=\"" << el.type << "\" ";
                        out << "release=\""<<el.rt<<"\" ";
                        out << "delay=\""<<el.delay<<"\" ";
                        out << "task=\""<<trs.first<<"\" ";
                        out << "data=\""<<el.data<<"\" ";
                        out << "datatype=\""<<el.datatype<<"\" ";
                        out << "conc=\""<<el.conc<<"\" ";
                        out << "/>" << endl;
                    }
                }
                for(pair<string, vector<transmission_phase_info>> trs : sched.tasks.at(t->id).writes) {
                    for(transmission_phase_info el : trs.second) {
                        out << "\t<transmission type=\"" << el.type << "\" ";
                        out << "release=\""<<el.rt<<"\" ";
                        out << "delay=\""<<el.delay<<"\" ";
                        out << "task=\""<<trs.first<<"\" ";
                        out << "data=\""<<el.data<<"\" ";
                        out << "datatype=\""<<el.datatype<<"\" ";
                        out << "conc=\""<<el.conc<<"\" ";
                        out << "/>" << endl;
                    }
                }

                out << "</task>" << endl;
            }

            for(meth_processor_t *p : tg.processors()) {
                uint32_t max_mem_usage = 0;
                for(Task *t : tg.tasks()) {
                    if(sched.tasks.count(t->id) && sched.tasks.at(t->id).proc == p)
                        max_mem_usage += t->data_read() + t->data_written() + t->local_memory_storage;
                }
                out << "    <processor id=\"" << p->id << "\">" << endl;

                if(conf.archi.spm.assign_region == true) {
                    for(spm_region_info r : sched.procs.at(p->id).memory_regions) {
                        if(r.size > 0)
                            out << "        <region id=\"" << r.label << "\" size=\"" << Utils::uint_to_size(r.size) << "\" />" << endl;
                    }
                }
                out << "    </processor>" << endl;
            }
        }

        out << "</schedule>" << endl;
        out.close();
    }
};

REGISTER_GENERATOR(RexXMLViewer, "resxml")