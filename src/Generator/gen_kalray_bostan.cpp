/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Generator.h"

#include <regex>
#include <boost/filesystem.hpp>

using namespace std;

class CodeGenKalrayBostan : public Generator {
    string appname;
    set<string> types;
public:
    explicit CodeGenKalrayBostan(const SystemModel &t, const config_t &c, const Schedule_t &s) : Generator(t,c,s) {}; 
        
    virtual void generate() {
        regex r("[^a-zA-Z0-9]");
        size_t pos = conf.files.filename.find_last_of('.');
        appname = regex_replace(conf.files.filename.substr(0,pos), r, "");
        boost::filesystem::create_directories(conf.files.path+"/codegen/"+appname);
        
        computeUsedTypes();
        
        generateMakefile();
        generateLDScripts();
        gnerateClusterBenchSkeleton();
        generateClusterMain();
        generateClusterSchedule();
        
        generateIOMain();
        generateSharedDefs();
    }
    
private:
    struct buffer_types {
        string init_type;
        size_t nbtokens;
    };
    map<string, buffer_types> buffer_types_list;
    
    void computeUsedTypes() {
        for(Task *t : tg.tasks()) {
            if(t->successors.size() > 0 && t->data_written() > 0) {
                buffer_types write_buf = {.init_type = t->successors.at(0)->previous_datatypes[t], .nbtokens = t->data_written() };
                string bdt = "buffer_"+write_buf.init_type+to_string(t->data_written())+"_t";
                buffer_types_list[bdt] = write_buf;
            }
            if(t->previous.size() > 1 && t->data_read() > 0) {
                buffer_types read_buf = {.init_type = (*(t->previous_datatypes.begin())).second, .nbtokens = t->data_read()};
                string bdt = "buffer_"+read_buf.init_type+to_string(t->data_read())+"_t";
                buffer_types_list[bdt] = read_buf;
            }
        }
        if(conf.interconnect.burst == "packet") {
            for(Task *t : tg.tasks()) {
                for(pair<string, vector<transmission_phase_info>> el : sched.tasks.at(t->id).reads) {
                    for(transmission_phase_info tr : el.second) {
                        if(tr.data == 0) continue;
                        buffer_types buf = {.init_type = tr.datatype, .nbtokens = tr.data };
                        string bdt = "buffer_"+buf.init_type+to_string(buf.nbtokens)+"_t";
                        buffer_types_list[bdt] = buf; 
                    }
                }
                for(pair<string, vector<transmission_phase_info>> el : sched.tasks.at(t->id).writes) {
                    for(transmission_phase_info tr : el.second) {
                        if(tr.data == 0) continue;
                        buffer_types buf = {.init_type = tr.datatype, .nbtokens = tr.data };
                        string bdt = "buffer_"+buf.init_type+to_string(buf.nbtokens)+"_t";
                        buffer_types_list[bdt] = buf;  
                    }
                }
            }
        }
        if(conf.interconnect.burst == "flit") {
            for(Task *t : tg.tasks()) {
                if(t->successors.size() > 1) {
                    for(Task *s : t->successors) { 
                        uint32_t data = t->data_written(*s);
                        if(data == 0) continue;
                        string copytype = s->previous_datatypes[t];
                        buffer_types buf = {.init_type = copytype, .nbtokens = data };
                        string bdt = "buffer_"+buf.init_type+to_string(buf.nbtokens)+"_t";
                        cout << " --> " << bdt << " -- " << buf.init_type << " -- " << buf.nbtokens << endl;
                        buffer_types_list[bdt] = buf;  
                    }
                }
            }
        }
    }
    
    void generateMakefile() {
        string code = 
#include "TPLMachine/KalrayBostan/GenMakefile.tpl"
        ;
        
        regex r1("%CLUSTERAPPNAME%");
        regex r2("%CLUSTERFILES%");
        regex r3("%IOFILES%");
        
        code = regex_replace(code, r1, appname);
        code = regex_replace(code, r2, "cluster_main.c "+appname+".c schedule.c");
        code = regex_replace(code, r3, "io_main.c");
        
        ofstream ofs(conf.files.path+"/codegen/"+appname+"/Makefile");
        ofs << code;
        ofs.close();
    }
    
    void generateLDScripts() {
        string code = 
#include "TPLMachine/KalrayBostan/GenLDScriptCluster.tpl"
        ;
        ofstream ofs(conf.files.path+"/codegen/"+appname+"/ldscript_cluster.ld");
        ofs << code;
        ofs.close();
        
        code = 
#include "TPLMachine/KalrayBostan/GenLDScriptClusterPrologue.tpl"
        ;
        ofstream ofs2(conf.files.path+"/codegen/"+appname+"/ldscript_cluster_prologue.ld");
        ofs2 << code;
        ofs2.close();
    }
    
    void generateClusterMain() {
        string cluster_c = 
#include "TPLMachine/KalrayBostan/GenClusterMainC.tpl"
        ;
        string cluster_h = 
#include "TPLMachine/KalrayBostan/GenClusterHeader.tpl"
        ;
        
        regex r2("%LAUCHSCHEDULEPE%");
        regex r2_1("%JOINSCHEDULEPE%");
        string code = "", decl = "", join = "";
        size_t procnum = 2; //nor 0, neither 1 : 0 is comm scheduler, 1 is paired with 0
        for(meth_processor_t *p : tg.processors()) {
            code +="\tutask_t thr"+p->id+";\n";
            code += "\tutask_start_pe(&thr"+p->id+", schedule_"+p->id+", (void *)&mybarrier, "+to_string(procnum++)+");\n";
            decl += "void schedule_"+p->id+"(void *);\n";
            join += "\tutask_join(thr"+p->id+", NULL);\n";
        }
        cluster_c = regex_replace(cluster_c, r2, code);
        cluster_c = regex_replace(cluster_c, r2_1, join);
        
        regex r3("%NBTHREADS%");
        cluster_c = regex_replace(cluster_c, r3, to_string(tg.processors().size()));
        
        string schedulefuncs = "";
        for(meth_processor_t *p : tg.processors())
            schedulefuncs += "void* schedule_"+p->id+"(void *argv);\n";
        
        for(Task *t : tg.tasks()) {
            for(pair<string, vector<transmission_phase_info>> el : sched.tasks.at(t->id).reads) {
                set<string> uniqfn;
                for(transmission_phase_info tr : el.second) {
                    string fnname = generateCommScheduleCallRead(t, el.first, tr, 1);
                    if(tr.data > 0 && uniqfn.find(fnname) == uniqfn.end()) {
                        schedulefuncs += "void "+fnname+";\n";
                        uniqfn.insert(fnname);
                    }
                }
            }
            schedulefuncs += "void "+t->id+"_exec();\n";
            for(pair<string, vector<transmission_phase_info>> el : sched.tasks.at(t->id).writes) {
                set<string> uniqfn;
                for(transmission_phase_info tr : el.second) {
                    string fnname = generateCommScheduleCallWrite(t, el.first, tr, 1);
                    if(tr.data > 0 && uniqfn.find(fnname) == uniqfn.end()) {
                        schedulefuncs += "void "+fnname+";\n";
                        uniqfn.insert(fnname);
                    }
                }
            }
        }
        
        regex r4("%SCHEDULEPES%");
        cluster_h = regex_replace(cluster_h, r4, schedulefuncs);
        
        ofstream ofs_c(conf.files.path+"/codegen/"+appname+"/cluster_main.c");
        ofs_c << cluster_c;
        ofs_c.close();
        
        ofstream ofs_h(conf.files.path+"/codegen/"+appname+"/cluster.h");
        ofs_h << cluster_h;
        ofs_h.close();
    }
    
    void generateSharedDefs() {
        string buffer_mgnt_tpl = 
#include "TPLMachine/KalrayBostan/GenSharedDefsBufferMgnt.tpl"
        ;
        
        string buffer_mgnt = "";
        regex rmgnt1("%TYPE%");
        regex rmgnt2("%SIZE%");
        for(pair<string, buffer_types> bt : buffer_types_list) {
            if(bt.second.nbtokens == 0) continue; //bug don't know why
            string tmp = regex_replace(buffer_mgnt_tpl, rmgnt1, bt.second.init_type);
            tmp = regex_replace(tmp, rmgnt2, to_string(bt.second.nbtokens));
            buffer_mgnt += tmp;
        }
        
        string code = 
#include "TPLMachine/KalrayBostan/GenSharedDefs.tpl"
        ;
        
        int bufsizemax = 2;
        
        regex r0("%TYPESTRUCT%");
        regex r1("%MAXITER%");
        regex r2("%BUFSIZEMAX%");
        
        code = regex_replace(code, r0, buffer_mgnt);
        code = regex_replace(code, r1, to_string(conf.codegen.maxiter));
        code = regex_replace(code, r2, to_string(bufsizemax));
        
        ofstream ofs(conf.files.path+"/codegen/"+appname+"/shared_defs.h");
        ofs << code;
        ofs.close();
    }
    
    struct transmission_phase_info_wrapper {
        transmission_phase_info tr;
        Task *owner;
        string relatedTaskId;
        
        string buffername;
        buffer_types buf;
    };
    
    void generateIOMain() {
        string buffers = "", buffers_init = "";
        
        vector<transmission_phase_info_wrapper> trs;
        for(Task *t : tg.tasks()) {
            buffer_types write_buf;
            buffer_types read_buf;
            if(t->successors.size() > 0 && t->data_written() > 0) {
                string bdt = "buffer_"+t->successors.at(0)->previous_datatypes[t]+to_string(t->data_written());
                buffers += "static volatile "+bdt+"_t "+t->ilp_label+";\n";
                buffers_init += "    init_"+bdt+"(&"+t->ilp_label+");\n";
                write_buf = buffer_types_list[bdt+"_t"];
            }
            
            if(t->successors.size() > 1) {
                for(Task *s : t->successors) { 
                    uint32_t data = t->data_written(*s);
                    if(data == 0) continue;
                    string bdt = "buffer_"+s->previous_datatypes[t]+to_string(t->data_written(*t));
                    buffers += "static volatile "+bdt+"_t "+s->id+"_rb;\n";
                    buffers_init += "    init_"+bdt+"(&"+s->id+"_rb);\n";
                    write_buf = buffer_types_list[bdt+"_t"];
                }
            }
            
            if(t->previous.size() > 1 && t->data_read() > 0) {
                string bdt = "buffer_"+(*(t->previous_datatypes.begin())).second+to_string(t->data_read());
                buffers += "static volatile "+bdt+"_t "+t->ilp_label+"_rb;\n";
                buffers_init += "    init_"+bdt+"(&"+t->ilp_label+");\n";
                read_buf = buffer_types_list[bdt+"_t"];
            }
            
            if(t->previous.size() == 1 && t->data_read() > 0) {
                string bdt = "buffer_"+(*(t->previous_datatypes.begin())).second+to_string(t->data_read());
                read_buf = buffer_types_list[bdt+"_t"];
            }
            
            for(pair<string, vector<transmission_phase_info>> el : sched.tasks.at(t->id).writes) {
                for(transmission_phase_info tr : el.second) {
                    if(conf.interconnect.burst == "none" || t->successors.size() == 1) {
                        trs.push_back({.tr = tr, .owner = t, .relatedTaskId = el.first, .buffername = t->ilp_label, .buf = write_buf});
                    }
                    else if(t->successors.size() > 1) {
                        Task *succ;
                        for(Task *s : t->successors) { 
                            if(s->id == el.first) {
                                succ = s;
                                break;
                            }
                        }
                        
                        string bdt = "buffer_"+succ->previous_datatypes[t]+to_string(t->data_written(*succ));
                        trs.push_back({.tr = tr, .owner = t, .relatedTaskId = el.first, .buffername = el.first+"_rb", .buf = buffer_types_list[bdt+"_t"]});
                    }
                }
            }
            
            for(pair<string, vector<transmission_phase_info>> el : sched.tasks.at(t->id).reads) {
                for(transmission_phase_info tr : el.second) {
                    // 3 cases: 
                    //   - owner has multiple predecessors (e.g. join node) -> create a new buffer aggregating each edges
                    //   - owner has siblings (common direct parent) (e.g. child of split) -> apply offset to array @ according to graph order in the code
                    //   - owner has one predecessor (e.g. common case) -> use the write buffer of this predecessor
                    
                    string buffername = "";
                    buffer_types bt;
                    if(conf.interconnect.burst == "none" && t->previous.size() > 1) {
                        buffername = t->ilp_label+"_rb";
                        bt = read_buf;
                    }
                    else if(t->previous.size() == 1) {
                        Task *prev = (*(t->previous.begin())).first;
                        
                        if(prev->successors.size() > 1) {
                            buffername = t->ilp_label+"_rb";
                            bt = buffer_types_list["buffer_"+t->previous_datatypes[prev]+to_string(t->data_read())+"_t"];
                        }
                        else {
                            buffername = prev->ilp_label;
                            for(transmission_phase_info_wrapper etr : trs) {
                                if(etr.owner == prev && etr.buffername == buffername) {
                                    bt = etr.buf;
                                    break;
                                }
                            }
                        }
                    }
                    
                    trs.push_back({.tr = tr, .owner = t, .relatedTaskId = el.first, .buffername = buffername, .buf = bt});
                }
            }
        }
        sort(trs.begin(), trs.end(), [](const transmission_phase_info_wrapper &a, const transmission_phase_info_wrapper &b) { return a.tr.rt < b.tr.rt; });
        
        
        string initsched = "";
        if(conf.interconnect.burst == "packet" || conf.interconnect.burst == "flit")
            initsched = buffers_init;
        string schedulecomm = "";
        set<string> tmp_buf_size;
        
        if(conf.interconnect.burst == "packet") {
            for(transmission_phase_info_wrapper etr : trs) {
                if(etr.tr.data == 0)
                    continue;
                // !! WARNING !! in I/O cluster, read phase->send to cluster, write phase->store in mem
                string tmpbuftype = etr.buf.init_type+to_string(etr.tr.data);
                if(etr.tr.type == "read") {
                    schedulecomm += "\t// "+etr.owner->id+" read/sendcluster\n";
                    schedulecomm += "\tfor(int i=0 ; i < "+to_string(etr.tr.data)+" ; ++i)\n";
                    schedulecomm += "\t\tpush_"+tmpbuftype;
                    schedulecomm +=     "(&tmp"+tmpbuftype+", ";
                    schedulecomm +=     "pop_"+etr.buf.init_type+to_string(etr.buf.nbtokens)+"(&"+etr.buffername+"));\n";
                    schedulecomm += "\tsendcluster(&tmp"+tmpbuftype+", sizeof(buffer_"+tmpbuftype+"_t));\n";
                }
                else {
                    schedulecomm += "\t// "+etr.owner->id+" write/storemem\n";
                    schedulecomm += "\tstoremem(&tmp"+tmpbuftype+", sizeof(buffer_"+tmpbuftype+"_t));\n";
                    schedulecomm += "\tfor(int i=0 ; i < "+to_string(etr.tr.data)+" ; ++i)\n";
                    schedulecomm += "\t\tpush_"+etr.buf.init_type+to_string(etr.buf.nbtokens);
                    schedulecomm +=     "(&"+etr.buffername+", ";
                    schedulecomm +=     "pop_"+tmpbuftype+"(&tmp"+tmpbuftype+"));\n";
                }
                tmp_buf_size.insert(tmpbuftype);
            }
            string tmpbuf = "";
            for(string b : tmp_buf_size)
                tmpbuf += "\tbuffer_"+b+"_t tmp"+b+";\n";
            schedulecomm = tmpbuf+"\n"+schedulecomm;
        }
        else if(conf.interconnect.burst == "flit") {
            uint32_t cnt = 0;
            for(transmission_phase_info_wrapper etr : trs) {
                if(etr.tr.data == 0)
                    continue;
                string s_cnt = to_string(++cnt);
                // !! WARNING !! in I/O cluster, read phase->send to cluster, write phase->store in mem
                string tmpbuftype = etr.buf.init_type;
                if(etr.tr.type == "read") {
                    schedulecomm += "\t// "+etr.owner->id+" read/sendcluster\n";
                    schedulecomm += "\t"+tmpbuftype+" tmp"+tmpbuftype+s_cnt+" = pop_"+tmpbuftype+to_string(etr.buf.nbtokens)+"(&"+etr.buffername+");\n";
                    schedulecomm += "\tsendcluster(&tmp"+tmpbuftype+s_cnt+", sizeof("+tmpbuftype+"));\n";
                }
                else {
                    schedulecomm += "\t// "+etr.owner->id+" write/storemem\n";
                    schedulecomm += "\t"+tmpbuftype+" tmp"+tmpbuftype+s_cnt+";\n";
                    schedulecomm += "\tstoremem(&tmp"+tmpbuftype+s_cnt+", sizeof("+tmpbuftype+"));\n";
                    schedulecomm += "\tpush_"+tmpbuftype+to_string(etr.buf.nbtokens)+"(&"+etr.buffername+", tmp"+tmpbuftype+s_cnt+");\n";
                }
              //  tmp_buf_size.insert(tmpbuftype);
            }
            //string tmpbuf = "";
            //for(string b : tmp_buf_size)
            //    tmpbuf += "\t"+b+" tmp"+b+";\n";
            //schedulecomm = tmpbuf+"\n"+schedulecomm;
        }
        else {
            for(transmission_phase_info_wrapper etr : trs) {
                if(etr.tr.data == 0)
                    continue;

                // !! WARNING !! in I/O cluster, read phase->send to cluster, write phase->store in mem
                string method = (etr.tr.type == "read") ? "sendcluster" : "storemem";
                schedulecomm += "\t"+method+"(&"+etr.buffername+", sizeof(buffer_"+etr.buf.init_type+to_string(etr.buf.nbtokens)+"_t));\n";
            }
        }
        
        
        string code = 
#include "TPLMachine/KalrayBostan/GenIOMain.tpl"
        ;
        
        regex r0("%BUFFERS%");
        regex r1("%INITSCHEDULECOMM%");
        regex r2("%SCHEDULECOMM%");
        regex r3("%CLUSTERAPPNAME%");
        
        
        code = regex_replace(code, r0, buffers);
        code = regex_replace(code, r1, initsched);
        code = regex_replace(code, r2, schedulecomm);
        code = regex_replace(code, r3, appname);
        
        ofstream ofs(conf.files.path+"/codegen/"+appname+"/io_main.c");
        ofs << code;
        ofs.close();
    }
    
    /*
     * mode = 0 -> decl func in C file
     * mode = 1 -> decl func in H file
     * mode = 2 -> call func
     */
    string generateCommScheduleCallRead(Task *t, const string &related, const transmission_phase_info &tr, int mode=0) {
        string funcname = t->id;
        if(related == t->id) {
            funcname += "_read";
            if(mode != 0)
                funcname += "()";
        }
        else {
            funcname += "_read_from_"+related;
            if(tr.data == t->data_read()) {
                if(mode == 1)
                    funcname += conf.interconnect.burst == "flit" ? "(register int packnum)" : "()";
                else if(mode == 2)
                    funcname += conf.interconnect.burst == "flit" ? "(1)" : "()";
            }
            else if(tr.packetnumber > 1) {
                funcname += "_"+to_string(tr.data);
                if(mode == 1)
                    funcname += "(register int packnum)";
                else if (mode == 2)
                    funcname += "("+to_string(tr.packetnum)+")";
            }
            else if(mode != 0)
                funcname += "(1)";
        }
        
        return funcname;
    }
    string generateCommScheduleCallWrite(Task *t, const string &related, const transmission_phase_info &tr,  int mode=0) {
        string funcname = t->id;
        if(related == t->id) {
            funcname += "_write";
            if(mode != 0)
                funcname += "()";
        }
        else {
            funcname += "_write_to_"+related;
            if(tr.data == t->data_written()) {
                if(mode == 1)
                    funcname += conf.interconnect.burst == "flit" ? "(register int packnum)" : "()";
                else if(mode == 2)
                    funcname += conf.interconnect.burst == "flit" ? "(1)" : "()";
            }
            else if(tr.packetnumber > 1) {
                funcname += "_"+to_string(tr.data);
                if(mode == 1)
                    funcname += "(register int packnum)";
                else if (mode == 2)
                    funcname += "("+to_string(tr.packetnum)+")";
            }
            else if(mode != 0)
                funcname += "(1)";
        }
        
        return funcname;
    }
    
    string generateClusterBenchSkeletonReadNoBurst(const string &funcname, const string &t, string datatype, int buffsize) {
        string code = "void "+funcname+"() {\n";
        code += "\tfetch(&"+t+"_rb, sizeof(buffer_"+datatype+to_string(buffsize)+"_t));\n";
        code += "\t__builtin_k1_wpurge();\n";
        code += "\t__builtin_k1_fence();\n";
        code += "}\n";
        return code;
    }
    string generateClusterBenchSkeletonWriteNoBurst(const string &funcname, const string &t, string datatype, int buffsize) {
        string code = "void "+funcname+"() {\n";
        code += "\tstore(&"+t+"_wb, sizeof(buffer_"+datatype+to_string(buffsize)+"_t));\n";
        code += "\t__builtin_k1_wpurge();\n";
        code += "\t__builtin_k1_fence();\n";
        code += "}\n";
        return code;
    }
    
    string generateClusterBenchSkeletonReadBurst1(const string &funcname, const string &t, string datatype, int buffsize) {
        string code = "void "+funcname+"() {\n";
        code += "\tfetch(&"+t+"_rb, sizeof(buffer_"+datatype+to_string(buffsize)+"_t));\n";
        code += "\t__builtin_k1_wpurge();\n";
        code += "\t__builtin_k1_fence();\n";
        code += "}\n";
        return code;
    }
    string generateClusterBenchSkeletonWriteBurst1(const string &funcname, const string &t, string datatype, int buffsize) {
        string code = "void "+funcname+"() {\n";
        code += "\tstore(&"+t+"_wb, sizeof(buffer_"+datatype+to_string(buffsize)+"_t));\n";
        code += "\t__builtin_k1_wpurge();\n";
        code += "\t__builtin_k1_fence();\n";
        code += "}\n";
        return code;
    }
    
    string generateClusterBenchSkeletonReadBurst(Task *t, const string &funcname, int nbtokens, string datatype, int buffsize, int packetsize) {
        string askhumanaction = (t->previous.size() > 1) ? "OFFSETEDGE" : "0";
        string code = "void "+funcname+"(register int packnum) {\n";
        code += "\tbuffer_"+datatype+to_string(nbtokens)+"_t tmp;\n";
        code += "\tfetch(&tmp, sizeof(buffer_"+datatype+to_string(nbtokens)+"_t));\n";
        code += "\tfor(register int i=0, j="+askhumanaction+"+(packnum-1)*"+to_string(packetsize)+" ; i < "+to_string(nbtokens)+" ; ++i, ++j) {\n";
        code += "\t\tinsert_"+datatype+to_string(buffsize)+"(&"+t->id+"_rb, peek_"+datatype+to_string(nbtokens)+"(&tmp, i), j);\n";
        code += "\t}\n";
        code += "\t__builtin_k1_wpurge();\n";
        code += "\t__builtin_k1_fence();\n";
        code += "}\n";
        return code;
    }
    
    string generateClusterBenchSkeletonWriteBurst(Task *t, const string &funcname, int nbtokens, string datatype, int buffsize, int packetsize) {
        string askhumanaction = (t->successors.size() > 1) ? "OFFSETEDGE" : "0";
        string code = "void "+funcname+"(register int packnum) {\n";
        code += "\tbuffer_"+datatype+to_string(nbtokens)+"_t tmp;\n";
        code += "\tinit_buffer_"+datatype+to_string(nbtokens)+"(&tmp);\n";
        code += "\tfor(register int i=0, j="+askhumanaction+"+(packnum-1)*"+to_string(packetsize)+" ; i < "+to_string(nbtokens)+" ; ++i, ++j) {\n";
        code += "\t\tpush_"+datatype+to_string(nbtokens)+"(&tmp, peek_"+datatype+to_string(buffsize)+"(&"+t->id+"_wb, j));\n";
        code += "\t}\n";
        code += "\tstore(&tmp, sizeof(buffer_"+datatype+to_string(nbtokens)+"_t));\n";
        code += "\t__builtin_k1_wpurge();\n";
        code += "\t__builtin_k1_fence();\n";
        code += "}\n";
        return code;
    }
    
    string generateClusterBenchSkeletonReadBurstUnit(Task *t, const string &funcname, string datatype, int buffsize) {
        string askhumanaction = (t->previous.size() > 1) ? "OFFSETEDGE" : "0";
        string code = "void "+funcname+"(register int packnum) {\n";
        code += "\t"+datatype+" "+" tmp;\n";
        code += "\tfetch(&tmp, sizeof("+datatype+"));\n";
        code += "\tinsert_"+datatype+to_string(buffsize)+"(&"+t->id+"_rb, tmp, "+askhumanaction+"+(packnum-1));\n";
        code += "\t__builtin_k1_wpurge();\n";
        code += "\t__builtin_k1_fence();\n";
        code += "}\n";
        return code;
    }
    
    string generateClusterBenchSkeletonWriteBurstUnit(Task *t, const string &funcname, string datatype, int buffsize) {
        string askhumanaction = (t->successors.size() > 1) ? "OFFSETEDGE" : "0";
        string code = "void "+funcname+"(register int packnum) {\n";
        code += "\t"+datatype+" tmp = peek_"+datatype+to_string(buffsize)+"(&"+t->id+"_wb, "+askhumanaction+"+(packnum-1));\n";
        code += "\tstore(&tmp, sizeof("+datatype+"));\n";
        code += "\t__builtin_k1_wpurge();\n";
        code += "\t__builtin_k1_fence();\n";
        code += "}\n";
        return code;
    }
    
    string generateClusterBenchSkeletonRead(Task *t) {
        string code = "";
        for(pair<string, vector<transmission_phase_info>> trs : sched.tasks.at(t->id).reads) {
            if(conf.interconnect.burst == "flit") {
                code += generateClusterBenchSkeletonReadBurstUnit(t, generateCommScheduleCallRead(t, trs.first, trs.second.at(0)), (*(t->previous_datatypes.begin())).second, t->data_read());
            } 
            else if(trs.first == t->id)
                code += generateClusterBenchSkeletonReadNoBurst(generateCommScheduleCallRead(t, trs.first, trs.second.at(0)), t->id, (*(t->previous_datatypes.begin())).second, t->data_read());
            else if(t->data_read() == trs.second.at(0).data) {
                code += generateClusterBenchSkeletonReadBurst1(generateCommScheduleCallRead(t, trs.first, trs.second.at(0)), t->id, (*(t->previous_datatypes.begin())).second, t->data_read());
            }
            else {
                set<string> uniqfn;
                int packetsize = (signed) CoreMemInterConnect::token_per_burst_chunk(conf, conf.datatypes.size_bits.at((*(t->previous_datatypes.begin())).second));
                for(transmission_phase_info tr : trs.second) {
                    string funcname = generateCommScheduleCallRead(t, trs.first, tr);
                    if(uniqfn.find(funcname) == uniqfn.end()) {
                        code += generateClusterBenchSkeletonReadBurst(t, funcname, tr.data, tr.datatype, t->data_read(), packetsize);
                        uniqfn.insert(funcname);
                    }
                } 
            }
        }
        return code;
    }
    
    string generateClusterBenchSkeletonWrite(Task *t) {
        string code = "";
        for(pair<string, vector<transmission_phase_info>> trs : sched.tasks.at(t->id).writes) {
            if(conf.interconnect.burst == "flit") {
                code += generateClusterBenchSkeletonWriteBurstUnit(t, generateCommScheduleCallWrite(t, trs.first, trs.second.at(0)), t->successors.at(0)->previous_datatypes[t], t->data_written());
            } 
            else if(trs.first == t->id)
                code += generateClusterBenchSkeletonWriteNoBurst(generateCommScheduleCallWrite(t, trs.first, trs.second.at(0)), t->id, t->successors.at(0)->previous_datatypes[t], t->data_written());
            else if(t->data_written() == trs.second.at(0).data) {
                code += generateClusterBenchSkeletonWriteBurst1(generateCommScheduleCallWrite(t, trs.first, trs.second.at(0)), t->id, t->successors.at(0)->previous_datatypes[t], t->data_written());
            }
            else {
                set<string> uniqfn;
                int packetsize = (signed) CoreMemInterConnect::token_per_burst_chunk(conf, conf.datatypes.size_bits.at(t->successors.at(0)->previous_datatypes[t]));
                for(transmission_phase_info tr : trs.second) {
                    string funcname = generateCommScheduleCallWrite(t, trs.first, tr);
                    if(uniqfn.find(funcname) == uniqfn.end()) {
                        code += generateClusterBenchSkeletonWriteBurst(t, funcname, tr.data, tr.datatype, t->data_written(), packetsize);
                        uniqfn.insert(funcname);
                    }
                } 
            }
        }
        return code;
    }
    
    void gnerateClusterBenchSkeleton() {
        string code = "#include \"cluster.h\"\n";
        code += "#include \"shared_defs.h\"\n\n";
        
        //Buffer declaration
        for(Task *t : tg.tasks()) {
            if(t->previous_datatypes.size() > 0 && t->data_read() > 0) {
                code += "static volatile ";
                code += "buffer_"+(*(t->previous_datatypes.begin())).second+to_string(t->data_read())+"_t ";
                code += t->id+"_rb ";
                code += "__attribute__((section(\".data_"+sched.tasks.at(t->id).proc->id+"\")));\n";
            }
            if(t->successors.size() > 0 && t->data_written() > 0) {
                code += "static volatile ";
                code += "buffer_"+t->successors.at(0)->previous_datatypes[t]+to_string(t->data_written())+"_t ";
                code += t->id+"_wb ";
                code += "__attribute__((section(\".data_"+sched.tasks.at(t->id).proc->id+"\")));\n";
            }
        }
        code += "\n";
        
        for(Task *t : tg.tasks()) {
            string rhint = "", whint = "";
            if(t->previous.size() > 0 && t->data_read() > 0) {
                code += generateClusterBenchSkeletonRead(t);
                
                string rdtc = (*(t->previous_datatypes.begin())).second+to_string(t->data_read());
                rhint = "\t//buffer in: pop_"+rdtc+"(&"+t->id+"_rb)\n";
            }
            
            if(t->successors.size() > 0 && t->data_written() > 0) {
                code += generateClusterBenchSkeletonWrite(t);
                
                string wdtc = t->successors.at(0)->previous_datatypes[t]+to_string(t->data_written());
                whint = "\t//buffer out: push_"+wdtc+"(&"+t->id+"_wb)\n";
            }
            
            code += "void "+t->id+"_exec() {\n";
            code += rhint;
            code += whint;
            code += "\n";
            code += "\t__builtin_k1_wpurge();\n";
            code += "\t__builtin_k1_fence();\n";
            code += "}\n";
        }
        
        //Init phase skeleton
        code += "void init_bench(int channel_read_fd, int channel_write_fd) {\n";
        code +=     "\t// Warm-up channel\n";
        code +=     "\tfloat warmup[100];\n";
        code +=     "\tfor(int i=0 ; i < 100 ; i++)\n";
        code +=     "\t\twarmup[i] = i*42.24;\n";
        code +=     "\tmppa_write(channel_write_fd, warmup, 100*sizeof(float));\n";
        code +=     "\tmppa_read(channel_read_fd, warmup, 100*sizeof(float));\n";
        code +=     "\t// Init buffers\n";
        for(Task *t : tg.tasks()) {
            if(t->previous_datatypes.size() > 0 && t->data_read() > 0) {
                code += "\tinit_buffer_"+(*(t->previous_datatypes.begin())).second+to_string(t->data_read());
                code += "(&"+t->id+"_rb);\n";
            }
            if(t->successors.size() > 0 && t->data_written() > 0) {
                code += "\tinit_buffer_"+t->successors.at(0)->previous_datatypes[t]+to_string(t->data_written());
                code += "(&"+t->id+"_wb);\n";
            }
        }
        code +=     "\t// Warm-up appli\n\n";
        code += "}\n\n";
        
        ofstream ofs(conf.files.path+"/codegen/"+appname+"/"+appname+".c");
        ofs << code;
        ofs.close();
    }
    
    void generateClusterSchedule() {
        map<string, uint16_t> procs;
        vector<meth_processor_t*> tmpprocs = tg.processors();
        sort(tmpprocs.begin(), tmpprocs.end(), [](meth_processor_t* a, meth_processor_t *b) { return a->id < b->id;});
        
        string code = "#include \"cluster.h\"\n";
        code += "#include \"shared_defs.h\"\n";
        code += "#include <HAL/hal/core/diagnostic.h>\n",
        code += "#define SCHED_RELEASE_TIME 10000\n\n";
        
        for(meth_processor_t *p : tmpprocs)
            code += generatePESchedule(p);
        
        code += generateCommSchedule();
        
        code += generateSequentialSschedule();
        
        ofstream ofs(conf.files.path+"/codegen/"+appname+"/schedule.c");
        ofs << code;
        ofs.close();
    }
    
    struct phase_wrapper {
        uint64_t rt;
        string owner;
        string type;
        string callee;
    };
    
    string generatePESchedule(meth_processor_t *p) {
        vector<phase_wrapper> phases;
        for(Task *t : tg.tasks()) {
            if(sched.tasks.at(t->id).proc == p) {
                phases.push_back({
                            .rt = sched.tasks.at(t->id).rtE,
                            .owner = t->id
                        });
            }
        } 
        sort(phases.begin(), phases.end(), [](phase_wrapper a, phase_wrapper b) { return a.rt < b.rt; });
        
        string code = "";
        //code += "static volatile uint32_t origin_world"+p->id+" __attribute__((section(\".data_"+p->id+"\")));\n";
        code += "register uint32_t origin_world"+p->id+" asm (\"r50\");\n";
        code += "static __inline__ void reload_origin"+p->id+"() {\n";
        code +=     "\torigin_world"+p->id+" = __builtin_k1_get(_K1_SFR_PM0); // subsitute of __k1_counter_num\n";
        code +=     "\torigin_world"+p->id+" += 32; // 9 + 23: time to interrogate the counter + time to perform the addition\n";
        code += "}\n";
        code += "static __inline__ void initSched"+p->id+"() {\n";
        code +=     "\t__k1_counter_reset_all();\n";
        code +=     "\t__k1_counter_enable(_K1_DIAGNOSTIC_COUNTER0, _K1_CYCLE_COUNT, _K1_DIAGNOSTIC_NOT_CHAINED);\n";
        code +=     "\t__builtin_k1_set(_K1_SFR_PM0, 0);\n";
        code +=     "\treload_origin"+p->id+"();\n";
        code += "}\n";
        code += "static __inline__ void sched"+p->id+"(uint32_t cycles) {\n";
        code +=     "\t//while(origin_world"+p->id+"+cycles > __builtin_k1_get(_K1_SFR_PM0)+32) ;\n";
        code +=     "\twhile(__builtin_k1_get(_K1_SFR_PM0)+32-origin_world"+p->id+" < cycles ) ;\n";
        code += "}\n";
        code += "void* schedule_"+p->id+"(void *argv) {\n";
        code +=     "\tutask_barrier_t *mybarrier = (utask_barrier_t *)argv;\n";
        code +=     "\tregister uint64_t start;\n";
        code +=     "\tregister uint64_t end;\n";
        code +=     "\tprintf(\"Start on PE %d\\n\", __k1_get_cpu_id());\n";
        code +=     "\tfor(register int i=0 ; i < MAX_ITERATION ; i++ ) {\n";
        code +=     "\t\tutask_barrier_wait(mybarrier);\n";
        code +=     "\t\tinitSched"+p->id+"();\n";
        code +=     "\t\tsched"+p->id+"(SCHED_RELEASE_TIME);\n";
        
        for(phase_wrapper ph : phases) {
            if(ph.rt > 0)
                code += "\t\tsched"+p->id+"("+to_string((uint64_t)(ph.rt*conf.codegen.safety))+");\n";
            if(conf.codegen.with_delay_printf)
                    code += "\t\tstart = __k1_read_dsu_timestamp();\n";
            code += "\t\t"+ph.owner+"_exec();\n";
            if(conf.codegen.with_delay_printf) {
                code += "\t\tend = __k1_read_dsu_timestamp();\n";
                code += "\t\tprintf(\""+ph.owner+"_exec: %llu\\n\", end-start);\n";
            }
        }
        code += "\t}\n";
        code += "\treturn NULL;\n";
        code += "}\n\n";
        return code;
    }
    
    string generateCommSchedule() {
        string code = "";
        vector<phase_wrapper> phases;
        for(Task *t : tg.tasks()) {
            for(pair<string, vector<transmission_phase_info>> el : sched.tasks.at(t->id).reads) {
                for(transmission_phase_info tr : el.second) {
                    if(tr.data > 0) {
                        phases.push_back({
                            .rt = tr.rt,
                            .owner = t->id,
                            .type = "read",
                            .callee = generateCommScheduleCallRead(t, el.first, tr, 2)
                        });
                    }
                }
            }
            
            for(pair<string, vector<transmission_phase_info>> el : sched.tasks.at(t->id).writes) {
                for(transmission_phase_info tr : el.second) {
                    if(tr.data > 0) {
                        phases.push_back({
                            .rt = tr.rt,
                            .owner = t->id,
                            .type = "write",
                            .callee = generateCommScheduleCallWrite(t, el.first, tr, 2)
                        });
                    }
                }
            }
        }
        sort(phases.begin(), phases.end(), [](phase_wrapper a, phase_wrapper b) { return a.rt < b.rt; });
        
        //Steady phase
        code += "uint32_t origin_worldComm __attribute__((section(\".data_PE0\")));\n";
        code += "static __inline__ void reload_originComm() {\n";
        code +=     "\torigin_worldComm = __builtin_k1_get(_K1_SFR_PM0); // subsitute of __k1_counter_num\n";
        code +=     "\torigin_worldComm += 32; // 9 + 23: time to interrogate the counter + time to perform the addition\n";
        code += "}\n";
        code += "static __inline__ void initSchedComm() {\n";
        code +=     "\t__k1_counter_reset_all();\n";
        code +=     "\t__k1_counter_enable(_K1_DIAGNOSTIC_COUNTER0, _K1_CYCLE_COUNT, _K1_DIAGNOSTIC_NOT_CHAINED);\n";
        code +=     "\t__builtin_k1_set(_K1_SFR_PM0, 0);\n";
        code +=     "\treload_originComm();\n";
        code += "}\n";
        code += "static __inline__ void schedComm(register uint32_t cycles) {\n";
        code +=     "\t//while(origin_worldComm+cycles > __builtin_k1_get(_K1_SFR_PM0)+32) ;\n";
        code +=     "\twhile(__builtin_k1_get(_K1_SFR_PM0)+32-origin_worldComm < cycles ) ;\n";
        code += "}\n";
        code += "void schedule_comm(utask_barrier_t *mybarrier) {\n";
        code +=     "\tprintf(\"Start comm schedule\\n\");\n";
        code +=     "\tregister uint64_t start;\n";
        code +=     "\tregister uint64_t end;\n";
        code +=     "\tfor(register int i=0 ; i < MAX_ITERATION ; i++ ) {\n";
        code +=     "\tutask_barrier_wait(mybarrier);\n";
        code +=     "\tinitSchedComm();\n";
        code +=     "\tschedComm(SCHED_RELEASE_TIME);\n";
        for(phase_wrapper ph : phases) {
            code += "\t\tschedComm("+to_string((uint64_t)(ph.rt*conf.codegen.safety))+");\n";
            if(conf.codegen.with_delay_printf)
                code += "\tstart = __k1_read_dsu_timestamp();\n";
            code += "\t\t"+ph.callee+";\n";
            if(conf.codegen.with_delay_printf) {
                code += "\tend = __k1_read_dsu_timestamp();\n";
                code += "\tprintf(\""+ph.owner+"_"+ph.type+": %llu\\n\", end-start);\n";
            }
        }
        code +=     "\t}\n";
        code += "}\n";
        
        
        return code;
    }
    
    string generateSequentialSschedule() {
        string code = "";
        vector<phase_wrapper> phases;
        for(Task *t : tg.tasks()) {
            for(pair<string, vector<transmission_phase_info>> el : sched.tasks.at(t->id).reads) {
                for(transmission_phase_info tr : el.second) {
                    if(tr.data > 0) {
                        phases.push_back({
                            .rt = tr.rt,
                            .owner = t->id,
                            .type = "read",
                            .callee = generateCommScheduleCallRead(t, el.first, tr, 2)
                        });
                    }
                }
            }
            
            phases.push_back({
                .rt = sched.tasks.at(t->id).rtE,
                .owner = t->id,
                .type = "exec",
                .callee = t->id+"_exec()"
            });
            
            for(pair<string, vector<transmission_phase_info>> el : sched.tasks.at(t->id).writes) {
                for(transmission_phase_info tr : el.second) {
                    if(tr.data > 0) {
                        phases.push_back({
                            .rt = tr.rt,
                            .owner = t->id,
                            .type = "write",
                            .callee = generateCommScheduleCallWrite(t, el.first, tr, 2)
                        });
                    }
                }
            }
        }
        sort(phases.begin(), phases.end(), [](phase_wrapper a, phase_wrapper b) { return a.rt < b.rt; });
        
        //Steady phase
        code += "void sequential_schedule() {\n";
        code +=     "\tprintf(\"Start comm schedule\\n\");\n";
        code +=     "\tregister uint64_t start;\n";
        code +=     "\tregister uint64_t end;\n";
        code +=     "\tfor( int i=0 ; i < MAX_ITERATION ; i++ ) {\n";
        for(phase_wrapper ph : phases) {
            if(conf.codegen.with_delay_printf)
                code += "\tstart = __k1_read_dsu_timestamp();\n";
            code += "\t\t"+ph.callee+";\n";
            if(conf.codegen.with_delay_printf) {
                code += "\tend = __k1_read_dsu_timestamp();\n";
                code += "\tprintf(\""+ph.owner+"_"+ph.type+": %llu\\n\", end-start);\n";
            }
        }
        code +=     "\t}\n";
        code += "}\n";
        return code;
    }
};

REGISTER_GENERATOR(CodeGenKalrayBostan, "kalray_bostan")