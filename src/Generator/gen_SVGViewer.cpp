/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Generator.h"

using namespace std;

class SVGViewer : public Generator {
public:
    explicit SVGViewer(const SystemModel& t, const config_t& c, const Schedule_t& s) : Generator(t, c, s) {}

    virtual void generate() {
        ofstream out(conf.files.svg);

        uint32_t step = conf.view.step > 0 ? conf.view.step : 1;

        uint64_t Tmax = sched.makespan / step;

        uint32_t nbP = sched.procs.size();

        uint32_t zoomx = conf.view.zoomX > 0 ? conf.view.zoomX : 5,
                 zoomy = conf.view.zoomY > 0 ? conf.view.zoomY : 5;

        uint32_t startx = 50, starty = 60,
                fontsize=10*zoomy/4,
                proc_line_height = 5*zoomy, proc_line_separator = 5*zoomy, proc_space = proc_line_height+proc_line_separator,
                proc_x_lbl = startx-fontsize*2, proc_x_line = startx-10,
                axex = (nbP-1)*(proc_line_height+proc_line_separator)+proc_line_separator*2+starty,
                axey = (nbP-1)*(proc_line_height+proc_line_separator)+proc_line_separator*2
                ;

        out << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>" << endl;
        out << "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:svg=\"http://www.w3.org/2000/svg\" ";
        out << "height=\"" << starty+axey*1.2+20 + ((fontsize+20)*nbP*conf.archi.spm.assign_region) << "px\" ";
        out << "width=\"" << Tmax*zoomx*1.2 << "px\" ";
        out << "viewBox=\"0 0 100% 100%\" ";
        out << "version=\"1.1\"> " << endl;
        out << "<defs><pattern patternUnits=\"userSpaceOnUse\" width=\"1\" height=\"6\" id=\"horiz-line\">" << endl; 
        out << "\t<g>" << endl;
        out << "\t\t<rect style=\"fill:none;fill-opacity:0.4\" width=\"2\" height=\"6\" x=\"0\" y=\"0\" />" << endl;
        out << "\t\t<rect style=\"fill:#ff0000;fill-opacity:0.7069768\" width=\"2\" height=\"1\" x=\"0\" y=\"0\" />" << endl;
        out << "\t\t<rect style=\"fill:#ff0000;fill-opacity:0.70588235\" width=\"2\" height=\"1\" x=\"0\" y=\"2.5\" />" << endl;
        out << "\t\t<rect style=\"fill:#ff0000;fill-opacity:0.70588235\" width=\"2\" height=\"1\" x=\"0\" y=\"5\" />" << endl;
        out << "\t</g>" << endl;
        out << "</pattern><pattern patternUnits=\"userSpaceOnUse\" width=\"4\" height=\"1\" id=\"verti-line\">" << endl;
        out << "\t<g>" << endl;
        out << "\t\t<rect style=\"fill:none;fill-opacity:0.4\" width=\"4\" height=\"1\" x=\"0\" y=\"0\" />" << endl;
        out << "\t\t<rect style=\"fill:#008000;fill-opacity:0.70588235\" width=\"2\" height=\"1\" x=\"0\" y=\"0\" />" << endl;
        out << "\t</g>" << endl;
        out << "</pattern></defs>" << endl;
        out << endl;

        out << "<g id=\"makespan\">" << sched.makespan << "</g>" << endl;
        out << "<g id=\"status\">" << sched.status << "</g>" << endl;
        out << "<g id=\"time\">" << (sched.solvetime/100.0) << "</g>" << endl;

        out << "<text xml:space=\"preserve\" ";
        out << "style=\"font-weight:normal;font-size:" << fontsize << "px;fill:#000;fill-opacity:1\" ";
        out << "x=\"10\" y=\"" << fontsize+10 << "\">Method: </text>";
        out << "<text xml:space=\"preserve\" ";
        out << "style=\"font-weight:bold;font-size:" << fontsize << "px;fill:#F00;fill-opacity:1\" ";
        out << "x=\"70\" y=\"" << fontsize+10 << "\">";
        out << conf.solving.type << "/" << conf.solving.solver << " in " << sched.solvetime << " secondes </text>" << endl;

        out << "<text xml:space=\"preserve\" ";
        out << "style=\"font-weight:normal;font-size:" << fontsize << "px;fill:#000;fill-opacity:1\" ";
        out << "x=\"10\" y=\"" << 2*fontsize+10 << "\">Communication mode: </text>";
        out << "<text xml:space=\"preserve\" ";
        out << "style=\"font-weight:bold;font-size:" << fontsize << "px;fill:#F00;fill-opacity:1\" ";
        out << "x=\"160\" y=\"" << 2*fontsize+10 << "\">";
        out << conf.interconnect.type << "/" << conf.interconnect.arbiter << " -- " << conf.interconnect.mechanism
           << "/" << conf.interconnect.behavior << " -- active window time: " << conf.interconnect.active_ress_time << "</text>";

        out << "<text xml:space=\"preserve\" ";
        out << "style=\"font-weight:normal;font-size:" << fontsize << "px;fill:#000;fill-opacity:1\" ";
        out << "x=\"10\" y=\"" << 3*fontsize+10 << "\">Status: </text>";
        out << "<text xml:space=\"preserve\" ";
        out << "style=\"font-weight:bold;font-size:" << fontsize << "px;fill:#F00;fill-opacity:1\" ";
        out << "x=\"70\" y=\"" << 3*fontsize+10 << "\">";
        out << sched.status << "</text>" << endl;

        out << "<text xml:space=\"preserve\" ";
        out << "style=\"font-weight:normal;font-size:" << fontsize << "px;fill:#000;fill-opacity:1\" ";
        out << "x=\"10\" y=\"" << 4*fontsize+10 << "\">Length: </text>";
        out << "<text xml:space=\"preserve\" ";
        out << "style=\"font-weight:bold;font-size:" << fontsize << "px;fill:#F00;fill-opacity:1\" ";
        out << "x=\"70\" y=\"" << 4*fontsize+10 << "\">";
        out << sched.makespan << "</text>" << endl;

        if(sched.status > 0) {
            uint32_t i = 0;
            for(pair<string, proc_schedule_info> psched : sched.procs) {
                string procid = psched.first;
                uint32_t y = nbP * proc_space + proc_space - proc_line_separator - (i+1) * proc_space + starty;
                out << "<text xml:space=\"preserve\" ";
                out << "style=\"font-weight:bold;font-size:" << fontsize << "px;fill:#000000;fill-opacity:1\" ";
                out << "x=\"" << proc_x_lbl << "\" y=\"" << y << "\">";
                out << procid << "</text>" << endl;

                out << "<path style=\"fill:none;fill-opacity:0;stroke:#000000;stroke-width:1px;stroke-opacity:1";
                out <<      ";stroke-miterlimit:4;stroke-dasharray:1,8;stroke-dashoffset:0\" ";
                out << "d=\"m " << proc_x_line << ".0," << y << ".0 " << Tmax*zoomx+10 << ".0,0\" ";
                out << "/>" << endl;

                for(pair<string, task_schedule_info> tsched : sched.tasks) {
                    try {
                        if(tsched.second.proc->id != procid)  continue;

                        string tid = tsched.first;
                        uint32_t wcet = tsched.second.WCET;
                        uint64_t res = tsched.second.rtE;
                        int cost = -1;
                        int release = -1;
                        for(pair<string, vector<transmission_phase_info>> eltr : tsched.second.reads) {
                            for(transmission_phase_info tr : eltr.second) {
                                release = tr.rt;
                                cost = tr.delay;
                                out << "<rect id=\"r" << tid << "_c\" style=\"fill:url(#verti-line);fill-opacity:1;stroke:#000000;stroke-opacity:1;stroke-width:0.4;\" "; // rgb(183, 255, 80) stroke:grey;stroke-width:1px;stroke-opacity:0.8
                                out << "width=\"" << (cost * zoomx)/step << "\" height=\"" << proc_line_height/8 << "\" ";
                                out << "x=\"" << startx+(release*zoomx)/step << "\" y=\"" << y+(proc_line_height/8)+1 << "\" />";

                                out << "<text id=\"tcr" << tid << "_c\" xml:space=\"preserve\" ";
                                out << "style=\"font-weight:normal;font-size:" << fontsize/2 << "px;fill:rgb(17, 86, 17);fill-opacity:1\" ";
                                out << "x=\"" << startx+(res*zoomx)/step+5 << "\" y=\"" << y+fontsize/2+(proc_line_height/8) << "\">";
                                out << cost << "</text>" << endl;
                            } 
                        }

                        out << "<rect id=\"r" << tid << "\" style=\"fill:" << (tsched.second.is_in_mutex ? "#3545ff" : "#40d6da") << ";fill-opacity:0.5;stroke:#ff00ff;stroke-width:1px;stroke-opacity:1\" ";
                        out << "width=\"" << (wcet*zoomx)/step << "\" height=\"" << proc_line_height << "\" ";
                        out << "x=\"" << startx+(res*zoomx)/step << "\" y=\"" << y-proc_line_height << "\" />";

                        out << "<text id=\"tt" << tid << "\" xml:space=\"preserve\" ";
                        out << "style=\"font-weight:normal;font-size:" << fontsize << "px;fill:#f10000;fill-opacity:1\" ";
                        out << "x=\"" << startx+(res*zoomx)/step+5 << "\" y=\"" << y-fontsize << "\">";
                        out << tid << "</text>" << endl;

                        for(pair<string, vector<transmission_phase_info>> eltr : tsched.second.writes) {
                            for(transmission_phase_info tr : eltr.second) {
                                release = tr.rt;
                                cost = tr.delay;
                                out << "<rect id=\"s" << tid << "_c\" style=\"fill:url(#horiz-line);fill-opacity:1;stroke:#000000;stroke-opacity:1;stroke-width:0.4;\" "; // rgb(255, 249, 185) stroke:grey;stroke-width:1px;stroke-opacity:0.8
                                out << "width=\"" << (cost * zoomx)/step << "\" height=\"" << proc_line_height/8 << "\" ";
                                out << "x=\"" << startx+(release*zoomx)/step << "\" y=\"" << y+(proc_line_height/8) << "\" />";

                                out << "<text id=\"tcs" << tid << "_c\" xml:space=\"preserve\" ";
                                out << "style=\"font-weight:normal;font-size:" << fontsize/2 << "px;fill:rgb(17, 86, 17);fill-opacity:1\" ";
                                out << "x=\"" << startx+(release*zoomx)/step-10 << "\" y=\"" << y+fontsize/2+(proc_line_height/8) << "\">";
                                out << cost << "</text>" << endl;
                            }
                        }
                    } catch(exception &) {}
                }
                i++;
            }

            out << "<path style=\"fill:none;stroke:#000000;stroke-width:1px;stroke-opacity:1\" ";
            out << "d=\"m " << startx << ".0, " << starty << ".0 0," << axey << ".0\" ";
            out << "/>" << endl;
            out << "<path style=\"fill:none;stroke:#000000;stroke-width:1px;stroke-opacity:1\" ";
            out << "d=\"m " << startx << ".0," << axex << ".0 " << Tmax*zoomx << ".0,0\" ";
            out << "/>" << endl;

            for(uint32_t i=0 ; i < Tmax ; i+=step*10) {
                out << "<path style=\"fill:none;stroke:#000000;stroke-width:1px;stroke-opacity:1";
                out <<      ";stroke-miterlimit:4;stroke-dasharray:1,8;stroke-dashoffset:0\" ";
                out << "d=\"m " << startx+i*zoomx << ".0, " << starty << ".0 0," << axey+proc_line_separator << ".0\" ";
                out << "/>" << endl;

                out << "<text xml:space=\"preserve\" ";
                out << "style=\"font-weight:normal;font-size:" << fontsize/2 << "px;fill:#000;fill-opacity:1\" ";
                out << "x=\"" << startx+i*zoomx << "\" y=\"" << axex+fontsize/2+8 << "\">";
                out << i << "</text>" << endl;
            }

            if(conf.archi.spm.assign_region == true) {
                int y = axex+fontsize/2+50;
                for(pair<string, proc_schedule_info> p : sched.procs) {
                    out << "<text xml:space=\"preserve\" ";
                    out << "style=\"font-weight:normal;font-size:" << fontsize << "px;fill:#000;fill-opacity:1\" ";
                    out << "x=\"" << startx << "\" y=\"" << y << "\">";
                    out << p.first << "</text>" << endl;

                    out << "<rect id=\"spm" << p.first << "\" style=\"fill-opacity:0;stroke-width:1; stroke-opacity:1;stroke:#000;\" ";
                    out << "width=\"" << 100 * zoomx + 2 << "\" height=\"" << fontsize << "\" ";
                    out << "x=\"" << startx+29 << "\" y=\"" << (y-fontsize) << "\" />" << endl;

                    uint32_t R = 40;
                    uint32_t G = 0;
                    uint32_t B = 140;

                    float x = startx+30;
                    for(spm_region_info r : sched.procs.at(p.first).memory_regions) {
                        if(r.size == 0) 
                            continue;
                        float width = r.size * 100 / (float) p.second.total_spm_size;

                        out << "<rect id=\"sr" << r.label << "\" style=\"fill:rgb(" << R << "," << G << "," << B << ");fill-opacity:1;stroke-width:0;\" ";
                        out << "width=\"" << width * zoomx << "\" height=\"" << fontsize-2 << "\" ";
                        out << "x=\"" << x << "\" y=\"" << (y-fontsize+1) << "\" />" << endl;

                        x += width*zoomx+0.1;

                        G = (G+50)%255;
                    }
                    y += fontsize+20;
                }
            }
        }
        out << "</svg>" << endl;

        out.close();
    }
};

REGISTER_GENERATOR(SVGViewer, "svg")
