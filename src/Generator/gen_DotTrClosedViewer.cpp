/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Generator.h"

using namespace std;

class DotTrClosedViewer : public Generator {
public:
    explicit DotTrClosedViewer(const SystemModel& t, const config_t& c, const Schedule_t& s) : Generator(t, c, s) {}

    virtual void generate() {
        ofstream out(conf.files.trdot);
        out << "strict Digraph G {" << endl;
        out << "rankdir=LR; " << endl
            << "margin=0;" << endl
            << "nodesep=0.2;" << endl
            << "ranksep=0.3;" << endl;


        for(Task * i : tg.tasks()) {
            out << "\"task_" << i << "\"[label=\"" << i->id << "\"];" << endl;
            for(Task *ii : i->previous_transitiv) {
                out << "\"task_" << ii << "\" -> \"task_" << i << "\"";
    //            if(ii.second->back_edge)
    //                out << "[style=dashed]";
                bool found = false;
                for(Task::prev_t iii : i->previous) {
                    if(iii.first == ii) {
                        found = true;
                        break;
                    }
                }
                out << "[color=" << (found ? "black" : "grey") << "]";

                out << ";" << endl;
            }
        }

        out << "}" << endl;

        out.close();
    }
};

REGISTER_GENERATOR(DotTrClosedViewer, "trdot")