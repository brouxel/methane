/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CP_CONDITIONNALTIMEINTERVAL_H
#define CP_CONDITIONNALTIMEINTERVAL_H

#include <iostream>
#include <string>
#include <map>
#include <fstream>

#include "config.h"
#include "SystemModel.h"
#include "Schedule.h"
#include "Solver.h"
#include "Utils.h"

#ifdef CPLEXOPL
#include <ilopl/iloopl.h>
#include <ilopl/ilooplprofiler.h>
#endif

using namespace std;

class CTIGenerator {
    const SystemModel &tg;
    const config_t &conf;
    
public:
    explicit CTIGenerator(const SystemModel &m, const config_t &c) : tg(m), conf(c) {}
    virtual void gen() = 0;
    virtual const string& getModelFileName() = 0;
};

using CTIGeneratorRegistry = registry::Registry<CTIGenerator, string, const SystemModel &, const config_t &>;

#define REGISTER_CTIGENERATOR(ClassName, Identifier) \
  REGISTER_SUBCLASS(CTIGenerator, ClassName, string, Identifier, const SystemModel &, const config_t &)


class CTISolver : public Solver {
    static const map<string, string> models;
    static const string modelFileName;
protected:
    map<string, int> tmp_results;
    
    virtual void presolve(Schedule_t *result);
    virtual void postsolve(Schedule_t *result);
    virtual void run(Schedule_t *result);
public:
    explicit CTISolver(const SystemModel &tg, const config_t &conf);
};

REGISTER_SOLVER(CTISolver, "cti")
       


#endif /* CP_CONDITIONNALTIMEINTERVAL_H */

