/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <iostream>
#include <cxxabi.h>
#include <boost/lexical_cast.hpp>
#include <boost/integer/common_factor.hpp>
#include <sys/stat.h>
#include <numeric>
#include <vector>

#ifdef __APPLE__
#include <mach/vm_statistics.h>
#include <mach/mach_types.h>
#include <mach/mach_init.h>
#include <mach/mach_host.h>
#include<mach/mach.h>
#include <sys/types.h>
#include <sys/sysctl.h>
#endif
#ifdef __linux
#include <sys/types.h>
#include <sys/sysinfo.h>
#include <fstream>
#endif

class Task;

namespace Utils {
    bool file_exists(const char* name);
    bool file_exists(const std::string &name);
    std::string demangle(const std::string & str);
    
    bool sort_Q_byComm(Task *a, Task *b);
    bool sort_Q_byName(Task *a, Task *b);
    bool sort_Q_byMemUsageDec(Task *a, Task *b);
    bool sort_Q_byWCET(Task *a, Task *b);
    
    void DEBUG(const std::string &);
    void WARN(const std::string &);
    void WARN(const std::string &, const std::string &);
    
    uint64_t size_to_uint(const std::string &);
    std::string uint_to_size(uint64_t);
    
    uint64_t lcm(const std::vector<uint64_t> &values);
    
    void memory_usage(const std::string &msg = "");
    uint64_t get_ram_usage();
    uint64_t get_swap_usage();
    uint64_t get_total_ram();
    uint64_t get_total_swap();
    
  /**
   *  @brief Copy the elements of a sequence for which a predicate is true.
   *  @ingroup mutating_algorithms
   *  @param  __first   An input iterator.
   *  @param  __last    An input iterator.
   *  @param  __result  An output iterator.
   *  @param  __pred    A predicate.
   *  @return   An iterator designating the end of the resulting sequence.
   *
   *  Copies each element in the range @p [__first,__last) for which
   *  @p __pred returns true to the range beginning at @p __result.
   *
   *  copy_if() is stable, so the relative order of elements that are
   *  copied is unchanged.
  */
  template<typename _InputIterator, typename _OutputIterator,
	   typename _Predicate, typename _Predicate2>
    _OutputIterator
    copy_if_until(_InputIterator __first, _InputIterator __last,
	    _OutputIterator __result, _Predicate __pred, _Predicate2 __stop)
    {
#ifdef __GNU_LIBRARY__
      // concept requirements
      __glibcxx_function_requires(_InputIteratorConcept<_InputIterator>)
      __glibcxx_function_requires(_OutputIteratorConcept<_OutputIterator,
	    typename iterator_traits<_InputIterator>::value_type>)
      __glibcxx_function_requires(_UnaryPredicateConcept<_Predicate,
	    typename iterator_traits<_InputIterator>::value_type>)
      __glibcxx_requires_valid_range(__first, __last);
#endif
      for (; __first != __last && !__stop(*__first); ++__first)
	if (__pred(*__first))
	  {
	    *__result = *__first;
	    ++__result;
	  }
      return __result;
    }
  
    /**
   *  @brief Copy the elements of a sequence for which a predicate is true.
   *  @ingroup mutating_algorithms
   *  @param  __first   An input iterator.
   *  @param  __last    An input iterator.
   *  @param  __result  An output iterator.
   *  @param  __pred    A predicate.
   *  @return   An iterator designating the end of the resulting sequence.
   *
   *  Copies each element in the range @p [__first,__last) for which
   *  @p __pred returns true to the range beginning at @p __result.
   *
   *  copy_if() is stable, so the relative order of elements that are
   *  copied is unchanged.
  */
  template<typename _InputIterator, typename _OutputIterator,
	   typename _Predicate, typename _Predicate2>
    _OutputIterator
    copy_from_if(_InputIterator __first, _InputIterator __last,
	    _OutputIterator __result, _Predicate __from, _Predicate2 __pred)
    {
#ifdef __GNU_LIBRARY__
      // concept requirements
      __glibcxx_function_requires(_InputIteratorConcept<_InputIterator>)
      __glibcxx_function_requires(_OutputIteratorConcept<_OutputIterator,
	    typename iterator_traits<_InputIterator>::value_type>)
      __glibcxx_function_requires(_UnaryPredicateConcept<_Predicate,
	    typename iterator_traits<_InputIterator>::value_type>)
      __glibcxx_requires_valid_range(__first, __last);
#endif
      for(; !__from(*__first) ; ++__first) ;
      ++__first;                            // starting element is not included
      
      for (; __first != __last; ++__first)
	if (__pred(*__first))
	  {
	    *__result = *__first;
	    ++__result;
	  }
      return __result;
    }
  
  
    template<class _OutputClass, class _InputClass>
    bool isa(_InputClass in) {
        _OutputClass tmp = dynamic_cast<_OutputClass>(in);
        return (tmp != nullptr);
    }
}

#endif /* UTILS_H */

