/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMMUNICATIONMODEL_H
#define COMMUNICATIONMODEL_H

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <map>
#include <vector>
#include <cmath>
#include <string>

using namespace std;

#include "config.h"
#include "registry.h"

class SystemModel;
class Task;
struct meth_processor_t;

class CoreMemInterConnect {
protected:
    const SystemModel &tg;
    const config_t &conf;
public:
    static CoreMemInterConnect* factory(const SystemModel &, const config_t &) ;
    
    explicit CoreMemInterConnect(const SystemModel & t, const config_t &c) : tg(t), conf(c) {}
    virtual ~CoreMemInterConnect() {}
    
    virtual uint32_t comm_delay(uint32_t concurrency, uint32_t data, string datatype, uint64_t forced_delay) const = 0;
    virtual uint32_t wait_time(uint32_t concurrency, uint32_t data) const = 0;
    
    virtual uint32_t getActiveWindowTime() = 0;
    
    static vector<uint32_t> burst(const config_t &conf, uint32_t data, uint32_t datatypesize);
    static uint32_t token_per_burst_chunk(const config_t &conf, uint32_t datatypesize);
};

using InterConnectRegistry = registry::Registry<CoreMemInterConnect, string, const SystemModel &, const config_t &>;

#define REGISTER_INTERCONNECT(ClassName, Identifier) \
  REGISTER_SUBCLASS(CoreMemInterConnect, ClassName, string, Identifier, const SystemModel &, const config_t &)

#endif /* COMMUNICATIONMODEL_H */

