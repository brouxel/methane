/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ILP.h"

using namespace std;

#ifdef CPLEX

#include <ilcplex/ilocplex.h>
#include <ilcplex/cplex.h>
#include <ilconcert/ilotupleset.h>
#include <ilconcert/ilosys.h>

#endif

class ILPSynchronized : public ILPFormGen {
public:
    explicit ILPSynchronized(const SystemModel &m, const config_t &c) : ILPFormGen(m, c) {}
    
    virtual void gen() override {
        if(conf.interconnect.behavior == "non-blocking" && conf.interconnect.burst != "none") {
            gen_with_cplexAPI();
            return;
        }
        
        
        
        SeqTauMax = tg.global_deadline();
        
        os.open(conf.files.lp);

        objectiv_func();

        unicity();
        detect_samecore();
        precedence();
        precedence_samecore();
        conflicts();
        read_exec_write();
        causality();
        //job_causality();
        
        precedence_communications();
        if(conf.interconnect.behavior == "non-blocking") {
            precedence_samecore_communications();
            conflicts_communications();
            causality_communications();
        }
        communication_delay();
        ensure_mutex();
        map_spm_region_to_task();
        
        //periodicity_deadlines();
        
        force_mapping();
        force_schedule();
        
        linkObj();

        add_bounds();
        
        declare_binaries();
        declare_generals();
        
        concludeILPFile();

        os.close();
    }
    
protected:    
    void communication_delay() override {
        os << endl;

        for(Task *t : tg.tasks()) {
            genvar += "DR_"+*t+"\n";
            genvar += "DW_"+*t+"\n";
            
            string tmp = "";
            uint32_t sum = 0;
            string datatype = "";
            if(conf.archi.prefetch_code)
                sum = t->memory_code_size;
            
            if(t->force_read_delay > 0) {
                os << "DelayR" << t << ": delay_r_" << t << " = " << t->force_read_delay << endl;
                os << "DR" << t << ": DR_" << t << " = 0" << endl;
            }
            else {
                for(Task::prev_t el : t->previous) {
                    uint32_t data = el.second;
                    tmp += " + "+to_string(data)+" m_" + el.first + "_" + *t;
                    sum += data;
                    datatype = t->previous_datatypes[el.first];
                }
                if(conf.interconnect.mechanism == "sharedonly") {
                    os << "DelayR" << t << ": delay_r_" << t << " = " << tg.interconnect()->comm_delay(0, sum, datatype, 0);
                    os << endl;
                    os << "DR" << t << ": DR_" << t << " = " << sum << endl;
                }
                else if(sum > 0) {
                    sum *= conf.datatypes.size_bits.at(datatype);
                    sum = ceil(sum/conf.interconnect.bit_per_timeunit); // probably + (bit_per_timeunit - (sum % bit_per_timeunit)) to add extra bit if data not aligned, e.g. send 5 bits when 8 bits are sent at a time, need to add 3 zeroes
                    os << "DR" << t << ": DR_" << t << tmp << " = " << sum << endl;
                    os << "TRSlotR" << t << ": DR_" << t << " - " << tg.interconnect()->getActiveWindowTime() << " trslot_r_" << t << " - remdata_r_" << t << " = 0" << endl;
                    os << "WSlotR" << t << ": DR_" << t << " - " << tg.interconnect()->getActiveWindowTime() << " waitslot_r_" << t << " + unused_r_" << t << " = 0" << endl;
                    os << "RoundR" << t << ": unused_r_" << t << " - remdata_r_" << t << " <= 1" << endl;
                    os << "DelayR" << t << ": delay_r_" << t
                       << " - " << tg.interconnect()->getActiveWindowTime() << " trslot_r_" << t 
                       << " - remdata_r_" << t << " = 0" << endl;
                    genvar += "trslot_r_"+*t+"\n";
                    genvar += "waitslot_r_"+*t+"\n";
                    genvar += "remdata_r_"+*t+"\n";
                    genvar += "unused_r_"+*t+"\n";
                }
                else {
                    os << "delay_r_" << t << " = 0" <<endl;
                    os << "DR" << t << ": DR_" << t << " = 0" << endl;
                }
            }

            os << endl;

            tmp = "";
            sum = 0;
            
            if(t->force_write_delay > 0) {
                os << "DelayW" << t << ": delay_w_" << t << " = " << t->force_write_delay << endl;
                os << "DW" << t << ": DW_" << t << " = 0" << endl;
            }
            else {
                for(Task *el : t->successors) {
                    uint32_t data = t->data_written(*el);
                    tmp += " + "+to_string(data)+" m_" + t + "_" + el ;
                    sum += data;
                    datatype = el->previous_datatypes[t];
                }
                if(conf.interconnect.mechanism == "sharedonly") {
                    os << "DelayW" << t << ": delay_w_" << t << " = " << tg.interconnect()->comm_delay(0, sum, datatype, 0) << endl;
                    os << "DW" << t << ": DW_" << t << " = " << sum << endl;
                }
                else if(sum > 0) {
                    sum *= conf.datatypes.size_bits.at(datatype);
                    sum = ceil(sum/conf.interconnect.bit_per_timeunit); // probably + (bit_per_timeunit - (sum % bit_per_timeunit)) to add extra bit if data not aligned, e.g. send 5 bits when 8 bits are sent at a time, need to add 3 zeroes
                    os << "DW" << t << ": DS_" << t << tmp << " = " << sum << endl;
                    os << "TRSlotW" << t << ": DW_" << t << " - " << tg.interconnect()->getActiveWindowTime() << " trslot_w_" << t << " - remdata_w_" << t << " = 0" << endl;
                    os << "WSlotW" << t << ": DW_" << t << " - " << tg.interconnect()->getActiveWindowTime() << " waitslot_w_" << t << " + unused_w_" << t << " = 0" << endl;
                    os << "RoundW" << t << ": unused_w_" << t << " - remdata_w_" << t << " <= 1" << endl;
                    os << "DelayW" << t << ": delay_w_" << t 
                       << " - " << tg.interconnect()->getActiveWindowTime() << " trslot_w_" << t 
                       << " - remdata_w_" << t << " = 0" << endl;

                    genvar += "trslot_w_"+*t+"\n";
                    genvar += "waitslot_w_"+*t+"\n";
                    genvar += "remdata_w_"+*t+"\n";
                    genvar += "unused_w_"+*t+"\n";
                }
                else {
                    os << "delay_w_" << t << " = 0" << endl;
                    os << "DW" << t << ": DW_" << t << " = 0" << endl;
                }
            }

            os << endl;
        }
    }
    
    void ensure_mutex() {
        os << endl ;
        for(Task *i : tg.tasks()) {
            for(Task *j : tg.tasks()) {
                if(i == j || !i->is_parallel(j)) continue;

                os << "Mutex1_" << i << "_" << j << ": rho_r_" << i << " + delay_r_" << i << " - rho_r_" << j << " + " << SeqTauMax << " a_rr_" << i << "_" << j << " <= " << SeqTauMax << endl;
                os << "Mutex2_" << i << "_" << j << ": rho_w_" << i << " + delay_w_" << i << " - rho_w_" << j << " + " << SeqTauMax << " a_ww_" << i << "_" << j << " <= " << SeqTauMax << endl;
                os << "Mutex3_" << i << "_" << j << ": rho_r_" << i << " + delay_r_" << i << " - rho_w_" << j << " + " << SeqTauMax << " a_rw_" << i << "_" << j << " <= " << SeqTauMax << endl;
                os << "Mutex4_" << i << "_" << j << ": rho_w_" << i << " + delay_w_" << i << " - rho_r_" << j << " + " << SeqTauMax << " a_wr_" << i << "_" << j << " <= " << SeqTauMax << endl;
            }
        }
    }
    
//------------------------------------------------------------------------------
// CPLEX API
//------------------------------------------------------------------------------
    
private:
       // Transmission packet for burst bus accesses
    typedef struct ILPPacket_s {
        string name;
        IloInt delay;
        IloInt data;
        string datatype;
        Task *owner;
        
        IloIntVar rho;
        map<ILPPacket_s*, IloBoolVar> precedence;
        
        vector<ILPPacket_s*> previous_transitiv;
        vector<ILPPacket_s*> previous;
        vector<ILPPacket_s*> successors;
    } ILPPacket_t;
    
    // Size of reservation area for transmitted packet, might includes several ones
    typedef struct ILPSPMPacket_s {
        string name;
        Task *owner;
        IloInt size;
        vector<ILPPacket_t*> includes;
        
        map<string, IloBoolVar> mapping;
        map<ILPSPMPacket_s*, IloBoolVar> samereg;
        map<ILPSPMPacket_s*, IloBoolVar> precedence;
        map<ILPSPMPacket_s*, IloBoolVar> precedence_samereg;
        IloIntVar startresa;
        IloIntVar endresa;
        
    } ILPSPMPacket_t;
    
    typedef struct ILPTask_s {
        IloIntVar rho;
        IloIntVar _end;
        
        map<meth_processor_t*, IloBoolVar> mapping;
        map<Task*, IloBoolVar> samecore;
        map<Task*, IloBoolVar> precedence;
        map<Task*, IloBoolVar> precedence_samecore;
        
        map<Task*, vector<ILPPacket_t*>> packets_r;
        map<Task*, vector<ILPPacket_t*>> packets_w;
        
        map<Task*, vector<ILPSPMPacket_t*>> spmpackets_r;
        map<Task*, vector<ILPSPMPacket_t*>> spmpackets_w;
        
        ILPSPMPacket_t *exec_special_spmpacket;
    } ILPTask_t;
    
    map<Task*, ILPTask_t> system;
    vector<ILPPacket_t*> flat_packets;
    vector<ILPSPMPacket_t*> flat_spmpackets;

    map<meth_processor_t*, map<string, IloIntVar>> regions;
    
    void compute_transitive_closure_packets() {
        for(ILPPacket_t *t : flat_packets) {
            t->previous_transitiv.clear();
        }
        vector<ILPPacket_t*> explored;
        for(ILPPacket_t *t : flat_packets) {
            if(t->successors.size() > 0) continue;

            explored.push_back(t);
            compute_transitive_closure_packets_aux(t, &explored);
        }

        for(ILPPacket_t *t : flat_packets) {
            sort(t->previous_transitiv.begin(), t->previous_transitiv.end(), [](ILPPacket_t *a, ILPPacket_t *b) {
                return a->name < b->name;
            });
            vector<ILPPacket_t*>::iterator it = unique(t->previous_transitiv.begin(), t->previous_transitiv.end());
            t->previous_transitiv.resize(distance(t->previous_transitiv.begin(), it));
        }
    }

    void compute_transitive_closure_packets_aux(ILPPacket_t *tc, vector<ILPPacket_t*> *explored) {
        for(ILPPacket_t* tmp : tc->previous) {
            tc->previous_transitiv.push_back(tmp);

            if(find(explored->begin(),explored->end(), tmp) == explored->end()) {
                explored->push_back(tmp);
                compute_transitive_closure_packets_aux(tmp, explored);
            }

            tc->previous_transitiv.insert(tc->previous_transitiv.end(), tmp->previous_transitiv.begin(), tmp->previous_transitiv.end());

            sort(tc->previous_transitiv.begin(), tc->previous_transitiv.end(), [](ILPPacket_t *a, ILPPacket_t *b) {
                return a->name < b->name;
            });
            vector<ILPPacket_t*>::iterator it = unique(tc->previous_transitiv.begin(), tc->previous_transitiv.end());
            tc->previous_transitiv.resize(distance(tc->previous_transitiv.begin(), it));
        }
    }
    
    void gen_with_cplexAPI() {
#ifdef CPLEX
        Utils::memory_usage("Init");
        IloEnv env;
        IloModel model(env);
        
        try {
            IloInt TauMax = SeqTauMax;
            IloIntVar makespan(env, 0, TauMax, "makespan");
            
            vector<string> regs;
            for(Task *t : tg.tasks()) {
                ILPTask_t &ilptask = system[t];
                for(meth_processor_t *proc : tg.processors()) {
                    ilptask.mapping[proc] = IloBoolVar(env, string("p_"+*t+"_"+*proc).c_str());
                }
                
                for(Task *u : tg.tasks()) {
                    if(t == u) continue;
                    ilptask.samecore[u] = IloBoolVar(env, string("m_"+*t+"_"+*u).c_str());
                    ilptask.precedence[u] = IloBoolVar(env, string("a_"+*t+"_"+*u).c_str());
                    ilptask.precedence_samecore[u] = IloBoolVar(env, string("am_"+*t+"_"+*u).c_str());
                }
                
                ilptask.rho = IloIntVar(env, 0, TauMax, string("rho_"+*t).c_str());
                ilptask._end = IloIntVar(env, 0, TauMax, string("fin_"+*t).c_str());
            
                for(Task::prev_t prev : t->previous) {
                    uint32_t D_ij = prev.second;
                    vector<uint32_t> slots = CoreMemInterConnect::burst(conf, D_ij, conf.datatypes.size_bits.at(t->previous_datatypes[prev.first]));
                    
                    // Edges spm granularity
                    string lbl = "r_"+*t+"_"+*prev.first;
                    ILPSPMPacket_t *spmread = new ILPSPMPacket_t;
                    spmread->owner = t;
                    spmread->name = lbl;
                    spmread->startresa = IloIntVar(env, 0, TauMax, ("startresa_"+lbl).c_str());
                    spmread->endresa = IloIntVar(env, 0, TauMax, ("finresa_"+lbl).c_str());
                    spmread->size = D_ij * conf.datatypes.size_bits.at(t->previous_datatypes[prev.first]);
                    ilptask.spmpackets_r[prev.first].push_back(spmread);
                    flat_spmpackets.push_back(spmread);
                    regs.push_back("MR"+lbl);
                    
                    for(uint32_t s=0 ; s < slots.size() ; ++s) {
                        string slbl = lbl+"_"+to_string(s);
                        ILPPacket_t *read = new ILPPacket_t;
                        read->owner = t;
                        read->name = slbl;
                        read->delay = tg.interconnect()->comm_delay(0, slots[s], t->previous_datatypes[prev.first], t->force_read_delay);
                        read->data = slots[s];
                        read->datatype = t->previous_datatypes[prev.first];
                        read->rho = IloIntVar(env, 0, TauMax, ("rho_"+slbl).c_str());
                        
                        spmread->includes.push_back(read);
                        
                        ilptask.packets_r[prev.first].push_back(read);
                        flat_packets.push_back(read);
                    }
                }
                
                for(Task *succ : t->successors) {
                    uint32_t D_ij = t->data_written(*succ);
                    vector<uint32_t> slots = CoreMemInterConnect::burst(conf, D_ij, conf.datatypes.size_bits.at(succ->previous_datatypes[t]));
                    
                    string lbl = "w_"+*t+"_"+*succ;
                    ILPSPMPacket_t *spmwrite = new ILPSPMPacket_t;
                    spmwrite->owner = t;
                    spmwrite->name = lbl;
                    spmwrite->startresa = IloIntVar(env, 0, TauMax, ("startresa_"+lbl).c_str());
                    spmwrite->endresa = IloIntVar(env, 0, TauMax, ("finresa_"+lbl).c_str());
                    spmwrite->size = D_ij * conf.datatypes.size_bits.at(succ->previous_datatypes[t]);
                    ilptask.spmpackets_r[succ].push_back(spmwrite);
                    flat_spmpackets.push_back(spmwrite);
                    regs.push_back("MR"+lbl);
                    
                    for(uint32_t s=0 ; s < slots.size() ; ++s) {
                        string slbl = lbl+"_"+to_string(s);
                        ILPPacket_t *write = new ILPPacket_t;
                        write->owner = t;
                        write->name = slbl;
                        write->delay = tg.interconnect()->comm_delay(0, slots[s], succ->previous_datatypes[t], t->force_write_delay);
                        write->data = slots[s];
                        write->datatype = succ->previous_datatypes[t];
                        write->rho = IloIntVar(env, 0, TauMax, ("rho_"+slbl).c_str());
                        
                        spmwrite->includes.push_back(write);
                        
                        ilptask.packets_w[succ].push_back(write);
                        flat_packets.push_back(write);
                    }
                }
                
                regs.push_back("MR"+*t);
                string lbl = "e_"+*t;
                ilptask.exec_special_spmpacket = new ILPSPMPacket_t;
                ilptask.exec_special_spmpacket->owner = t;
                ilptask.exec_special_spmpacket->name = lbl;
                ilptask.exec_special_spmpacket->size = t->local_memory_storage + t->memory_code_size;
                ilptask.exec_special_spmpacket->startresa = IloIntVar(env, 0, TauMax, ("startresa_"+lbl).c_str());
                ilptask.exec_special_spmpacket->endresa = IloIntVar(env, 0, TauMax, ("finresa_"+lbl).c_str());
                flat_spmpackets.push_back(ilptask.exec_special_spmpacket);
            }
            
            for(pair<Task*, ILPTask_t> elt : system) {
                for(pair<Task*, vector<ILPPacket_t*>> pac_t : elt.second.packets_r) {
                    if(pac_t.second.size() == 0) continue;
                    
                    for(int i=0 ; i < pac_t.second.size() ; ++i) {
                        if(i+1 < pac_t.second.size()) {
                            pac_t.second[i]->successors.push_back(pac_t.second[i+1]);
                            pac_t.second[i+1]->previous.push_back(pac_t.second[i]);
                        }
                    }
                    vector<ILPPacket_t*> &pac_u = system[pac_t.first].packets_w[elt.first];
                    pac_u[pac_u.size()-1]->successors.push_back(pac_t.second[0]);
                    pac_t.second[0]->previous.push_back(pac_u[pac_u.size()-1]);
                }
                for(pair<Task*, vector<ILPPacket_t*>> pac_t : elt.second.packets_w) {
                    if(pac_t.second.size() == 0) continue;
                    
                    for(int i=0 ; i < pac_t.second.size() ; ++i) {
                        if(i+1 < pac_t.second.size()) {
                            pac_t.second[i]->successors.push_back(pac_t.second[i+1]);
                            pac_t.second[i+1]->previous.push_back(pac_t.second[i]);
                        }
                    }
                }
            }
            compute_transitive_closure_packets();
            
            for(meth_processor_t *p : tg.processors()) {
                for(string r : regs) {
                    regions[p][r] = IloIntVar(env, 0, p->spm_size, ("spmregsize_"+*p+"_"+r).c_str());
                }
            }
            
            for(ILPPacket_t *i : flat_packets) {
                for(ILPPacket_t *j : flat_packets) {
                    if(i == j) continue;
                    
                    i->precedence[j] = IloBoolVar(env, ("a_"+i->name+"_"+j->name).c_str());
                }
            }
            
            for(ILPSPMPacket_t *i : flat_spmpackets) {
                for(pair<string, IloIntVar> r : regions[tg.processors()[0]]) {
                    i->mapping[r.first] = IloBoolVar(env, ("spmp_"+i->name+"_"+r.first).c_str());
                }
                for(ILPSPMPacket_t *j : flat_spmpackets) {
                    if(i == j)continue;
                    i->samereg[j] = IloBoolVar(env, ("spmm_"+i->name+"_"+j->name).c_str());
                    i->precedence[j] = IloBoolVar(env, ("spma_"+i->name+"_"+j->name).c_str());
                    i->precedence_samereg[j] = IloBoolVar(env, ("spmam_"+i->name+"_"+j->name).c_str());
                }
            }
            
            Utils::memory_usage("Var ready");

            //force mapping
            for(pair<Task*, ILPTask_t> el : system) {
                if(!el.first->force_mapping_proc.empty()) {
                    for(pair<meth_processor_t*, IloBoolVar> var : el.second.mapping) {
                        if(el.first->force_mapping_proc == var.first->id) {
                            model.add(var.second == 1);
                        }
                    }
                }
            }

            // unicity
            for(pair<Task*, ILPTask_t> el : system) {
                IloBoolVarArray map(env);
                for(pair<meth_processor_t*, IloBoolVar> var : el.second.mapping) {
                    map.add(var.second);
                }
                model.add(IloSum(map) == 1);
            }
            
            // detect same core --> possibilité d'optimiser le nombre de contraintes ici
            for(map<Task*, ILPTask_t>::iterator it=system.begin(), et=system.end() ; it != et ; ++it) {
                for(map<Task*, ILPTask_t>::iterator jt=next(it) ; jt != et ; ++jt) {
                    ILPTask_t &i = it->second, &j = jt->second;
                    
                    if(find(it->first->previous_transitiv.begin(), it->first->previous_transitiv.end(), jt->first) != it->first->previous_transitiv.end() ||
                        find(jt->first->previous_transitiv.begin(), jt->first->previous_transitiv.end(), it->first) != jt->first->previous_transitiv.end()) continue;
                    
                    /*IloOr myor(env);
                    for (meth_processor_t *proc : tg.processors()) {
                        IloAnd myand(env);
                        myand.add(i.mapping[proc] == 1);
                        myand.add(j.mapping[proc] == 1);
                        myor.add(myand);
                    }
                    model.add(i.samecore[jt->first] == myor);
                     */
                    IloBoolVarArray acctmp_m(env);
                    for (meth_processor_t *proc : tg.processors()) {
                        IloBoolVar tmp_m(env, ("tmp_m_"+*(it->first)+"_"+jt->first+"_"+proc).c_str());
                        acctmp_m.add(tmp_m);
                        model.add(i.mapping[proc] + j.mapping[proc] <= tmp_m+1);
                        model.add(i.mapping[proc] >= tmp_m);
                        model.add(j.mapping[proc] >= tmp_m);
                    }
                    model.add(IloSum(acctmp_m) == i.samecore[jt->first]);
                    
                    model.add(i.samecore[jt->first] == j.samecore[it->first]);
                }
            }
            
            // precedence
            for(map<Task*, ILPTask_t>::iterator it=system.begin(), et=system.end() ; it != et ; ++it) {
                for(map<Task*, ILPTask_t>::iterator jt=next(it) ; jt != et ; ++jt) {
                    ILPTask_t &i = it->second, &j = jt->second;
                    
                    if(find(it->first->previous_transitiv.begin(), it->first->previous_transitiv.end(), jt->first) != it->first->previous_transitiv.end() ||
                        find(jt->first->previous_transitiv.begin(), jt->first->previous_transitiv.end(), it->first) != jt->first->previous_transitiv.end()) continue;
                    
                    model.add(i.precedence[jt->first] + j.precedence[it->first] == 1);
                }
            }
            
            // precedence same core
            for(pair<Task*, ILPTask_t> t : system) {
                for(pair<Task*, ILPTask_t> u : system) {
                    if(t.first == u.first || 
                        find(t.first->previous_transitiv.begin(), t.first->previous_transitiv.end(), u.first) != t.first->previous_transitiv.end() ||
                        find(u.first->previous_transitiv.begin(), u.first->previous_transitiv.end(), t.first) != u.first->previous_transitiv.end()) continue;
                    
                    /*IloAnd myand(env);
                    myand.add(t.second.precedence[u.first] == 1);
                    myand.add(t.second.samecore[u.first] == 1);
                    model.add(t.second.precedence_samecore[u.first] == myand);
                     */
                    model.add(t.second.precedence[u.first] + t.second.samecore[u.first] <= t.second.precedence_samecore[u.first]+1);
                    model.add(t.second.precedence[u.first] >= t.second.precedence_samecore[u.first]);
                    model.add(t.second.samecore[u.first] >= t.second.precedence_samecore[u.first]);
                }
            }
            
            // conflicts
            for(pair<Task*, ILPTask_t> t : system) {
                for(pair<Task*, ILPTask_t> u : system) {
                    if(t.first == u.first || 
                        find(t.first->previous_transitiv.begin(), t.first->previous_transitiv.end(), u.first) != t.first->previous_transitiv.end() ||
                        find(u.first->previous_transitiv.begin(), u.first->previous_transitiv.end(), t.first) != u.first->previous_transitiv.end()) continue;
                    
                    /*
                    IloIfThen mutexsamecore(env, 
                        t.second.precedence_samecore[u.first] == 1,
                        t.second.rho + IloInt(t.first->C) <= u.second.rho
                    );
                    model.add(mutexsamecore);
                    */
                    model.add(t.second.rho + IloInt(t.first->C) <= u.second.rho + TauMax * (1 - t.second.precedence_samecore[u.first]) );
                }
            }
            
            //causality
            for(pair<Task*, ILPTask_t> t : system) {
                for(Task::prev_t el : t.first->previous) {
                    model.add(system[el.first].rho + IloInt(el.first->C) <= t.second.rho);
                }
            }
            
            //-------------------
            // READ
            //-------------------
            // causality packet
            for(pair<Task*, ILPTask_t> t : system) {
                for(pair<Task*, vector<ILPPacket_t*>> pac : t.second.packets_r) {
                    for(int s=0 ; s < pac.second.size() ; ++s) {
                        if(s+1 >= pac.second.size()) continue;
                        model.add(pac.second[s]->rho + pac.second[s]->delay <= pac.second[s+1]->rho);
                    }
                }
            }
            
            // exec after read
            for(pair<Task*, ILPTask_t> t : system) {
                for(pair<Task*, vector<ILPPacket_t*>> pac : t.second.packets_r) {
                    if(pac.second.size() == 0) continue;
                    model.add(pac.second[pac.second.size()-1]->rho + pac.second[pac.second.size()-1]->delay <= t.second.rho);
                }
            }
           
            //-------------------
            // WRITE
            //-------------------
            // causality packet
            for(pair<Task*, ILPTask_t> t : system) {
                for(pair<Task*, vector<ILPPacket_t*>> pac : t.second.packets_w) {
                    for(int s=0 ; s < pac.second.size() ; ++s) {
                        if(s+1 >= pac.second.size()) continue;
                        model.add(pac.second[s]->rho + pac.second[s]->delay <= pac.second[s+1]->rho);
                    }
                }
            }
            
            // write after exec, end after last write
            for(pair<Task*, ILPTask_t> t : system) {
                for(pair<Task*, vector<ILPPacket_t*>> pac : t.second.packets_w) {
                    if(pac.second.size() == 0) continue;
                    model.add(t.second.rho + IloInt(t.first->C) <= pac.second[0]->rho);
                    model.add(pac.second[pac.second.size()-1]->rho + pac.second[pac.second.size()-1]->delay <= t.second._end);
                }
                if(t.second.packets_w.size() == 0) {
                    model.add(t.second.rho + IloInt(t.first->C) <= t.second._end);
                }
            }
             
            //-------------------------
            // BOTH
            //-------------------------
            // Note: can't optimize more the number of constraints with the previous_transitiv idea
            //       a read can be very advanced, or a write can be delayed with the lost of some causal dependency between packets
            //       |_ transitiv close of packets is not the same as tasks
            
            map<ILPPacket_t*, map<ILPPacket_t*, IloExtractable>> bkprec;
            // Order packet
            for(vector<ILPPacket_t*>::iterator it=flat_packets.begin(), et=flat_packets.end() ; it != et ; ++it) {
                for(vector<ILPPacket_t*>::iterator jt=it+1 ; jt != et ; ++jt) {
                    ILPPacket_t *i = *it, *j = *jt;
                    if(find(i->previous_transitiv.begin(), i->previous_transitiv.end(), j) != i->previous_transitiv.end() ||
                        find(j->previous_transitiv.begin(), j->previous_transitiv.end(), i) != j->previous_transitiv.end()) continue;
                    
                    model.add((*it)->precedence[*jt] + (*jt)->precedence[*it] == 1);
                }
            }
           
            // ensure mutex
            for(ILPPacket_t *i : flat_packets) {
                for(pair<ILPPacket_t*, IloBoolVar> j : i->precedence) {
                    if(i == j.first) continue;
                    
                    if(find(i->previous_transitiv.begin(), i->previous_transitiv.end(), j.first) != i->previous_transitiv.end() ||
                        find(j.first->previous_transitiv.begin(), j.first->previous_transitiv.end(), i) != j.first->previous_transitiv.end()) continue;
                    
                    /*
                    IloIfThen myif(env,
                        j.second == 1,
                        i->rho + i->delay <= j.first->rho
                    );
                    model.add(myif);
                     */
                    model.add(i->rho + i->delay <= j.first->rho + TauMax * (1 - j.second));
                }
            }
            
            // Causality
            // The first read must start after the last write
            for(pair<Task*, ILPTask_t> elt : system) {
                for(pair<Task*, vector<ILPPacket_t*>> pac_t : elt.second.packets_r) {
                    if(pac_t.second.size() == 0) continue;
                    
                    vector<ILPPacket_t*> &pac_u = system[pac_t.first].packets_w[elt.first];
                    model.add(pac_u[pac_u.size()-1]->rho + pac_u[pac_u.size()-1]->delay <= pac_t.second[0]->rho);
                }
            }
            
            //-------------
            //  SPM
            //-------------
            
            if(conf.archi.spm.assign_region) {

                // Order packet
                for(vector<ILPSPMPacket_t*>::iterator it=flat_spmpackets.begin(), et=flat_spmpackets.end() ; it != et ; ++it) {
                    for(vector<ILPSPMPacket_t*>::iterator jt=it+1 ; jt != et ; ++jt) {
                        model.add((*it)->precedence[*jt] + (*jt)->precedence[*it] == 1);
                    }
                }

                // Unicity, a task is mapped on only one region
                for(pair<Task*, ILPTask_t> elt : system) {
                    for(pair<Task*, vector<ILPSPMPacket_t*>> elpac : elt.second.spmpackets_r) {
                        for(ILPSPMPacket_t *pac : elpac.second) {
                            IloBoolVarArray map(env);
                            for(pair<string, IloIntVar> r : regions[tg.processors()[0]]) {
                                map.add(pac->mapping[r.first]);
                            }
                            model.add(IloSum(map) == 1);
                        }
                    }
                    for(pair<Task*, vector<ILPSPMPacket_t*>> elpac : elt.second.spmpackets_w) {
                        for(ILPSPMPacket_t *pac : elpac.second) {
                            IloBoolVarArray map(env);
                            for(pair<string, IloIntVar> r : regions[tg.processors()[0]]) {
                                map.add(pac->mapping[r.first]);
                            }
                            model.add(IloSum(map) == 1);
                        }
                    }
                    IloBoolVarArray map(env);
                    for(pair<string, IloIntVar> r : regions[tg.processors()[0]]) {
                        map.add(elt.second.exec_special_spmpacket->mapping[r.first]);
                    }
                    model.add(IloSum(map) == 1);
                }

                // startresa / endresa
                for(pair<Task*, ILPTask_t> elt : system) {
                    for(pair<Task*, vector<ILPSPMPacket_t*>> elpac : elt.second.spmpackets_r) {
                        for(ILPSPMPacket_t *pac : elpac.second) {
                            for(ILPPacket_t *i : pac->includes)
                                model.add(pac->startresa <= i->rho);
                            model.add(pac->endresa == elt.second.rho + IloInt(elt.first->C));
                        }
                    }
                    for(pair<Task*, vector<ILPSPMPacket_t*>> elpac : elt.second.spmpackets_w) {
                        for(ILPSPMPacket_t *pac : elpac.second) {
                            model.add(pac->startresa == elt.second.rho);
                            for(ILPPacket_t *i : pac->includes)
                                model.add(pac->endresa >= i->rho + i->delay);
                        }
                    }
                    if(conf.archi.prefetch_code) {
                        IloIntVarArray min(env);
                        for(pair<Task*, vector<ILPSPMPacket_t*>> elpac : elt.second.spmpackets_r) {
                            for(ILPSPMPacket_t *pac : elpac.second) {
                                min.add(pac->startresa);
                            }
                        }
                        model.add(elt.second.exec_special_spmpacket->startresa == IloMin(min));
                        model.add(elt.second.exec_special_spmpacket->endresa == elt.second._end);
                    }
                    else {
                        model.add(elt.second.exec_special_spmpacket->startresa == 0);
                        model.add(elt.second.exec_special_spmpacket->endresa == TauMax);
                    }
                }

                // Determine the size of the region that is the biggest amount of stored data in it
                for(pair<meth_processor_t*, map<string, IloIntVar>> p : regions) {
                    for(pair<string, IloIntVar> r : p.second) {
                        for(ILPSPMPacket_t *pac : flat_spmpackets) {
                     //       IloAnd myand(env);
                     //       myand.add(pac->mapping[r.first] == 1);
                     //       myand.add(system[pac->owner].mapping[p.first] == 1);
                     //       model.add(r.second >= pac->size * myand);
                            IloBoolVar tmp_size(env, ("tmp_size_"+*(p.first)+"_"+r.first+"_"+pac->name).c_str());
                            model.add(pac->mapping[r.first] + system[pac->owner].mapping[p.first] <= tmp_size+1);
                            model.add(pac->mapping[r.first] >= tmp_size);
                            model.add(system[pac->owner].mapping[p.first] >= tmp_size);
                            
                            model.add(r.second >= pac->size * tmp_size);
                        }
                    }
                }

                // The total size of regions must not exceed the size of the spm
                for(pair<meth_processor_t*, map<string, IloIntVar>> p : regions) {
                    IloIntVarArray size(env);
                    for(pair<string, IloIntVar> r : p.second) {
                        size.add(r.second);
                    }
                    model.add(IloSum(size) <= IloInt(p.first->spm_size));
                }

                // Detect same region
                for(vector<ILPSPMPacket_t*>::iterator it=flat_spmpackets.begin(), et=flat_spmpackets.end() ; it != et ; ++it) {
                    for(vector<ILPSPMPacket_t*>::iterator jt=next(it) ; jt != et ; ++jt) {
                        ILPSPMPacket_t *i = *it, *j = *jt;
              /*
                        IloOr myor(env);
                        for(pair<string, IloIntVar> r : regions[tg.processors()[0]]) {
                            IloAnd myand(env);
                            myand.add(i->mapping[r.first] == 1);
                            myand.add(j->mapping[r.first] == 1);
                            myor.add(myand);
                        }
              */
              /*          if(i->owner != j->owner) {// if equals then trivially on the same core
                            IloAnd myand2(env);
                            myand2.add(myor);
                            myand2.add(system[i->owner].samecore[j->owner] == 1);
                            model.add(i->samereg[j] == myand2);
                        }
                        else
                            model.add(i->samereg[j] == myor);
               */         
                        if(i->owner == j->owner) {
                            IloBoolVarArray acctmp_spmm(env);
                            for(pair<string, IloIntVar> r : regions[tg.processors()[0]]) {
                                IloBoolVar tmp_spmm(env);
                                model.add(i->mapping[r.first] + j->mapping[r.first] <= 1+tmp_spmm);
                                model.add(i->mapping[r.first] >= tmp_spmm);
                                model.add(j->mapping[r.first] >= tmp_spmm);
                                acctmp_spmm.add(tmp_spmm);
                                
                                model.add(i->samereg[j] <= tmp_spmm);
                            }
                            model.add(IloSum(acctmp_spmm) >= i->samereg[j]);
                        }
                        else {
                            IloBoolVar tmp_spmm_diff(env);
                            IloBoolVarArray acctmp_spmm(env);
                            for(pair<string, IloIntVar> r : regions[tg.processors()[0]]) {
                                IloBoolVar tmp_spmm(env);
                                model.add(i->mapping[r.first] + j->mapping[r.first] <= 1+tmp_spmm);
                                model.add(i->mapping[r.first] >= tmp_spmm);
                                model.add(j->mapping[r.first] >= tmp_spmm);
                                acctmp_spmm.add(tmp_spmm);
                                
                                model.add(tmp_spmm_diff <= tmp_spmm);
                            }
                            model.add(IloSum(acctmp_spmm) >= tmp_spmm_diff);
                            
                            model.add(tmp_spmm_diff + system[i->owner].samecore[j->owner] <= 1+i->samereg[j]);
                            model.add(tmp_spmm_diff >= i->samereg[j]);
                            model.add(system[i->owner].samecore[j->owner] >= i->samereg[j]);
                        }
                        

                        model.add(i->samereg[j] == j->samereg[i]);
                    }
                }

                // precedence same core
                for(ILPSPMPacket_t *t : flat_spmpackets) {
                    for(ILPSPMPacket_t *u : flat_spmpackets) {
                        if(t == u) continue;

                   //     IloAnd myand(env);
                   //     myand.add(t->precedence[u] == 1);
                   //     myand.add(t->samereg[u] == 1);
                   //     model.add(t->precedence_samereg[u] == myand);
                        model.add(t->precedence[u] + t->samereg[u] <= 1+t->precedence_samereg[u]);
                        model.add(t->precedence[u] >= t->precedence_samereg[u]);
                        model.add(t->samereg[u] >= t->precedence_samereg[u]);
                    }
                }


                //conflicts same core
                for(ILPSPMPacket_t *t : flat_spmpackets) {
                    for(ILPSPMPacket_t *u : flat_spmpackets) {
                        if(t == u) continue;

                     //   IloIfThen myif(env,
                     //       t->precedence_samereg[u] == 1,
                     //       t->endresa <= u->startresa
                     //   );
                     //   model.add(myif);
                        model.add(t->endresa <= u->startresa + TauMax * (1 - t->precedence_samereg[u]));
                    }
                }
            }

            for(pair<Task*, ILPTask_t> t : system) {
                model.add(t.second._end <= makespan);
            }
            
            model.add(IloMinimize(env, makespan));
            
            IloCplex cplex(env);
            cplex.extract(model);
            cplex.exportModel(conf.files.lp.c_str());
            Utils::memory_usage("Ready to leave");
            for(ILPPacket_t *t : flat_packets)
                free(t);
            for(ILPSPMPacket_t *t : flat_spmpackets)
                free(t);
        }
        catch(IloException& e) {
            for(ILPPacket_t *t : flat_packets)
                free(t);
            for(ILPSPMPacket_t *t : flat_spmpackets)
                free(t);
            throw MyException("solver", e.getMessage());
        }
#else
        Utils::WARN("ERROR", "As of now, burst mode is available only with CPLEX for the optimal method");
#endif
    }
};

REGISTER_ILPGENERATOR(ILPSynchronized, "synchronized")
