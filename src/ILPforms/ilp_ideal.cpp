/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ILP.h"

using namespace std;

class ILPIdeal : public ILPFormGen {
public:
    explicit ILPIdeal(const SystemModel &m, const config_t &c) : ILPFormGen(m, c) {}
    
    virtual void gen() {
        SeqTauMax = tg.global_deadline();

        os.open(conf.files.lp);

        objectiv_func();

        unicity();
        detect_samecore();
        precedence();
        precedence_samecore();
        conflicts();
        read_exec_write();
        causality();
        
        if(conf.interconnect.behavior == "non-blocking") {
            precedence_communications();
            precedence_samecore_communications();
            conflicts_communications();
            causality_communications();
        }
        
        communication_delay();
        
        map_spm_region_to_task();
        
//    if(conf.parallel_grain != paralle_group_e::NONE && conf.add_barrier)
//        const_add_barrier();
        
        force_mapping();
        force_schedule();

        linkObj();

        add_bounds();
        
        declare_binaries();
        declare_generals();

        concludeILPFile();

        os.close();
    }
    
protected:
    void communication_delay() {
        os << endl;

        for(Task *t : tg.tasks()) {
            os << "DelayR" << t << ": delay_r_" << t << " = 0" << endl;
            os << "DR" << t << ": DR_" << t << " = 0" << endl;
            os << "DelayW" << t << ": delay_w_" << t << " = 0" << endl;
            os << "DW" << t << ": DW_" << t << " = 0" << endl;
        }
    }
    
};

REGISTER_ILPGENERATOR(ILPIdeal, "ideal")
