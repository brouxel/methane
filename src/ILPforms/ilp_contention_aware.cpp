/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ILP.h"

using namespace std;

class ILPContentionAware : public ILPFormGen {
public:
    explicit ILPContentionAware(const SystemModel &m, const config_t &c) : ILPFormGen(m, c) {}
    
    virtual void gen() override {
        SeqTauMax = tg.global_deadline();

        os.open(conf.files.lp);

        objectiv_func();
        
        unicity();
        detect_samecore();
        precedence();
        precedence_samecore();
        conflicts();
        read_exec_write();
        causality();
        
        if(conf.interconnect.behavior == "non-blocking") {
            precedence_communications();
            precedence_samecore_communications();
            conflicts_communications();
            causality_communications();
        }
        
        communication_delay();
        detect_communication();
        detect_overlapping();
        contention();
        
        
        map_spm_region_to_task();

        force_mapping();
        force_schedule();
        
        linkObj();

        add_bounds();
        for(Task *t : tg.tasks()) {
            if(conf.interconnect.mechanism != "sharedonly") {
                os << "0 <= remdata_r_" << t << " <= " << tg.interconnect()->getActiveWindowTime()-1 << endl;
                os << "0 <= remdata_w_" << t << " <= " << tg.interconnect()->getActiveWindowTime()-1 << endl;
                os << "0 <= unused_r_" << t << " <= " << tg.interconnect()->getActiveWindowTime()-1 << endl;
                os << "0 <= unused_w_" << t << " <= " << tg.interconnect()->getActiveWindowTime()-1 << endl;
            }
            
            os << "0 <= conc_r_" << t << " <= " << tg.processors().size()-1 << endl;
            os << "0 <= conc_w_" << t << " <= " << tg.processors().size()-1 << endl;
        }
        
        
        declare_binaries();
        declare_generals();

        concludeILPFile();

        os.close();
    }

    void communication_delay() override {
        os << endl;

        for(Task *t : tg.tasks()) {
            if(conf.interconnect.mechanism != "sharedonly") {
                genvar += "DR_"+*t+"\n";
                genvar += "trslot_r_"+*t+"\n";
                genvar += "waitslot_r_"+*t+"\n";
                genvar += "remdata_r_"+*t+"\n";
                genvar += "unused_r_"+*t+"\n";

                genvar += "DW_"+*t+"\n";
                genvar += "trslot_w_"+*t+"\n";
                genvar += "waitslot_w_"+*t+"\n";
                genvar += "remdata_w_"+*t+"\n";
                genvar += "unused_w_"+*t+"\n";
            }

            string tmp = "";
            uint32_t sum = 0;
            string datatype = "";
            if(conf.archi.prefetch_code)
                sum = t->memory_code_size;
            for(Task::prev_t el : t->previous) {
                uint32_t data = el.second;
                tmp += " + "+to_string(data)+" m_" + el.first + "_" + t;
                sum += data;
                datatype = t->previous_datatypes[el.first];
            }
            if(sum > 0) {
                sum *= conf.datatypes.size_bits.at(datatype);
                sum = ceil(sum/(float)conf.interconnect.bit_per_timeunit); // probably + (bit_per_timeunit - (sum % bit_per_timeunit)) to add extra bit if data not aligned, e.g. send 5 bits when 8 bits are sent at a time, need to add 3 zeroes
                if(conf.interconnect.mechanism == "sharedonly") {
                    os << "DelayR" << t << ": delay_r_" << t
                        << " - " << conf.interconnect.active_ress_time * ceil(sum/(float)conf.interconnect.active_ress_time) << " conc_r_" << t 
                        << " = " << tg.interconnect()->getActiveWindowTime() * floor(sum/(float)conf.interconnect.active_ress_time) 
                                        + (sum % conf.interconnect.active_ress_time)
                        << endl;
                    os << "DR" << t << ": DR_" << t << " = " << sum << endl;
                }
                else {
                    os << "DR" << t << ": DR_" << t << tmp << " = " << sum << endl;
                    os << "TRSlotR" << t << ": DR_" << t << " - " << tg.interconnect()->getActiveWindowTime() << " trslot_r_" << t << " - remdata_r_" << t << " = 0" << endl;
                    os << "WSlotR" << t << ": DR_" << t << " - " << tg.interconnect()->getActiveWindowTime() << " waitslot_r_" << t << " + unused_r_" << t << " = 0" << endl;
                    os << "RoundR" << t << ": unused_r_" << t << " - remdata_r_" << t << " <= 1" << endl;

                    os << "DelayR" << t << ": delay_r_" << t
                       << " - " << conf.interconnect.active_ress_time * ceil(sum/(float)conf.interconnect.active_ress_time) << " conc_r_" << t 
                       << " - " << tg.interconnect()->getActiveWindowTime() << " trslot_r_" << t 
                       << " - remdata_r_" << t << " = 0" << endl;
                }
            }
            else {
                if(conf.interconnect.mechanism == "sharedonly") {
                    os << "delay_r_" << t << " = 0" <<endl;
                    os << "DR" << t << ": DR_" << t << " = 0" << endl;
                }
                else {
                    os << "DR_" << t << " = 0" << endl;
                    os << "trslot_r_" << t << " = 0" <<endl;
                    os << "waitslot_r_" << t << " = 0" << endl;
                    os << "remdata_r_" << t << " = 0" <<endl;
                    os << "unused_r_" << t << " = 0" <<endl;
                    os << "delay_r_" << t << " = 0" <<endl;
                }
            }
            os << endl;

            tmp = "";
            sum = 0;
            for(Task *el : t->successors) {
                uint32_t data = t->data_written(*el);
                tmp += " + "+to_string(data)+" m_" + t + "_" + el ;
                sum += data;
                datatype = el->previous_datatypes[t];
            }
            if(sum > 0) {
                sum *= conf.datatypes.size_bits.at(datatype);
                sum = ceil(sum/(float)conf.interconnect.bit_per_timeunit); // probably + (bit_per_timeunit - (sum % bit_per_timeunit)) to add extra bit if data not aligned, e.g. send 5 bits when 8 bits are sent at a time, need to add 3 zeroes
                if(conf.interconnect.mechanism == "sharedonly") {
                    os << "DelayW" << t << ": delay_w_" << t 
                        << " - " << conf.interconnect.active_ress_time * ceil(sum/(float)conf.interconnect.active_ress_time) << " conc_w_" << t
                        << " = " << tg.interconnect()->getActiveWindowTime() * floor(sum/(float)conf.interconnect.active_ress_time)
                                        + (sum % conf.interconnect.active_ress_time)
                        << endl;
                    os << "DW" << t << ": DW_" << t << " = " << sum << endl;
                }
                else {
                    os << "DW" << t << ": DW_" << t << tmp << " = " << sum << endl;
                    os << "TRSlotW" << t << ": DW_" << t << " - " << tg.interconnect()->getActiveWindowTime() << " trslot_w_" << t << " - remdata_w_" << t << " = 0" << endl;
                    os << "WSlotW" << t << ": DW_" << t << " - " << tg.interconnect()->getActiveWindowTime() << " waitslot_w_" << t << " + unused_w_" << t << " = 0" << endl;
                    os << "RoundW" << t << ": unused_w_" << t << " - remdata_w_" << t << " <= 1" << endl;

                    os << "DelayW" << t << ": delay_w_" << t 
                       << " - " << conf.interconnect.active_ress_time * ceil(sum/(float)conf.interconnect.active_ress_time) << " conc_w_" << t
                       << " - " << tg.interconnect()->getActiveWindowTime() << " trslot_w_" << t 
                       << " - remdata_w_" << t << " = 0" << endl;
                }
            }
            else {
                if(conf.interconnect.mechanism == "sharedonly") {
                    os << "delay_w_" << t << " = 0" << endl;
                    os << "DW" << t << ": DW_" << t << " = 0" << endl;
                }
                else {
                    os << "DW_" << t << " = 0" << endl;
                    os << "trslot_w_" << t << " = 0" << endl;
                    os << "waitslot_w_" << t << " = 0" << endl;
                    os << "remdata_w_" << t << " = 0" << endl;
                    os << "unused_w_" << t << " = 0" <<endl;
                    os << "delay_w_" << t << " = 0" << endl;
                }
            }
            os << endl;
        }
    }
    
    void detect_communication() {
        for(Task *t : tg.tasks()) {
            os << "hasDR1_" << t << ": 0 NhasDR_" << t << " + 1 hasDR_" << t << " - delay_r_" << t << " <= 0" << endl;
            os << "hasDR2_" << t << ": 1 NhasDR_" << t << " + " << SeqTauMax << " hasDR_" << t << " - delay_r_" << t << " >= 1" << endl;
            os << "hasDR3_" << t << ": NhasDR_" << t << " + hasDR_" << t << " = 1" << endl;
            binvar += "NhasDR_"+*t+"\n";
            binvar += "hasDR_"+*t+"\n";

            os << "hasDW1_" << t << ": 0 NhasDW_" << t << " + 1 hasDW_" << t << " - delay_w_" << t << " <= 0" << endl;
            os << "hasDW2_" << t << ": 1 NhasDW_" << t << " + " << SeqTauMax << " hasDW_" << t << " - delay_w_" << t << " >= 1" << endl;
            os << "hasDW3_" << t << ": NhasDW_" << t << " + hasDW_" << t << " = 1" << endl;

            binvar += "NhasDW_"+*t+"\n";
            binvar += "hasDW_"+*t+"\n";
        }
    }
    
    void detect_overlapping() {
        for (vector<Task*>::const_iterator it = tg.tasks().begin(), e = tg.tasks().end(); it != e; ++it) {
            Task *i = *it;
            for (vector<Task*>::const_iterator jt = it + 1; jt != e; ++jt) {
                Task *j = *jt;

                binvar += "ov_rr_"+*i+"_"+j+"\n";
                binvar += "ov_rr_"+*j+"_"+i+"\n";
                binvar += "ov_ww_"+*i+"_"+j+"\n";
                binvar += "ov_ww_"+*j+"_"+i+"\n";
                binvar += "ov_rw_"+*i+"_"+j+"\n";
                binvar += "ov_rw_"+*j+"_"+i+"\n";
                binvar += "ov_wr_"+*i+"_"+j+"\n";
                binvar += "ov_wr_"+*j+"_"+i+"\n";

                if(!i->is_parallel(j)) {
                    os << "ov_rr_" << i << "_" << j << " = 0" << endl;
                    os << "ov_rr_" << j << "_" << i << " = 0" << endl;
                    os << "ov_ww_" << i << "_" << j << " = 0" << endl;
                    os << "ov_ww_" << j << "_" << i << " = 0" << endl;
                    os << "ov_wr_" << i << "_" << j << " = 0" << endl;
                    os << "ov_wr_" << j << "_" << i << " = 0" << endl;
                    os << "ov_rw_" << i << "_" << j << " = 0" << endl;
                    os << "ov_rw_" << j << "_" << i << " = 0" << endl;
                    continue;
                }

                // rho_r_i < rho_j
                os << "-" << SeqTauMax << " zrr2_" << i << "_" << j << " + 1 tovrr2_" << i << "_" << j << " - rho_" << j << " + rho_r_" << i << " <= 0" << endl;
                os << "- 1 zrr2_" << i << "_" << j << " - " << SeqTauMax << " tovrr2_" << i << "_" << j << " + rho_" << j << " - rho_r_" << i << " <= -1" << endl; 
                os << "tovrr2_" << i << "_" << j << " + zrr2_" << i << "_" << j << " = 1" << endl;
                binvar += "tovrr2_"+*i+"_"+j+"\n";
                binvar += "zrr2_"+*i+"_"+j+"\n";
                os << endl;

                //rho_r_j < rho_i
                os << "-" << SeqTauMax << " zrr3_" << i << "_" << j << " + 1 tovrr3_" << i << "_" << j << " - rho_" << i << " + rho_r_" << j << " <= 0" << endl;
                os << "- 1 zrr3_" << i << "_" << j << " - " << SeqTauMax << " tovrr3_" << i << "_" << j << " + rho_" << i << " - rho_r_" << j << " <= -1" << endl; 
                os << "tovrr3_" << i << "_" << j << " + zrr3_" << i << "_" << j << " = 1" << endl;
                binvar += "tovrr3_"+*i+"_"+j+"\n";
                binvar += "zrr3_"+*i+"_"+j+"\n";
                os << endl;

                // rho_r_i < rho_j AND rho_r_j < rho_i AND DR_i > 0 AND DR_j > 0
                os << "ov_rr_" << i << "_" << j << " - tovrr3_" << i << "_" << j << " - tovrr2_" << i << "_" << j << " - hasDR_" << i << " - hasDR_" << j <<  " >= -3" << endl;
                os << "ov_rr_" << i << "_" << j << " - tovrr3_" << i << "_" << j << " <= 0" << endl;
                os << "ov_rr_" << i << "_" << j << " - tovrr2_" << i << "_" << j << " <= 0" << endl;
                os << "ov_rr_" << i << "_" << j << " - hasDR_" << i << " <= 0" << endl;
                os << "ov_rr_" << i << "_" << j << " - hasDR_" << j << " <= 0" << endl;

                os << "ov_rr_" << i << "_" << j << " - ov_rr_" << j << "_" << i << " = 0" << endl;

    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                // rho_w_i < end_j
                os << "-" << SeqTauMax << " zww2_" << i << "_" << j << " + 1 tovww2_" << i << "_" << j << " - end_" << j << " + rho_w_" << i << " <= 0" << endl;
                os << "- 1 zww2_" << i << "_" << j << " - " << SeqTauMax << " tovww2_" << i << "_" << j << " + end_" << j << " - rho_w_" << i << " <= -1" << endl; 
                os << "tovww2_" << i << "_" << j << " + zww2_" << i << "_" << j << " = 1" << endl;
                binvar += "tovww2_"+*i+"_"+j+"\n";
                binvar += "zww2_"+*i+"_"+j+"\n";
                os << endl;

                //rho_w_j < end_i
                os << "-" << SeqTauMax << " zww3_" << i << "_" << j << " + 1 tovww3_" << i << "_" << j << " - end_" << i << " + rho_w_" << j << " <= 0" << endl;
                os << "- 1 zww3_" << i << "_" << j << " - " << SeqTauMax << " tovww3_" << i << "_" << j << " + end_" << i << " - rho_w_" << j << " <= -1" << endl; 
                os << "tovww3_" << i << "_" << j << " + zww3_" << i << "_" << j << " = 1" << endl;
                binvar += "tovww3_"+*i+"_"+j+"\n";
                binvar += "zww3_"+*i+"_"+j+"\n";
                os << endl;

                os << "ov_ww_" << i << "_" << j << " - tovww3_" << i << "_" << j << " - tovww2_" << i << "_" << j << " - hasDW_" << i << " - hasDW_" << j <<  " >= -3" << endl;
                os << "ov_ww_" << i << "_" << j << " - tovww3_" << i << "_" << j << " <= 0" << endl;
                os << "ov_ww_" << i << "_" << j << " - tovww2_" << i << "_" << j << " <= 0" << endl;
                os << "ov_rr_" << i << "_" << j << " - hasDW_" << i << " <= 0" << endl;
                os << "ov_rr_" << i << "_" << j << " - hasDW_" << j << " <= 0" << endl;

                os << "ov_ww_" << i << "_" << j << " - ov_ww_" << j << "_" << i << " = 0" << endl;
            }
        }
        for(Task *i : tg.tasks()) {
            for(Task *j : tg.tasks()) {
                if(i == j || !i->is_parallel(j)) continue;
    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                // rho_r_i < end_j
                os << "-" << SeqTauMax << " zrw2_" << i << "_" << j << " + 1 tovrw2_" << i << "_" << j << " - end_" << j << " + rho_r_" << i << " <= 0" << endl;
                os << "- 1 zrw2_" << i << "_" << j << " - " << SeqTauMax << " tovrw2_" << i << "_" << j << " + end_" << j << " - rho_r_" << i << " <= -1" << endl; 
                os << "tovrw2_" << i << "_" << j << " + zrw2_" << i << "_" << j << " = 1" << endl;
                binvar += "tovrw2_"+*i+"_"+j+"\n";
                binvar += "zrw2_"+*i+"_"+j+"\n";
                os << endl;

                //rho_w_j < rho_i
                os << "-" << SeqTauMax << " zrw3_" << i << "_" << j << " + 1 tovrw3_" << i << "_" << j << " - rho_" << i << " + rho_w_" << j << " <= 0" << endl;
                os << "- 1 zrw3_" << i << "_" << j << " - " << SeqTauMax << " tovrw3_" << i << "_" << j << " + rho_" << i << " - rho_w_" << j << " <= -1" << endl; 
                os << "tovrw3_" << i << "_" << j << " + zrw3_" << i << "_" << j << " = 1" << endl;
                binvar += "tovrw3_"+*i+"_"+j+"\n";
                binvar += "zrw3_"+*i+"_"+j+"\n";
                os << endl;

                os << "ov_rw_" << i << "_" << j << " - tovrw3_" << i << "_" << j << " - tovrw2_" << i << "_" << j << " - hasDR_" << i << " - hasDW_" << j <<  " >= -3" << endl;
                os << "ov_rw_" << i << "_" << j << " - tovrw3_" << i << "_" << j << " <= 0" << endl;
                os << "ov_rw_" << i << "_" << j << " - tovrw2_" << i << "_" << j << " <= 0" << endl;
                os << "ov_rr_" << i << "_" << j << " - hasDR_" << i << " <= 0" << endl;
                os << "ov_rr_" << i << "_" << j << " - hasDW_" << j << " <= 0" << endl;

    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                os << "ov_wr_" << i << "_" << j << " - ov_rw_" << j << "_" << i << " = 0" << endl;
                os << "ov_wr_" << j << "_" << i << " - ov_rw_" << i << "_" << j << " = 0" << endl;

            }
        }
    }
    
    void contention() {
        for(Task *t : tg.tasks()) {
            genvar += "conc_r_"+*t+"\n";
            genvar += "conc_w_"+*t+"\n";
            
            /*
            string tmp_r = "";
            string tmp_w = "";
            for(Task *j : tg.tasks()) {
                if(t == j || !t->is_parallel(j)) continue;
                tmp_r += " - ov_rr_"+*t+"_"+*j+" - "+"ov_rw_"+*t+"_"+*j;
                tmp_w += " - ov_ww_"+*t+"_"+*j+" - "+"ov_wr_"+*t+"_"+*j;
            }

            //EMSOFT
            // Problem: - concurrency can be higher than the number of cores
            //          - concurrency represent the concurrency between tasks, not interferences on the bus
            //      but was used as the number of interfering cores
            os << "conc_r_" << t << tmp_r << " = 0" << endl;
            os << "conc_w_" << t << tmp_w << " = 0" << endl;
            */
            
            //foreach i,j ; i != j
                //ov_r_i_j = ov_rr_i_j \/ ov_rw_i_j
            //foreach i,j,p ; i != j
                // tmpov_r_i_j_p = ov_r_i_j /\ p_j_p
            //foreach i,p ;
                //ov_r_i_p = foreach j; \/ tmpov_i_j_p
            //conc_r_i = sum foreach p ov_r_i_p
            
            for(Task *j : tg.tasks()) {
                if(t == j || !t->is_parallel(j)) continue;
                //ov_r_i_j = ov_rr_i_j \/ ov_rw_i_j
                os << "ov_r_" << t << "_" << j << " - ov_rr_" << t << "_" << j << " - ov_rw_" << t << "_" << j << " <= 0" << endl;
                os << "ov_r_" << t << "_" << j << " - ov_rr_" << t << "_" << j << " >= 0" << endl;
                os << "ov_r_" << t << "_" << j << " - ov_rw_" << t << "_" << j << " >= 0" << endl;
                
                //ov_w_i_j = ov_ww_i_j \/ ov_wr_i_j
                os << "ov_w_" << t << "_" << j << " - ov_ww_" << t << "_" << j << " - ov_wr_" << t << "_" << j << " <= 0" << endl;
                os << "ov_w_" << t << "_" << j << " - ov_ww_" << t << "_" << j << " >= 0" << endl;
                os << "ov_w_" << t << "_" << j << " - ov_wr_" << t << "_" << j << " >= 0" << endl;
                
                for(meth_processor_t *p : tg.processors()) {
                    // tmpov_r_i_j_p = ov_r_i_j /\ p_j_p
                    os << "tmpov_r_" << t << "_" << j << "_" << p << " - ov_r_" << t << "_" << j << " - p_" << j << "_" << p << " >= -1" << endl;
                    os << "tmpov_r_" << t << "_" << j << "_" << p << " - ov_r_" << t << "_" << j << " <= 0" << endl;
                    os << "tmpov_r_" << t << "_" << j << "_" << p << " - p_" << j << "_" << p << " <= 0" << endl;
                    binvar += "tmpov_r_"+*t+"_"+*j+"_"+*p+"\n";
                    
                    // tmpov_w_i_j_p = ov_w_i_j /\ p_j_p
                    os << "tmpov_w_" << t << "_" << j << "_" << p << " - ov_w_" << t << "_" << j << " - p_" << j << "_" << p << " >= -1" << endl;
                    os << "tmpov_w_" << t << "_" << j << "_" << p << " - ov_w_" << t << "_" << j << " <= 0" << endl;
                    os << "tmpov_w_" << t << "_" << j << "_" << p << " - p_" << j << "_" << p << " <= 0" << endl;
                    binvar += "tmpov_w_"+*t+"_"+*j+"_"+*p+"\n";
                }
            }
            
            for(meth_processor_t *p : tg.processors()) {
                //ov_r_i_p = foreach j; \/ tmpov_r_i_j_p
                string tmp_r = "";
                string tmp_w = "";
                for(Task *j : tg.tasks()) {
                    if(t == j || !t->is_parallel(j)) continue;
                    tmp_r += " - tmpov_r_"+*t+"_"+*j+"_"+*p;
                    os << "ov_r_" << t << "_" << p << " - tmpov_r_" << t << "_" << j << "_" << p << " >= 0" << endl;
                    
                    tmp_w += " - tmpov_w_"+*t+"_"+*j+"_"+*p;
                    os << "ov_w_" << t << "_" << p << " - tmpov_w_" << t << "_" << j << "_" << p << " >= 0" << endl;
                }
                os << "ov_r_" << t << "_" << p << tmp_r << " <= 0" << endl;
                binvar += "ov_r_"+*t+"_"+*p+"\n";
                
                os << "ov_w_" << t << "_" << p << tmp_w << " <= 0" << endl;
                binvar += "ov_w_"+*t+"_"+*p+"\n";
            }
            
            //conc_r_i = sum foreach p ov_r_i_p
            string tmp_r = "";
            string tmp_w = "";
            for(meth_processor_t *p : tg.processors()) {
                tmp_r += " - ov_r_"+*t+"_"+*p;
                tmp_w += " - ov_w_"+*t+"_"+*p;
            }
            os << "conc_r_" << t << tmp_r << " = 0" << endl;
            os << "conc_w_" << t << tmp_w << " = 0" << endl;
        }
    }
};

REGISTER_ILPGENERATOR(ILPContentionAware, "contention_aware")
        
