/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ILP.h"

using namespace std;

class ILPWorstConcurrency : public ILPFormGen {
public:
    explicit ILPWorstConcurrency(const SystemModel &m, const config_t &c) : ILPFormGen(m, c) {}
    
    virtual void gen() {
        SeqTauMax = tg.global_deadline();

        os.open(conf.files.lp);

        objectiv_func();

        unicity();
        detect_samecore();
        precedence();
        precedence_samecore();
        conflicts();
        read_exec_write();
        causality();
        communication_delay();
        
        if(conf.interconnect.behavior == "non-blocking") {
            precedence_communications();
            precedence_samecore_communications();
            conflicts_communications();
            causality_communications();
        }
        
        map_spm_region_to_task();
        
        force_mapping();
        force_schedule();

        linkObj();

        add_bounds();
        if(conf.interconnect.mechanism != "sharedonly") {
            for(Task *t : tg.tasks()) {
                os << "0 <= remdata_r_" << t << " <= " << tg.interconnect()->getActiveWindowTime()-1 << endl;
                os << "0 <= remdata_w_" << t << " <= " << tg.interconnect()->getActiveWindowTime()-1 << endl;
                os << "0 <= unused_r_" << t << " <= " << tg.interconnect()->getActiveWindowTime()-1 << endl;
                os << "0 <= unused_w_" << t << " <= " << tg.interconnect()->getActiveWindowTime()-1 << endl;
            }
        }
        
        
        declare_binaries();
        declare_generals();

        concludeILPFile();

        os.close();
    }
    
protected:
    void communication_delay() {
        os << endl;

        for(Task *t : tg.tasks()) {
            if(conf.interconnect.mechanism != "sharedonly") {
                genvar += "DR_"+*t+"\n";
                genvar += "trslot_r_"+*t+"\n";
                genvar += "waitslot_r_"+*t+"\n";
                genvar += "remdata_r_"+*t+"\n";
                genvar += "unused_r_"+*t+"\n";

                genvar += "DW_"+*t+"\n";
                genvar += "trslot_w_"+*t+"\n";
                genvar += "waitslot_w_"+*t+"\n";
                genvar += "remdata_w_"+*t+"\n";
                genvar += "unused_w_"+*t+"\n";
            }

            string dr = "DR_"+ *t;
            string tmp = "";
            uint32_t sum = 0;
            string datatype = "";
            
            if(t->force_read_delay > 0) {
                os << "DelayR" << t << ": delay_r_" << t << " = " << t->force_read_delay << endl;
                os << "DR_" << t << " = 0" << endl;
            }
            else {
                if(conf.archi.prefetch_code)
                    sum = t->memory_code_size;

                for(Task::prev_t el : t->previous) {
                    uint32_t data = el.second;
                    tmp += " + "+to_string(data)+" m_" + el.first + "_" + t;
                    sum += data;
                    datatype = t->previous_datatypes[el.first];
                }
                if(conf.interconnect.mechanism == "sharedonly") {
                    os << "DelayR" << t << ": delay_r_" << t << " = " << tg.interconnect()->comm_delay(tg.processors().size()-1, sum, datatype, 0) << endl;
                    os << "DR_" << t << " = " << sum << endl; // compatibility
                }
                else if(sum > 0) {
                    sum *= conf.datatypes.size_bits.at(datatype);
                    sum = ceil(sum/conf.interconnect.bit_per_timeunit); // probably + (bit_per_timeunit - (sum % bit_per_timeunit)) to add extra bit if data not aligned, e.g. send 5 bits when 8 bits are sent at a time, need to add 3 zeroes
                    os << "DR" << t << ": " << dr << tmp << " = " << sum << endl;
                    os << "TRSlotR" << t << ": DR_" << t << " - " << tg.interconnect()->getActiveWindowTime() << " trslot_r_" << t << " - remdata_r_" << t << " = 0" << endl;
                    os << "WSlotR" << t << ": DR_" << t << " - " << tg.interconnect()->getActiveWindowTime() << " waitslot_r_" << t << " + unused_r_" << t << " = 0" << endl;
                    os << "RoundR" << t << ": unused_r_" << t << " - remdata_r_" << t << " <= 1" << endl;
                    os << "DelayR" << t << ": delay_r_" << t
                       << " - " << (tg.interconnect()->getActiveWindowTime()*(tg.processors().size()-1)) << " waitslot_r_" << t
                       << " - " << tg.interconnect()->getActiveWindowTime() << " trslot_r_" << t 
                       << " - remdata_r_" << t << " = 0" << endl;
                }
                else {
                    os << "DR_" << t << " = 0" << endl;
                    os << "trslot_r_" << t << " = 0" <<endl;
                    os << "remdata_r_" << t << " = 0" <<endl;
                    os << "delay_r_" << t << " = 0" <<endl;
                    os << "waitslot_r_" << t << " = 0" << endl;
                    os << "unused_r_" << t << " = 0" << endl;
                }
            }

            os << endl;

            string ds = "DW_"+*t;
            tmp = "";
            sum = 0;
            if(t->force_write_delay > 0) {
                os << "DelayW" << t << ": delay_w_" << t << " = " << t->force_write_delay << endl;
                os << "DW_" << t << " = 0" << endl;
            }
            else {
                for(Task *el : t->successors) {
                    uint32_t data = t->data_written(*el);
                    tmp += " + "+to_string(data)+" m_" + t + "_" + el;
                    sum += data;
                    datatype = el->previous_datatypes[t];
                }
                if(conf.interconnect.mechanism == "sharedonly") {
                    os << "DelayW" << t << ": delay_w_" << t << " = " << tg.interconnect()->comm_delay(tg.processors().size()-1, sum, datatype, 0) << endl;
                    os << "DW_" << t << " = " << sum << endl; // compatibility
                }
                else if(sum > 0) {
                    sum *= conf.datatypes.size_bits.at(datatype);
                    sum = ceil(sum/conf.interconnect.bit_per_timeunit); // probably + (bit_per_timeunit - (sum % bit_per_timeunit)) to add extra bit if data not aligned, e.g. send 5 bits when 8 bits are sent at a time, need to add 3 zeroes
                    os << "DW" << t << ": " << ds << tmp << " = " << sum << endl;
                    os << "TRSlotW" << t << ": DW_" << t << " - " << tg.interconnect()->getActiveWindowTime() << " trslot_w_" << t << " - remdata_w_" << t << " = 0" << endl;
                    os << "WSlotW" << t << ": DW_" << t << " - " << tg.interconnect()->getActiveWindowTime() << " waitslot_w_" << t << " + unused_w_" << t << " = 0" << endl;
                    os << "RoundW" << t << ": unused_w_" << t << " - remdata_w_" << t << " <= 1" << endl;
                    os << "DelayW" << t << ": delay_w_" << t 
                       << " - " << (tg.interconnect()->getActiveWindowTime()*(tg.processors().size()-1)) << " waitslot_w_" << t
                       << " - " << tg.interconnect()->getActiveWindowTime() << " trslot_w_" << t 
                       << " - remdata_w_" << t << " = 0" << endl;
                }
                else {
                    os << "DW_" << t << " = 0" << endl;
                    os << "trslot_w_" << t << " = 0" << endl;
                    os << "remdata_w_" << t << " = 0" << endl;
                    os << "delay_w_" << t << " = 0" << endl;
                    os << "waitslot_w_" << t << " = 0" << endl;
                    os << "unused_w_" << t << " = 0" << endl;
                }
            }

            os << endl;
        }
    }
    
};

REGISTER_ILPGENERATOR(ILPWorstConcurrency, "worst_concurrency")
