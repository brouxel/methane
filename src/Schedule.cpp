/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Schedule.h"

using namespace std;

bool operator== (const spm_region_info &a, const string &b) { return a.label == b; }
bool operator== (const spm_region_info &a, const spm_region_info &b) { return a.label == b.label; }

ostream& operator<< (ostream& os, const task_schedule_info &inf) {
    os << "mapto: " << ((inf.proc == NULL) ? "NULL" : inf.proc->id) ;
    os << " (mem region: " ; 
    for(pair<string, spm_region_info> el : inf.memory_region.read) {
        if(el.second.label.empty()) continue;
        os << "{" << el.first << "," << el.second.label << "},";
    }
    os << " / " << inf.memory_region.exec.label << " / ";
    for(pair<string, spm_region_info> el : inf.memory_region.write) {
        if(el.second.label.empty()) continue;
        os << "{" << el.first << "," << el.second.label << "},";
    }
    os << ") ";
    os << "-- rtDR: " << inf.rtR() << " / DR: " << inf.delayR() ;
    os << " / rt: " << inf.rtE ;
    os << " / rtDS: " << inf.rtW() << " / DS: " << inf.delayW() << " / end: " << inf.end() ;
    os << " ---- " << inf.__RR_conc_ << "," << inf.__WW_conc_ << "," << inf.__RW_conc_ << "," << inf.__WR_conc_ ;
    os << " -- " << inf.is_in_mutex;
    return os;
}

ostream& operator<< (ostream& os, const Schedule_t &inf) {
    os << "-------------------------------------------------------------" << endl;
    os << "\tMakespan: " << retrieve_schedule_length(inf) << " -- Status: " << inf.status << endl;
    for(pair<string,task_schedule_info> el : inf.tasks) {
        os << "\t\t\t" << el.first << " -> " << el.second << endl;
    }
    os << endl;
    for(pair<string, proc_schedule_info> el : inf.procs) {
        os << "\t\t\t" << el.first << " -> free spm size : " << el.second.available_spm_size << endl;
    }
    os << "-------------------------------------------------------------" << endl;
    return os;
}

string operator+ (const string &a, const task_schedule_info &b) {
    stringstream ss;
    ss << a << b;
    return ss.str();
}
string operator+ (const task_schedule_info &b, const string &a) {
    stringstream ss;
    ss << b << a;
    return ss.str();
}
string operator+ (const string &a, const task_schedule_info *b) {
    stringstream ss;
    ss << a << b;
    return ss.str();
}
string operator+ (const task_schedule_info *b, const string &a) {
    stringstream ss;
    ss << b << a;
    return ss.str();
}
string operator+ (const char* a, const task_schedule_info &b) {
    stringstream ss;
    ss << a << b;
    return ss.str();
}
string operator+ (const task_schedule_info &b, const char* a) {
    stringstream ss;
    ss << b << a;
    return ss.str();
}
//----------------------------------------------------
string operator+ (const string &a, const Schedule_t &b) {
    stringstream ss;
    ss << a << b;
    return ss.str();
}
string operator+ (const Schedule_t &b, const string &a) {
    stringstream ss;
    ss << b << a;
    return ss.str();
}
string operator+ (const string &a, const Schedule_t *b) {
    stringstream ss;
    ss << a << b;
    return ss.str();
}
string operator+ (const Schedule_t *b, const string &a) {
    stringstream ss;
    ss << b << a;
    return ss.str();
}
string operator+ (const char* a, const Schedule_t &b) {
    stringstream ss;
    ss << a << b;
    return ss.str();
}
string operator+ (const Schedule_t &b, const char* a) {
    stringstream ss;
    ss << b << a;
    return ss.str();
}

uint64_t retrieve_schedule_length(const Schedule_t &sched) {
    uint64_t makespan = 0;
    for(pair<string, task_schedule_info> el : sched.tasks) {
        if(el.second.rtE + el.second.WCET > el.second.end()) {
            if(el.second.rtE + el.second.WCET > makespan)
                makespan = el.second.rtE + el.second.WCET;
        }
        else if(el.second.end() > makespan)
            makespan = el.second.end();
    }
    return makespan;
}

void map_task_proc(Schedule_t *sched, meth_processor_t *proc, Task * t, uint64_t release_R, uint64_t release_E, uint64_t release_W) {
    map_task_proc(sched, proc, t, t->C, 
        release_R, sched->tasks[t->id].dataR(), sched->tasks[t->id].datatypeR(), -1, sched->tasks[t->id].delayR(),
        release_E, 
        release_W, sched->tasks[t->id].dataW(), sched->tasks[t->id].datatypeW(), -1, sched->tasks[t->id].delayW());
}
void map_task_proc(Schedule_t *sched, meth_processor_t *proc, Task * t, 
        uint32_t data_R, string datatypeR, uint32_t conc_R, uint32_t delay_R,
        uint32_t data_W, string datatypeW, uint32_t conc_W, uint32_t delay_W) {
    map_task_proc(sched, proc, t, t->C, 
        sched->tasks[t->id].rtR(), data_R, datatypeR, conc_R, delay_R,
        sched->tasks[t->id].rtE, 
        sched->tasks[t->id].rtW(), data_W, datatypeW, conc_W, delay_W);
}

void map_task_proc(Schedule_t *sched, meth_processor_t *proc, Task * t, uint64_t wcet, 
        uint64_t release_R, uint32_t data_R, string datatypeR, uint32_t conc_R, uint32_t delay_R,
        uint64_t release_E, 
        uint64_t release_W, uint32_t data_W, string datatypeW, uint32_t conc_W, uint32_t delay_W) {
    string taskid = t->id;
    
    sched->tasks[taskid].proc = proc;
    sched->tasks[taskid].rtE = release_E;
    sched->tasks[taskid].WCET = wcet;
    
    transmission_phase_info trRead("read", release_R, delay_R, data_R, datatypeR);
    trRead.conc = conc_R;
    
    if(data_R + delay_R > sched->tasks[taskid].rtE)
        sched->tasks[taskid].rtE = data_R + delay_R;
    
    if(sched->tasks[taskid].rtE + wcet > release_W)
        release_W = sched->tasks[taskid].rtE + wcet;
    
    transmission_phase_info trWrite("write", release_W, delay_W, data_W, datatypeW);
    trWrite.conc = conc_W;
    
    // Careful, legacy code for blocking with only one communication 
    sched->tasks[taskid].reads[taskid].clear();
    sched->tasks[taskid].reads[taskid].push_back(trRead);
    sched->tasks[taskid].writes[taskid].clear();
    sched->tasks[taskid].writes[taskid].push_back(trWrite);
}

void map_task_proc(Schedule_t *sched, meth_processor_t *proc, Task * t, map<string, vector<transmission_phase_info>> reads, uint64_t release_E, map<string, vector<transmission_phase_info>> writes) {
    string taskid = t->id;
    
    sched->tasks[taskid].proc = proc;
    sched->tasks[taskid].rtE = release_E;
    sched->tasks[taskid].WCET = t->C;
    
    sched->tasks[taskid].reads = reads;
    sched->tasks[taskid].writes = writes;
}

const vector<Task*> build_samecore_taskset(const Schedule_t &sched, const vector<Task*> &Qdone, Task *t) {
    vector<Task*> result(Qdone.size());
    vector<Task*>::iterator it = Utils::copy_if_until(Qdone.begin(), Qdone.end(), result.begin(), 
        [&sched, t](Task* a) { return sched.tasks.at(a->id).proc == sched.tasks.at(t->id).proc; },
        [t](Task *a) { return t == a;});
    result.resize(distance(result.begin(), it));
    sort(result.begin(), result.end(), [&sched](Task *a, Task*b) { return sched.tasks.at(a->id).rtR() < sched.tasks.at(b->id).rtR(); });
    
    return result;
}

const vector<Task*> build_diffcore_taskset(const Schedule_t &sched, const vector<Task*> &Qdone, Task *t) {
    vector<Task*> result(Qdone.size());
    vector<Task*>::iterator it = Utils::copy_if_until(Qdone.begin(), Qdone.end(), result.begin(), 
        [&sched, t](Task* a) { return sched.tasks.at(a->id).proc != sched.tasks.at(t->id).proc; },
        [t](Task *a) { return t == a;});
    result.resize(distance(result.begin(), it));
    sort(result.begin(), result.end(), [&sched](Task *a, Task*b) { return sched.tasks.at(a->id).rtR() < sched.tasks.at(b->id).rtR(); });
    
    return result;
}

const vector<Task*> build_allbeforeme_taskset(const Schedule_t &sched, const vector<Task*> &Qdone, Task *t) {
    vector<Task*> result(Qdone.size());
    vector<Task*>::iterator it = Utils::copy_if_until(Qdone.begin(), Qdone.end(), result.begin(), 
        [&sched, t](Task* a) { return true; },
        [t](Task *a) { return t == a;});
    result.resize(distance(result.begin(), it));
    sort(result.begin(), result.end(), [&sched](Task *a, Task*b) { return sched.tasks.at(a->id).rtR() < sched.tasks.at(b->id).rtR(); });
    
    return result;
}

const vector<Task*> build_allafterme_taskset(const Schedule_t &sched, const vector<Task*> &Qdone, Task *t) {
    vector<Task*> result(Qdone.size());
    vector<Task*>::iterator it = Utils::copy_from_if(Qdone.begin(), Qdone.end(), result.begin(), 
        [t](Task *a) { return t == a;},
        [&sched, t](Task* a) { return true; });
    result.resize(distance(result.begin(), it));
    sort(result.begin(), result.end(), [&sched](Task *a, Task*b) { return sched.tasks.at(a->id).rtR() < sched.tasks.at(b->id).rtR(); });
    
    return result;
}

void assign_spm_region(Schedule_t *sched, const string &procid, const string &label, uint32_t size) {
    
}

transmission_phase_info& transmission_phase_info::operator=(const transmission_phase_info& rhs) {
    rt = rhs.rt;
    data = rhs.data;
    datatype = rhs.datatype;
    delay = rhs.delay;
    conc = rhs.conc;
    spm = rhs.spm;
    type = rhs.type;
    packetnum = rhs.packetnum;
    packetnumber = rhs.packetnumber;

    return *this;
}

uint64_t task_schedule_info::rtR() const {
    uint64_t rt = -1;
    for(pair<std::string, vector<transmission_phase_info>> trs : reads) {
        for(transmission_phase_info tr : trs.second) {
            if(tr.rt < rt)
                rt = tr.rt;
        }
    }
    return rt;
}
uint64_t task_schedule_info::rtW() const {
    uint64_t rt = -1;
    for(pair<std::string, vector<transmission_phase_info>> trs : writes) {
        for(transmission_phase_info tr : trs.second) {
            if(tr.rt < rt)
                rt = tr.rt;
        }
    }
    return rt;
}
uint64_t task_schedule_info::end() const {
    uint64_t e = 0;
    for(pair<std::string, vector<transmission_phase_info>> trs : writes) {
        for(transmission_phase_info tr : trs.second) {
            if(tr.rt+tr.delay > e)
                e = tr.rt+tr.delay;
        }
    }
    return e;
}

uint32_t task_schedule_info::delayR() const {
    uint32_t d = 0;
    for(pair<std::string, vector<transmission_phase_info>> trs : reads) {
        for(transmission_phase_info tr : trs.second) {
            d += tr.delay;
        }
    }
    return d;
}
uint32_t task_schedule_info::delayW() const {
    uint32_t d = 0;
    for(pair<std::string, vector<transmission_phase_info>> trs : writes) {
        for(transmission_phase_info tr : trs.second) {
            d += tr.delay;
        }
    }
    return d;
}
uint32_t task_schedule_info::dataR() const {
    uint32_t d = 0;
    for(pair<std::string, vector<transmission_phase_info>> trs : reads) {
        for(transmission_phase_info tr : trs.second) {
            d += tr.data;
        }
    }
    return d;
}
uint32_t task_schedule_info::dataW() const {
    uint32_t d = 0;
    for(pair<std::string, vector<transmission_phase_info>> trs : writes) {
        for(transmission_phase_info tr : trs.second) {
            d += tr.data;
        }
    }
    return d;
}
string task_schedule_info::datatypeR() const {
    if(reads.size() == 0)
        return "void";
    pair<std::string, vector<transmission_phase_info>> tr = *(reads.begin());
    if(tr.second.size() == 0)
        return "void";
    return tr.second.at(0).datatype;
}
string task_schedule_info::datatypeW() const {
    if(writes.size() == 0)
        return "void";
    pair<std::string, vector<transmission_phase_info>> tr = *(writes.begin());
    if(tr.second.size() == 0)
        return "void";
    return tr.second.at(0).datatype;
}

task_schedule_info& task_schedule_info::operator=(const task_schedule_info& rhs) {
    is_in_mutex = rhs.is_in_mutex;
    rtE = rhs.rtE;
    WCET = rhs.WCET;
    proc = rhs.proc;
    __RR_conc_ = rhs.__RR_conc_;
    __WW_conc_ = rhs.__WW_conc_;
    __RW_conc_ = rhs.__RW_conc_;
    __WR_conc_ = rhs.__WR_conc_;
    memory_region.exec = rhs.memory_region.exec;
    memory_region.read = rhs.memory_region.read;
    memory_region.write = rhs.memory_region.write;
    reads = rhs.reads;
    writes = rhs.writes;
    return *this;
}

Schedule_t& Schedule_t::operator= (const Schedule_t &rhs) {
    tasks = rhs.tasks;
    procs = rhs.procs;
    solvetime = rhs.solvetime;
    makespan = rhs.makespan;
    status = rhs.status;
    heur_DFSDelay = rhs.heur_DFSDelay;
    heur_DFS = rhs.heur_DFS;
    heur_BFS = rhs.heur_BFS;

    return *this;
}
