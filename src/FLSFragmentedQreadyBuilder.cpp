/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "FLSFragmentedQreadyBuilder.h"

bool FLSFragQreadyBuilder::sortFun(SchedElt *a, SchedElt *b) {
    if(a->type == SchedElt::Type::PACKET && b->type == SchedElt::Type::TASK)
        return true;
    if(a->type == SchedElt::Type::TASK && b->type == SchedElt::Type::PACKET)
        return false;
    
    if(a->type == SchedElt::Type::TASK && b->type == SchedElt::Type::TASK)
        return Utils::sort_Q_byMemUsageDec(((SchedEltTask*)a)->task, ((SchedEltTask*)b)->task);
    
    if(a->type == SchedElt::Type::PACKET && b->type == SchedElt::Type::PACKET)
        return Utils::sort_Q_byMemUsageDec(((SchedEltPacket*)a)->sortingschedtask->task, ((SchedEltPacket*)b)->sortingschedtask->task);
//        return ((SchedEltPacket*)a)->packetnum < ((SchedEltPacket*)b)->packetnum;
    
    assert(false);
}

void FLSFragQreadyBuilder::buildListDFS(const SystemModel &m, const config_t &c, FLSFragStrategy *s, vector<SchedElt*> *schedorder) {
    FLSFragQreadyBuilder builder(m, c, s);
    builder.buildList(schedorder, [&builder](vector<SchedElt*> *Qready, vector<SchedElt*> &Qdone, map<Task*, SchedElt*> *pool) {
        builder.build_QreadyDFS(Qready, Qdone, pool);
    });
}

void FLSFragQreadyBuilder::buildListDFSWithRDelay(const SystemModel &m, const config_t &c, FLSFragStrategy *s, vector<SchedElt*> *schedorder) {
    FLSFragQreadyBuilder builder(m, c, s);
    builder.buildList(schedorder, [&builder](vector<SchedElt*> *Qready, vector<SchedElt*> &Qdone, map<Task*, SchedElt*> *pool) {
        builder.build_QreadyDFSwithreaddelay(Qready, Qdone, pool);
    });
}

void FLSFragQreadyBuilder::buildListBFS(const SystemModel &m, const config_t &c, FLSFragStrategy *s, vector<SchedElt*> *schedorder) {
    FLSFragQreadyBuilder builder(m, c, s);
    builder.buildList(schedorder, [&builder](vector<SchedElt*> *Qready, vector<SchedElt*> &Qdone, map<Task*, SchedElt*> *pool) {
        builder.build_QreadyBFS(Qready, Qdone, pool);
    });
}

 void FLSFragQreadyBuilder::buildList(vector<SchedElt*> *schedorder, FLSFragQreadyBuilder::func_graph_walk_through_t fun_walk_through) {
    // TODO: handle several entry point for the same DAG
    vector<vector<SchedElt*>> DAGS;
    map<Task*, SchedElt*> pool;
    for(Task *t : tg.tasks()) {
        SchedEltTask *elt = new SchedEltTask(tg, conf, t, conf.interconnect.burst == "none" ? 2 : -1);
        elt->wct = t->C;
        pool[t] = elt;
    }
    if(conf.interconnect.burst == "none") {
        // must be done in 2 steps as successors/predecessors list needs to have all packets a priori created
        for(Task *t : tg.tasks()) {
            build_packet_list_noburst((SchedEltTask *)pool[t]);
        }
        for(Task *t : tg.tasks()) {
            build_successors_noburst((SchedEltTask *)pool[t], &pool);
        }
    }
    else {
        // predecessors/successors lists done when building packets
        for(Task *t : tg.tasks()) {
            build_packet_list_burst((SchedEltTask *)pool[t], &pool);
        }
    }
    
    vector<Task*> roots;
    for(Task *t : tg.tasks()) {
        if(t->previous.size() == 0) {
            roots.push_back(t);
        }
    }
    
    for(vector<Task*>::iterator itr1=roots.begin(), etr1=roots.end() ; itr1 != etr1 ; ++itr1) {
        bool found = false;
        // let's check if we didn't put that root as part of an other DAG
        for(vector<SchedElt*> rdag : DAGS) {
            if(find(rdag.begin(), rdag.end(), pool[*itr1]) != rdag.end()) {
                found = true;
                break;
            }
        }
        if(found)
            continue;
        
        vector<SchedElt*> DAG;
        DAG.push_back(pool[*itr1]);
        
        // if 2 roots have at least a common successor (transitively) then they are part of the same DAG
        for(vector<Task*>::iterator itr2 = itr1+1 ; itr2 != etr1 ; ++itr2) {
            bool found = false;
            for(Task *succ : (*itr1)->successors_transitiv) {
                if(find((*itr2)->successors_transitiv.begin(), (*itr2)->successors_transitiv.end(), succ) != (*itr2)->successors_transitiv.end()) {
                    found = true;
                    break;
                }
            }
            if(found)
                DAG.push_back(pool[*itr2]);
        }
        
        vector<SchedElt*> QtmpDone;
        fun_walk_through(&DAG, QtmpDone, &pool);
        DAGS.push_back(DAG);
    }
    
    sort(DAGS.begin(), DAGS.end(), [](const vector<SchedElt*> &a, const vector<SchedElt*> &b) {
        uint64_t seqLengthA = 0, seqLengthB = 0;
        for(SchedElt *ela : a) {
            seqLengthA += ela->wct;
        }
        for(SchedElt *elb : b) {
            seqLengthB += elb->wct;
        }
        return seqLengthA > seqLengthB;
    });
    
    for(vector<SchedElt*> dag : DAGS) {
        Utils::DEBUG("-------"+to_string(dag.size()));
        for(SchedElt* elt : dag) {
            Utils::DEBUG(elt->toString());
            schedorder->push_back(elt);
        }
    }
    //assert(false);
}
 
void FLSFragQreadyBuilder::get_next_readyBFS(vector<SchedElt*> &Qdone, SchedElt *current, vector<SchedElt*> *Qready, map<Task*, SchedElt*> *pool) {
    vector<SchedElt*> Qtmp;
    
    for(SchedElt *s : current->successors) {
        if(find(Qdone.begin(), Qdone.end(), s) != Qdone.end())
            continue;
        bool ok = true;
        for(SchedElt* el : s->previous) {
            if(find(Qdone.begin(), Qdone.end(), el) == Qdone.end()) {
                ok = false;
                break;
            }
        }
        if(ok)
            Qtmp.push_back(s);
    }
    sort(Qtmp.begin(), Qtmp.end(), FLSFragQreadyBuilder::sortFun);
    Qready->insert(Qready->end(), Qtmp.begin(), Qtmp.end());
}

void FLSFragQreadyBuilder::build_QreadyBFS(vector<SchedElt*> *Qready, vector<SchedElt*> &Qdone, map<Task*, SchedElt*> *pool) {
    vector<SchedElt*> nextQready;
    for(SchedElt *root : *Qready) {
        Qdone.push_back(root);
        get_next_readyBFS(Qdone, root, &nextQready, pool);
    }
    if(nextQready.empty()) {
        return;
    }

    build_QreadyBFS(&nextQready, Qdone, pool);
    Qready->insert(Qready->end(), nextQready.begin(), nextQready.end());
}

void FLSFragQreadyBuilder::get_next_readyDFS(vector<SchedElt*> &Qdone, SchedElt *current, vector<SchedElt*> *Qready, map<Task*, SchedElt*> *pool) {
    if(find(Qdone.begin(), Qdone.end(), current) != Qdone.end())
        return;
    
    bool ok = true;
    for(SchedElt* el : current->previous) {
        if(find(Qdone.begin(), Qdone.end(), el) == Qdone.end()) {
            ok = false;
            break;
        }
    }
    if(ok) {
        Qready->push_back(current);
        Qdone.push_back(current);

        vector<SchedElt*> succs(current->successors);
        sort(succs.begin(), succs.end(), FLSFragQreadyBuilder::sortFun);
        for(SchedElt *s : succs) {
            get_next_readyDFS(Qdone, s, Qready, pool);
        }
    }
}

void FLSFragQreadyBuilder::build_QreadyDFS(vector<SchedElt*> *Qready, vector<SchedElt*> &, map<Task*, SchedElt*> *pool) {
    vector<SchedElt*> nextQready;
    vector<SchedElt*> Qdone;

    for(SchedElt *root : *Qready) {
        Qdone.push_back(root);

        vector<SchedElt*> succs(root->successors);
        sort(succs.begin(), succs.end(), FLSFragQreadyBuilder::sortFun);
        for(SchedElt *s : succs) {
            get_next_readyDFS(Qdone, s, &nextQready, pool);
        }
    }

    Qready->insert(Qready->end(), nextQready.begin(), nextQready.end());
}

void FLSFragQreadyBuilder::get_next_readyDFSwithreaddelay(vector<SchedElt*> &Qdone, vector<SchedElt*> &curlist, vector<SchedElt*> *Qready, map<Task*, SchedElt*> *pool) {
    while(!curlist.empty()) {
        SchedElt *current = *(curlist.begin());
        curlist.erase(curlist.begin());
        
        if(find(Qdone.begin(), Qdone.end(), current) != Qdone.end()) {
            continue;
        }
    
        bool ok = true;
        for(SchedElt* el : current->previous) {
            if(find(Qdone.begin(), Qdone.end(), el) == Qdone.end()) {
                ok = false;
                break;
            }
        }
    
        if(!ok) {
            curlist.push_back(current);
            continue;
        }
    
        // for read packet, look if all other first read packet of edges from the same task are elligible to schedule
        // this is to delay reads for joiner in the scheduling order list
        if(current->type == SchedElt::Type::PACKET && ((SchedEltPacket*)current)->dirlbl == "r") {
            ok = true;

            for(SchedElt* sib : current->siblings) {
                for(SchedElt* el : sib->previous) {
                    if(find(Qdone.begin(), Qdone.end(), el) == Qdone.end()) {
                        ok = false;
                        break;
                    }
                }
            }

            if(!ok){
                curlist.push_back(current);
                continue;
            }
            // if ok, all siblings have been delayed, so they must be added now
            for(SchedElt* sib : current->siblings) {
                Qready->push_back(sib);
                Qdone.push_back(sib);

                vector<SchedElt*> succs(sib->successors);
                sort(succs.begin(), succs.end(), FLSFragQreadyBuilder::sortFun);
                curlist.insert(curlist.begin(), succs.begin(), succs.end());
            }
        }
    
        Qready->push_back(current);
        Qdone.push_back(current);

        vector<SchedElt*> succs(current->successors);
        sort(succs.begin(), succs.end(), FLSFragQreadyBuilder::sortFun);
        curlist.insert(curlist.begin(), succs.begin(), succs.end());
    }
}

void FLSFragQreadyBuilder::build_QreadyDFSwithreaddelay(vector<SchedElt*> *Qready, vector<SchedElt*> &unused_compat_fun_walk_through, map<Task*, SchedElt*> *pool) {
    if(conf.interconnect.behavior == "blocking") {
        // because there is no distinction between communication phases and exec one, a simple DFS is enough
        build_QreadyDFS(Qready, unused_compat_fun_walk_through, pool);
    }
    
    vector<SchedElt*> Qdone;
    vector<SchedElt*> roots(*Qready);
    Qready->clear();
    get_next_readyDFSwithreaddelay(Qdone, roots, Qready, pool);
}

void FLSFragQreadyBuilder::build_packet_list_burst(SchedEltTask *schedtask, map<Task*, SchedElt*> *pool) {
    if(schedtask->is_successors_created)
        return;
    schedtask->is_successors_created = true;
    
    vector<SchedEltPacket*> siblings;
    
    for(Task::prev_t prev : schedtask->task->previous) {
        SchedEltTask *prevelt = (SchedEltTask*) pool->at(prev.first);
        uint32_t D_ij = prev.second;

        if(D_ij == 0) {
            schedtask->previous.push_back(prevelt);
            prevelt->successors.push_back(schedtask);
            continue;
        }
        vector<uint32_t> slots = CoreMemInterConnect::burst(conf, D_ij, conf.datatypes.size_bits.at(schedtask->task->previous_datatypes[prev.first]));
        
        SchedEltPacket *currentpacket = new SchedEltPacket(tg, conf);
        currentpacket->data = slots[0];
        currentpacket->datatype = schedtask->task->previous_datatypes[prev.first];
        currentpacket->wct = tg.interconnect()->comm_delay(0, slots[0], schedtask->task->previous_datatypes[prev.first], prev.first->force_write_delay);
        currentpacket->packetnum = 1;
        currentpacket->transmission_nb_packet = slots.size();
        currentpacket->schedtask = prevelt;
        currentpacket->tofromschedtask = schedtask;
        currentpacket->sortingschedtask = schedtask;
        currentpacket->dirlbl = "w";
        prevelt->packet_list.push_back(currentpacket);
        prevelt->successors.push_back(currentpacket);
        currentpacket->previous.push_back(prevelt);
        
        for(int s=1 ; s < slots.size() ; ++s) {
            SchedEltPacket *packet = new SchedEltPacket(tg, conf);
            packet->data = slots[s];
            packet->datatype = schedtask->task->previous_datatypes[prev.first];
            packet->wct = tg.interconnect()->comm_delay(0, slots[s], schedtask->task->previous_datatypes[prev.first], prev.first->force_write_delay);
            packet->packetnum = s+1;
            packet->transmission_nb_packet = slots.size();
            packet->schedtask = prevelt;
            packet->tofromschedtask = schedtask;
            packet->sortingschedtask = schedtask;
            packet->dirlbl = "w";

            prevelt->packet_list.push_back(packet);
            
            currentpacket->successors.push_back(packet);
            packet->previous.push_back(currentpacket);
            currentpacket = packet;
        }
        SchedEltPacket *lastwritepacket = currentpacket;
        
        currentpacket = new SchedEltPacket(tg, conf);
        currentpacket->data = slots[0];
        currentpacket->datatype = schedtask->task->previous_datatypes[prev.first];
        currentpacket->wct = tg.interconnect()->comm_delay(0, slots[0], schedtask->task->previous_datatypes[prev.first], schedtask->task->force_read_delay);
        currentpacket->packetnum = 1;
        currentpacket->transmission_nb_packet = slots.size();
        currentpacket->schedtask = schedtask;
        currentpacket->tofromschedtask = prevelt;
        currentpacket->dirlbl = "r";
        currentpacket->sortingschedtask = prevelt;
        schedtask->packet_list.push_back(currentpacket);
        siblings.push_back(currentpacket);
        
        lastwritepacket->successors.push_back(currentpacket);
        currentpacket->previous.push_back(lastwritepacket);

        for(int s=1 ; s < slots.size() ; ++s) {
            SchedEltPacket *packet = new SchedEltPacket(tg, conf);
            packet->data = slots[s];
            packet->datatype = schedtask->task->previous_datatypes[prev.first];
            packet->wct = tg.interconnect()->comm_delay(0, slots[s], schedtask->task->previous_datatypes[prev.first], schedtask->task->force_read_delay);
            packet->packetnum = s+1;
            packet->transmission_nb_packet = slots.size();
            packet->schedtask = schedtask;
            packet->tofromschedtask = prevelt;
            packet->dirlbl = "r";
            packet->sortingschedtask = prevelt;

            schedtask->packet_list.push_back(packet);
            
            currentpacket->successors.push_back(packet);
            packet->previous.push_back(currentpacket);
            currentpacket = packet;
        }
        
        currentpacket->successors.push_back(schedtask);
        schedtask->previous.push_back(currentpacket);
    }
    //can be optimized with iterator
    for(SchedEltPacket *p : siblings) {
        for(SchedEltPacket *s : siblings) {
            if(s == p) continue;
            p->siblings.push_back(s);
        }
    }
}
void FLSFragQreadyBuilder::build_packet_list_noburst(SchedEltTask *schedtask) {
    // Non burst mode : 2 packets -> [0] for read [1] for write
    
    uint32_t data = schedtask->task->data_read();
    if(data > 0) {
        SchedEltPacket *readpacket = new SchedEltPacket(tg, conf);
        readpacket->wct = tg.interconnect()->comm_delay(
            strategy->compute_read_concurrency(readpacket), data,
                    (*(schedtask->task->previous_datatypes.begin())).second, schedtask->task->force_read_delay);
        readpacket->data = data;
        readpacket->datatype = (*(schedtask->task->previous_datatypes.begin())).second;
        readpacket->packetnum = 1;
        readpacket->transmission_nb_packet = 1;
        readpacket->schedtask = schedtask;
        readpacket->tofromschedtask = schedtask;
        readpacket->sortingschedtask = schedtask;
        readpacket->dirlbl = "r";

        schedtask->packet_list[0] = readpacket;
    }
    else
        schedtask->packet_list[0] = nullptr;

    data = schedtask->task->data_written();
    if(data > 0) {
        SchedEltPacket *writepacket = new SchedEltPacket(tg, conf);
        writepacket->wct = tg.interconnect()->comm_delay(
                strategy->compute_write_concurrency(writepacket), data, 
                schedtask->task->successors.at(0)->previous_datatypes[schedtask->task], schedtask->task->force_write_delay);
        writepacket->packetnum = 1;
        writepacket->data = data;
        writepacket->datatype = schedtask->task->successors.at(0)->previous_datatypes[schedtask->task];
        writepacket->transmission_nb_packet = 1;
        writepacket->schedtask = schedtask;
        writepacket->tofromschedtask = schedtask;
        writepacket->sortingschedtask = schedtask;
        writepacket->dirlbl = "w";

        schedtask->packet_list[1] = writepacket;
    }
    else 
        schedtask->packet_list[1] = nullptr;
}


void FLSFragQreadyBuilder::build_successors_noburst(SchedEltTask *schedtask, map<Task*, SchedElt*> *pool) {
    if(schedtask->is_successors_created)
        return;
    
    schedtask->is_successors_created = true;
    
    
    SchedEltPacket *readpacket = schedtask->packet_list[0];
    if(readpacket != nullptr) {
        readpacket->successors.push_back(schedtask);
        schedtask->previous.push_back(readpacket);

        for(Task::prev_t prev: schedtask->task->previous) {
            SchedEltTask *prevelt = (SchedEltTask*) pool->at(prev.first);
            if(prevelt->packet_list[1] != nullptr) {
                readpacket->previous.push_back(prevelt->packet_list[1]);
                prevelt->packet_list[1]->successors.push_back(readpacket);
            }
            else {
                readpacket->previous.push_back(prevelt);
                prevelt->successors.push_back(readpacket);
            }
        }
    }
    else {
        for(Task::prev_t prev: schedtask->task->previous) {
            SchedEltTask *prevelt = (SchedEltTask*) pool->at(prev.first);
            if(prevelt->packet_list[1] != nullptr) {
                schedtask->previous.push_back(prevelt->packet_list[1]);
                prevelt->packet_list[1]->successors.push_back(schedtask);
            }
            else {
                schedtask->previous.push_back(prevelt);
                prevelt->successors.push_back(schedtask);
            }
        }
    }

    SchedEltPacket *writepacket = schedtask->packet_list[1];
    if(writepacket != nullptr) {
        schedtask->successors.push_back(writepacket);
        writepacket->previous.push_back(schedtask);
    }
}
