/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPLITFLS_H
#define SPLITFLS_H

#include <cstdlib>
#include <vector>
#include <string>
#include <map>
#include <algorithm> 
#include <array>
#include <cmath>
#include <cassert>
#include <chrono>
#include <tuple>
#include <functional>
//#include <stdint.h> 

#include <boost/algorithm/string.hpp> 

#include "config.h"
#include "Utils.h"
#include "SystemModel.h"
#include "Schedule.h"
#include "Solver.h"
#include "registry.h"

class SchedEltTask;
class SchedElt {
public:
    enum Type {
        PACKET,
        TASK
    };
    
    const SchedElt::Type type;
    const SystemModel &tg;
    const config_t &conf;
    
    vector<SchedElt*> successors;
    vector<SchedElt*> previous;
    vector<SchedElt*> siblings; // for edge.packet[0], corresponds to all other edge.packet[0]
    
    uint64_t rt = 0;
    uint32_t wct = 0; // worst case timing
    
    explicit SchedElt(const SystemModel &m, const config_t &c, const SchedElt::Type t) : tg(m), conf(c), type(t) {}
    virtual ~SchedElt() {
        successors.clear();
        previous.clear();
        siblings.clear();
    }
    
    virtual string toString() = 0;
    virtual SchedEltTask* getTask() = 0;
};

class SchedEltPacket;
class SchedEltTask : public SchedElt {
public:
    Task *task;
    bool is_successors_created = false;
    vector<SchedEltPacket*> packet_list;
    
    explicit SchedEltTask(const SystemModel &tg, const config_t &conf, Task *t): SchedElt(tg, conf, SchedElt::Type::TASK), task(t) {
    }
    explicit SchedEltTask(const SystemModel &tg, const config_t &conf, Task *t, int nbpac) : SchedEltTask(tg, conf, t) {
        if(nbpac > 0)
            packet_list.resize(nbpac);
    }
    virtual ~SchedEltTask() {
        packet_list.clear();
    }
    
    string toString() override { return "Task "+task->id; }
    
    SchedEltTask* getTask() { return this;}
};

class SchedEltPacket : public SchedElt {
public:
    uint32_t data = 0;
    string datatype = "";
    uint32_t packetnum = 0;
    uint32_t transmission_nb_packet = 0;
    SchedEltTask *schedtask;
    SchedEltTask *sortingschedtask;
    SchedEltTask *tofromschedtask;
    
    string dirlbl = "";
    
    explicit SchedEltPacket(const SystemModel &tg, const config_t &conf) : SchedElt(tg, conf, SchedElt::Type::PACKET) {
    }
    virtual ~SchedEltPacket() {
        
    }
    
    string toString() override { return "Packet "+(schedtask->task->id)+"<->"+(tofromschedtask->task->id)+"-"+to_string(packetnum)+"-"+dirlbl; }
    SchedEltTask* getTask() { return schedtask; }
};

struct memento_sched_state_s {
    Schedule_t schedule;
    vector<SchedElt*> schedorder;
    vector<SchedElt*> scheddone;
    map<SchedEltTask*, vector<meth_processor_t*>> forbid_proc;
};

class FLSFragStrategy {
public:
    map<SchedEltTask*, vector<meth_processor_t*>> forbidden_proc_mapping;
            
    explicit FLSFragStrategy(const SystemModel &m, const config_t &c) : tg(m), conf(c) {
    };
    
protected:
  
protected:
    const SystemModel &tg;
    const config_t &conf;
    
public:
    virtual uint16_t compute_read_concurrency(SchedEltPacket *t) = 0;
    virtual uint16_t compute_write_concurrency(SchedEltPacket *t) = 0;
    
    virtual void chose_proc(Schedule_t *sched, const vector<SchedElt*> &Qdone, SchedElt *elt);
    virtual void chose_proc(Schedule_t *sched, const vector<SchedElt*> &Qdone, SchedEltTask *t) = 0;
    virtual void chose_proc(Schedule_t *sched, const vector<SchedElt*> &Qdone, SchedEltPacket *t) = 0;
    
    virtual spm_region_info allocate_region(Schedule_t* sched, const vector<SchedElt*>& Qdone, Task* current, uint32_t data, uint64_t startresa, uint64_t endresa, const string &suffix);
    virtual void map_phases_to_region(Schedule_t* sched, const vector<SchedElt*>& Qdone, Task* current);
};

using FLSFragStrategyRegistry = registry::Registry<FLSFragStrategy, string, const SystemModel &, const config_t &>;

#define REGISTER_FLSFRAGSTRATEGY(ClassName, Identifier) \
  REGISTER_SUBCLASS(FLSFragStrategy, ClassName, string, Identifier, const SystemModel &, const config_t &)

class FLSFragmented : public Solver {
private:
    FLSFragStrategy *strategy;
    
public:
    explicit FLSFragmented(const SystemModel &tg, const config_t &conf) : Solver(tg, conf) {
        strategy = FLSFragStrategyRegistry::Create(conf.solving.type, tg, conf);
    };
    
    virtual void run(Schedule_t *result) override;
    virtual void do_run(Schedule_t *sched, vector<SchedElt*> &schedorder);
};

REGISTER_SOLVER(FLSFragmented, "flsfragmented")

#endif /* SPLITFLS_H */

