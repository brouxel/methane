/*
    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"
#include "Utils.h"

void init_config(config_t *conf) {
    conf->files.in = DEFAULT_TEST_LOCATION;
    conf->files.lp = DEFAULT_TEST_LOCATION;
    conf->files.dot = DEFAULT_TEST_LOCATION;
    conf->files.trdot = DEFAULT_TEST_LOCATION;
    conf->files.svg = DEFAULT_TEST_LOCATION;
    conf->files.out = DEFAULT_TEST_LOCATION;
    
    conf->solving.solve = true;
    conf->solving.timeout = 0;
    conf->solving.solver = "cplex";
    conf->solving.type = "worst_concurrency";
    conf->solving.optimal = false;
    conf->solving.cores = 0;
    
    conf->clean = false;
    
    conf->view.zoomX = 0;
    conf->view.zoomY = 0;
    
    conf->outputs.insert("resxml");
    
    conf->interconnect.type = "NONE";
    conf->interconnect.arbiter = "NONE";
    conf->interconnect.active_ress_time = 4;
    conf->interconnect.bit_per_timeunit = 8;
    conf->interconnect.mechanism = "sharedonly";
    conf->interconnect.behavior = "blocking";
    conf->interconnect.burst = "none";
    
    conf->datatypes.default_type = "bit";
    conf->datatypes.size_bits["void"] = 8;
    conf->datatypes.size_bits["bit"] = 8;
    conf->datatypes.size_bits["bool"] = 8;
    conf->datatypes.size_bits["char"] = 8;
    conf->datatypes.size_bits["short"] = 16;
    conf->datatypes.size_bits["int"] = 32;
    conf->datatypes.size_bits["long"] = 64;
    conf->datatypes.size_bits["float"] = 32;
    conf->datatypes.size_bits["double"] = 64;
    conf->datatypes.size_bits["long double"] = 128;
    
    conf->archi.prefetch_code = false;
    conf->archi.spm.assign_region = false;
    conf->archi.spm.store_code = false;
    
    conf->codegen.machine = "";
    conf->codegen.maxiter = 1;
    conf->codegen.safety = 1.0;
    conf->codegen.with_delay_printf = false;
}

void check_todo(const config_t &conf) {
    if(conf.solving.solver == "test" || conf.solving.type == "test") {
        Utils::WARN("Warning: Test mode");
        Utils::DEBUG("Warning: Test mode");
        return;
    }
    if(conf.interconnect.type != "BUS")
        throw Todo("Only BUS for the communication medium");
    if(conf.interconnect.arbiter != "FAIR")
        throw Todo("Only FAIR Bus");
    if(conf.interconnect.behavior != "blocking" && conf.interconnect.behavior != "non-blocking")
        throw Todo("communication behavior \""+conf.interconnect.behavior+"\" not ready");
    if(conf.interconnect.mechanism != "shared" && conf.interconnect.mechanism != "sharedonly")
        throw Todo("communication mechanism \""+conf.interconnect.mechanism+"\" not ready");
    
    if(conf.interconnect.burst != "none" && conf.interconnect.behavior != "non-blocking")
        throw Todo("This configuration does sound useful to me");
    
    if(conf.interconnect.burst != "none" && conf.interconnect.mechanism != "sharedonly")
        throw Todo("Burst mode "+conf.interconnect.burst+" must be used with a sharedonly mechanism");

    if(conf.interconnect.burst != "none") {
        if(conf.solving.solver != "synchronized")
            throw Todo("Burst mode "+conf.interconnect.burst+" only for synchronized");
        if(conf.interconnect.mechanism != "sharedonly")
            throw Todo("Burst mode "+conf.interconnect.burst+" must be used with a sharedonly mechanism");
        if(conf.solving.solver != "cplex" && conf.solving.solver != "flsfragmented")
            throw Todo("With fragmented communication, only solvers \"cples\" and \"flsfragmented\" are allowed");
    }
    
    if(conf.archi.spm.assign_region && conf.solving.solver == "fls" && conf.interconnect.mechanism != "sharedonly" && conf.interconnect.mechanism != "shared")
        throw Todo("Alloc SPM region with the configured communication mechanism is reserved for future work");
    
    if(conf.solving.solver == "flsfragmented" && conf.solving.type == "synchronized" && conf.archi.prefetch_code && conf.archi.spm.store_code)
        throw Todo("Prefetch code at read phase, not handle for flsfragmented/synchronized");
    
    if(conf.solving.solver == "gurobi")
        throw Todo("gurobi solver is outdated");
    
    if(conf.solving.solver == "flsfragmented" && conf.solving.type != "synchronized")
        throw Todo("only synchronized/worst for flsfragmented solver");
    
}
