#######
#    Copyright (C) 2017  Benjamin Rouxel, Irisa/INRIA, benjamin.rouxel@inria.fr
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#######

ifeq ($(shell uname), Darwin)
CPLEX=/Users/brouxel/Projects/Software/cplex_osx
CXXFLAGS+=-Wno-inconsistent-missing-override
else ifeq ($(shell uname), Linux)
CPLEX=/udd/brouxel/cplex-12.6/
endif   

CXX=g++ -Wfatal-errors -g -std=c++11 -Wno-deprecated-declarations -Wno-ignored-attributes $(CXXFLAGS) -O3
INCS=-I src $(shell pkg-config libxml++-2.6 --cflags) \
    -I $(CPLEX)/cplex/include -I $(CPLEX)/concert/include
#     -I /opt/gurobi651/linux64/include
LIBS=$(shell pkg-config libxml++-2.6 --libs) \
       -lm -lpthread \
       -lboost_graph -lboost_filesystem -lboost_system -lboost_program_options 
      #-L/home/opt/gurobi651/linux64/lib/ -lgurobi_c++ -lgurobi65 \

ifeq ($(shell uname), Darwin)
INCS+=-I/usr/local/Cellar/boost/1.69.0/include
LIBS+=-L/usr/local/Cellar/boost/1.69.0/lib \
      -L$(CPLEX)/concert/lib/x86-64_osx/static_pic -lconcert \
      -L$(CPLEX)/cpoptimizer/lib/x86-64_osx/static_pic -lcp \
      -L$(CPLEX)/cplex/lib/x86-64_osx/static_pic -lilocplex -lcplex 
CXX+=-Wno-\#pragma-messages -Wno-inconsistent-missing-override
else ifeq ($(shell uname), Linux)
INCS+=-I $(CPLEX)/opl/include
LIBS+=-L$(CPLEX)/opl/lib/x86-64_linux/static_pic -Wl,-Bstatic -lopl -liljs  \
      -L$(CPLEX)/opl/lib/x86-64_linux/static_pic -Wl,-Bstatic -lilocplex -lcp -lconcert \
      -L$(CPLEX)/opl/bin/x86-64_linux -Wl,-Bdynamic -loplnl1 -lcplex1263 -ldbkernel -ldblnkdyn -lilog -ldl \
      -L$(CPLEX)/opl/bin/x86-64_linux -Wl,-Bdynamic -licuuc -licui18n -licuio -licudata \
      -Wl,-Bdynamic
endif   

#-DGUROBI -- licence expired 
DEFS=-DIL_STD -DCPLEX
ifeq ($(shell uname), Linux)
DEFS+=-DCPLEXOPL #no available on osx
endif
 
# turn verbosity on 
#DEFS += -D_DEBUG

BUILD_DIR=build
TARGET_DIR=dist
SRC_DIR=src
TARGET=methane

OBJS=$(patsubst $(SRC_DIR)/%, $(BUILD_DIR)/%, $(patsubst %.cpp,%.o,$(wildcard $(SRC_DIR)/*.cpp)))
OBJS+=$(patsubst $(SRC_DIR)/ForwardListScheduling/%, $(BUILD_DIR)/%, $(patsubst %.cpp,%.o,$(wildcard $(SRC_DIR)/ForwardListScheduling/*.cpp)))
OBJS+=$(patsubst $(SRC_DIR)/ILPforms/%, $(BUILD_DIR)/%, $(patsubst %.cpp,%.o,$(wildcard $(SRC_DIR)/ILPforms/*.cpp)))
OBJS+=$(patsubst $(SRC_DIR)/ILPSolvers/%, $(BUILD_DIR)/%, $(patsubst %.cpp,%.o,$(wildcard $(SRC_DIR)/ILPSolvers/*.cpp)))
OBJS+=$(patsubst $(SRC_DIR)/SplitFLS/%, $(BUILD_DIR)/%, $(patsubst %.cpp,%.o,$(wildcard $(SRC_DIR)/SplitFLS/*.cpp)))
OBJS+=$(patsubst $(SRC_DIR)/Generator/%, $(BUILD_DIR)/%, $(patsubst %.cpp,%.o,$(wildcard $(SRC_DIR)/Generator/*.cpp)))
OBJS+=$(patsubst $(SRC_DIR)/InputParser/%, $(BUILD_DIR)/%, $(patsubst %.cpp,%.o,$(wildcard $(SRC_DIR)/InputParser/*.cpp)))
OBJS+=$(patsubst $(SRC_DIR)/Interconnect/%, $(BUILD_DIR)/%, $(patsubst %.cpp,%.o,$(wildcard $(SRC_DIR)/Interconnect/*.cpp)))

.PHONY: $(TARGET) mkdirs
$(TARGET): mkdirs $(TARGET_DIR)/$(TARGET)

mkdirs:
	mkdir -p $(TARGET_DIR) $(BUILD_DIR)

$(TARGET_DIR)/$(TARGET): $(OBJS)
	$(CXX) -o $@ $^ $(LIBS)
	
$(BUILD_DIR)/ConditionalTimeInterval.o: $(SRC_DIR)/ConditionalTimeInterval.cpp $(wildcard $(SRC_DIR)/ConditionalTimeInterval/*.mod) $(wildcard $(SRC_DIR)/*.h)
	$(CXX) $(DEFS) $(INCS) -c -o $@ $<

$(BUILD_DIR)/sfls_%.o: $(SRC_DIR)/SplitFLS/sfls_%.cpp $(wildcard $(SRC_DIR)/*.h)
	$(CXX) $(DEFS) $(INCS) -c -o $@ $<
	
$(BUILD_DIR)/fls_%.o: $(SRC_DIR)/ForwardListScheduling/fls_%.cpp $(wildcard $(SRC_DIR)/*.h)
	$(CXX) $(DEFS) $(INCS) -c -o $@ $<

$(BUILD_DIR)/ilp_%.o: $(SRC_DIR)/ILPforms/ilp_%.cpp $(wildcard $(SRC_DIR)/*.h)
	$(CXX) $(DEFS) $(INCS) -c -o $@ $<

$(BUILD_DIR)/is_%.o: $(SRC_DIR)/ILPSolvers/is_%.cpp $(wildcard $(SRC_DIR)/*.h)
	$(CXX) $(DEFS) $(INCS) -c -o $@ $<
	
$(BUILD_DIR)/gen_%.o: $(SRC_DIR)/Generator/gen_%.cpp $(wildcard $(SRC_DIR)/*.h) $(wildcard $(SRC_DIR)/Generator/*/*.tpl)
	$(CXX) $(DEFS) $(INCS) -c -o $@ $<

$(BUILD_DIR)/par_%.o: $(SRC_DIR)/InputParser/par_%.cpp $(wildcard $(SRC_DIR)/*.h)
	$(CXX) $(DEFS) $(INCS) -c -o $@ $<
	
$(BUILD_DIR)/con_%.o: $(SRC_DIR)/Interconnect/con_%.cpp $(wildcard $(SRC_DIR)/*.h)
	$(CXX) $(DEFS) $(INCS) -c -o $@ $<

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CXX) $(DEFS) $(INCS) -c -o $@ $<

clean:
	rm -f $(BUILD_DIR)/*.o
	rm -f $(TARGET_DIR)/$(TARGET)
